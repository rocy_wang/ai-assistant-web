# 这是一个基于VUE及AI的Web程序

## 一、功能特点：

1、AI 换脸：他人的脸，换成自己的  
2、AI 会话：随问随答;  
3、AI 绘画：生成各种尺寸图片;  
4、AI 私域：提供文件,能让大模型来参考回答;支持各种文本格式  
5、AI PPT：只要从一个标题开始，就可以生成图文演示;用于汇报宣讲、教学广告等等  

## 二、线上应用

[点击体验](https://rocy-ai.wang)

## 三、功能截图

<div style="display:flex;align-items:top">
    <img src="./pc/chat/navigation/截屏2023-10-17 14.12.22.png"  style="border:1px solid lightgray;margin-right:8px;" width="200px"/>
    <img src="./pc/chat/navigation/截屏2023-10-17 14.16.58.png"  style="border:1px solid lightgray;" width="200px"/>
</div>

### 1.会话/绘画


<div>
    <img src="./pc/chat/basic/截屏2023-10-17 11.01.04.png" style="display:inline-block;margin-right:8px;" width="40%"/>
    <img src="./pc/chat/basic/截屏2023-10-17 10.52.20.png" width="40%"/>
</div>

<div style="margin-top:8px;">
    <img src="./pc/chat/basic/截屏2023-11-14 08.07.50.png" style="display:inline-block;margin-right:8px;" width="40%"/>
    <img src="./pc/chat/basic/截屏2023-11-14 08.15.57.png" width="40%"/>
</div>


### 2.私有模型
<div style="display:flex;justify-content:space-between;flex-wrap:wrap">
   <img src="./pc/model/截屏2023-10-17 15.12.11.png" style="border:1px solid lightgray;margin-right:8px;" width="600px"/>
</div>

### 3.机器人

<div style="display:flex;justify-content:center;width:100%;flex-wrap:wrap">
   <img src="./pc/publish/截屏2023-10-18 09.11.42.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="30%"/>
   <img src="./pc/publish/截屏2023-10-18 09.12.35.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="30%"/>
   <img src="./pc/publish/截屏2023-10-18 09.18.22.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="30%"/>
</div>

<div style="display:flex;justify-content:center;width:100%;margin-top:16px">
   <img src="./pc/publish/截屏2023-10-18 09.18.48.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="20%"/>
   <img src="./pc/publish_list/截屏2023-10-18 09.32.34.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="20%"/>
   <img src="./pc/search/截屏2023-10-17 20.16.53.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="20%"/>
   <img src="./pc/invoke/截屏2023-10-18 09.42.04.png" style="display:inline-block;border:1px solid lightgray;margin-right:8px;" width="20%"/>
</div>

## 四、相关应用
## 1.包含更多AI能力的小程序代码
[微信小程序代码](https://e.gitee.com/rocy_wang/repos/rocy_wang/aioffice/sources)

线上体验  
<img src="screenshots/AI馨办公小助手二维码.png" width="200px" alt="AI 助理">


## 2.包含更多AI能力的Adroid APP代码
[Adroid APP代码](https://e.gitee.com/rocy_wang/repos/rocy_wang/ai-assistant-android/sources)
线上体验  
<img src="screenshots/AA下载二维码.png" width="200px" alt="AI 助理">

或者[从应用商店下载](http://zhushou.360.cn/detail/index/soft_id/4674455)

## 五、后端服务
以下方式2选1即可  
### 1.用现成的线上服务
Python编写的Fast API服务  
请将 src/js/request/service.js 文件中的 AI_SERVICE_HOST 赋值为 "https://rocy-ai.wang" 或者您自己的服务接口地址  
如果需要订制化服务能力，随时欢迎垂询，有问必答
### 2.从开源代码启动服务
[后端服务代码](https://e.gitee.com/rocy_wang/repos/rocy_wang/ai-assitant-service-open/sources)

## 六、技术交流、疑难解答、技术合作、贡献代码等等

Email: 13810347889@139.com  
微信: rocy-data
