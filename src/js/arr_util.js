import _ from "lodash";


export const MODEL_ID_SEPARATOR = "/";

export function arrRemove(arr,val){
    return _.reject(arr,item=>item === val)
}

export function arrSplice(arr, start, delete_count, new_arr) {
    return _.concat(_.take(arr, start), new_arr, _.takeRight(arr, arr.length - start - delete_count))
}

export function arrLastReplace(arr, new_last) {
    return _.concat(_.initial(arr), [new_last])
}

export function arrStartWith(arr1, arr2, real) {
    let realStart = real
    if (_.isNil(realStart)) realStart = false
    if (realStart === true)
        return _.isEqual(_.take(arr1, arr2.length), arr2) && arr1.length > arr2.length
    return _.isEqual(_.take(arr1, arr2.length), arr2)
}

export function arrEquals(arr1, arr2) {
    return arr1.join(MODEL_ID_SEPARATOR) === arr2.join(MODEL_ID_SEPARATOR)
}