import {DAYS_PROVIDER_MIN} from "@/js/constants";

export const CREATE_SERVICE = {
    mps: [],
    days: DAYS_PROVIDER_MIN,
    description: "我的 ChatGPT 机器人",
    buy_contact: undefined,
    user_price: 1,
    bypass: true
}