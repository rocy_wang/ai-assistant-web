export const aa_examples = [{
    img: "/img/sun.svg",
    title: "使用示例",
    canSend: true,
    questions: ['请简单解释一下量子计算','为一个10岁的孩子过生日有什么创意吗？', '如何用Javascript发出HTTP请求？']
}, {
    img: "/img/capacity.svg",
    title: "功能亮点",
    questions: ['悄悄回答您的私人问题', '微信里也能享受同样的智能服务', '能回答任何问题，还能绘画']
}, {
    img: "/img/limit.svg",
    title: "使用限制",
    questions: ['可能会生成不正确的信息', '可能会产生有害的、有偏见的内容', '用户量过大时响应可能会有延迟']
},]