
;
const BASE1 = 100
const BASE2 = 10
function randMilli() {
    return BASE1 + Math.round(Math.random() * BASE2)
}
const STEP1 = 1
const STEP2 = 4
function randStep() {
    return STEP1 + Math.round(Math.random() * STEP2)
}
export function print_text(selector, sequence, done_callback) {
    let len = sequence.length
    let cnt = 0
    function setText(text) {
        $(selector).text(text)
    }
    function runStep() {
        if (cnt === len) {
            setText("")
            done_callback && done_callback()
            return
        }
        setTimeout(() => {
            cnt += randStep()
            cnt = Math.min(len, cnt)
            let text = sequence.substr(0, cnt)
            setText(text)
            runStep()
        }, randMilli())
    }
    runStep()
}