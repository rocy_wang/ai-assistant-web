

export function str_splice(text, index, remove, add) {
    if (_.isNil(text)) return text
    const left = text.substr(0, index)
    const right = text.substr(index)
    return `${left}${add || ""}${right.substr(remove || 0)}`
}