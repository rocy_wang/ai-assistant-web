

const timers = {}

export function startCountDown(key, seconds, init_callback, time_callback) {
    stopCountDown(key)
    init_callback();
    let count = seconds
    timers[key] = setInterval(() => {
        if (count > 0) {
            count -= 1;
            time_callback(count)
        } else {
            stopCountDown(key)
            init_callback()
        }
    }, 1000);
}


export function stopCountDown(key) {
    const timer = timers[key];
    if (!_.isNil(timer)) {
        clearInterval(timer)
        delete timers[key]
    }
}