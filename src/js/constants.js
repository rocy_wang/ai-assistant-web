
import _ from 'lodash';
export const CHAT_GPT_MODEL_ID = 0
export const COMPRESS_LENGTH = 100
export const DIALOGUE_RETURN_COUNT = 20
export const QA_RETURN_COUNT = 20

export const EXTENSIONS = [".pdf", ".docx", ".xlsx", ".pptx", ".txt", ".md", ".csv"]
export const PPT_IMPORT_EXTENSIONS = [".pdf", ".docx", ".pptx", ".txt", ".xmind", ".ai"]

export const COUNTING_SECONDS = 30

const HOUR_MILLI = 3600 * 1000;
export const TIME_ZONE_MILLI_OFFSET = 8 * HOUR_MILLI
export const DAY_MILLI = 24 * HOUR_MILLI

export const CHARGE_REASONS = {
    CHARGE: "充值", CHARGE_GIVE: "充值赠送", FILE_CONSUME: "上传文件", QUESTION_CONSUME: "AI问答", IMAGE_CONSUME: "AI绘画", REGISTER_GIVE: "注册赠送", SHARE_GIVE: "分享赠送",
}

export const GPT_3DOT5_TURBO = "gpt-3.5-turbo"
export const GPT_3DOT5_TURBO_16K = "gpt-3.5-turbo-16k"
export const GPT_4 = "gpt-4"

export const IMAGE_DALL_E3 = "dall-e-3"
export const IMAGE_SD_XL = "stable-diffusion-xl"

export const AI_MODEL2TOKENS = {
    [GPT_3DOT5_TURBO]: 4096,
    [GPT_3DOT5_TURBO_16K]: 16384,
    [GPT_4]: 8192,
}
export const OPEN_AI_DEFAULT_IMAGE_SIZE = "中"
export const SD_DEFAULT_IMAGE_SIZE = "头像1"
export const SD_DEFAULT_STYLE = "Base"
export const IMAGE_SIZE_VALUES = {
    "小": "small",
    "中": "middle",
    "大": "big",

    "头像1": '768x768',
    "头像2": '1024x1024',
    "头像3": '1536x1536',
    "头像4": '2048x2048',

    "文章配图1": '1024x768',
    "文章配图2": '2048x1536',

    "海报传单1": '576x1024',
    "海报传单2": '768x1024',
    "海报传单3": '1536x2048',

    "电脑壁纸1": '1024x576',
    "电脑壁纸2": '2048x1152',
}
export const IMAGE_SIZES = {
    "小": "256x256",
    "中": "512x512",
    "大": "1024x1024",

    "头像1": '768x768',
    "头像2": '1024x1024',
    "头像3": '1536x1536',
    "头像4": '2048x2048',

    "文章配图1": '1024x768',
    "文章配图2": '2048x1536',

    "海报传单1": '576x1024',
    "海报传单2": '768x1024',
    "海报传单3": '1536x2048',

    "电脑壁纸1": '1024x576',
    "电脑壁纸2": '2048x1152',
}
export const AI_IMAGE_MODELS = {
    [IMAGE_DALL_E3]: {
        size: ["小", "中", "大"]
    },
    [IMAGE_SD_XL]: {
        size: ["头像1", "头像2", "头像3", "头像4", "文章配图1", "文章配图2", "海报传单1", "海报传单2", "海报传单3", "电脑壁纸1", "电脑壁纸2"],
        styles: {
            "基础风格": "Base",
            "3D模型": "3D Model",
            "模拟胶片": "Analog Film",
            "动漫": "Anime",
            "电影": "Cinematic",
            "漫画": "Comic Book",
            "工艺黏土": "Craft Clay",
            "数字艺术": "Digital Art",
            "增强": "Enhance",
            "幻想艺术": "Fantasy Art",
            "等距风格": "Isometric",
            "线条艺术": "Line Art",
            "低多边形": "Lowpoly",
            "霓虹朋克": "Neonpunk",
            "折纸": "Origami",
            "摄影": "Photographic",
            "像素艺术": "Pixel Art",
            "纹理": "Texture",
        }
    },
}

export const TEMPERATURE_DEFAULT = 0.7
export const MAX_TOKENS_TIMES = 0.3
export const MAX_TOKENS = 2048
export const DAYS_PROVIDER_MIN = 10


export const MPTextAAServicePrefixConfig = "MPTextAAServicePrefixConfig"
export const MPImageAAServicePrefixConfig = "MPImageAAServicePrefixConfig"
export const MPProxyKeywordServiceReplyQuestionConfig = "MPProxyKeywordServiceReplyQuestionConfig"

export const MPRobotLoopNewMessageOnlineConfig = "MPRobotLoopNewMessageOnlineConfig"
export const MPSendMessageTimeCommandConfig = "MPSendMessageTimeCommandConfig"

export const ROBOT_TYPE_WEWORK = "WEWORK"
export const ROBOT_TYPE_MM = "MM"
export const ROBOT_TYPE_MOBILE_QQ = "MOBILE_QQ"
export const ROBOT_TYPE_CONTROL_PANEL = "CONTROL_PANEL"

export const ROBOT_TYPES = [
    ROBOT_TYPE_WEWORK,
    ROBOT_TYPE_MM,
    ROBOT_TYPE_MOBILE_QQ
]

export const ROBOT_TYPE_NAMES = {
    [ROBOT_TYPE_WEWORK]: "企业微信",
    [ROBOT_TYPE_MM]: "微信",
    [ROBOT_TYPE_MOBILE_QQ]: "手机QQ"
}

export function robot_type_int2zh(i) {
    return ROBOT_TYPE_NAMES[ROBOT_TYPES[i]]
}


export function robot_type_en2int(i) {
    return ROBOT_TYPES.indexOf(i)
}

export const COMMAND_TYPE_GET_ROOMS = "GET_ROOMS"
export const COMMAND_TYPE_GET_ROOMS_MESSAGES = "GET_ROOMS_MESSAGES"
export const COMMAND_TYPE_LOOP_GET_NEW_ROOMS_MESSAGES = "LOOP_GET_NEW_ROOMS_MESSAGES"
export const COMMAND_TYPE_STOP = "STOP";
export const COMMAND_TYPE_SEND_MESSAGES = "SEND_MESSAGES";
export const COMMAND_TYPE_GET_CONTACTS = "GET_CONTACTS"
export const COMMAND_TYPES = [
    COMMAND_TYPE_GET_ROOMS,
    COMMAND_TYPE_GET_ROOMS_MESSAGES,
    COMMAND_TYPE_LOOP_GET_NEW_ROOMS_MESSAGES,
    COMMAND_TYPE_STOP,
    COMMAND_TYPE_SEND_MESSAGES,
    COMMAND_TYPE_GET_CONTACTS
]

export const COMMAND_TYPE_NAMES = {
    [COMMAND_TYPE_GET_ROOMS]: "获取群聊",
    [COMMAND_TYPE_GET_ROOMS_MESSAGES]: "获取群聊消息",
    [COMMAND_TYPE_LOOP_GET_NEW_ROOMS_MESSAGES]: "获取新消息",
    [COMMAND_TYPE_STOP]: "停止获取新消息",
    [COMMAND_TYPE_SEND_MESSAGES]: "回复消息",
    [COMMAND_TYPE_GET_CONTACTS]: "获取通讯录"
}

export function command_type_int2zh(v) {
    return COMMAND_TYPE_NAMES[COMMAND_TYPES[v]]
}

export function command_type_int2en(v) {
    return COMMAND_TYPES[v]
}


export function command_type_zh2int(zh) {
    return COMMAND_TYPES.indexOf(COMMAND_TYPE_NAMES_EN[zh])
}

export const COMMAND_TYPE_NAMES_EN = _.fromPairs(_.map(COMMAND_TYPE_NAMES, (value, key) => [value, key]))


export const POST = "POST"


export const RobotGetRoomsOnlineMPConfig = "RobotGetRoomsOnlineMPConfig"
export const RobotGetContactsOnlineMPConfig = "RobotGetContactsOnlineMPConfig"
export const MPSendCodeCommandConfig = "MPSendCodeCommandConfig"
export const MPRegisterCommandAAServicePrefixConfig = "MPRegisterCommandAAServicePrefixConfig"
export const MPHELPCommandConfig = "MPHELPCommandConfig"


export const DAY2CHINESE = {
    "MONDAY": "星期一",
    "TUESDAY": "星期二",
    "WEDNESDAY": "星期三",
    "THURSDAY": "星期四",
    "FRIDAY": "星期五",
    "SATURDAY": "星期六",
    "SUNDAY": "星期日",
}


export const REPORT_TYPE_ROOM_MESSAGES = 0
export const REPORT_TYPE_ROOMS = 1
export const REPORT_TYPE_HEART_BEAT = 2
export const REPORT_TYPE_CONTACTS = 3