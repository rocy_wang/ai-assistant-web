export const copyText = function (document,text) {
    let area = document.createElement('textarea')
    updateMarkStyles(area)
    area.textContent = text
    document.body.appendChild(area)
    area.select()
    document.execCommand('Copy')
    document.body.removeChild(area)
}
const updateMarkStyles = (mark) => {
    mark.style.all = "unset";
    mark.style.position = "fixed";
    mark.style.top = '0';
    mark.style.clip = "rect(0, 0, 0, 0)";
    mark.style.whiteSpace = "pre";
    mark.style.userSelect = "text";
}