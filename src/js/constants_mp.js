import {isAnyEmpty} from "@/js/lodash_util";

export const MP_DEFAULT = {
    answer_prefix: "#",
    answer_suffix: "#",
    enabled: true
}
export const MP_MAPPINGS = {
    "MPSendMessageTimeCommandConfig": {
        titles: ["name", "time"],
        img: "send",
        check: (mp) => {
            if (isAnyEmpty(mp['time'])) return '请指定发送时间'
            if (isAnyEmpty(mp['command']['robotType'])) return '请指定发送目标'
            if (isAnyEmpty(mp['command']['sendMessages'])) return '请指定发送配置'
            return undefined
        }
    },
    "MPTextAAServicePrefixConfig": {
        titles: ["name", "model_name"],
        img: "question",
        check: (mp) => {
            if (mp['user_private'] === true && isAnyEmpty(mp['model_id'])) return "若使用私有模型，请指定"
            return undefined
        }
    },
    "MPImageAAServicePrefixConfig": {
        titles: ["name", "size"],
        img: "drawing",
        check: (mp) => {
            if (isAnyEmpty(mp['size'])) return '请指定图片大小'
            return undefined
        }
    },
    "MPProxyKeywordServiceReplyQuestionConfig": {
        titles:["name", "url"],
        img: "proxy",
        check: (mp) => {
            if (isAnyEmpty(mp['url'])) return '请指定代理地址'
            return undefined
        }
    },
    "MPRobotLoopNewMessageOnlineConfig": {
        titles:["name", "description"],
        img: "mm"
    },
    "RobotGetRoomsOnlineMPConfig": {
        titles:["name", "description"],
        img: "get_rooms"
    },
    "RobotGetContactsOnlineMPConfig": {
        titles:["name", "description"],
        img: "get_contacts"
    },
    "MPSendCodeCommandConfig": {
        titles:["name", "description"],
        img: "send_check_code"
    },
    "MPRegisterCommandAAServicePrefixConfig": {
        titles:["name", "description"],
        img: "register"
    },
    "MPHELPCommandConfig": {
        titles:["name", "description"],
        img: "help"
    },
}