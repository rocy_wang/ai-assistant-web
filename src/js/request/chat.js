import {service} from "@/js/request/service";


export function sendQuestion(question, model_id, session_id = -1) {
    return service.post("/dialogue/chat", {question: question, model_id: model_id, session_id: session_id})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function chatList() {
    return service.post("/dialogue/list")
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function chatDetail(session_id) {
    return service.post("/dialogue/detail", {session_id: session_id})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function chatDelete(session_id) {
    return service.post("/dialogue/delete", {session_id: session_id})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}