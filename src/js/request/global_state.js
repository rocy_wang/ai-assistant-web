import {createStore} from "vuex";

export const loading_state = createStore({
    state() {
        return {
            isLoading: {}
        }
    },
    getters:{
        getState:(state) => (key)=> _.get(state.isLoading,[key]) === true
    },
    mutations: {
        setLoading(state, payload) {
            const {key, value} = payload
            _.set(state.isLoading, key, value)
        }
    }
})