import {service} from "@/js/request/service";

export function sendSMSCode(phoneNumber) {
    return service.post('/user/send_code', {phone: phoneNumber})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function SMSLogin(phoneNumber, verificationCode, shareCode, wxId = 0) {
    return service.post('/user/SMSlogin', {phone: phoneNumber, code: verificationCode, s: shareCode, id: wxId})
        .then(rs => rs)
        .catch(e => console.log(e))
}

export function getOR() {
    return service.post('/user/getOR')
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function getUserInfo() {
    return service.post('/user/info')
        .then(rs => rs.data)
        .catch(e => console.log(e))
}