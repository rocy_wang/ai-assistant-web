import {service} from "@/js/request/service";

export function modelFileDelete(model_id, file_name) {
    return service.post('/model/file/delete', {model_id, file_name})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function modelUpdate(model_id, model_name, file) {
    return service.post('/model/update', {model_id, model_name, file})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function modelDelete(model_id) {
    return service.post('/model/delete', {model_id: parseInt(model_id)})
        .then(rs => rs.data)
        .catch(e => console.log(e))
}

export function modelCreate(model_name) {
    return service.post('/model/create', {model_name, file_list: []})
        .then(rs => rs.data.data)
        .catch(e => console.log(e))
}


export function getModelList() {
    return service.post("/model/list")
        .then(rs => rs.data)
        .catch(_.noop)
}
