import axios from 'axios'
import { AUTHORIZATION, SHARE_TOKEN_KEY, userStore } from '../user'

import { router } from '@/router'
import { LOGIN, MONEY } from "@/js/document";
import { loading_state } from "@/js/request/global_state";
import _ from 'lodash';
import { ElMessage, ElMessageBox } from "element-plus";

axios.defaults.withCredentials = true;

export const AI_SERVICE_PORT = 18000
export const AI_WEB_PORT = 80
export const AI_SERVICE_HOST = `https://rocy-ai.wang`
// export const AI_SERVICE_HOST = `http://localhost`
export const VUE_APP_BASE_API = `${AI_SERVICE_HOST}:${AI_SERVICE_PORT}`;
export const VUE_SOCKET_BASE_API = `${AI_SERVICE_HOST}:${AI_SERVICE_PORT}`.replace("http", "ws");
export const VUE_WEB_BASE_API = `${AI_SERVICE_HOST}${AI_WEB_PORT === 80 ? '' : `:${AI_WEB_PORT}`}`;

export const LOGIN_URL = `${VUE_WEB_BASE_API}/login`
export const PPT_URL = `${VUE_WEB_BASE_API}/ppt`
export const PPT_PLAY_URL = `${PPT_URL}/play`
export function webServerUrl(path) {
    if (_.isEmpty(path)) return path
    return `${VUE_WEB_BASE_API}${path}`
}
export function pptUrl(token) {
    return `${PPT_URL}?${SHARE_TOKEN_KEY}=${token}`
}
export function sharePPTUrl(token) {
    return `${PPT_PLAY_URL}?${SHARE_TOKEN_KEY}=${token}`
}
export function shareLoginUrl(token) {
    return `${LOGIN_URL}?${SHARE_TOKEN_KEY}=${token}`
}
export const service = axios.create({
    baseURL: VUE_APP_BASE_API,
    timeout: 60 * 1000,
    withCredentials: true,
    responseType: 'json',
    headers: {
        "Access-Control-Allow-Origin": "*"
    }
})

export const web_service = axios.create({
    timeout: 60 * 1000,
    withCredentials: true,
    headers: {
        "Access-Control-Allow-Origin": "*"
    }
})

service.defaults.withCredentials = true;
service.interceptors.request.use((config) => {
    if (userStore.getters.isLogged) config.headers[AUTHORIZATION] = userStore.getters.bear;
    return config;
});

function setLoading(loading_key, value) {
    if (!_.isNil(loading_key)) loading_state.commit("setLoading", { key: loading_key, value })
}

function processError(e, page, error, loading_key) {
    setLoading(loading_key, false)
    if (_.isNil(e.response)) {
        ElMessage({ showClose: true, message: '服务器发生错误', type: 'error' })
    }
    else if (e.response.status === 401) {
        userStore.commit("unsetUserInfo")
        router.push({ name: LOGIN })
    } else if (e.response.status === 402) {
        ElMessageBox.confirm('知识无价,支持一下这个产品呗', '我要充值', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning', })
            .then(value => (value === "confirm") && router.push({ name: MONEY }))

    } else if (e.response.status === 451) {
        const detail = _.get(e, ["response", 'data', "detail"])
        ElMessage({ message: detail, type: "warning", duration: 3000 })
    } else {
        console.error(e)
        error && error(e)
    }
}

export function service_get(url, params, success, error, page, loading_key) {
    setLoading(loading_key, true)
    return service.get(url, { params })
        .then(rs => {
            setLoading(loading_key, false)
            success && rs && success(rs.data)
        })
        .catch(e => processError(e, page, error, loading_key))
}

export function service_post_config(url, data, config) {
    return service.post(url, data, config)
        .then(rs => rs.data)
        .catch(e => console.error(e))
}

export function service_post(url, data, success, error, page, loading_key) {
    setLoading(loading_key, true)
    const is_form = _.find(["file", "images"], key => Object.keys(data || {}).includes(key)) !== undefined
    let config = is_form ? { headers: { 'Content-Type': 'multipart/form-data' } } : undefined
    if (_.get(data, ["download"]) !== undefined) config = { responseType: 'blob' }
    return service.post(url, data, config)
        .then(rs => {
            setLoading(loading_key, false)
            success && rs && success(rs.data)
            return rs.data
        })
        .catch(e => processError(e, page, error, loading_key))
}
