import _, { cloneDeep } from 'lodash'
import { v4 as uuid4 } from 'uuid';

export function deleteItem(arr, target) {
    return _.reject(arr, item => item === target)
}

export function splicePathClone(obj, path, start, deleteCount, ...arr) {
    const c = cloneDeep(obj)
    _.get(c, path).splice(start, deleteCount, ...arr)
    return c
}

export function spliceClone(obj, start, deleteCount, ...arr) {
    const c = cloneDeep(obj)
    c.splice(start, deleteCount, ...arr)
    return c
}

export function setClone(obj, path, value) {
    const c = cloneDeep(obj)
    _.set(c, path, value)
    return c
}


export function unsetClone(obj, path) {
    const c = cloneDeep(obj)
    _.unset(c, path)
    return c
}
export function getKeyByValue(obj, value) {
    return _.get(_.invert(obj), value)
}
export function swapClone(arr, index1, index2) {
    const a = _.cloneDeep(arr)
    const tmp = a[index1]
    a[index1] = a[index2]
    a[index2] = tmp
    return a
}

export function getIndexOfKey(object, key) {
    return _.indexOf(_.sortBy(Object.keys(object)), key)
}

export function getKeyAtIndex(object, index) {
    return _.sortBy(Object.keys(object))[index]
}

export function repeatOp(count, op) {
    return _.map(_.range(0, count), idx => op(idx))
}

export function renameKey(obj, old_key, new_key) {
    _.set(obj, new_key, _.get(obj, old_key))
    _.unset(obj, old_key)
    return obj
}

export function int2percent(value, max) {
    return float2percent(value / max)
}

export function noramlize(values) {
    const min = _.min(values)
    const max = _.max(values)
    return _.map(values, v => float2percent((v - min) / (max - min)))
}
export function randomShareCode() {
    return uuid4()
}
export function float2percent(value) {
    return `${_.toInteger(value * 100)}%`
}

export function get_default(obj, path, defaultValue) {
    const rs = _.get(obj, path)
    if (_.isNil(rs)) _.set(obj, path, defaultValue)
    return _.get(obj, path)
}

export function get_or_else(obj, path, other) {
    const rs = _.get(obj, path)
    return (_.isNil(rs)) ? other : rs
}

export function rejectNil(collection) {
    return _.reject(collection, _.isNil)
}

export function nilTo(value, other, me) {
    return _.isNil(value) ? other : (_.isNil(me) ? value : me)
}

export function emptyTo(value, other, me) {
    return isAnyEmpty(value) ? other : (isAnyEmpty(me) ? value : me)
}

export function noNumber2value(v, other) {
    return (v === undefined || _.isNaN(v)) ? other : v
}

export function isAnyEmpty(v) {
    return _.isNil(v) || _.isEmpty(v) || (_.isString(v) && _.trim(v).length === 0)
}

export function isNotEmpty(v) {
    return !isAnyEmpty(v)
}