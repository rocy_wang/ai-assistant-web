
import {DAY_MILLI, TIME_ZONE_MILLI_OFFSET} from "@/js/constants";

export function timestampToTime(timestamp) {
    return addMillis(new Date(timestamp), TIME_ZONE_MILLI_OFFSET).toLocaleTimeString()
}

export function timestampToDate(timestamp) {
    return addMillis(new Date(timestamp), TIME_ZONE_MILLI_OFFSET).toLocaleDateString()
}


export function timestampToText(timestamp) {
    return `${timestampToDate(timestamp)} ${timestampToTime(timestamp)}`
}

export function addMillis(date, milli) {
    const time = date.getTime();
    date.setTime(time + milli)
    return date
}


export function addDays(date, days) {
    const time = date.getTime();
    date.setTime(time + days * DAY_MILLI)
    return date
}

export function digit2(digit) {
    return digit < 10 ? `0${digit}` : String(digit)
}

export function date2Local(date) {
    return addMillis(_.isDate(date) ? date : new Date(date), TIME_ZONE_MILLI_OFFSET)
}

export function date2LocalText(date) {
    return date2text(date2Local(date))
}


export function days_diff(date) {
    date = _.isDate(date) ? date : new Date(date)
    return Math.ceil((new Date().getTime() - date.getTime()) / DAY_MILLI)
}

export function date2text(date) {
    return `${digit2(date.getMonth() + 1)}-${digit2(date.getDate())} ${digit2(date.getHours())}:${digit2(date.getMinutes())}:${digit2(date.getSeconds())}`
}

export function text2timestamp(text) {
    return Date.parse(text)
}

export function parse2timestamp(text) {
    if (_.isString(text)) return text2timestamp(text)
    else if (_.isInteger(text)) return text
    else if (_.isDate(text)) return text.getTime()
    else throw new Error(`invalid date:${text}`)
}