import {defineComponent} from "vue";
import {mixinCommon} from "@/js/document";

import {ROBOT_TYPE_MM, ROBOT_TYPE_MOBILE_QQ, ROBOT_TYPE_WEWORK} from "@/js/constants";

function parse_arr(data, path) {
    return JSON.parse(_.get(data, path) || "[]");
}

export const mixinService = defineComponent({
    mixins: [mixinCommon], data() {
        return {
            WEWORK: ROBOT_TYPE_WEWORK, MM: ROBOT_TYPE_MM, MOBILE_QQ: ROBOT_TYPE_MOBILE_QQ
        }
    }, props: {
        service: {
            type: Object, required: true
        }
    }, computed: {
        rooms() {
            return _.get(this.service, "rooms") || []
        }, contacts() {
            return _.get(this.service, "contacts") || []
        }, wework_rooms_info() {
            return _.find(this.rooms, r => r.robotType === ROBOT_TYPE_WEWORK);
        }, wework_rooms() {
            return parse_arr(this.wework_rooms_info, "rooms")
        }, mm_rooms_info() {
            return _.find(this.rooms, r => r.robotType === ROBOT_TYPE_MM);
        }, mm_rooms() {
            return parse_arr(this.mm_rooms_info, "rooms")
        }, mobile_qq_rooms_info() {
            return _.find(this.rooms, r => r.robotType === ROBOT_TYPE_MOBILE_QQ);
        }, mobile_qq_rooms() {
            return parse_arr(this.mobile_qq_rooms_info, "rooms")
        }, wework_contacts_info() {
            return _.find(this.contacts, r => r.robotType === ROBOT_TYPE_WEWORK);
        }, wework_contacts() {
            return parse_arr(this.wework_contacts_info, "contacts")
        }, mm_contacts_info() {
            return _.find(this.contacts, r => r.robotType === ROBOT_TYPE_MM);
        }, mm_contacts() {
            return parse_arr(this.mm_contacts_info, "contacts")
        }, mobile_qq_contacts_info() {
            return _.find(this.contacts, r => r.robotType === ROBOT_TYPE_MOBILE_QQ);
        }, mobile_qq_contacts() {
            return parse_arr(this.mobile_qq_contacts_info, "contacts")
        }
    }
})