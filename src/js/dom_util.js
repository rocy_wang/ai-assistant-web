
export function is_ancestor(child, ancestor) {
    let temp = child
    while (temp !== ancestor && _.isNil(temp) === false) temp = temp.parentNode
    return temp === ancestor
}