import {defineComponent} from "vue";


export const mixinCommand = defineComponent({
    props: {
        command: {
            type: Object,
            required: true
        },
        readonly: {
            type: Boolean,
            required: true
        },
    },
    computed: {
        robotType: {
            get() {
                return _.get(this.command, ['robotType'])
            },
            set(val) {
                _.set(this.command, ['robotType'], val)
            }
        },
    }
})