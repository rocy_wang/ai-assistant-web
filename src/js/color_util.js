
function supportFunc(number) {
    var hex = number.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
}
export function rgb2hext(color) {
    if (_.isNil(color)) return undefined
    if (_.startsWith(color,"#")) return color
    const arr = color.match(/\d+/g)
    const [r, g, b] = arr
    return '#' + supportFunc(parseInt(r)) + supportFunc(parseInt(g)) + supportFunc(parseInt(b))
} 