import _ from 'lodash';
import {TOKEN_KEY, userStore} from "@/js/user";
import {defineComponent} from "vue";
import {emptyTo, isAnyEmpty, isNotEmpty} from "@/js/lodash_util";
import {VUE_APP_BASE_API, VUE_WEB_BASE_API} from "@/js/request/service";
import {copyText} from "@/js/copy";
import {date2LocalText} from "@/js/time_util";
import {startCountDown, stopCountDown} from "@/js/timer";
import {COMMAND_TYPE_NAMES, COMMAND_TYPES, COUNTING_SECONDS} from "@/js/constants";


export const LOGIN = "login";
export const MONEY = "money";
export const CHAT = "chat";
export const NAVIGATOR = "navigator";
export const FROM = "from";


function scrollTop(document, window) {
    $(document).scrollTop($(document).height() - $(window).height());
}

export function maxWindow(window, callback) {
    __maxWindow();
    $(window).resize(() => __maxWindow(window, callback));
}

function pageNavigate(ownerPage) {
    ownerPage && ownerPage.toPage && ownerPage.toPage()
}

export function navigatePage(page) {
    pageNavigate(findOwnerPage(page));
}

function findOwnerPage(current) {
    if (_.isNil(current)) return undefined
    const ownerPage = _.get(current, "ownerPage");
    if (_.isNil(ownerPage)) return current
    else return findOwnerPage(ownerPage)
}

function __maxWindow(window, callback) {
    if ($(window).width() < 800) callback();
}

export function insureLogged(component, updateUser) {
    let token = component.$route.query[TOKEN_KEY];
    if (!_.isNil(token)) userStore.commit("updateToken", token)
    if (userStore.getters.isNotLogged) {
        component.$router.push('/login')
        return false
    }
    if (_.isNil(userStore.state.info) || updateUser === true) userStore.commit('getUserInfo', component)
    return true
}

export const mixinCommon = defineComponent({
    props: {
        ownerPage: {
            type: Object,
            required: false
        }
    },
    data() {
        return {
            COMMAND_TYPES,
            COMMAND_TYPE_NAMES,
            VUE_APP_BASE_API: VUE_APP_BASE_API,
            VUE_WEB_BASE_API: VUE_WEB_BASE_API,
            LOGIN,
            CHAT,
            FROM,
            counting: COUNTING_SECONDS,
            COUNTING_SECONDS,
            COUNT_INVOKE_TEXT: "获取验证码",
            COUNT_INVOKE_KEY: undefined,
        }
    },
    mounted() {
        if (userStore.getters.isNotLogged) this.toPage()
    },
    computed: {
        seconding() {
            return this.counting !== COUNTING_SECONDS
        },
        countDownText() {
            return this.counting !== COUNTING_SECONDS ? `${this.counting}秒` : this.COUNT_INVOKE_TEXT;
        },
        route() {
            return this.$route
        },
        apiKey() {
            return _.get(this.$route, ["query", "api_key"])
        },
        dialogueCount: () => _.get(userStore.state.info, "dialogue_count"),
        isAdmin: () => userStore.getters.isAdmin,
        is_month: () => userStore.getters.is_month,
        account: () => (_.get(userStore.state.info, "account") || 0).toFixed(2),
        lock_money: () => (_.get(userStore.state.info, "lock_money") || 0).toFixed(2),
        user: () => userStore.state.info,
        phone: () => _.get(userStore.state.info, "phone"),
        isLogged: () => userStore.getters.isLogged,
        from_full_path() {
            return _.get(this.route, ["query", FROM])
        },
        from_path() {
            return _.head(_.split(this.from_full_path, "?"))
        },
        from_query() {
            const query_text = _.last(_.split(this.from_full_path, "?"));
            if (this.isAnyEmpty(query_text)) return undefined
            return _.fromPairs(_.map(_.split(query_text, "&"), s => _.take(_.split(s, "="), 2)))
        },
        current_path() {
            return _.get(this.route, "fullPath")
        },
        current_name() {
            return _.get(this.route || this.ownerPage.route, "name")
        }
    },
    methods: {
        openBlankTo(location) {
            window.open(this.$router.resolve(location).href, '_blank');
        },
        startCountDown,
        stopCountDown,
        exists: (col, check) => _.find(col, c => check(c)) !== undefined,
        date2LocalText,
        countUp() {
            this.startCountDown(this.COUNT_INVOKE_KEY || "KEY", this.COUNTING_SECONDS, () => this.counting = COUNTING_SECONDS, (cnt) => this.counting = cnt)
        },
        copyText: (text) => copyText(document, text),
        isNil: _.isNil,
        isNotNil: (v) => !_.isNil(v),
        isAnyEmpty: (v) => isAnyEmpty(v),
        isNotEmpty: (v) => isNotEmpty(v),
        isArray: _.isArray,
        getOptions: _.get,
        setOptions: _.set,
        emptyTo: emptyTo,
        toPage() {
            if (this.current_name === LOGIN) {
                if (this.isLogged === false) return
                if (this.isAnyEmpty(this.from_full_path)) this.$router.push({name: NAVIGATOR})
                else this.$router.push({path: this.from_path, query: this.from_query})
            } else this.$router.push({name: LOGIN, query: {from: this.current_path}});
        },
        isNumber: _.isNumber,
        isString: _.isString,
        isBoolean: _.isBoolean,
    }
})