import html2canvas from "html2canvas"

export function downloadBlob(blob, download_name) {
    let dom = document.createElement('a')
    let url = window.URL.createObjectURL(blob)
    dom.href = url
    dom.download = decodeURI(download_name)
    dom.style.display = 'none'
    document.body.appendChild(dom)
    dom.click()
    dom.parentNode.removeChild(dom)
    window.URL.revokeObjectURL(url)
}

export function saveImage(imageElement, filename) {
    var canvas = document.createElement('canvas');
    canvas.width = imageElement.width * 3;
    canvas.height = imageElement.height * 3;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(imageElement, 0, 0, canvas.width, canvas.height);
    var dataURL = canvas.toDataURL('image/png');
    var a = document.createElement('a');
    a.href = dataURL;
    a.download = filename || 'downloaded-image.png';
    a.click();
}

export function convertHtmltoBlob(element) {
    return new Promise(resolve => html2canvas(element).then(canvas => canvas.toBlob(resolve)))
}



export function convertHtmltoBase64(element) {
    const options = {
        backgroundColor: "transparent", logging: true, ignoreElements: (element) => {

        }
    }
    return new Promise(resolve => html2canvas(element, options).then(canvas => canvas.toBlob(resolve)))
        .then(convertBlob2Base64)
}
export function tripBase64(text) {
    if (_.startsWith(text, "data:image")) return _.last(_.split(text, ","))
    return text
}
export function convertBlob2Base64(blob) {
    return new Promise((resolve) => {
        const reader = new FileReader()
        reader.onload = (event) => resolve(tripBase64(event.target.result))
        reader.readAsDataURL(blob)
    })
}

export function downloadHtmlImages(element, image_size) {
    return html2canvas(element).then(canvas => convertCanvasToImage(canvas, image_size))
}

export function convertCanvasToImage(canvas, image_size) {
    const { width, height } = image_size
    var image = new Image(width, height);
    image.src = canvas.toDataURL("image/png");
    return image;
}