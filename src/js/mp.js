import {createStore} from "vuex";
import {service_post} from "@/js/request/service";
import {ElMessage} from "element-plus";

export const mpStore = createStore({
    state() {
        return {
            sys_mps: undefined
        }
    },
    getters: {
        sys_mp_list: (state) => state.sys_mps
    },
    mutations: {
        getSysMps(state, component) {
            service_post("/mp/list_sys_mps", null, (mps) => state.sys_mps = mps, () => ElMessage({message: "获取系统信息错误", type: "warning"}), component)
        }
    }
})