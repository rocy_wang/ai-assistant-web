import {createStore} from "vuex";
import {userStore} from "@/js/user";
import {io} from "socket.io-client";
import {VUE_SOCKET_BASE_API} from "@/js/request/service";


const CLIENT_EVENT_ERROR = "error"
const CLIENT_EVENT_COMMAND = "command"
const CLIENT_EVENT_ACCEPT = "accept"

export const socketStore = createStore({
    state() {
        return {
            connected_sockets: {}
        }
    },
    getters: {
        user_token: () => userStore.getters.getUserToken,
    },
    actions: {
        connect({state}, service_token) {
            if (_.get(state.connected_sockets, [service_token]) === undefined)
                _.set(state.connected_sockets, [service_token], {
                    connected: false,
                    accepted: false,
                    commands: [],
                    socket: undefined,
                    error: undefined
                })
            const socket_state = _.get(state.connected_sockets, [service_token]);
            if (socket_state.connected === true) return
            socket_state.socket = io(VUE_SOCKET_BASE_API, {
                transports: ["websocket"], query: {
                    user_token: socketStore.getters.user_token,
                    service_token,
                },
                autoConnect: true
            });

            socket_state.socket.on("connect", () => {
                socket_state.connected = true;
            });

            socket_state.socket.on("disconnect", () => {
                socket_state.connected = false;
            });
            socket_state.socket.on(CLIENT_EVENT_ACCEPT, () => {
                socket_state.accepted = true;
            });
            socket_state.socket.on(CLIENT_EVENT_COMMAND, (command) => {
                const command_json = JSON.parse(command);
                _.set(command_json, "datetime", new Date().toLocaleTimeString())
                socket_state.commands.push(command_json)
            });

            socket_state.socket.on(CLIENT_EVENT_ERROR, (error) => {
                socket_state.error = error
            });
        },
    },
    mutations: {
        clear_command_history(state, service_token) {
            const socket_state = _.get(state.connected_sockets, [service_token]);
            if (socket_state !== undefined) _.set(socket_state, "commands", [])
        },
        disconnect(state, service_token) {
            const socket_state = _.get(state.connected_sockets, [service_token]);
            if (socket_state !== undefined && socket_state.socket !== undefined) {
                socket_state.socket.disconnect();
                delete state.connected_sockets[service_token]
            }
        },
        emit(state, payload) {
            const {service_token, event, data, callback} = payload
            const message = event || "message";
            const socket_state = _.get(state.connected_sockets, [service_token]);
            socket_state.socket && socket_state.socket.emit(message, data)
        }
    }
});