import {defineComponent} from "vue";
import {socketStore} from "@/js/socket/socket_store";


export const socketMixin = defineComponent({
    computed: {
        connected_sockets: () => socketStore.state.connected_sockets
    },
    methods: {
        clear_command_history(service_token){
            socketStore.commit("clear_command_history", service_token)
        },
        connect(service_token) {
            socketStore.dispatch("connect", service_token).then(r => _.noop)
        },
        disconnect(service_token) {
            socketStore.commit("disconnect", service_token)
        },
        emit_message(service_token, message) {
            socketStore.commit("emit", {"service_token": service_token, "data": message})
        }
    }
})