;
import VuexPersistence from 'vuex-persist'
import { createStore } from "vuex";
import { service_post, shareLoginUrl } from "@/js/request/service";
import { copyText } from "@/js/copy";
import { ElNotification } from "element-plus";
import { h } from "vue";

export const TOKEN_KEY = "accessToken";
export const SHARE_TOKEN_KEY = "shareToken";
export const TOKEN_TYPE_KEY = "tokenType";
export const AUTHORIZATION = "Authorization"
export const CODE_KEY = "code";
export const INFO_KEY = "info";
export const BEARER = "bearer";

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

export function showPhone(phone) {
    return phone && `${phone.slice(0, 3)}****${phone.slice(7)}`
}

export function showDate(dt) {
    return dt && new Date(Date.parse(dt)).toLocaleString()
}

export function recommend(document) {
    copyText(document, shareLoginUrl(userStore.state.info["share_token"]))
    ElNotification({ title: '提示', message: h('i', { style: 'color: #20a0ff' }, `分享链接已经拷贝`), duration: 2000 })
}
function updateInfo(state, info) {
    if (_.isNil(state.info)) state.info = {}
    state.info = _.merge({}, state.info, info)
}
export const userStore = createStore({
    state() {
        return {
            token: undefined,
            info: undefined
        }
    },
    getters: {
        month_balances: (state) => _.get(state.info, "month_balances") || [],
        is_month: (state) => _.get(state.info, "is_month") === true,
        userId: (state) => _.get(state.info, "id"),
        userEmail: (state) => _.get(state.info, "phone"),
        userEmailTail: (state) => _.get(state.info, "phone").substring(7),
        getUserToken: (state) => state.token,
        isLogged: (state, getters) => !getters.isNotLogged,
        isNotLogged: (state, getters) => _.isNil(getters.getUserToken),
        bear: (state, getters) => getters.isLogged ? `${BEARER} ${getters.getUserToken}` : undefined,
        dialogueCount: (state) => _.get(state.info, "dialogue_count", 0),
        isAdmin: (state) => _.get(state.info, "is_admin", false) === true
    },
    mutations: {
        updateToken: (state, token) => {
            if (!_.isNil(token) && token.startsWith(BEARER)) token = _.last(token.split(/\s+/))
            state.token = token
        },
        dialogueCountIncrease: (state) => {
            updateInfo(state, { "dialogue_count": userStore.getters.dialogueCount + 1 })
        },
        dialogueCountDecrease: (state) => {
            updateInfo(state, { "dialogue_count": userStore.getters.dialogueCount - 1 })
        },
        getUserInfo: (state, component) => {
            service_post("/user/me", null, (user) => updateInfo(state, user), undefined, component)
        },
        unsetUserInfo: (state) => {
            state.token = undefined
            state.info = undefined
        }
    },
    plugins: [vuexLocal.plugin]
})
