import {defineComponent} from "vue";
import {mixinCommon} from "@/js/document";

import {
    MPImageAAServicePrefixConfig,
    MPProxyKeywordServiceReplyQuestionConfig,
    MPRobotLoopNewMessageOnlineConfig,
    MPSendMessageTimeCommandConfig,
    MPTextAAServicePrefixConfig,
    RobotGetRoomsOnlineMPConfig,
    RobotGetContactsOnlineMPConfig,
    MPSendCodeCommandConfig,
    MPRegisterCommandAAServicePrefixConfig,
    MPHELPCommandConfig,
    ROBOT_TYPE_MM,
    ROBOT_TYPE_MOBILE_QQ,
    ROBOT_TYPE_WEWORK
} from "@/js/constants";
import {mixinService} from "@/js/mixin_service";
import {MP_MAPPINGS} from "@/js/constants_mp";

export const mixinMP = defineComponent({
    mixins: [mixinCommon, mixinService],
    data() {
        return {
            MP_MAPPINGS,
            showDialog: false,
            MPTextAAServicePrefixConfig,
            MPImageAAServicePrefixConfig,
            MPProxyKeywordServiceReplyQuestionConfig,
            MPRobotLoopNewMessageOnlineConfig,
            MPSendMessageTimeCommandConfig,
            RobotGetRoomsOnlineMPConfig,
            RobotGetContactsOnlineMPConfig,
            MPSendCodeCommandConfig,
            MPRegisterCommandAAServicePrefixConfig,
            MPHELPCommandConfig,
            ROBOT_TYPE_WEWORK,
            ROBOT_TYPE_MM,
            ROBOT_TYPE_MOBILE_QQ
        }
    },
    props: {
        config: {
            type: Object,
            required: true
        },
        service: {
            type: Object,
            required: true
        },
        readonly: {
            type: Boolean,
            required: true
        },
        title_only: {
            type: Boolean,
            required: true
        },
        has_wrapper: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    computed: {
        im_rooms_contacts() {
            switch (this.category) {
                case ROBOT_TYPE_WEWORK:
                    return {
                        rooms: this.wework_rooms,
                        contacts: this.wework_contacts,
                        name: "企业微信"
                    }
                case ROBOT_TYPE_MOBILE_QQ:
                    return {
                        rooms: this.mobile_qq_rooms,
                        contacts: this.mobile_qq_contacts,
                        name: "微信"
                    }
                case ROBOT_TYPE_MM:
                    return {
                        rooms: this.mm_rooms,
                        contacts: this.mm_contacts,
                        name: "移动QQ"
                    }
                default :
                    return undefined
            }
        },
        category() {
            return this.config["category"]
        },
        description() {
            return this.config['description']
        },
        title_properties() {
            return this.MP_MAPPINGS[this.category].titles
        },
        img() {
            return `/img/mp/${this.MP_MAPPINGS[this.category].img}.svg`
        },
        titles() {
            let feature_titles = _.pick(this.config, this.title_properties)
            return _.reject(_.values(feature_titles), _.isNil)
        },

    }
})