import { MARGIN, SUB_TITLE_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE } from "./template_constants";
import { officeState } from '@/js/office/office_state';
export const ZHANGJIE_LAYOUT_NORMAL_ROW = "normal_row"

export function generateZhangjieHtml(layout, image_use, image_arrow_use) {
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    switch (layout) {
        case "normal_row":
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex justify-content-center align-items-center"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                ${image_use ?
                    `<img class="k position_size image me-3" src='/img/office/content.svg' style="width:32px;height:32px;"/>` :
                    `<div class="k position_size zhang_index me-3" style="font-size:${TITLE_SIZE}px">第 01 部分</div>`
                }
                <div class="d-flex flex-column h-100 justify-content-center ">
                    <div class="k position_size zhang_title mb-2" style="font-size:${TITLE_SIZE}px">${TITLE_HOLDER}</div>
                    <div class="k position_size zhang_sub_title mb-2" style="font-size:${TEXT_SIZE}px">${SUB_TITLE_HOLDER}</div>
                </div>
                ${image_arrow_use ?
                    `<img class="k position_size arrow m-3" src='/img/office/content.svg' style="width:32px;height:32px;"/>` :
                    ``
                }
                
            </div>
        </div>`;
        case "normal_column":
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column  justify-content-center align-items-center"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                ${image_use ?
                    `<img class="k position_size image me-3" src='/img/office/content.svg' style="width:32px;height:32px;"/>` :
                    `<div class="k position_size zhang_index me-3" style="font-size:${TITLE_SIZE}px">第 01 部分</div>`
                }
                <div class="d-flex flex-column justify-content-center ">
                    <div class="k position_size zhang_title mb-2" style="font-size:${TITLE_SIZE}px">${TITLE_HOLDER}</div>
                    <div class="k position_size zhang_sub_title mb-2" style="font-size:${TEXT_SIZE}px">${SUB_TITLE_HOLDER}</div>
                </div>
                ${image_arrow_use ?
                    `<img class="k position_size arrow m-3" src='/img/office/content.svg' style="width:32px;height:32px;"/>` :
                    ``
                }
            </div>
        </div>`;
    }
}