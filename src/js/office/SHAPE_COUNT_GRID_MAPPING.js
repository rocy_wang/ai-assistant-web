import { mapArea, IMAGE_CENTER_AREA33, IMAGE_CENTER_AREA55, IMAGE_CENTER_AREA44, IMAGE_CENTER_AREA77 } from './generate_guanxi_slides';
import _ from 'lodash'
let _SHAPE_COUNT_GRID_MAPPING = undefined
export function SHAPE_COUNT_GRID_MAPPING() {
    if (_SHAPE_COUNT_GRID_MAPPING === undefined) initializeGripMapping()
    return _SHAPE_COUNT_GRID_MAPPING
}
function addOffset(area_mapping, offset) {
    const [item_offset, image_offset, item_offset_v, image_offset_v] = offset || []
    return _.merge({}, area_mapping, { item_offset: item_offset || 0, image_offset: image_offset || 0, item_offset_v: item_offset_v || 0, image_offset_v: image_offset_v || 0 })
}
export function initializeGripMapping() {
    _SHAPE_COUNT_GRID_MAPPING = {
        "流程": {
            "base": "/img/office/shapes/flow",
            "2": [
                mapArea(IMAGE_CENTER_AREA33, ["bottom", 2], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["bottom", 2], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["right", 1], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["right", 1], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["right", 1], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["right", 1], ["left", 4]),
            ],
            "3": [
                mapArea(IMAGE_CENTER_AREA33, ["r", 1], ["r", 2], ["r", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["l", 2], ["r", 2], ["l", 4]),
            ],
            "4": [
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["b", 4], ["l", 4]),
            ],
            "5": [
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["r", 1], ["r", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 1], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 1], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 1], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["r", 2], ["b", 2], ["b", 4], ["l", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 2], ["t", 2], ["b", 2], ["b", 4], ["l", 3]),
            ],
            "6": [
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 1], ["r", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["l", 2], ["t", 2], ["r", 1], ["r", 3], ["l", 4], ["b", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["l", 2], ["t", 2], ["r", 1], ["r", 3], ["l", 4], ["b", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["l", 2], ["t", 2], ["r", 1], ["r", 3], ["l", 4], ["b", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["l", 2], ["t", 2], ["r", 1], ["r", 3], ["l", 4], ["b", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["t", 1], ["t", 3], ["r", 2], ["l", 3], ["b", 2], ["b", 4]),
            ]
        },
        "循环": {
            "base": "/img/office/shapes/cycle",
            "2": [
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
            ],
            "3": [
                mapArea(IMAGE_CENTER_AREA55, ["l", 4, 1], ["b", 4], ["r", 3, -1]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 1], ["b", 3], ["r", 3]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3], ["b", 2], ["b", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3], ["b", 2], ["b", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3, 1], ["b", 2, -1], ["b", 6, -1]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3, 1], ["b", 2, -1], ["b", 6, -1]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3, 1], ["b", 2, -1], ["b", 6, -1]),
                mapArea(IMAGE_CENTER_AREA55, ["t", 3, 1], ["b", 2, -1], ["b", 6, -1]),
            ],
            "4": [
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
            ],
            "5": [
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 3], ["left", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 3], ["left", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA55, ["left", 3], ["left", 5], ["top", 3], ["bottom", 4], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 2], ["left", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 2], ["bottom", 2], ["left", 3], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 2], ["bottom", 2], ["left", 3], ["bottom", 6]),
            ],
            "6": [
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["top", 2], ["left", 4], ["right", 1], ["bottom", 3], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["top", 2], ["left", 4], ["right", 1], ["bottom", 3], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["top", 2], ["left", 4], ["right", 1], ["bottom", 3], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["top", 2], ["left", 4], ["right", 1], ["bottom", 3], ["right", 3]),
            ]
        },
        "金字塔": {
            "base": "/img/office/shapes/pyramid",
            "3": [
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [0, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [-30, 0, -50, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [-20, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["t", 4, 3], ["t", 4, 5], ["t", 4, 7]), [-20, 0, -20, 25]),
            ],
            "4": [
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["t", 3], ["b", 4], ["r", 3, -1], ["l", 4, 1]), [0, 8]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
                addOffset(mapArea(IMAGE_CENTER_AREA55, ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-30, 0, 0, 25]),
            ],
            "5": [
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["r", 6], ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-200]),
                addOffset(mapArea(IMAGE_CENTER_AREA77, ["r", 6], ["r", 2, -4], ["r", 3, -3], ["r", 4, -2], ["r", 5, -1]), [-150, 0, 10]),
            ]
        },
        "并列": {
            "base": "/img/office/shapes/side_by_side",
            "2": [
                mapArea([3, 3, 3, 2], ["right", 2], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["bottom", 4]),
                mapArea([3, 3, 3, 2], ["right", 1], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["bottom", 2], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["right", 1], ["left", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["right", 1]),
            ],
            "3": [
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["left", 2], ["right", 1], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA55, ["left", 4], ["right", 4], ["top", 3]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 4], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 6], ["top", 3]),
                mapArea(IMAGE_CENTER_AREA55, ["left", 4], ["right", 4], ["top", 3]),
                mapArea([3, 4, 2, 5], ["top", 1], ["top", 3], ["top", 5]),
            ],
            "4": [
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["bottom", 2], ["bottom", 3], ["bottom", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA44, ["bottom", 2], ["bottom", 3], ["bottom", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["top", 3], ["bottom", 2], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA44, ["bottom", 2], ["bottom", 3], ["bottom", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 2], ["right", 2], ["bottom", 3], ["left", 3]),
                mapArea(IMAGE_CENTER_AREA44, ["top", 1], ["right", 1], ["bottom", 4], ["left", 4]),
            ],
            "5": [
                mapArea(IMAGE_CENTER_AREA55, ["bottom", 2], ["bottom", 3], ["bottom", 4], ["bottom", 5], ["bottom", 6]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 1], ["top", 4], ["right", 3], ["bottom", 5], ["bottom", 2]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 1], ["top", 4], ["right", 3], ["bottom", 4], ["left", 4]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 3], ["left", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 3], ["left", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 3], ["right", 3], ["bottom", 3], ["left", 4], ["bottom", 5]),
                mapArea(IMAGE_CENTER_AREA55, ["top", 2], ["top", 4], ["right", 2], ["bottom", 4], ["left", 3]),
            ],
            "6": [
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["left", 3], ["left", 4], ["right", 1], ["right", 2], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["left", 2], ["left", 3], ["left", 4], ["right", 1], ["right", 2], ["right", 3]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 2], ["top", 3], ["bottom", 2], ["bottom", 3], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 2], ["top", 3], ["bottom", 2], ["bottom", 3], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 2], ["top", 3], ["bottom", 2], ["bottom", 3], ["bottom", 4]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 3], ["bottom", 2], ["bottom", 4], ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 3], ["bottom", 2], ["bottom", 4], ["left", 3], ["right", 2]),
                mapArea(IMAGE_CENTER_AREA33, ["top", 1], ["top", 3], ["bottom", 2], ["bottom", 4], ["left", 3], ["right", 2]),
            ]
        }
    };
}

