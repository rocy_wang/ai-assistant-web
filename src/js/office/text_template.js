import { containerWrap, imageDiv, textDiv, titleDiv, wrapDiv } from "./template_constants"

export function generateTextHtmls() {
    return [generateTextHtml(), generateTextHtml2(true),generateTextHtml2(false),]
}
export function generateTextHtml() {
    return containerWrap(true, undefined, "align-items-stretch", `
        ${wrapDiv("flex-row w-100 justify-content-between", titleDiv(), imageDiv("quote"))}
        ${textDiv("flex-grow-1", "max-height:100%")}
    `)
}
export function generateTextHtml2(is_horizontal) {
    return containerWrap(true, undefined, "align-items-stretch", `
        ${wrapDiv("flex-row w-100 justify-content-between", titleDiv(), imageDiv("quote"))}
        ${wrapDiv(`${is_horizontal ? 'flex-row' : "flex-column"} flex-grow-1 w-100 align-items-stretch`, textDiv("flex-grow-1 overflow-auto m-1", "max-height:200px"), textDiv("flex-grow-1  overflow-auto m-1", "max-height:200px"))}
    `)
}