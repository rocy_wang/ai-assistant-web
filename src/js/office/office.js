
import { randomNumberExclude } from '@/js/office/svg_background'
import '@/js/bootstrap.bundle.min'
import { webServerUrl } from '../request/service'
export const PRIMARY_COLOR = "#409EFF"
export const PRIMARY_TEXT_COLOR = "#303133"
export const SUCCESS_COLOR = "#67C23A"
export const WARNING_COLOR = "#E6A23C"
export const DANGER_COLOR = "#F56C6C"
export const INFO_COLOR = "#909399"
export const BLACK_COLOR = "#000000"
export const WHITE_COLOR = "#FFFFFF"
export const REGULAR_TEXT_COLOR = "#606266"
export const COLORS = [PRIMARY_COLOR, SUCCESS_COLOR, WARNING_COLOR, DANGER_COLOR, INFO_COLOR]
export const COLORS_MAP = {
    "primary": PRIMARY_COLOR,
    "success": SUCCESS_COLOR,
    "warning": WARNING_COLOR,
    "danger": DANGER_COLOR,
    "info": INFO_COLOR,
    "black":BLACK_COLOR,
    "white":WHITE_COLOR
}

export function randomColor() {
    return COLORS[randomNumberExclude(COLORS.length)]
}
export const predefine_colors = [
    'rgba(255, 255, 255,1)',
    'rgba(0, 0, 0,1)',
    "#20a0ff",
    '#ff4500',
    '#ff8c00',
    '#ffd700',
    '#90ee90',
    '#00ced1',
    '#1e90ff',
    '#c71585',
    'rgba(255, 69, 0, 0.68)',
    'rgb(255, 120, 0)',
    'hsv(51, 100, 98)',
    'hsva(120, 40, 94, 0.5)',
    'hsl(181, 100%, 37%)',
    'hsla(209, 100%, 56%, 0.73)',
    '#c7158577',
]

export const default_background = {
    "background-color": "rgba(255, 255, 255,1)",
    "background-position": "0% 0%",
    "background-repeat": "repeat",
    "background-origin": "padding-box",
    "background-clip": "border-box",
    "background-attachment": "scroll",
    "background-image": "unset",
}
export const OFFICE_TYPE_PPT = "ppt"
export const OFFICE_TYPE_WORD = "word"
export const BASE64_PREFIX = "data:image"
export const IMAGE_BASE = `/img/office/bootstrap-icons-1.11.2`
export function base64image(image) {
    if(image === null || image === undefined) return image
    if (_.startsWith(image, "http")) return image
    if (_.startsWith(image, BASE64_PREFIX)) return image
    if (_.endsWith(image, ".svg")) {
        if (image.includes("/")) return webServerUrl(image)
        else return webServerUrl(`${IMAGE_BASE}/${image}`)
    }
    if (image.length > 200) return `data:image/png;base64,${image}`
    return image
}
export function bootstrapIconImage(image) {
    return `${IMAGE_BASE}/${image}`
}

export function isBootstrapSVGIcon(image) {
    return _.startsWith(image, IMAGE_BASE) && _.endsWith(".svg")
}