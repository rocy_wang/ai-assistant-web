import { DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_TITLE, DATA_CATEGORY_RICH } from '../ppt';
import { COLORS } from './office';
import { SUB_TITLE_HOLDER, SUB_TITLE_SIZE, TEXT_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE } from './template_constants';
import _ from 'lodash';
import { GUANXI_TEXT_HEIGHT, GUANXI_TEXT_WIDTH, generateGuanXiSlidesBinglieHtml } from './guanxi_template';
import { randomOne, getPositions, wrapperHtmlEditor, addStyle, numberImages, wrapperImage, wrapperSlide, getValidPositions } from './auto_ppt_layout';
import { getGridItemsHtml, generateGuanXiSlidesTimeAxisHtml } from '@/js/office/guanxi_template.js'
import { SHAPE_COUNT_GRID_MAPPING } from './SHAPE_COUNT_GRID_MAPPING';
const skew_angle = 10;


export function generateGuanXiSlides(scene, color_serial, has_icon, logo, option) {
    const gradient_color = `linear-gradient(to right, ${randomOne(COLORS)}, ${randomOne(COLORS)})`;
    const shape = _.get(option, "shape");
    const count = _.get(option, "count");
    const context = _.get(option, "context");
    const mappings = SHAPE_COUNT_GRID_MAPPING()
    const mapping = _.get(mappings, [shape, count])
    const base = _.get(mappings, [shape, "base"])
    const slides = mapping === undefined ? [] : generateSideBySideSlideOptions(mapping, `${base}/item${count}`, scene, color_serial, has_icon, logo, context)
    let others = []
    if (shape === "并列") others = getFlexSlides(shape, count, context, skew_angle, gradient_color, scene, color_serial)
    if (shape === "时间轴") others = getTimeAxisSlides(count, context, scene, color_serial)
    return _.concat(slides, others)
}
function toArea() {
    const [row_start, row_count, column_start, column_count] = arguments
    return _.join([row_start, column_start, row_start + row_count, column_start + column_count], "/")
}
export const IMAGE_CENTER_AREA33 = [3, 3, 3, 3]
export const IMAGE_CENTER_AREA44 = [2, 4, 2, 4]
export const IMAGE_CENTER_AREA55 = [2, 5, 2, 5]
export const IMAGE_CENTER_AREA77 = [1, 7, 1, 7]
export function mapArea() {
    const image_area = _.first(arguments)
    const [row_start, row_count, column_start, column_count] = image_area
    const item_areas = _.map(_.tail(arguments), ([location, offset, cut]) => {
        cut = cut || 0
        switch (location) {
            case "t":
            case "top": return [row_start - 1 + cut, 1, column_start + offset - 1, 1]
            case "r":
            case "right": return [row_start + offset - 1, 1, column_start + column_count + cut, 1]
            case "b":
            case "bottom": return [row_start + row_count + cut, 1, column_start + offset - 2, 1]
            case "l":
            case "left": return [row_start + offset - 2, 1, column_start - 1 + cut, 1]
        }
    })
    return { image_area: toArea(...image_area), item_areas: _.map(item_areas, item => toArea(...item)) }
}

function generateSideBySideSlideOptions(area_mappings, base, scene, color_serial, has_icon, logo, context, all_offset, all_offset_v) {
    return _.reverse(_.map(area_mappings, (area_mapping, index) => {
        const { item_offset, image_offset, item_offset_v, image_offset_v } = _.merge({}, { item_offset: 0, image_offset: 0, item_offset_v: 0, image_offset_v: 0 }, area_mapping)
        all_offset = all_offset || 0
        all_offset_v = all_offset_v || 0
        const gradient_color = `linear-gradient(to right, ${randomOne(COLORS)}, ${randomOne(COLORS)})`;
        let positions = getPositions(getGridItemsHtml(area_mapping), context);
        function o(p, o, ov) { return _.merge(p, { left: `${parseInt(p['left']) + o}px`, top: `${parseInt(p['top']) + ov}px` }) }
        positions = _.mapValues(positions, (position, k) => {
            if (k.includes("guanxi_title")) return position
            return k.includes("image_area") ? o(position, all_offset + image_offset, all_offset_v + image_offset_v) : o(position, all_offset + item_offset, all_offset_v + item_offset_v)
        })
        const guanxi_title = _.find(positions, (v, k) => k.includes("guanxi_title"));
        const image_area_styles = _.filter(positions, (v, k) => k.includes("image_area"));
        const item_area_styles = _.filter(positions, (v, k) => k.includes("item_area"));
        const image_objects = _.map(image_area_styles, image_area_style => wrapperImage(`${base}/${index + 1}.svg`, [], image_area_style, color_serial));
        const guanxi_title_object = wrapperHtmlEditor(TITLE_HOLDER, randomOne(['text-primary', 'text-success']), guanxi_title, TITLE_SIZE, DATA_CATEGORY_TITLE);
        const item_area_objects = _.map(item_area_styles, g => wrapperHtmlEditor(SUB_TITLE_HOLDER, _.concat(randomOne(['text-secondary']), ["p-2"]),
            addStyle(g, {
                transform: `skew(-${skew_angle}deg)`,
                "box-shadow": "2px 2px 2px 1px rgba(0, 0, 255, .2)",
                background: gradient_color
            }), SUB_TITLE_SIZE, DATA_CATEGORY_SUB_TITLE));
        const objects = _.concat(guanxi_title_object, image_objects, item_area_objects);
        return wrapperSlide(scene, color_serial, objects, positions, has_icon, logo);
    }));
}
function getTimeAxisSlides(count, context, scene, color_serial) {
    const directions = [true, false]
    return _.map(directions, is_vertical => {
        const tag_image = randomOne(["tag1.svg", "tag2.svg"])
        const gradient_color = `linear-gradient(to right, ${randomOne(COLORS)}, ${randomOne(COLORS)})`;
        const positions = getPositions(generateGuanXiSlidesTimeAxisHtml(count, is_vertical), context);
        const guanxi_title = _.find(positions, (v, k) => k.includes("guanxi_title"));
        const guanxi_sub_titles = _.filter(positions, (v, k) => k.includes("guanxi_sub_title"));
        const image_axises = _.filter(positions, (v, k) => k.includes("image_axis"));
        const image_tags = _.filter(positions, (v, k) => k.includes("image_tag"));
        const guanxi_title_object = wrapperHtmlEditor(TITLE_HOLDER, randomOne(['text-primary', 'text-success']),
            guanxi_title, TITLE_SIZE, DATA_CATEGORY_TITLE);
        const guanxi_sub_title_objects = _.map(guanxi_sub_titles, g => wrapperHtmlEditor(SUB_TITLE_HOLDER, _.concat(randomOne(['text-secondary']), ["p-2"]),
            addStyle(g, {
                // transform: `skew(-90deg)`,
                "box-shadow": "2px 2px 2px 1px rgba(0, 0, 255, .2)",
                background: gradient_color
            }), SUB_TITLE_SIZE, DATA_CATEGORY_SUB_TITLE));
        const image_axis_objects = _.map(image_axises, (i, index) => wrapperImage(is_vertical ? "/img/office/shapes/time_axis/vertical.svg" : "/img/office/shapes/time_axis/horizontal.svg", [], i));
        const image_tag_objects = _.map(image_tags, (i, index) => wrapperImage(`/img/office/shapes/time_axis/${tag_image}`, [], i));
        const objects = _.concat(guanxi_title_object, guanxi_sub_title_objects, image_axis_objects, image_tag_objects);
        return wrapperSlide(scene, color_serial, objects, positions);
    })
}


function getFlexSlides(shape, count, context, skew_angle, gradient_color, scene, color_serial) {
    const positions = getPositions(generateGuanXiSlidesBinglieHtml(shape, count), context);
    const guanxi_title = _.find(positions, (v, k) => k.includes("guanxi_title"));
    const guanxi_sub_titles = _.filter(positions, (v, k) => k.includes("guanxi_sub_title"));
    const images = _.filter(positions, (v, k) => k.includes("image"));
    const guanxi_texts = _.filter(positions, (v, k) => k.includes("guanxi_text"));
    const guanxi_title_object = wrapperHtmlEditor(TITLE_HOLDER, randomOne(['text-primary', 'text-success']),
        guanxi_title, TITLE_SIZE, DATA_CATEGORY_TITLE);
    const guanxi_sub_title_objects = _.map(guanxi_sub_titles, g => wrapperHtmlEditor(SUB_TITLE_HOLDER, _.concat(randomOne(['text-secondary']), ["p-2"]),
        addStyle(g, {
            transform: `skew(-${skew_angle}deg)`,
            "box-shadow": "2px 2px 2px 1px rgba(0, 0, 255, .2)",
            background: gradient_color
        }), SUB_TITLE_SIZE, DATA_CATEGORY_SUB_TITLE));

    const guanxi_text_objects = _.map(guanxi_texts, g => wrapperHtmlEditor(TEXT_HOLDER, _.concat(randomOne(['text-body']), ['border', 'border-rounded', 'border-opacity-25', 'border-secondary', 'p-2']),
        addStyle(g, { width: `${GUANXI_TEXT_WIDTH}px`, height: `${GUANXI_TEXT_HEIGHT}px` }), TEXT_SIZE, DATA_CATEGORY_RICH));
    const number_image_urls = numberImages(Object.keys(images).length);
    const image_objects = _.map(images, (i, index) => wrapperImage(number_image_urls[index], [], i));
    const objects = _.concat(guanxi_title_object, guanxi_sub_title_objects, guanxi_text_objects, image_objects);
    const rs = wrapperSlide(scene, color_serial, objects, positions);
    return [rs];
}


