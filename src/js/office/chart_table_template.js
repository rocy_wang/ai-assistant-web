import { containerSize, containerWrap, imageDiv, subTitleDiv, textDiv, titleDiv, wrapDiv } from "./template_constants"

export function generateTuBiaoHtml(is_reverse) {
    return containerWrap(is_reverse ? "flex-column-reverse" : "flex-column", undefined, `align-items-stretch`, `
        ${titleDiv("w-100")}
        ${imageDiv("flex-grow-1")}
    `)
}