import _ from "lodash"

import { officeState } from "./office_state"
import { default_background } from '@/js/office/office'
import { get_default, isAnyEmpty } from '@/js/lodash_util'
import { SVG_IMAGE_ID } from '@/js/office/svg_background.js'
import { service_post } from '@/js/request/service.js'
import { getSVGBackground } from "./auto_ppt_layout"
import { MODE_DESIGN, MODE_RUN, POPOVER_STYLE } from '@/js/ppt'

export const formatMixin = {
    emits: ["focusin:object", "update:config"],
    data() {
        return {
            POPOVER_STYLE,
            _config: { classes: [], style: {} },
            svg_background_settings: undefined
        }
    },
    props: {
        is_selected: {
            type: Boolean,
            required: false,
            default: false
        },
        is_focused: {
            type: Boolean,
            required: false,
            default: false
        },
        config: {
            type: Object,
            required: false
        },
        mode: { type: Number, default: MODE_DESIGN, required: false },
    },
    watch: {
        config: {
            handler(val, old) {
                this._config = _.cloneDeep(val)
            }, immediate: true, deep: true
        },
        style: {
            handler(val, old) {
                this.update_style()
            }, immediate: true, deep: true
        },
    },
    methods: {
        saveAutoSVGBackground() {
            if (this.svg_background_settings === undefined) return Promise.resolve()
            const svg_image_id = _.get(this.style, SVG_IMAGE_ID)
            if (svg_image_id === undefined) return Promise.resolve()
            if (svg_image_id >= 0) return Promise.resolve()
            return service_post("/office/save_background_setting", { background_setting: this.svg_background_settings }).then(id => {
                _.set(this.svg_background_settings, ["id"], id)
                this.style = _.merge({}, this.style, { [SVG_IMAGE_ID]: id })
                return id
            })
        },
        prepareSVG(svg_setting) {
            _.set(this.svg_setting, ["canvas", "width"], this.format_container_width())
            _.set(this.svg_setting, ["canvas", "height"], this.format_container_height())
            this.svg_background_settings = svg_setting
            const f = this.format_container_dolar()
            if (f.hasClass("position-relative") === false) f.addClass("position-relative")
            f.css('background-color', 'transpant')
            f.css('background-image', 'unset')
        },
        updateSVGBackground(style) {
            const svg_image_id = _.get(style, SVG_IMAGE_ID)
            if (svg_image_id === undefined) return
            if (_.toInteger(svg_image_id) < 0) this.prepareSVG(getSVGBackground(svg_image_id))
            else
                service_post("/office/get_background_one", { background_id: svg_image_id }).then(data => {
                    if (_.isNil(data)) return
                    this.prepareSVG(data)
                }).catch(_.noop)

        },
        update_style(update) {
            const to_update = update || this.style
            if (isAnyEmpty(to_update)) return
            if (isAnyEmpty(this.format_container())) return
            const svg_image_id = _.get(to_update, SVG_IMAGE_ID)
            _.forEach(to_update, (value, key) => this.format_container_dolar().css(key, value))
            if (svg_image_id === undefined) this.svg_background_settings = undefined
            else this.updateSVGBackground(to_update)
        },
        format_container_width() {
            return this.format_container_dolar().width()
        },
        format_container_height() {
            return this.format_container_dolar().height()
        },
        format_container_dolar() {
            return $(this.format_container())
        },
        format_container() {
            let f = this.$refs["format_container"]
            if (_.isArray(f)) f = _.head(f)
            return f
        },
        formatObjectFocusin(is_slide) {
            if (this.is_run) return
            let format_object = {
                obj: this,
                getContainer: this.format_container,
                getStyleValues: this.getStyleValues,
                getBackground: this.getBackground,
                borderChanged: this.borderChanged,
                backgroundChanged: this.backgroundChanged,
                sizeChanged: this.sizeChanged,
                styleChanged: this.styleChanged,
            }
            if (is_slide === true) officeState.state.current_format_slide_object = format_object
            else officeState.state.current_format_object = format_object
            this.$emit("focusin:object")
        },
        getStyleValues(propertyNames) {
            if (isAnyEmpty(this.format_container())) return
            return this.format_container_dolar().css(propertyNames)
        },
        styleChanged(style) {
            this.setStyle(style)
        },
        switchChanged(switchConfig) {
            this.setSwitchConfig(switchConfig)
        },
        sizeChanged(width_height) {
            this.setStyle(width_height)
        },
        borderChanged(classes) {
            this.classes = _.concat(this.classes, classes || [])
        },
        setStyle(obj) {
            this.style = _.merge({}, this.style, obj)
        },
        setSwitchConfig(config) {
            this.switch_config = _.merge({}, this.switch_config, config)
        },
        backgroundChanged({ background, full_apply }) {
            this.setStyle(background)
            if (full_apply === true) officeState.state.current_background = background
        },
        getBackground() {
            if (_.isEmpty(this.style)) return officeState.state.current_background
            return _.mapValues(default_background, (value, key) => _.get(this.style, key) || "unset")
        },
        toggleMaxContainer() {
            if (this.item_is_maxed) this.style = this.old_style
            else {
                _.set(this._config, "old_style", _.cloneDeep(this.style))
                this.style = _.merge(this.style, { width: "100%", height: "100%", top: 0, left: 0 })
            }
        }
    },
    computed: {
        focused_object_key: { get() { return officeState.state.focused_object_key }, set(val) { officeState.state.focused_object_key = val } },
        multi_select_object_keys: { get() { return officeState.state.multi_select_object_keys }, set(val) { officeState.state.multi_select_object_keys = val } },
        is_run() { return this.mode === MODE_RUN },
        style_width() { return _.get(this.style, "width") },
        style_height() { return _.get(this.style, "height") },
        item_is_maxed() {
            return _.get(this.style, ["width"]) === "100%" && _.get(this.style, ["height"]) === "100%"
        },
        switch_config: {
            get() { return get_default(this._config, "switch", {}) },
            set(val) {
                _.set(this._config, "switch", val)
                this.$emit("update:config", this._config)
            },
        },
        old_style() {
            return _.get(this._config, "old_style") || this.style
        },
        style: {
            get() { return get_default(this._config, "style", {}) },
            set(val) {
                _.set(this._config, "style", val)
                this.$emit("update:config", this._config)
            },
        },
        classes: {
            get() { return get_default(this._config, "classes", []) },
            set(val) {
                _.set(this._config, "classes", val)
                this.$emit("update:config", this._config)
            },
        },
        format_classes() {
            let cs = []
            if (this.is_selected) cs.push("selected_item")
            if (this.is_focused) cs.push("focused_item")
            return _.concat(this.classes, cs)
        }
    },
    mounted() {
        this.format_container_dolar().on("focusin", () => this.$emit("focusin:object"))
        this.update_style()
    }
}