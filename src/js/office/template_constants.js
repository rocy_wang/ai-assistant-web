import _ from "lodash"
import { officeState } from "./office_state"
import { randomInt } from "./svg_background"
import { randomOne } from "./auto_ppt_layout"
export const TITLE_HOLDER = "标题内容"
export const SUB_TITLE_HOLDER = "副标题内容"
export const TEXT_HOLDER = `人工智能（Artificial Intelligence），英文缩写为AI。 它是研究、开发用于模拟、延伸和扩展人的智能的理论、方法、技术及应用系统的一门新的技术科学。人工智能是新一轮科技革命和产业变革的重要驱动力量。
人工智能是智能学科重要的组成部分，它企图了解智能的实质，并生产出一种新的能以人类智能相似的方式做出反应的智能机器，该领域的研究包括机器人、语言识别、图像识别、自然语言处理和专家系统等。人工智能从诞生以来，理论和技术日益成熟，应用领域也不断扩大，可以设想，未来人工智能带来的科技产品，将会是人类智慧的“容器”。人工智能可以对人的意识、思维的信息过程的模拟。人工智能不是人的智能，但能像人那样思考、也可能超过人的智能。
人工智能是一门极富挑战性的科学，从事这项工作的人必须懂得计算机知识，心理学和哲学等。人工智能是包括十分广泛的科学，它由不同的领域组成，如机器学习，计算机视觉等等。总的来说，人工智能研究的一个主要目标是使机器能够胜任一些通常需要人类智能才能完成的复杂工作。但不同的时代、不同的人对这种“复杂工作”的理解是不同的。`
export const CONTENT_HOLDER = "输入目录内容"
export const CONTENT_DETAIL_HOLDER = "输入具体内容"

export const MARGIN = 150
export const TITLE_SIZE = 28
export const SUB_TITLE_SIZE = 16
export const TEXT_SIZE = 14
export const REPORT_HEIGHT = 60
export const REPORTER_WIDTH = 130
export const REPORTER = "汇报人姓名"
export const REPORT_SIZE = 14
export const IMAGE_URL_HOLDER_BASE = "/img/office/templates"
const IMAGE_URL_HOLDER_COUNT = 20
export function IMAGE_URL_QUOTE() {
    return randomOne([
        "/img/office/bootstrap-icons-1.11.2/quote.svg",
        "/img/office/bootstrap-icons-1.11.2/blockquote-left.svg",
        "/img/office/bootstrap-icons-1.11.2/chat-quote-fill.svg",
        "/img/office/bootstrap-icons-1.11.2/chat-right-quote-fill.svg",
        "/img/office/bootstrap-icons-1.11.2/chat-right-quote.svg",
    ])
}
export function IMAGE_URL_HOLDER() {
    const r = randomInt(IMAGE_URL_HOLDER_COUNT) + 1
    return `${IMAGE_URL_HOLDER_BASE}/${r}.svg`
}
export function TITLE_HOLDER_HTML() {
    return `<h3 class="w-100 text-center">${TITLE_HOLDER}</h3>`
}
export function obj2style(style) {
    style = style || {}
    return _.join(_.map(style, (value, key) => `"${key}":"${value}"`), ";")
}
export function imageDiv(classes, style) {
    return `<div class="k position_size image ${classes || ''}" style="min-width:100px;">
    <img style="" src="/img/office/picture_holder.svg"/>
</div>`
}
export function richDiv(text, size, classes, style) {
    size = size ? `font-size:${size}px;` : ``
    return `<div class="k position_size rich text-break word-break overflow-auto ${classes || ''}" style="white-space: break-spaces;${size};${style || "max-height:150px"}">${text}</div>`
}

export function titleDiv(classes) {
    return richDiv(TITLE_HOLDER, TITLE_SIZE, `title ${classes}`)
}

export function subTitleDiv(classes) {
    return richDiv(SUB_TITLE_HOLDER, SUB_TITLE_SIZE, `sub_title ${classes}`)
}

export function textDiv(classes, style) {
    return richDiv(TEXT_HOLDER, TEXT_SIZE, `text ${classes}`, style)
}

export function wrapDiv(classes, ...children) {
    return `<div class='d-flex ${classes || ""}'>
            ${_.join(children, "\n")}
    </div>`
}
export function containerSize() {
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    return { width: slide_width - MARGIN, height: slide_height - MARGIN }
}
export function containerWrap(is_column, justify_content, align_items, inner) {
    const flex = _.isBoolean(is_column) ? (is_column ? 'flex-column' : 'flex-row') : is_column
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    return `<div class="border border-primary d-flex  justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="${flex} border border-primary d-flex ${justify_content || 'justify-content-between'} ${align_items || 'align-items-center'}"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                ${inner}
            </div>
        </div>`;
}

