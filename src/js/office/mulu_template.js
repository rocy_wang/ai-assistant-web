import { officeState } from '@/js/office/office_state';
import { content_title_size, content_text_size } from './auto_ppt_layout';
import { CONTENT_HOLDER, CONTENT_DETAIL_HOLDER } from '@/js/office/template_constants.js'
export const CONTENT_LEFT = "content_left"
export const CONTENT_TOP = "content_top"

function divWrapContentTitle(classes, content_holder) {
    return `<div class="k position_size content_title ${classes || ""}}" style='min-width:200px!important;height:55px!important;font-size:${content_title_size}px'>${content_holder || CONTENT_HOLDER}</div>`
}
function divWrapContent(classes) {
    return `<div class="k position_size content me-3 ${classes || ""}" style="min-width:200px!important;height:80px!important;font-size:48px">Content</div>`
}
function divWrapContentDetail(classes) {
    return ` <div class="k position_size content_text ${classes || ""}}" style='min-width:200px!important;height:50px!important;font-size:${content_text_size}px'>${CONTENT_DETAIL_HOLDER}</div>`
}
function divWrapContentImage(classes) {
    return `<img class="k position_size image me-3 ${classes || ""}" src='/img/office/content.svg' style="width:32px;height:32px;"/>`
}
export function generateMuLuHtml(count, layout) {
    const length = _.isArray(count) ? count.length : count
    function getTitle(idx) { return _.isArray(count) ? count[idx] : CONTENT_HOLDER }
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    const margin = 150;
    switch (layout) {
        case CONTENT_LEFT:
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex justify-content-between align-items-center"
                style="width:${slide_width - margin}px;height: ${slide_height - margin}px;">
                ${divWrapContent()}
                <div class="d-flex flex-column flex-wrap h-100 flex-grow-1">
                    ${_.join(_.map(_.range(0, length), idx => {
                return `<div class="d-flex justify-content-start align-items-center mb-3" style="width:fit-content">
                            ${divWrapContentImage(`k_${idx}`)}
                            <div class="d-flex flex-column ">
                                ${divWrapContentTitle(`k_${idx}`, getTitle(idx))}
                                ${divWrapContentDetail(`k_${idx}`)}
                            </div>
                        </div>`;
            }), "")}
                </div>
            </div>
        </div>`;
        case "content_top":
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column justify-content-between align-items-center"
                style="width:${slide_width - margin}px;height: ${slide_height - margin}px;">
                ${divWrapContent()}
                <div class="d-flex justify-content-between flex-wrap w-100">
                    ${_.join(_.map(_.range(0, length), idx => {
                return `<div class="d-flex justify-content-start align-items-center mb-3" style="width:fit-content">
                            ${divWrapContentImage(`k_${idx}`)}
                            <div class="d-flex flex-column ">
                                ${divWrapContentTitle(`k_${idx}`, getTitle(idx))}
                                ${divWrapContentDetail(`k_${idx}`)}
                            </div>
                        </div>`;
            }), "")}
                </div>
            </div>
        </div>`;
        case "titles_step":
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column justify-content-between align-items-center"
                style="width:${slide_width - margin}px;height: ${slide_height - margin}px;">
                ${divWrapContent()}
                <div class="d-flex flex-column flex-wrap w-100 flex-grow-1" style='min-height:200px;'>
                    ${_.join(_.map(_.range(0, length), idx => {
                return `<div class="d-flex justify-content-start align-items-center mb-3" style="margin-left:${20 * idx}px;width:fit-content">
                            ${divWrapContentImage(`k_${idx}`)}
                            <div class="d-flex flex-column ">
                                ${divWrapContentTitle(`k_${idx}`, getTitle(idx))}
                                ${divWrapContentDetail(`k_${idx}`)}
                            </div>
                        </div>`;
            }), "")}
                </div>
            </div>
        </div>`;
        case "titles_step_reverse":
            return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column justify-content-between align-items-center"
                style="width:${slide_width - margin}px;height: ${slide_height - margin}px;">
                ${divWrapContent()}
                <div class="d-flex flex-column-reverse flex-wrap w-100 flex-grow-1" style='min-height:200px;'>
                    ${_.join(_.map(_.range(0, length), idx => {
                return `<div class="d-flex justify-content-start align-items-center mb-3" style="margin-left:${20 * idx}px;width:fit-content">
                            ${divWrapContentImage(`k_${idx}`)}
                            <div class="d-flex flex-column ">
                                ${divWrapContentTitle(`k_${idx}`, getTitle(idx))}
                                ${divWrapContentDetail(`k_${idx}`)}
                            </div>
                        </div>`;
            }), "")}
                </div>
            </div>
        </div>`;

    }

}
