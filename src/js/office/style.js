import _ from "lodash"
import { rgb2hext } from '@/js/color_util'
const SEPARATOR = " "
export function construct(style) {
    if (_.isNil(style)) return style
    const { width, height } = getWidthHeight(style)
    return _.mapValues(style, (value, key) => {
        switch (key) {
            case "transform-origin": return construct_transform_origin(value, width, height)
            case "box-shadow": return construct_box_shadow(value)
            case "transform": return construct_transform(value)
            default: return extractNumber(value)
        }
    })
}
function getWidthHeight(style) {
    return { width: extractNumber(style["width"]), height: extractNumber(style["height"]) }
}
const props_px = ["left", "top", "right", "bottom", "width", "height"]
const funcs_deg = ["rotateX", "rotateY", "rotateZ"]
export function destruct(style) {
    return _.mapValues(style, (value, key) => {
        if (props_px.includes(key)) return `${value}px`
        switch (key) {
            case "transform-origin": return destruct_transform_origin(value)
            case "box-shadow": return destruct_box_shadow(value)
            case "transform": return destruct_transform(value)
            default: return value
        }
    })
}
function destruct_transform(value) {
    return _.join(_.map(value, (v, k) => {
        let unit = ""
        if (funcs_deg.includes(k)) unit = "deg"
        return `${k}(${_.join(_.map(v, item => `${item}${unit}`), ",")})`
    }), SEPARATOR)
}
function construct_transform(value) {
    const arr = _.filter(text2arr(value), _.isObject)
    return _.fromPairs(_.map(arr, item => [[item.func_name], _.map(item.arguments, extractNumber)]))
}
function construct_box_shadow(value) {
    const arr = text2arr(value)
    if (arr.length === 1) return { "h-shadow": 0, "v-shadow": 0, "blur": 0, "spread": 0, "color": "black", "inset": false }
    else {
        const [arr1, arr2] = _.partition(arr, is_length)
        const [h_shadow, v_shadow, blur, spread] = arr1
        const [arr3, arr4] = _.partition(arr2, item => item === "inset")
        return {
            "h-shadow": h_shadow || 0,
            "v-shadow": v_shadow || 0,
            blur: blur || 0,
            spread: spread || 0,
            color: rgb2hext(_.head(arr4)) || "black",
            inset: arr3.length === 1
        }
    }
}
function destruct_box_shadow(value) {
    value = _.mapValues(value, (v, k) => ["color", "inset"].includes(k) ? v : `${v}px`)
    value["inset"] = value["inset"] === true ? "inset" : ""
    return _.join(_.values(value), SEPARATOR)
}
function construct_transform_origin(value, width, height) {
    if (_.trim("value") === "none") value = "0% 0% 0"
    let [x_axis, y_axis, z_axis] = text2arr(value)
    x_axis = to_percent(x_axis, width) || 0
    y_axis = to_percent(y_axis, height) || 0
    z_axis = trim_unit(z_axis) || 0
    return { "x-axis": x_axis, "y-axis": y_axis, "z-axis": z_axis }
}
function trim_unit(text) {
    if (_.isNil(text)) return text
    else if (_.isNumber(text)) return text
    else if (_.isString(text)) return extractNumber(text)
    return text
}
function destruct_transform_origin(value) {
    return `${_.get(value, "x-axis")}% ${_.get(value, "y-axis")}% ${_.get(value, "z-axis")}`
}
function text2arr(text) {
    const reg1 = /[\w.-]+\(.+\)/
    return _.map(text.match(/[\w.-]+\(.+\)|[\w.-]+/g), item => {
        if (reg1.test(item)) {
            const matches = item.match(/([\w.-]+)/g)
            return {
                func_name: _.first(matches),
                arguments: _.tail(matches)
            }
        } else return item
    })
}
export function extractNumber(text) {
    if (text === undefined) return undefined
    if (_.isNumber(text)) return text
    if (_.isNil(text)) return undefined
    const arr = text.match(/\d+/)
    return _.toInteger(_.first(arr))
}
function to_percent(value, width) {
    if (_.isNil(value)) return value
    if (_.endsWith(value, "px")) value = _.toNumber(extractNumber(value) / width)
    else if (_.endsWith(value, "%")) value = extractNumber(value)
    else value = _.toInteger(value)
    return value
}
function is_length(value) {
    return _.find(["px", "%"], item => _.trim(value).endsWith(item)) !== undefined
}

export function far_point(arr) {
    if (_.isNil(arr)) return undefined
    if (arr.length < 2) return undefined
    arr = _.sortBy(arr)
    const find = _.maxBy(_.range(1, arr.length), index => arr[index] - arr[index - 1])
    const middle = _.toInteger((arr[find] + arr[find - 1]) / 2)
    return middle
}
