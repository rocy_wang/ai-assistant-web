import { createStore } from "vuex";
import VuexPersistence from 'vuex-persist'
import { MAX_TOKENS, TEMPERATURE_DEFAULT } from "@/js/constants";
import { A4_SIZE, B5_SIZE, ITEM_TYPE_RICH, PAGE_A4, PAGE_B5 } from "./word";
import _ from "lodash";
import { getIndexFromSlideObjectId } from "../ppt";

export function updateStyle(style, radial) {
    const styles = ['姹紫', '嫣红', '草绿', '天蓝', '炫彩', '透明']
    style = style || styles[_.toInteger(Math.random() * styles.length)]
    $("#main_style").remove()
    let name = `${style}${radial === true ? '_射线' : ''}`
    if (style === "透明") name = style
    $("<link></link>").prop("rel", "stylesheet").prop("href", `/src/styles/main_style/${name}.css`).prop("id", "main_style").appendTo($("head"))
}

let increased_index = 0

export function nextIncreasedIndex() {
    return increased_index++
}

const vuexLocal = new VuexPersistence({
    storage: window.sessionStorage
})

export function get_screen_ratio() { return window.screen.height / window.screen.width }
export const SIZE_RATIO_RANGES = ["当前页面", "当前及以前", "当前及以后", "所有页面"]
export const SLIDE_RATIO_HW_34 = 3 / 4
export const SLIDE_RATIO_HW_916 = 9 / 16
export const RATIOS = [{
    ratio: SLIDE_RATIO_HW_34,
    image: "4-3",
    name: "标准 4:3"
}, {
    ratio: SLIDE_RATIO_HW_916,
    image: "16-9",
    name: "宽屏 16:9"
}, {
    ratio: get_screen_ratio(),
    image: "monitor",
    name: "显示器比例"
},]
export const screen_height = window.screen.height
const placements = {
    "left": "right",
    "right": "left",
    "top": "bottom",
    "bottom": "top",
}
export const SHOW_RATIO_MIN = 30
export const SHOW_RATIO_MAX = 300
export const menu_positions = { 'top': '上', 'bottom': '下', 'left': '左', 'right': '右' }
export const menu_position_names = _.invert(menu_positions)
export const officeStateCache = createStore({
    state() {
        return {
            current_object_is_title: false,
            current_object_type: undefined,
            dynamic_name: undefined,
            is_slide_view: true,
            prev_slide_size: undefined,

        }
    },
    getters: {
        isRich: (state) => state.current_object_type === ITEM_TYPE_RICH,
    },
    mutations: {
        setSlideView(state, is_slide_view) {
            state.is_slide_view = is_slide_view === true
        },
        setDynamicName(state, dynamic_name) {
            state.dynamic_name = dynamic_name
        },
        setCurrentObjectType(state, current_object_type) {
            state.current_object_type = current_object_type
        },
    }
})
export const officeState = createStore({
    state() {
        return {
            current_slide_zoom: 100,
            is_slide_view_travel: false,
            show_note: true,
            show_annotation: false,
            is_dark: false,
            screen_width: window.screen.width,
            screen_height: window.screen.height,
            page_width: undefined,
            page_height: undefined,
            slide_width_specified: undefined,
            slide_height_specified: undefined,
            slide_ratio_hw_specified: undefined,

            dragging_info: {
                drag_index: undefined
            },
            multi_select_object_keys: [],
            focused_object_key: undefined,
            maxized_item: undefined,
            play_ppt: undefined,
            SVG_AUTO_CACHES: undefined,
            SVG_AUTO_CACHE_ID: -1,
            model_config: {
                my_title: undefined,
                model_id: undefined,
                model_name: undefined,
                temperature: TEMPERATURE_DEFAULT,
                max_tokens: MAX_TOKENS,
            },
            model_config_visible: false,
            image_selection: {
                change_image_callback: undefined,
                selected_images: [],
                selected_images_multiple: false,
            },
            menu_position: "right",
            image_selection_visible: false,
            is_run_slide: false,
            current_title_object: undefined,
            current_range: undefined,
            current_editor: undefined,
            current_format_object: undefined,
            current_format_slide_object: undefined,
            current_ai_generation: undefined,
            current_background: undefined,
            current_slide_object: undefined,
            is_ppt: undefined
        }
    },
    getters: {
        current_model_id: (state) => _.get(state.model_config, "model_id"),
        size: (state, getters) => ({
            screen_width: state.screen_width,
            screen_height: state.screen_height,
            page_width: state.page_width,
            page_height: state.page_height,
            slide_width_specified: state.slide_width_specified,
            slide_height_specified: state.slide_height_specified,
            slide_ratio_hw_specified: state.slide_ratio_hw_specified,
            get_screen_ratio: getters.get_screen_ratio,
            current_slide_size: getters.current_slide_size,
            screen_ratio: getters.screen_ratio,
            slide_width: getters.slide_width,
            slide_height: getters.slide_height
        }),
        get_screen_ratio: (state) => state.screen_height / state.screen_width,
        current_slide_size_style: (state, getters) => ({ width: `${getters.slide_width}px`, height: `${getters.slide_height}px` }),
        current_slide_size: (state, getters) => ({ width: getters.slide_width, height: getters.slide_height }),
        screen_ratio: (state) => state.screen_height / state.screen_width,
        current_slide_selcted_object_count: (state) => (state.multi_select_object_keys || []).length,
        current_slide_selcted_object_index_list: (state) => _.map(state.multi_select_object_keys || [], getIndexFromSlideObjectId),
        is_column: (state) => ['left', 'right'].includes(state.menu_position),
        placement: (state) => placements[state.menu_position],
        slide_width_ratio: (state, getters) => ratio => (getters.slide_height / (ratio || SLIDE_RATIO_HW_34)),
        slide_width: (state, getters) => state.slide_width_specified || (getters.slide_height / (state.slide_ratio_hw_specified || SLIDE_RATIO_HW_34)),
        slide_height: (state) => Math.max(300, state.slide_height_specified || (state.page_height - 16)),
        max_ratio: (state, getters) => {
            const width_ratio = state.screen_width / getters.slide_width
            const height_ratio = state.screen_height / getters.slide_height
            return { width_ratio, height_ratio }
        },
        current_format_obj: (state) => state.current_format_object || state.current_format_slide_object,
    },
    mutations: {
        unsetSize(state) {
            state.slide_width_specified = undefined
            state.slide_height_specified = undefined
            state.slide_ratio_hw_specified = undefined
        },
        setCurrentSlideZoom(state, zoom) {
            state.current_slide_zoom = zoom
        },
        setTravel(state, travel) {
            state.is_slide_view_travel = travel
        },
        setShowNote(state, show_note) {
            state.show_note = show_note === true
        },
        setShowAnnotation(state, show_annotation) {
            state.show_annotation = show_annotation === true
        },
        setTheme(state, is_dark) {
            state.is_dark = is_dark === true
        },
        updateSlideSizeRatio(state, ratio) {
            state.slide_ratio_hw_specified = ratio
        },
        updateSlideSize(state, size) {
            if (_.isBoolean(size)) state.slide_ratio_hw_specified = (size === true ? SLIDE_RATIO_HW_34 : SLIDE_RATIO_HW_916)
            else if (_.isString(size)) {
                switch (size) {
                    case PAGE_A4:
                        state.slide_width_specified = A4_SIZE.width
                        state.slide_height_specified = A4_SIZE.height
                        break
                    case PAGE_B5:
                        state.slide_width_specified = B5_SIZE.width
                        state.slide_height_specified = B5_SIZE.height
                        break
                }
            }
        },
        updateRunState(state, is_run) {
            state.is_run_slide = is_run
        },
        updateImageVisible(state, val) {
            state.image_selection_visible = val
        },
        showImageSelection(state, image_selection) {
            state.image_selection_visible = true
            state.image_selection = _.merge({
                selected_images: [],
                selected_images_multiple: false,
            }, image_selection)
        },
        confirmImageSelection(state) {
            state.image_selection_visible = false
            state.image_selection.change_image_callback(state.image_selection.selected_images)
        },
    },
    plugins: [vuexLocal.plugin]
})