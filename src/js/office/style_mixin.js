
import _ from "lodash"
import { officeState } from '@/js/office/office_state';
import { construct, destruct, extractNumber } from '@/js/office/style';
export const styleMixin = {
    emits: ["changed"],
    props: {
        prop_old_style: {
            type: Object
        }
    },
    watch: {
        prop_old_style: {
            handler(val) {
                this.new_style = _.isNil(val) ? {} : this.construct(_.cloneDeep(val))
            },
            immediate: true, deep: true
        },
        new_style: {
            handler(val) {
                this.update_style(this.destruct(val))
            }, immediate: true, deep: true
        }
    },
    data() {
        return {
            new_style: {}
        }
    },
    mounted() {
        this.update_style()
    },
    computed: {
        slide_width: () => officeState.getters.slide_width,
        slide_height: () => officeState.getters.slide_height,
    },
    methods: {
        construct, destruct, extractNumber,
        items(prop) { return _.reject(_.split(prop || "", " "), item => _.trim(item).length === 0) },

        setStyleValue(path, value) {
            _.set(this.new_style, path, value)
        },
        getStyleValue(path, default_value) {
            return _.get(this.new_style, path) || default_value
        },
        apply() {
            this.$emit("changed", this.dStyle(this.new_style))
        },
        format_container() { return this.$refs.format_container },
        dStyle(update) {
            return _.omit(this.destruct(update), ["width", "height"])
        },
        update_style(update) {
            const to_update = _.omit(update || this.destruct(this.new_style), ["width", "height"])
            if (_.isNil(to_update) || _.isEmpty(to_update)) return
            if (_.isNil(this.format_container())) return
            const format_object = $(this.format_container())
            _.forEach(to_update, (value, key) => format_object.css(key, value))
        }
    },
}