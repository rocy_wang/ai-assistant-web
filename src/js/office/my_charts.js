import _ from "lodash";
import { flare } from "./flare";
import { gaugeData, sunburst_data } from "../data";

const labelRight = {
    position: 'right'
};
// prettier-ignore
const hours = [
    '00H', '01H', '02H', '03H', '04H', '05H', '06H',
    '07H', '08H', '09H', '10H', '11H',
    '12H', '13H', '14H', '15H', '16H', '17H',
    '18H', '19H', '20H', '21H', '22H', '23H'
];
// prettier-ignore
const days = [
    'Saturday', 'Friday', 'Thursday',
    'Wednesday', 'Tuesday', 'Monday', 'Sunday'
];

const data = [
    [
        [28604, 77, 17096869, 'Australia', 1990],
        [31163, 77.4, 27662440, 'Canada', 1990],
        [1516, 68, 1154605773, 'China', 1990],
        [13670, 74.7, 10582082, 'Cuba', 1990],
        [28599, 75, 4986705, 'Finland', 1990],
        [29476, 77.1, 56943299, 'France', 1990],
        [31476, 75.4, 78958237, 'Germany', 1990],
        [28666, 78.1, 254830, 'Iceland', 1990],
        [1777, 57.7, 870601776, 'India', 1990],
        [29550, 79.1, 122249285, 'Japan', 1990],
        [2076, 67.9, 20194354, 'North Korea', 1990],
        [12087, 72, 42972254, 'South Korea', 1990],
        [24021, 75.4, 3397534, 'New Zealand', 1990],
        [43296, 76.8, 4240375, 'Norway', 1990],
        [10088, 70.8, 38195258, 'Poland', 1990],
        [19349, 69.6, 147568552, 'Russia', 1990],
        [10670, 67.3, 53994605, 'Turkey', 1990],
        [26424, 75.7, 57110117, 'United Kingdom', 1990],
        [37062, 75.4, 252847810, 'United States', 1990]
    ],
    [
        [44056, 81.8, 23968973, 'Australia', 2015],
        [43294, 81.7, 35939927, 'Canada', 2015],
        [13334, 76.9, 1376048943, 'China', 2015],
        [21291, 78.5, 11389562, 'Cuba', 2015],
        [38923, 80.8, 5503457, 'Finland', 2015],
        [37599, 81.9, 64395345, 'France', 2015],
        [44053, 81.1, 80688545, 'Germany', 2015],
        [42182, 82.8, 329425, 'Iceland', 2015],
        [5903, 66.8, 1311050527, 'India', 2015],
        [36162, 83.5, 126573481, 'Japan', 2015],
        [1390, 71.4, 25155317, 'North Korea', 2015],
        [34644, 80.7, 50293439, 'South Korea', 2015],
        [34186, 80.6, 4528526, 'New Zealand', 2015],
        [64304, 81.6, 5210967, 'Norway', 2015],
        [24787, 77.3, 38611794, 'Poland', 2015],
        [23038, 73.13, 143456918, 'Russia', 2015],
        [19360, 76.5, 78665830, 'Turkey', 2015],
        [38225, 81.4, 64715810, 'United Kingdom', 2015],
        [53354, 79.1, 321773631, 'United States', 2015]
    ]
];

const heatmap_data = [[0, 0, 5], [0, 1, 1], [0, 2, 0], [0, 3, 0], [0, 4, 0], [0, 5, 0], [0, 6, 0], [0, 7, 0], [0, 8, 0], [0, 9, 0], [0, 10, 0], [0, 11, 2], [0, 12, 4], [0, 13, 1], [0, 14, 1], [0, 15, 3], [0, 16, 4], [0, 17, 6], [0, 18, 4], [0, 19, 4], [0, 20, 3], [0, 21, 3], [0, 22, 2], [0, 23, 5], [1, 0, 7], [1, 1, 0], [1, 2, 0], [1, 3, 0], [1, 4, 0], [1, 5, 0], [1, 6, 0], [1, 7, 0], [1, 8, 0], [1, 9, 0], [1, 10, 5], [1, 11, 2], [1, 12, 2], [1, 13, 6], [1, 14, 9], [1, 15, 11], [1, 16, 6], [1, 17, 7], [1, 18, 8], [1, 19, 12], [1, 20, 5], [1, 21, 5], [1, 22, 7], [1, 23, 2], [2, 0, 1], [2, 1, 1], [2, 2, 0], [2, 3, 0], [2, 4, 0], [2, 5, 0], [2, 6, 0], [2, 7, 0], [2, 8, 0], [2, 9, 0], [2, 10, 3], [2, 11, 2], [2, 12, 1], [2, 13, 9], [2, 14, 8], [2, 15, 10], [2, 16, 6], [2, 17, 5], [2, 18, 5], [2, 19, 5], [2, 20, 7], [2, 21, 4], [2, 22, 2], [2, 23, 4], [3, 0, 7], [3, 1, 3], [3, 2, 0], [3, 3, 0], [3, 4, 0], [3, 5, 0], [3, 6, 0], [3, 7, 0], [3, 8, 1], [3, 9, 0], [3, 10, 5], [3, 11, 4], [3, 12, 7], [3, 13, 14], [3, 14, 13], [3, 15, 12], [3, 16, 9], [3, 17, 5], [3, 18, 5], [3, 19, 10], [3, 20, 6], [3, 21, 4], [3, 22, 4], [3, 23, 1], [4, 0, 1], [4, 1, 3], [4, 2, 0], [4, 3, 0], [4, 4, 0], [4, 5, 1], [4, 6, 0], [4, 7, 0], [4, 8, 0], [4, 9, 2], [4, 10, 4], [4, 11, 4], [4, 12, 2], [4, 13, 4], [4, 14, 4], [4, 15, 14], [4, 16, 12], [4, 17, 1], [4, 18, 8], [4, 19, 5], [4, 20, 3], [4, 21, 7], [4, 22, 3], [4, 23, 0], [5, 0, 2], [5, 1, 1], [5, 2, 0], [5, 3, 3], [5, 4, 0], [5, 5, 0], [5, 6, 0], [5, 7, 0], [5, 8, 2], [5, 9, 0], [5, 10, 4], [5, 11, 1], [5, 12, 5], [5, 13, 10], [5, 14, 5], [5, 15, 7], [5, 16, 11], [5, 17, 6], [5, 18, 0], [5, 19, 5], [5, 20, 3], [5, 21, 4], [5, 22, 2], [5, 23, 0], [6, 0, 1], [6, 1, 0], [6, 2, 0], [6, 3, 0], [6, 4, 0], [6, 5, 0], [6, 6, 0], [6, 7, 0], [6, 8, 0], [6, 9, 0], [6, 10, 1], [6, 11, 0], [6, 12, 2], [6, 13, 1], [6, 14, 3], [6, 15, 4], [6, 16, 0], [6, 17, 0], [6, 18, 0], [6, 19, 0], [6, 20, 1], [6, 21, 2], [6, 22, 2], [6, 23, 6]]
    .map(function (item) {
        return [hours[item[1]], days[item[0]], item[2] || '-'];
    });
export const DEFAULT_LINES = [{
    name: "基础折线图",
    option: { xAxis: { type: 'category', data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }, yAxis: { type: 'value' }, series: [{ data: [150, 230, 224, 218, 135, 147, 260], type: 'line' }] }
},
{
    name: "基础平滑折线图",
    option: { xAxis: { type: 'category', data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }, yAxis: { type: 'value' }, series: [{ data: [820, 932, 901, 934, 1290, 1330, 1320], type: 'line', smooth: true }] }
}, {
    name: "基础面积图",
    option: { xAxis: { type: 'category', boundaryGap: false, data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }, yAxis: { type: 'value' }, series: [{ data: [820, 932, 901, 934, 1290, 1330, 1320], type: 'line', areaStyle: {} }] }
}, {
    name: "折线图堆叠",
    option: { title: { text: 'Stacked Line' }, tooltip: { trigger: 'axis' }, legend: { data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine'] }, grid: { left: '3%', right: '4%', bottom: '3%', containLabel: true }, toolbox: { feature: { saveAsImage: {} } }, xAxis: { type: 'category', boundaryGap: false, data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }, yAxis: { type: 'value' }, series: [{ name: 'Email', type: 'line', stack: 'Total', data: [120, 132, 101, 134, 90, 230, 210] }, { name: 'Union Ads', type: 'line', stack: 'Total', data: [220, 182, 191, 234, 290, 330, 310] }, { name: 'Video Ads', type: 'line', stack: 'Total', data: [150, 232, 201, 154, 190, 330, 410] }, { name: 'Direct', type: 'line', stack: 'Total', data: [320, 332, 301, 334, 390, 330, 320] }, { name: 'Search Engine', type: 'line', stack: 'Total', data: [820, 932, 901, 934, 1290, 1330, 1320] }] }
}, {
    name: "渐变堆叠面积图",
    option: { color: ['#80FFA5', '#00DDFF', '#37A2FF', '#FF0087', '#FFBF00'], tooltip: { trigger: 'axis', axisPointer: { type: 'cross', label: { backgroundColor: '#6a7985' } } }, legend: { data: ['Line 1', 'Line 2', 'Line 3', 'Line 4', 'Line 5'] }, toolbox: { feature: { saveAsImage: {} } }, grid: { left: '3%', right: '4%', bottom: '3%', containLabel: true }, xAxis: [{ type: 'category', boundaryGap: false, data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }], yAxis: [{ type: 'value' }], series: [{ name: 'Line 1', type: 'line', stack: 'Total', smooth: true, lineStyle: { width: 0 }, showSymbol: false, areaStyle: { opacity: 0.8, color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ offset: 0, color: 'rgb(128, 255, 165)' }, { offset: 1, color: 'rgb(1, 191, 236)' }]) }, emphasis: { focus: 'series' }, data: [140, 232, 101, 264, 90, 340, 250] }, { name: 'Line 2', type: 'line', stack: 'Total', smooth: true, lineStyle: { width: 0 }, showSymbol: false, areaStyle: { opacity: 0.8, color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ offset: 0, color: 'rgb(0, 221, 255)' }, { offset: 1, color: 'rgb(77, 119, 255)' }]) }, emphasis: { focus: 'series' }, data: [120, 282, 111, 234, 220, 340, 310] }, { name: 'Line 3', type: 'line', stack: 'Total', smooth: true, lineStyle: { width: 0 }, showSymbol: false, areaStyle: { opacity: 0.8, color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ offset: 0, color: 'rgb(55, 162, 255)' }, { offset: 1, color: 'rgb(116, 21, 219)' }]) }, emphasis: { focus: 'series' }, data: [320, 132, 201, 334, 190, 130, 220] }, { name: 'Line 4', type: 'line', stack: 'Total', smooth: true, lineStyle: { width: 0 }, showSymbol: false, areaStyle: { opacity: 0.8, color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ offset: 0, color: 'rgb(255, 0, 135)' }, { offset: 1, color: 'rgb(135, 0, 157)' }]) }, emphasis: { focus: 'series' }, data: [220, 402, 231, 134, 190, 230, 120] }, { name: 'Line 5', type: 'line', stack: 'Total', smooth: true, lineStyle: { width: 0 }, showSymbol: false, label: { show: true, position: 'top' }, areaStyle: { opacity: 0.8, color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ offset: 0, color: 'rgb(255, 191, 0)' }, { offset: 1, color: 'rgb(224, 62, 76)' }]) }, emphasis: { focus: 'series' }, data: [220, 302, 181, 234, 210, 290, 150] }] }
}]
export const DEFAULT_BARS = [{
    name: "基础柱状图",
    option: { xAxis: { type: 'category', data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] }, yAxis: { type: 'value' }, series: [{ data: [120, 200, 150, 80, 70, 110, 130], type: 'bar' }] }
}, {
    name: "瀑布图",
    option: { tooltip: { trigger: 'axis', axisPointer: { type: 'shadow' }, formatter: function (params) { var tar = params[1]; return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value; } }, grid: { left: '3%', right: '4%', bottom: '3%', containLabel: true }, xAxis: { type: 'category', splitLine: { show: false }, data: ['Total', 'Rent', 'Utilities', 'Transportation', 'Meals', 'Other'] }, yAxis: { type: 'value' }, series: [{ name: 'Placeholder', type: 'bar', stack: 'Total', itemStyle: { borderColor: 'transparent', color: 'transparent' }, emphasis: { itemStyle: { borderColor: 'transparent', color: 'transparent' } }, data: [0, 1700, 1400, 1200, 300, 0] }, { name: 'Life Cost', type: 'bar', stack: 'Total', label: { show: true, position: 'inside' }, data: [2900, 1200, 300, 200, 900, 300] }] }
}, {
    name: "交错正负轴标签",
    option: { tooltip: { trigger: 'axis', axisPointer: { type: 'shadow' } }, grid: { top: 80, bottom: 30 }, xAxis: { type: 'value', position: 'top', splitLine: { lineStyle: { type: 'dashed' } } }, yAxis: { type: 'category', axisLine: { show: false }, axisLabel: { show: false }, axisTick: { show: false }, splitLine: { show: false }, data: ['ten', 'nine', 'eight', 'seven', 'six', 'five', 'four', 'three', 'two', 'one'] }, series: [{ name: 'Cost', type: 'bar', stack: 'Total', label: { show: true, formatter: '{b}' }, data: [{ value: -0.07, label: labelRight }, { value: -0.09, label: labelRight }, 0.2, 0.44, { value: -0.23, label: labelRight }, 0.08, { value: -0.17, label: labelRight }, 0.47, { value: -0.36, label: labelRight }, 0.18] }] }
}, {
    name: "数据比较柱状图",
    option: { legend: {}, tooltip: {}, dataset: { source: [['product', '2015', '2016', '2017'], ['Matcha Latte', 43.3, 85.8, 93.7], ['Milk Tea', 83.1, 73.4, 55.1], ['Cheese Cocoa', 86.4, 65.2, 82.5], ['Walnut Brownie', 72.4, 53.9, 39.1]] }, xAxis: { type: 'category' }, yAxis: {}, series: [{ type: 'bar' }, { type: 'bar' }, { type: 'bar' }] }
}, {
    name: "颜色数据可视化",
    option: { dataset: { source: [['score', 'amount', 'product'], [89.3, 58212, 'Matcha Latte'], [57.1, 78254, 'Milk Tea'], [74.4, 41032, 'Cheese Cocoa'], [50.1, 12755, 'Cheese Brownie'], [89.7, 20145, 'Matcha Cocoa'], [68.1, 79146, 'Tea'], [19.6, 91852, 'Orange Juice'], [10.6, 101852, 'Lemon Juice'], [32.7, 20112, 'Walnut Brownie']] }, grid: { containLabel: true }, xAxis: { name: 'amount' }, yAxis: { type: 'category' }, visualMap: { orient: 'horizontal', left: 'center', min: 10, max: 100, text: ['High Score', 'Low Score'], dimension: 0, inRange: { color: ['#65B581', '#FFCE34', '#FD665F'] } }, series: [{ type: 'bar', encode: { x: 'amount', y: 'product' } }] }
}, {
    name: "极坐标柱状图",
    option: { polar: { radius: [30, '80%'] }, radiusAxis: { max: 4 }, angleAxis: { type: 'category', data: ['a', 'b', 'c', 'd'], startAngle: 75 }, tooltip: {}, series: { type: 'bar', data: [2, 1.2, 2.4, 3.6], coordinateSystem: 'polar', label: { show: true, position: 'middle', formatter: '{b}: {c}' } }, animation: false }
}]

export const DEFAULT_PIES = [{
    name: "基础饼图",
    option: { tooltip: { trigger: 'item' }, legend: { orient: 'vertical', left: 'left' }, series: [{ name: 'Access From', type: 'pie', radius: '50%', data: [{ value: 1048, name: 'Search Engine' }, { value: 735, name: 'Direct' }, { value: 580, name: 'Email' }, { value: 484, name: 'Union Ads' }, { value: 300, name: 'Video Ads' }], emphasis: { itemStyle: { shadowBlur: 10, shadowOffsetX: 0, shadowColor: 'rgba(0, 0, 0, 0.5)' } } }] }
}, {
    name: "圆角环形图",
    option: { tooltip: { trigger: 'item' }, legend: { top: '5%', left: 'center' }, series: [{ name: 'Access From', type: 'pie', radius: ['40%', '70%'], avoidLabelOverlap: false, itemStyle: { borderRadius: 10, borderColor: '#fff', borderWidth: 2 }, label: { show: false, position: 'center' }, emphasis: { label: { show: true, fontSize: 40, fontWeight: 'bold' } }, labelLine: { show: false }, data: [{ value: 1048, name: 'Search Engine' }, { value: 735, name: 'Direct' }, { value: 580, name: 'Email' }, { value: 484, name: 'Union Ads' }, { value: 300, name: 'Video Ads' }] }] }
}, {
    name: "半环形图",
    option: { tooltip: { trigger: 'item' }, legend: { top: '5%', left: 'center', selectedMode: false }, series: [{ name: 'Access From', type: 'pie', radius: ['20%', '50%'], center: ['40%', '60%'], startAngle: 180, label: { show: true, formatter(param) { return param.name + ' (' + param.percent * 2 + '%)'; } }, data: [{ value: 1048, name: 'Search Engine' }, { value: 735, name: 'Direct' }, { value: 580, name: 'Email' }, { value: 484, name: 'Union Ads' }, { value: 300, name: 'Video Ads' }, { value: 1048 + 735 + 580 + 484 + 300, itemStyle: { color: 'none', decal: { symbol: 'none' } }, label: { show: false } }] }] }
}, {
    name: "玫瑰图",
    option: { legend: { top: 'bottom' }, toolbox: { show: true, feature: { mark: { show: true }, dataView: { show: true, readOnly: false }, restore: { show: true }, saveAsImage: { show: true } } }, series: [{ name: 'Nightingale Chart', type: 'pie', radius: [25, 125], center: ['50%', '50%'], roseType: 'area', itemStyle: { borderRadius: 8 }, data: [{ value: 40, name: 'rose 1' }, { value: 38, name: 'rose 2' }, { value: 32, name: 'rose 3' }, { value: 30, name: 'rose 4' }, { value: 28, name: 'rose 5' }, { value: 26, name: 'rose 6' }, { value: 22, name: 'rose 7' }, { value: 18, name: 'rose 8' }] }] }
}
]
export const DEFAULT_SCATTERS = [{
    name: "基础散点图",
    option: {
        xAxis: {}, yAxis: {}, series: [{
            symbolSize: 40, data:
                [[10.0, 8.04], [8.07, 6.95], [13.0, 7.58], [9.05, 8.81], [11.0, 8.33], [14.0, 7.66], [13.4, 6.81], [10.0, 6.33], [14.0, 8.96], [12.5, 6.82], [9.15, 7.2], [11.5, 7.2], [3.03, 4.23], [12.2, 7.83], [2.02, 4.47], [1.05, 3.33], [4.05, 4.96], [6.03, 7.24], [12.0, 6.26], [12.0, 8.84], [7.08, 5.82], [5.02, 5.68]],
            type: 'scatter'
        }]
    }
}, {
    name: "气泡图",
    option: { legend: { right: '10%', top: '3%', data: ['1990', '2015'] }, grid: { left: '8%', top: '10%' }, xAxis: { splitLine: { lineStyle: { type: 'dashed' } } }, yAxis: { splitLine: { lineStyle: { type: 'dashed' } }, scale: true }, series: [{ symbolSize: 40, name: '1990', data: data[0], type: 'scatter', emphasis: { focus: 'series', label: { show: false, formatter: function (param) { return param.data[3]; }, position: 'top' } }, itemStyle: { shadowBlur: 10, shadowColor: 'rgba(120, 36, 50, 0.5)', shadowOffsetY: 5, color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{ offset: 0, color: 'rgb(251, 118, 123)' }, { offset: 1, color: 'rgb(204, 46, 72)' }]) } }, { symbolSize: 50, name: '2015', data: data[1], type: 'scatter', emphasis: { focus: 'series', label: { show: false, formatter: function (param) { return param.data[3]; }, position: 'top' } }, itemStyle: { shadowBlur: 10, shadowColor: 'rgba(25, 100, 150, 0.5)', shadowOffsetY: 5, color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{ offset: 0, color: 'rgb(129, 227, 238)' }, { offset: 1, color: 'rgb(25, 183, 207)' }]) } }] }
}]
export const DEFAULT_Ks = [{
    name: "基础K线图",
    option: { xAxis: { data: ['2017-10-24', '2017-10-25', '2017-10-26', '2017-10-27'] }, yAxis: {}, series: [{ type: 'candlestick', data: [[20, 34, 10, 38], [40, 35, 30, 50], [31, 38, 33, 44], [38, 15, 5, 42]] }] }
}
]
export const DEFAULT_LADARs = [{
    name: "基础雷达图",
    option: { title: { text: 'Basic Radar Chart' }, legend: { data: ['Allocated Budget', 'Actual Spending'] }, radar: { indicator: [{ name: 'Sales', max: 6500 }, { name: 'Administration', max: 16000 }, { name: 'Information Technology', max: 30000 }, { name: 'Customer Support', max: 38000 }, { name: 'Development', max: 52000 }, { name: 'Marketing', max: 25000 }] }, series: [{ name: 'Budget vs spending', type: 'radar', data: [{ value: [4200, 3000, 20000, 35000, 50000, 18000], name: 'Allocated Budget' }, { value: [5000, 14000, 28000, 26000, 42000, 21000], name: 'Actual Spending' }] }] }
}
]
export const DEFAULT_BOXPLOTS = [{
    name: "基础盒须图",
    option: { dataset: [{ source: [[850, 740, 900, 1070, 930, 850, 950, 980, 980, 880, 1000, 980, 930, 650, 760, 810, 1000, 1000, 960, 960], [960, 940, 960, 940, 880, 800, 850, 880, 900, 840, 830, 790, 810, 880, 880, 830, 800, 790, 760, 800], [880, 880, 880, 860, 720, 720, 620, 860, 970, 950, 880, 910, 850, 870, 840, 840, 850, 840, 840, 840], [890, 810, 810, 820, 800, 770, 760, 740, 750, 760, 910, 920, 890, 860, 880, 720, 840, 850, 850, 780], [890, 840, 780, 810, 760, 810, 790, 810, 820, 850, 870, 870, 810, 740, 810, 940, 950, 800, 810, 870]] }, { transform: { type: 'boxplot', config: { itemNameFormatter: '数据 {value}' } } }, { fromDatasetIndex: 1, fromTransformResult: 1 }], tooltip: { trigger: 'item', axisPointer: { type: 'shadow' } }, grid: { left: '10%', right: '10%', bottom: '15%' }, xAxis: { type: 'category', boundaryGap: true, nameGap: 30, splitArea: { show: false }, splitLine: { show: false } }, yAxis: { type: 'value', splitArea: { show: true } }, series: [{ name: 'boxplot', type: 'boxplot', datasetIndex: 1 }, { name: 'outlier', type: 'scatter', datasetIndex: 2 }] }
}, {
    name: "垂直盒须图",
    option: { dataset: [{ source: [[850, 740, 900, 1070, 930, 850, 950, 980, 980, 880, 1000, 980, 930, 650, 760, 810, 1000, 1000, 960, 960], [960, 940, 960, 940, 880, 800, 850, 880, 900, 840, 830, 790, 810, 880, 880, 830, 800, 790, 760, 800], [880, 880, 880, 860, 720, 720, 620, 860, 970, 950, 880, 910, 850, 870, 840, 840, 850, 840, 840, 840], [890, 810, 810, 820, 800, 770, 760, 740, 750, 760, 910, 920, 890, 860, 880, 720, 840, 850, 850, 780], [890, 840, 780, 810, 760, 810, 790, 810, 820, 850, 870, 870, 810, 740, 810, 940, 950, 800, 810, 870]] }, { transform: { type: 'boxplot', config: { itemNameFormatter: function (params) { return '数据 ' + params.value; } } } }, { fromDatasetIndex: 1, fromTransformResult: 1 }], tooltip: { trigger: 'item', axisPointer: { type: 'shadow' } }, grid: { left: '10%', right: '10%', bottom: '15%' }, yAxis: { type: 'category', boundaryGap: true, nameGap: 30, splitArea: { show: false }, splitLine: { show: false } }, xAxis: { type: 'value', splitArea: { show: true } }, series: [{ name: 'boxplot', type: 'boxplot', datasetIndex: 1 }, { name: 'outlier', type: 'scatter', encode: { x: 1, y: 0 }, datasetIndex: 2 }] }
}
]
export const DEFAULT_HEATMAPS = [{
    name: "笛卡尔坐标系热力图",
    option: { tooltip: { position: 'top' }, grid: { height: '50%', top: '10%' }, xAxis: { type: 'category', data: hours, splitArea: { show: true } }, yAxis: { type: 'category', data: days, splitArea: { show: true } }, visualMap: { min: 0, max: 10, calculable: true, orient: 'horizontal', left: 'center', bottom: '15%' }, series: [{ name: 'Punch Card', type: 'heatmap', data: heatmap_data, label: { show: true }, emphasis: { itemStyle: { shadowBlur: 10, shadowColor: 'rgba(0, 0, 0, 0.5)' } } }] }
}]
export const DEFAULT_TREES = [{
    name: "从左到右树状图",
    option: { tooltip: { trigger: 'item', triggerOn: 'mousemove' }, series: [{ type: 'tree', data: [flare], top: '1%', left: '7%', bottom: '1%', right: '20%', symbolSize: 7, label: { position: 'left', verticalAlign: 'middle', align: 'right', fontSize: 9 }, leaves: { label: { position: 'right', verticalAlign: 'middle', align: 'left' } }, emphasis: { focus: 'descendant' }, expandAndCollapse: true, animationDuration: 550, animationDurationUpdate: 750 }] }
}, {
    name: "径向树状图",
    option: { tooltip: { trigger: 'item', triggerOn: 'mousemove' }, series: [{ layout: 'radial', symbol: 'emptyCircle', type: 'tree', data: [flare], top: '1%', left: '7%', bottom: '1%', right: '20%', symbolSize: 7, label: { position: 'left', verticalAlign: 'middle', align: 'right', fontSize: 9 }, leaves: { label: { position: 'right', verticalAlign: 'middle', align: 'left' } }, emphasis: { focus: 'descendant' }, expandAndCollapse: true, animationDuration: 550, animationDurationUpdate: 750 }] }

}]
export const DEFAULT_RECT_TREES = [{
    name: "基础矩形树图",
    option: { series: [{ type: 'treemap', data: [{ name: 'nodeA', value: 10, children: [{ name: 'nodeAa', value: 4 }, { name: 'nodeAb', value: 6 }] }, { name: 'nodeB', value: 20, children: [{ name: 'nodeBa', value: 20, children: [{ name: 'nodeBa1', value: 20 }] }] }] }] }
}
]
export const DEFAULT_SUNBURSTS = [{
    name: "基础旭日图",
    option: { series: [{ type: 'sunburst', data: sunburst_data, radius: [0, '90%'], label: { rotate: 'radial' } }] }
}, {
    name: "圆角旭日图",
    option: { series: [{ itemStyle: { borderRadius: 7, borderWidth: 2 }, type: 'sunburst', data: sunburst_data, radius: [0, '90%'], label: { rotate: 'radial' } }] }
}, {
    name: "视觉编码旭日图",
    option: { visualMap: { type: 'continuous', inRange: { color: ['#2F93C8', '#AEC48F', '#FFDB5C', '#F98862'] } }, series: [{ itemStyle: { borderRadius: 7, borderWidth: 2 }, type: 'sunburst', data: sunburst_data, radius: [0, '90%'], label: { rotate: 'radial' } }] }
},
]
export const DEFAULT_SANKEYS = [{
    name: "基础桑基图",
    option: { series: [{ type: 'sankey', layout: 'none', emphasis: { focus: 'adjacency' }, data: [{ name: 'a' }, { name: 'b' }, { name: 'a1' }, { name: 'a2' }, { name: 'b1' }, { name: 'c' }], links: [{ source: 'a', target: 'a1', value: 5 }, { source: 'a', target: 'a2', value: 3 }, { source: 'b', target: 'b1', value: 8 }, { source: 'a', target: 'b1', value: 3 }, { source: 'b1', target: 'a1', value: 1 }, { source: 'b1', target: 'c', value: 2 }] }] }
}, {
    name: "垂直方向的桑基图",
    option: { series: [{ orient: 'vertical', label: { position: 'top' }, type: 'sankey', layout: 'none', emphasis: { focus: 'adjacency' }, data: [{ name: 'a' }, { name: 'b' }, { name: 'a1' }, { name: 'a2' }, { name: 'b1' }, { name: 'c' }], links: [{ source: 'a', target: 'a1', value: 5 }, { source: 'a', target: 'a2', value: 3 }, { source: 'b', target: 'b1', value: 8 }, { source: 'a', target: 'b1', value: 3 }, { source: 'b1', target: 'a1', value: 1 }, { source: 'b1', target: 'c', value: 2 }] }] }
}, {
    name: "桑基图渐变色边",
    option: { series: [{ lineStyle: { color: 'gradient', curveness: 0.5 }, orient: 'vertical', label: { position: 'top' }, type: 'sankey', layout: 'none', emphasis: { focus: 'adjacency' }, data: [{ name: 'a' }, { name: 'b' }, { name: 'a1' }, { name: 'a2' }, { name: 'b1' }, { name: 'c' }], links: [{ source: 'a', target: 'a1', value: 5 }, { source: 'a', target: 'a2', value: 3 }, { source: 'b', target: 'b1', value: 8 }, { source: 'a', target: 'b1', value: 3 }, { source: 'b1', target: 'a1', value: 1 }, { source: 'b1', target: 'c', value: 2 }] }] }
},]
export const DEFAULT_FUNNELS = [{
    name: "漏斗图",
    option: { tooltip: { trigger: 'item', formatter: '{a} <br/>{b} : {c}%' }, toolbox: { feature: { dataView: { readOnly: false }, restore: {}, saveAsImage: {} } }, legend: {}, series: [{ name: 'Funnel', type: 'funnel', left: '10%', top: 60, bottom: 60, width: '80%', min: 0, max: 100, minSize: '0%', maxSize: '100%', sort: 'descending', gap: 2, label: { show: true, position: 'inside' }, labelLine: { length: 10, lineStyle: { width: 1, type: 'solid' } }, itemStyle: { borderColor: '#fff', borderWidth: 1 }, emphasis: { label: { fontSize: 20 } }, data: [{ value: 60, name: 'Visit' }, { value: 40, name: 'Inquiry' }, { value: 20, name: 'Order' }, { value: 80, name: 'Click' }, { value: 100, name: 'Show' }] }] }
}
]
export const DEFAULT_GAUGES = [{
    name: "基础仪表盘",
    option: { series: [{ type: 'gauge', anchor: { show: true, showAbove: true, size: 18, itemStyle: { color: '#FAC858' } }, pointer: { icon: 'path://M2.9,0.7L2.9,0.7c1.4,0,2.6,1.2,2.6,2.6v115c0,1.4-1.2,2.6-2.6,2.6l0,0c-1.4,0-2.6-1.2-2.6-2.6V3.3C0.3,1.9,1.4,0.7,2.9,0.7z', width: 8, length: '80%', offsetCenter: [0, '8%'] }, progress: { show: true, overlap: true, roundCap: true }, axisLine: { roundCap: true }, data: gaugeData, title: { fontSize: 14 }, detail: { width: 40, height: 14, fontSize: 14, color: '#fff', backgroundColor: 'inherit', borderRadius: 3, formatter: '{value}%' } }] }
}, {
    name: "带标签数字动画",
    option: { series: [{ type: 'gauge', anchor: { show: true, showAbove: true, size: 18, itemStyle: { color: '#FAC858' } }, pointer: { icon: 'path://M2.9,0.7L2.9,0.7c1.4,0,2.6,1.2,2.6,2.6v115c0,1.4-1.2,2.6-2.6,2.6l0,0c-1.4,0-2.6-1.2-2.6-2.6V3.3C0.3,1.9,1.4,0.7,2.9,0.7z', width: 8, length: '80%', offsetCenter: [0, '8%'] }, progress: { show: true, overlap: true, roundCap: true }, axisLine: { roundCap: true }, data: gaugeData, title: { fontSize: 14 }, detail: { valueAnimation: true, width: 40, height: 14, fontSize: 14, color: '#fff', backgroundColor: 'inherit', borderRadius: 3 } }] }
},
]
export const DEFAULT_RIVERS = [{
    name: "主题河流图",
    option: { tooltip: { trigger: 'axis', axisPointer: { type: 'line', lineStyle: { color: 'rgba(0,0,0,0.2)', width: 1, type: 'solid' } } }, legend: {}, singleAxis: { top: 50, bottom: 50, axisTick: {}, axisLabel: {}, type: 'time', axisPointer: { animation: true, label: { show: true } }, splitLine: { show: true, lineStyle: { type: 'dashed', opacity: 0.2 } } }, series: [{ type: 'themeRiver', emphasis: { itemStyle: { shadowBlur: 20, shadowColor: 'rgba(0, 0, 0, 0.8)' } }, data: [['2015/11/08', 10, 'DQ'], ['2015/11/09', 15, 'DQ'], ['2015/11/10', 35, 'DQ'], ['2015/11/11', 38, 'DQ'], ['2015/11/12', 22, 'DQ'], ['2015/11/13', 16, 'DQ'], ['2015/11/14', 7, 'DQ'], ['2015/11/15', 2, 'DQ'], ['2015/11/16', 17, 'DQ'], ['2015/11/17', 33, 'DQ'], ['2015/11/18', 40, 'DQ'], ['2015/11/19', 32, 'DQ'], ['2015/11/20', 26, 'DQ'], ['2015/11/21', 35, 'DQ'], ['2015/11/22', 40, 'DQ'], ['2015/11/23', 32, 'DQ'], ['2015/11/24', 26, 'DQ'], ['2015/11/25', 22, 'DQ'], ['2015/11/26', 16, 'DQ'], ['2015/11/27', 22, 'DQ'], ['2015/11/28', 10, 'DQ'], ['2015/11/08', 35, 'TY'], ['2015/11/09', 36, 'TY'], ['2015/11/10', 37, 'TY'], ['2015/11/11', 22, 'TY'], ['2015/11/12', 24, 'TY'], ['2015/11/13', 26, 'TY'], ['2015/11/14', 34, 'TY'], ['2015/11/15', 21, 'TY'], ['2015/11/16', 18, 'TY'], ['2015/11/17', 45, 'TY'], ['2015/11/18', 32, 'TY'], ['2015/11/19', 35, 'TY'], ['2015/11/20', 30, 'TY'], ['2015/11/21', 28, 'TY'], ['2015/11/22', 27, 'TY'], ['2015/11/23', 26, 'TY'], ['2015/11/24', 15, 'TY'], ['2015/11/25', 30, 'TY'], ['2015/11/26', 35, 'TY'], ['2015/11/27', 42, 'TY'], ['2015/11/28', 42, 'TY'], ['2015/11/08', 21, 'SS'], ['2015/11/09', 25, 'SS'], ['2015/11/10', 27, 'SS'], ['2015/11/11', 23, 'SS'], ['2015/11/12', 24, 'SS'], ['2015/11/13', 21, 'SS'], ['2015/11/14', 35, 'SS'], ['2015/11/15', 39, 'SS'], ['2015/11/16', 40, 'SS'], ['2015/11/17', 36, 'SS'], ['2015/11/18', 33, 'SS'], ['2015/11/19', 43, 'SS'], ['2015/11/20', 40, 'SS'], ['2015/11/21', 34, 'SS'], ['2015/11/22', 28, 'SS'], ['2015/11/23', 26, 'SS'], ['2015/11/24', 37, 'SS'], ['2015/11/25', 41, 'SS'], ['2015/11/26', 46, 'SS'], ['2015/11/27', 47, 'SS'], ['2015/11/28', 41, 'SS'], ['2015/11/08', 10, 'QG'], ['2015/11/09', 15, 'QG'], ['2015/11/10', 35, 'QG'], ['2015/11/11', 38, 'QG'], ['2015/11/12', 22, 'QG'], ['2015/11/13', 16, 'QG'], ['2015/11/14', 7, 'QG'], ['2015/11/15', 2, 'QG'], ['2015/11/16', 17, 'QG'], ['2015/11/17', 33, 'QG'], ['2015/11/18', 40, 'QG'], ['2015/11/19', 32, 'QG'], ['2015/11/20', 26, 'QG'], ['2015/11/21', 35, 'QG'], ['2015/11/22', 40, 'QG'], ['2015/11/23', 32, 'QG'], ['2015/11/24', 26, 'QG'], ['2015/11/25', 22, 'QG'], ['2015/11/26', 16, 'QG'], ['2015/11/27', 22, 'QG'], ['2015/11/28', 10, 'QG'], ['2015/11/08', 10, 'SY'], ['2015/11/09', 15, 'SY'], ['2015/11/10', 35, 'SY'], ['2015/11/11', 38, 'SY'], ['2015/11/12', 22, 'SY'], ['2015/11/13', 16, 'SY'], ['2015/11/14', 7, 'SY'], ['2015/11/15', 2, 'SY'], ['2015/11/16', 17, 'SY'], ['2015/11/17', 33, 'SY'], ['2015/11/18', 40, 'SY'], ['2015/11/19', 32, 'SY'], ['2015/11/20', 26, 'SY'], ['2015/11/21', 35, 'SY'], ['2015/11/22', 4, 'SY'], ['2015/11/23', 32, 'SY'], ['2015/11/24', 26, 'SY'], ['2015/11/25', 22, 'SY'], ['2015/11/26', 16, 'SY'], ['2015/11/27', 22, 'SY'], ['2015/11/28', 10, 'SY'], ['2015/11/08', 10, 'DD'], ['2015/11/09', 15, 'DD'], ['2015/11/10', 35, 'DD'], ['2015/11/11', 38, 'DD'], ['2015/11/12', 22, 'DD'], ['2015/11/13', 16, 'DD'], ['2015/11/14', 7, 'DD'], ['2015/11/15', 2, 'DD'], ['2015/11/16', 17, 'DD'], ['2015/11/17', 33, 'DD'], ['2015/11/18', 4, 'DD'], ['2015/11/19', 32, 'DD'], ['2015/11/20', 26, 'DD'], ['2015/11/21', 35, 'DD'], ['2015/11/22', 40, 'DD'], ['2015/11/23', 32, 'DD'], ['2015/11/24', 26, 'DD'], ['2015/11/25', 22, 'DD'], ['2015/11/26', 16, 'DD'], ['2015/11/27', 22, 'DD'], ['2015/11/28', 10, 'DD']] }] }
}
]
export const DEFAULT_BAR3DRS = [{
    name: "3D柱状图",
    option: { tooltip: {}, visualMap: { max: 20, inRange: { color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026'] } }, xAxis3D: { type: 'category', }, yAxis3D: { type: 'category', }, zAxis3D: { type: 'value' }, grid3D: { boxWidth: 200, boxDepth: 80, viewControl: { projection: 'orthographic' }, light: { main: { intensity: 1.2, shadow: true }, ambient: { intensity: 0.3 } } }, series: [{ type: 'bar3D', data: heatmap_data, shading: 'lambert', label: { fontSize: 16, borderWidth: 1 }, emphasis: { label: { fontSize: 20, color: '#900' }, itemStyle: { color: '#900' } } }] }
}, {
    name: "3D柱状半透明图",
    option: { tooltip: {}, visualMap: { max: 20, inRange: { color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026'] } }, xAxis3D: { type: 'category', }, yAxis3D: { type: 'category', }, zAxis3D: { type: 'value' }, grid3D: { boxWidth: 200, boxDepth: 80, viewControl: { projection: 'orthographic' }, light: { main: { intensity: 1.2, shadow: true }, ambient: { intensity: 0.3 } } }, series: [{itemStyle: { opacity: 0.4 }, type: 'bar3D', data: heatmap_data, shading: 'lambert', label: { fontSize: 16, borderWidth: 1 }, emphasis: { label: { fontSize: 20, color: '#900' }, itemStyle: { color: '#900' } } }] }
}
]
export const DEFAULT_CHARTS = [
    { name: "折线图", examples: DEFAULT_LINES, },
    { name: "柱状图", examples: DEFAULT_BARS, },
    { name: "饼图", examples: DEFAULT_PIES },
    { name: "散点图", examples: DEFAULT_SCATTERS },
    { name: "K线图", examples: DEFAULT_Ks },
    { name: "雷达图", examples: DEFAULT_LADARs },
    { name: "盒须图", examples: DEFAULT_BOXPLOTS },
    { name: "热力图", examples: DEFAULT_HEATMAPS },
    { name: "树图", examples: DEFAULT_TREES },
    { name: "矩形树图", examples: DEFAULT_RECT_TREES },
    { name: "旭日图", examples: DEFAULT_SUNBURSTS },
    { name: "桑基图", examples: DEFAULT_SANKEYS },
    { name: "漏斗图", examples: DEFAULT_FUNNELS },
    { name: "仪表盘", examples: DEFAULT_GAUGES },
    { name: "主题河流图", examples: DEFAULT_RIVERS },
    { name: "3D柱状图", examples: DEFAULT_BAR3DRS },
]
const options = _.flatMap(DEFAULT_CHARTS, d => d.examples)
let option_index = -1
export function lastOption() {
    return nextOption(options.length - 1)
}
export function nextOption(index) {
    let rs = undefined
    if (index !== undefined) rs = { ..._.get(options, [index]), index }
    else {
        option_index += 1
        rs = { ..._.get(options, [option_index]), index: option_index }
    }
    return rs
}