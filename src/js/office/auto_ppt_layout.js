import {
    DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_TITLE,
    DATA_CATEGORY_REPORTER, DATA_CATEGORY_REPORT_DATE,
    DATA_CATEGORY_CONTENT_TITLE,
    DATA_CATEGORY_CONTENT_TEXT, DATA_CATEGORY_ZHANG, DATA_CATEGORY_ZHANG_TEXT, DATA_CATEGORY_THANK, DATA_CATEGORY_RICH, DATA_CATEGORY_CONSTANT, DATA_CATEGORY_CONTENT_SUB_TITLE
} from '../ppt'
import { TYPE_SLD_LAYOUT_BLANK, TYPE_SLD_LAYOUT_COMPARISON, TYPE_SLD_LAYOUT_CONTENTS, TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION, TYPE_SLD_LAYOUT_TITLE, TYPE_SLD_LAYOUT_TITLE_AND_CONTENT, TYPE_SLD_LAYOUT_TITLE_ONLY, TYPE_SLD_LAYOUT_TWO_CONTENT } from '@/js/ppt'

import { officeState } from '@/js/office/office_state'
import { ITEM_TYPE_RICH, ITEM_TYPE_IMAGE, ITEM_TYPE_CHART, ITEM_TYPE_TABLE, ITEM_TYPE_COLLAPSE, ITEM_TYPE_TABS, ITEM_TYPE_POPUP, ITEM_TYPE_STEPS, ITEM_TYPE_TIMELINE } from '@/js/office/word.js'
import { COLORS, PRIMARY_TEXT_COLOR, REGULAR_TEXT_COLOR } from './office'
import { SVG_IMAGE_ID, autoSVGBackgroundSetting, opposite, position_corners, randomInt, randomColor, iconRangesNoObject, object_range_convert, iconRanges, appendLogo, COLOR_TYPE_BLUE } from '@/js/office/svg_background.js'
import { arrow_down_icons, heart_icons, randomNoNumberIcon, randomNumberIcons } from '@/js/icons.js'
import { CONTENT_LEFT, generateMuLuHtml, CONTENT_TOP } from './mulu_template'
import { generateZhangjieHtml } from './zhangjie_template'
import { TEXT_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE, REPORT_HEIGHT, REPORTER_WIDTH, REPORTER, REPORT_SIZE, SUB_TITLE_HOLDER, SUB_TITLE_SIZE, IMAGE_URL_HOLDER, TITLE_HOLDER_HTML, IMAGE_URL_QUOTE } from './template_constants'
import { generateThankHtml } from './thank_template'
import _, { map, repeat } from 'lodash'
import { generateGuanXiSlides } from './generate_guanxi_slides'
import { generateChangguiHtml } from './changgui_template'
import { generateTuWenHtmls } from './tuwen_template'
import { generateTextHtmls } from './text_template'
import { generateTuBiaoHtml } from './chart_table_template'
import { DEFAULT_CHARTS } from './my_charts'
import { sample_table_data } from '../data'
import { repeatOp } from '../lodash_util'
import { GENERATE_SCENE } from './ppt_layout'
import { extractNumber } from './style'

function unitConverts(structs) {
    const slide_width = officeState.getters.slide_width
    const slide_height = officeState.getters.slide_height
    function unitConvert(structures, key) {
        if (_.isArray(structures)) return _.map(structures, unitConvert)
        else if (_.isObject(structures)) return _.mapValues(structures, (s, k) => unitConvert(s, k))
        else if (_.isString(structures)) {
            if (_.endsWith(structures, "SW")) {
                const times = (["top", "height"].includes(key) ? slide_height : slide_width)
                const px = Math.round(parseFloat(structures) * times)
                return `${px}px`
            } else return structures
        } else return structures
    }
    return unitConvert(structs, undefined)
}



export function generateStructureSlides(structures, background, has_icon, logo, option) {
    structures = unitConverts(structures)
    const is_objects = _.every(structures, structure => Object.keys(structure).includes("objects"))
    if (is_objects === true) return generateSlidesByObjects(background, logo, structures)
    return generateSlidesByDeepStructures(has_icon, option, background, logo, structures)
}
function generateSlidesByObjects(background, logo, structures) {
    const { scene, color_serial, bg } = background
    const style = bg || { [SVG_IMAGE_ID]: generateSVGBackground(scene, undefined, color_serial, undefined, "xMidYMid", logo) } || officeState.state.current_background
    const config = { classes: [], style }
    const slides = _.map(structures, structure => _.merge({}, structure, { config }))
    return slides
}
function generateSlidesByDeepStructures(has_icon, option, background, logo, structures) {
    const rs = []
    const { scene, color_serial, bg } = background
    function loop(title, structs, level) {
        level = level || ""
        title = title || "未命名标题"
        const [struct_texts, struct_categories] = _.partition(structs, struct => struct.children.length === 0)
        if (struct_texts.length > 0) {
            const content = _.join(_.map(struct_texts, struct_text => struct_text.content), "\n")
            if (_.trim(content).length > 0) {
                const slide = generateTitleContentSlide(`${level}${title}`, content, has_icon, option, bg)
                rs.push(slide)
            }
        }
        if (struct_categories.length > 0) {
            const struct_contents = _.reject(_.map(struct_categories, struct_category => struct_category.content), content => _.isEmpty(_.trim(content)))
            const slide = generateMuluSlide(struct_contents, CONTENT_TOP, { body: document.body }, scene, color_serial, has_icon, logo, false, bg)
            const content_titles = _.filter(_.get(slide, ["objects"]), object => object["data_category"] === DATA_CATEGORY_CONTENT_TITLE)
            _.forEach(content_titles, (ct, idx) => _.set(ct, ["html"], `${level}${idx + 1} ${struct_contents[idx]}`))
            const main_title = _.find(_.get(slide, ["objects"]), object => object["data_category"] === DATA_CATEGORY_CONSTANT)
            if (main_title !== undefined) _.set(main_title, ["html"], `${level}${title}`)
            rs.push(slide)
            _.forEach(struct_categories, (struct_category, index) => loop(struct_category.content, struct_category.children, `${level}${index + 1}.`))
        }
    }
    if (structures.length === 1) {
        const { content, children } = _.head(structures)
        const slide = generateFengMianSlidePos("top", content, scene, color_serial, has_icon, logo, bg)
        rs.push(slide)
        loop(content, children, undefined)
    } else loop(undefined, structures)
    return rs
}

export function generateSlides(category, scene, color_serial, has_icon, logo, option) {
    resetAutoCaches()
    switch (category) {
        case "标准":
            return generateChangguiSlides(scene, color_serial, has_icon, logo, option)
        case "封面":
            return generateFendMianSlides(scene, color_serial, has_icon, logo, option)
        case "目录":
            return generateMuLuSlides(scene, color_serial, has_icon, logo, option)
        case "章节":
            return generateZhangJieSlides(scene, color_serial, has_icon, logo, option)
        case "结束":
            return generateJieShuSlides(scene, color_serial, has_icon, logo, option)
        case "关系":
            return generateGuanXiSlides(scene, color_serial, has_icon, logo, option)
        case "图文":
            return generateTuWenSlides(scene, color_serial, has_icon, logo, option)
        case "文本":
            return generateWenBenSlides(scene, color_serial, has_icon, logo, option)
        case "图表":
            return generateTuBiaoSlides(scene, color_serial, has_icon, logo, option)
        case "表格":
            return generateBiaogeSlides(scene, color_serial, has_icon, logo, option)
        case "切换":
            return generateTabsSlides(scene, color_serial, has_icon, logo, option)
        case "折叠":
            return generateCollapseSlides(scene, color_serial, has_icon, logo, option)
        case "弹出":
            return generatePopupSlides(scene, color_serial, has_icon, logo, option)
        case "步骤":
            return generateStepsSlides(scene, color_serial, has_icon, logo, option)
        case "时间":
            return generateTimelineSlides(scene, color_serial, has_icon, logo, option)
    }
}
const title_size = 48
const title_height = 93
const sub_title_height = 62
const sub_title_size = 24

const title_width = 288
const FENGMIAN_MARGIN = 20


const sub_title = "在这里添加副标题"

export function wrapperHtml(text, style) {
    const ele = $("<span></span>").text(text)
    _.forEach(style, (value, key) => ele.css(key, value))
    return ele.prop("outerHTML")
}
const sub_title_html = () => wrapperHtml(sub_title, { "font-size": `${sub_title_size}px`, "color": PRIMARY_TEXT_COLOR })
const reporter_html = () => wrapperHtml(REPORTER, { "font-size": `${REPORT_SIZE}px`, "color": REGULAR_TEXT_COLOR })
const report_date_html = () => wrapperHtml(new Date().toLocaleDateString(), { "font-size": `${REPORT_SIZE}px`, "color": REGULAR_TEXT_COLOR })

function resetAutoCaches() {
    officeState.state.SVG_AUTO_CACHES = undefined
}
function pushSVGBackground(setting) {
    if (officeState.state.SVG_AUTO_CACHES === undefined) officeState.state.SVG_AUTO_CACHES = {}
    const id = officeState.state.SVG_AUTO_CACHE_ID
    _.set(officeState.state.SVG_AUTO_CACHES, [(officeState.state.SVG_AUTO_CACHE_ID--).toString()], setting)
    return id
}
export function getSVGBackground(id) {
    if (officeState.state.SVG_AUTO_CACHES === undefined) return undefined
    return _.get(officeState.state.SVG_AUTO_CACHES, [id])
}
function generateSVGBackground(category, size, color_type, icon_position, preserve_ratio, logo) {
    const slide_width = officeState.getters.slide_width
    const slide_height = officeState.getters.slide_height
    const bg = autoSVGBackgroundSetting(slide_width, slide_height, category, size, color_type, icon_position, preserve_ratio)
    if (logo !== undefined) appendLogo(bg, logo)
    return pushSVGBackground(bg)
}
function generateFengMianSlide([left, top, width, height], title, scene, position, color_serial, has_icon, logo, bg) {
    const title_x = left + width / 2 - title_width / 2
    const title_y = top + height / 2 - title_height / 2 - REPORT_HEIGHT
    const sub_title_x = title_x
    const sub_title_y = title_y + title_height + FENGMIAN_MARGIN
    const report_x = title_x
    const report_y = sub_title_y + sub_title_height + FENGMIAN_MARGIN
    const report_date_x = report_x + REPORTER_WIDTH + FENGMIAN_MARGIN
    const report_date_y = report_y
    const title_html = wrapperHtml(title, { "font-size": `${title_size}px`, "color": PRIMARY_TEXT_COLOR })
    const size = has_icon ? (position === "center" ? 1 / 4 : 1 / 2) : undefined
    const icon_position = has_icon ? (position === "center" ? position_corners[randomInt(position_corners.length)] : opposite(position)) : undefined
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, icon_position, "xMidYMid", logo)
    const background_style = bg || { [SVG_IMAGE_ID]: generated_bg_id }
    return {
        config: {
            classes: [], style: { background_style, ...officeState.getters.current_slide_size_style }
        },
        objects: [{
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_TITLE,
            html: title_html,
            config: {
                classes: ['position-absolute'], style: {
                    left: `${title_x}px`,
                    top: `${title_y}px`,
                    width: "auto",
                    height: "auto",
                }
            }
        }, {
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_SUB_TITLE,
            html: sub_title_html(),
            config: {
                classes: ['position-absolute'], style: {
                    left: `${sub_title_x}px`,
                    top: `${sub_title_y}px`,
                    width: "auto",
                    height: "auto",
                }
            }
        }, {
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_REPORTER,
            html: reporter_html(),
            config: {
                classes: ['position-absolute'], style: {
                    left: `${report_x}px`,
                    top: `${report_y}px`,
                    width: "auto",
                    height: "auto",
                }
            }
        }, {
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_REPORT_DATE,
            html: report_date_html(),
            config: {
                classes: ['position-absolute'], style: {
                    left: `${report_date_x}px`,
                    top: `${report_date_y}px`,
                    width: "auto",
                    height: "auto",
                }
            }
        }]
    }
}
export function generateFendMianSlides(scene, color_serial, has_icon, logo, option) {
    const titles = ["部门工作汇报", "工作总结汇报", "述职报告", "个人汇报", "营销计划汇报"]
    const title_positions = ["left", "right", "top", "bottom", "center"]
    return _.flatMap(titles, title => _.map(title_positions, position => generateFengMianSlidePos(position, title, scene, color_serial, has_icon, logo)))
}
export function generateFengMianSlidePos(position, title, scene, color_serial, has_icon, logo, bg) {
    const slide_width = officeState.getters.slide_width
    const slide_height = officeState.getters.slide_height
    switch (position) {
        case "left": return generateFengMianSlide([0, 0, slide_width / 2, slide_height], title, scene, position, color_serial, has_icon, logo, bg)
        case "right": return generateFengMianSlide([slide_width / 2, 0, slide_width / 2, slide_height], title, scene, position, color_serial, has_icon, logo, bg)
        case "top": return generateFengMianSlide([0, 0, slide_width, slide_height / 2], title, scene, position, color_serial, has_icon, logo, bg)
        case "bottom": return generateFengMianSlide([0, slide_height / 2, slide_width, slide_height / 2], title, scene, position, color_serial, has_icon, logo, bg)
        case "center": return generateFengMianSlide([slide_width / 4, slide_height / 4, slide_width / 2, slide_height / 2], title, scene, position, color_serial, has_icon, logo, bg)
    }
}

function addCenter(locations) {
    return _.mapValues(locations, l => _.assign({}, l, { center: { x: l['left'] + l['width'] / 2, y: l['top'] + l['height'] / 2 } }))
}
function removeCenter(locations) {
    return _.mapValues(locations, l => _.omit(l, ["center"]))
}
export function test(context) {
    return getPositions(`<div class="border border-primary d-flex justify-content-center align-items-center"
    style='position:relative;width:800px;height:600px;'>
    <div class="border border-primary d-flex justify-content-between align-items-center"
        style="width:500px;height: 400px;">
        <div class="k position_size k1">Content</div>
        <div class="d-flex flex-column ">
            <div class="d-flex justify-content-start align-items-center mb-3" style="width:300px">
                <div class="me-2">image</div>
                <div class="d-flex flex-column ">
                    <div class="k position_size k2">标题内容</div>
                    <div class="k position_size k3">具体内容</div>
                </div>
            </div>
            <div class="d-flex justify-content-start align-items-center" style="width:300px">
                <div class="me-2">image</div>
                <div class="d-flex flex-column ">
                    <div class="k position_size k4">标题内容</div>
                    <div class="k position_size k5">具体内容</div>
                </div>
            </div>
        </div>
    </div>
</div>`, context)
}
export function getHeaders(html, body) {
    try {
        const h = (html === undefined ? document.body : $(`<div>${html}</div>`, body || document.body))
        const headers = $("h1,h2,h3,h4,h5,h6", h)
        return _.map(headers, header_dom => ({ level: extractNumber($(header_dom).prop('tagName')), header: $(header_dom).text(), header_dom }))
    } catch (e) {
        console.error("getHeaders", e)
        console.error("getHeaders", html)
    }
}
export function getPositions(html, context) {
    context = context || document.body
    const container = $(html, context || document.body).appendTo(context === undefined ? document.body : context.body)
    const { top: parent_top, left: parent_left } = container.offset()
    const rs = _.fromPairs(_.map($(".k", container), item => {
        const { top: child_top, left: child_left } = $(item).offset()
        const classes = $(item).attr("class")
        const key = _.join(_.filter(_.split(classes, " "), item => item !== "k"))
        const width = $(item).width()
        const height = $(item).height()
        const size_style = classes.includes("position_size") ? { width: `${width}px`, height: `${height}px` } : {}
        return [key, {
            top: `${_.toInteger(child_top - parent_top)}px`,
            left: `${_.toInteger(child_left - parent_left)}px`,
            ...size_style
        }]
    }))
    container.remove()
    return rs
}
const content_size = 32
export const content_title_size = 28
export const content_text_size = 16
export function randomOne(arr) {
    return _.first(_.shuffle(arr))
}
function distance(p1, p2) {
    return Math.sqrt(Math.pow(p1["x"] - p2["x"], 2), Math.pow(p1["y"] - p2["y"], 2))
}
export function randomSizePos(positions_obj) {
    if (_.isEmpty(positions_obj)) return {}
    const arr = getValidPositions(positions_obj)
    if (_.isEmpty(arr)) return {}
    const center_point = center(positions_obj)
    const [size, positions] = arr.length === 2 ? _.maxBy(arr, ([s, p]) => s) : _.first(arr)
    const icons = iconRanges(officeState.getters.slide_width, officeState.getters.slide_height, size)
    const pos = _.maxBy(positions, pos => distance(icons[pos], center_point))
    return { size, pos }
}
export function relative2absolute(ppt_item) {
    const c = _.cloneDeep(ppt_item)
    _.set(c, ["config"], _.get(c, ["old_config"]))
    _.unset(c, ["old_config"])
    return c
}
export function absolute2relative(ppt_item) {
    ppt_item = _.cloneDeep(ppt_item)
    const old_config = _.cloneDeep(_.get(ppt_item, ["config"]))
    _.set(ppt_item, ["old_config"], old_config)
    const classes = _.get(ppt_item, ["config", "classes"], [])
    let classes_new = _.difference(classes, ['position-absolute'])
    classes_new = _.concat(classes_new, ['position-relative', "w-100", "h-100"])
    _.set(ppt_item, ["config", "classes"], classes_new)
    _.unset(ppt_item, ["config", "style", "left"])
    _.unset(ppt_item, ["config", "style", "top"])
    _.unset(ppt_item, ["config", "style", "width"])
    _.unset(ppt_item, ["config", "style", "height"])
    return ppt_item
}
export function setExampleHeight(ppt_item, max) {
    const classes = _.get(ppt_item, ["config", "classes"], [])
    _.set(ppt_item, ["config", "classes"], _.difference(classes, ["h-100"]))
    _.set(ppt_item, ["config", "style", "height"], `${max}px`)
    _.set(ppt_item, ["config", "style", "overflow"], `auto`)
    return ppt_item
}
export function timelineWraps() {
    return _.map(_.range(0, 4), (idx) => timelineWrap(undefined, { left: `${idx * 20 + 10}%`, top: "20%" }))
}
export function r(text) {
    return absolute2relative(richWrap(DATA_CATEGORY_SUB_TITLE, text, { width: "200px", height: "60px" }, SUB_TITLE_SIZE, ["m-1"]))
}
export function timelineWrapRandom() {
    return { timestamp: "2025", type: randomOne(types), color: randomOne(COLORS), size: "normal", hollow: randomOne([true, false]), content_object: [r("成功")] }
}
export function timelineWrap(config, style) {
    const types = ['primary', 'success', 'warning', 'danger', 'info']
    return {
        type: ITEM_TYPE_TIMELINE,
        object: [
            { timestamp: "2021", type: randomOne(types), color: randomOne(COLORS), size: "normal", hollow: randomOne([true, false]), content_object: [r("起步")] },
            { timestamp: "2022", type: randomOne(types), color: randomOne(COLORS), size: "normal", hollow: randomOne([true, false]), content_object: [r("发展")] },
            { timestamp: "2023", type: randomOne(types), color: randomOne(COLORS), size: "normal", hollow: randomOne([true, false]), content_object: [r("曲折")] },
            { timestamp: "2024", type: randomOne(types), color: randomOne(COLORS), size: "large", hollow: randomOne([true, false]), content_object: [r("成功")] },
        ],
        config: config || { classes: ['position-absolute', 'w-auto', 'h-100', 'p-3'], style: style || { top: 0, left: 0 } },
    }
}
export function stepsWrapsSimple() {
    let index = 0
    return crossJoin2([[1, 2], [true, false]], ([active, align]) => {
        const style = { top: `${(index++) * 20 + 10}%`, left: 0 }
        return stepsWrap({ classes: ['position-absolute', 'w-100', 'h-auto', 'p-3'], style },
            { direction: "horizontal", active, "align-center": align, simple: true }
        )
    })
}
export function stepsWraps(is_horizontal) {
    let index = 0
    return crossJoin2([[1, 2], [true, false]], ([active, align]) => {
        const style = is_horizontal ? { top: `${(index++) * 20 + 10}%`, left: 0 } : { top: 0, left: `${(index++) * 20 + 10}%` }
        return stepsWrap({ classes: ['position-absolute', is_horizontal ? 'w-100' : "w-auto", is_horizontal ? 'h-auto' : "h-100", 'p-3'], style },
            { direction: is_horizontal ? "horizontal" : "vertical", active, "align-center": align, simple: false }
        )
    })
}
export function stepsWrap(config, option) {
    return {
        type: ITEM_TYPE_STEPS,
        object: [
            { title: "Step 1", description: "文字描述1", "process-status": 'process' },
            { title: "Step 2", description: "文字描述2", "process-status": 'error' },
            { title: "Step 3", description: "文字描述3", "process-status": 'success' },
        ],
        config: config || { classes: ['position-absolute', 'w-100', 'h-100', 'p-3'], style: { top: 0, left: 0 } },
        option: option || {
            direction: "horizontal",
            active: 1,
            "align-center": true,
            simple: false
        }
    }
}

export function popupWraps() {
    const classes = ['position-absolute', 'w-auto', 'h-auto']
    return [
        popupWrap("点击弹出", { classes, style: { top: "15%", left: "45%" } }, { popover: { placement: "top" }, is_card_mode: false }),
        popupWrap("全屏弹出", { classes, style: { top: "25%", left: "45%" } }, { dialog: { fullscreen: true }, is_card_mode: false }),
        popupWrap("居中弹出", { classes, style: { top: "35%", left: "45%" } }, { dialog: { fullscreen: false }, is_card_mode: false }),
        popupWrap("上侧弹出", { classes, style: { top: "45%", left: "45%" } }, { drawer: { direction: "ttb" }, is_card_mode: false }),
        popupWrap("右侧弹出", { classes, style: { top: "55%", left: "45%" } }, { drawer: { direction: "rtl" }, is_card_mode: false }),
        popupWrap("下侧弹出", { classes, style: { top: "65%", left: "45%" } }, { drawer: { direction: "btt" }, is_card_mode: false }),
        popupWrap("左侧弹出", { classes, style: { top: "75%", left: "45%" } }, { drawer: { direction: "ltr" }, is_card_mode: false }),
    ]
}

export function popupWrap(title, config, option) {
    return {
        type: ITEM_TYPE_POPUP,
        object: {
            [title || "Content For Popup"]: [popupSampleItem()],
        },
        config: config || {
            classes: ['position-absolute', 'w-100', 'h-100', 'text-primary'],
            style: { top: 0, left: 0 }
        },
        option: option || {
            dialog: { fullscreen: true }
        }
    }
}
export function popupSampleItem(idx) {
    return setExampleHeight(absolute2relative(richWrap(DATA_CATEGORY_RICH, `${idx ? idx + 1 : ""} ${TEXT_HOLDER}`, undefined, TEXT_SIZE, ["w-100", "h-100"])), 100)
}
export function tabsSampleItem(idx) {
    return setExampleHeight(absolute2relative(richWrap(DATA_CATEGORY_RICH, `${idx ? idx + 1 : ""} ${TEXT_HOLDER}`, undefined, TEXT_SIZE, ["w-100", "h-100"])), 100)
}
export function collapseSampleItem(idx) {
    return setExampleHeight(absolute2relative(richWrap(DATA_CATEGORY_RICH, `${idx ? idx + 1 : ""} ${TEXT_HOLDER}`, undefined, TEXT_SIZE, ["w-100", "h-100"])), 100)
}
export function collapseWrap() {
    return {
        type: ITEM_TYPE_COLLAPSE,
        object: {
            "Collapse1": repeatOp(2, (idx) => tabsSampleItem(idx)),
            "Collapse2": repeatOp(2, (idx) => tabsSampleItem(idx)),
        },
        config: { classes: ['position-absolute', 'w-100', 'h-100'], style: { top: 0, left: 0 } }
    }
}
export function tabsWrap() {
    return {
        type: ITEM_TYPE_TABS,
        object: {
            "Tabs 1": repeatOp(2, (idx) => tabsSampleItem(idx)),
            "Tabs 2": repeatOp(2, (idx) => tabsSampleItem(idx)),
        },
        config: { classes: ['position-absolute', 'w-100', 'h-100'], style: { top: 0, left: 0 } }
    }
}
export function richWrap(category, holder, style, size, classes) {
    return {
        type: ITEM_TYPE_RICH,
        data_category: category,
        html: holder,
        config: { classes: _.concat(['position-absolute'], classes || []), style: _.merge({}, style, { 'font-size': `${size}px` }) }
    }
}
export function getValidPositions(positions) {
    const object_ranges = _.map(_.values(positions), ({ left, top, width, height }) => ({ x: left, y: top, width, height }))
    const slide_width = officeState.getters.slide_width
    const slide_height = officeState.getters.slide_height
    return iconRangesNoObject(slide_width, slide_height, object_ranges)
}
function center(positions) {
    if (_.isEmpty(positions)) return undefined
    const ranges = _.map(_.values(positions), object_range_convert)
    const centers = _.map(ranges, ({ x, y, width, height }) => ({ x: x + (width || 0) / 2, y: y + (height || 0) / 2 }))
    const x = _.mean(_.map(centers, c => c["x"]))
    const y = _.mean(_.map(centers, c => c["y"]))
    return { x, y }
}

export function generateChangguiSlides(scene, color_serial, has_icon, logo, option) {
    const context = _.get(option, "context")
    const layouts = [TYPE_SLD_LAYOUT_TITLE, TYPE_SLD_LAYOUT_CONTENTS, TYPE_SLD_LAYOUT_TITLE_AND_CONTENT, TYPE_SLD_LAYOUT_TWO_CONTENT, TYPE_SLD_LAYOUT_COMPARISON, TYPE_SLD_LAYOUT_TITLE_ONLY, TYPE_SLD_LAYOUT_BLANK, TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION,]
    return _.map(layouts, layout => generateChangguiSlide(layout, context, has_icon, scene, color_serial, logo))
}
export function generateChangguiSlide(layout, context, has_icon, scene, color_serial, logo) {
    context = context || window.document
    const positions = [TYPE_SLD_LAYOUT_CONTENTS, TYPE_SLD_LAYOUT_BLANK].includes(layout) ? {} : getPositions(generateChangguiHtml(layout), context)
    const { size, pos } = has_icon === true ? randomSizePos(positions) : {}
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, pos, "xMidYMid", logo)
    const config = { classes: [], style: { [SVG_IMAGE_ID]: generated_bg_id, ...officeState.getters.current_slide_size_style } }
    let objects = []
    switch (layout) {
        case TYPE_SLD_LAYOUT_TITLE:
            objects = [richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE),
            richWrap(DATA_CATEGORY_SUB_TITLE, SUB_TITLE_HOLDER, _.find(positions, (v, k) => k.includes("sub_title")), SUB_TITLE_SIZE),
            richWrap(DATA_CATEGORY_REPORTER, REPORTER, _.find(positions, (v, k) => k.includes("report")), REPORT_SIZE)]
            break
        case TYPE_SLD_LAYOUT_CONTENTS:
            return generateMuluSlide(3, CONTENT_LEFT, context, scene, color_serial, has_icon, logo, true)
        case TYPE_SLD_LAYOUT_TITLE_AND_CONTENT:
            objects = [richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE),
            richWrap(DATA_CATEGORY_RICH, TEXT_HOLDER, _.find(positions, (v, k) => k.includes("text")), TEXT_SIZE)]
            break
        case TYPE_SLD_LAYOUT_TWO_CONTENT:
            const styles = _.filter(positions, (v, k) => k.includes("text"))
            objects = [richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE),
            ...map(styles, style => richWrap(DATA_CATEGORY_RICH, TEXT_HOLDER, style, TEXT_SIZE))]
            break
        case TYPE_SLD_LAYOUT_COMPARISON:
            objects = [
                ...map(_.filter(positions, (v, k) => k.includes("title")), style => richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, style, TITLE_SIZE)),
                ...map(_.filter(positions, (v, k) => k.includes("text")), style => richWrap(DATA_CATEGORY_RICH, TEXT_HOLDER, style, TEXT_SIZE))
            ]
            break
        case TYPE_SLD_LAYOUT_TITLE_ONLY:
            objects = [richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE)]
            break
        case TYPE_SLD_LAYOUT_BLANK: break
        case TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION:
            objects = [richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE),
            imageWrap(_.find(positions, (v, k) => k.includes("image")))]
            break
    }
    return { config, objects }
}

export function generateMuLuSlides(scene, color_serial, has_icon, logo, option) {
    const count = _.get(option, "count")
    const context = _.get(option, "context")
    const layouts = [
        CONTENT_LEFT,
        "content_top",
        "titles_step",
        "titles_step_reverse"
    ]
    const image_use_borders = [true, false]
    return _.flattenDeep(_.map(layouts, layout => _.map(image_use_borders, image_use_border => generateMuluSlide(count, layout, context, scene, color_serial, has_icon, logo, image_use_border))))
}
export function generateTitleContentSlide(title, content, has_icon, option, bg) {
    const context = _.get(option, "context")
    const positions = getPositions(generateChangguiHtml(TYPE_SLD_LAYOUT_TITLE_AND_CONTENT), context)
    const { size, pos } = has_icon === true ? randomSizePos(positions) : {}
    const style = bg || { [SVG_IMAGE_ID]: generateSVGBackground(GENERATE_SCENE, size, COLOR_TYPE_BLUE, pos, "xMidYMid", undefined) } || officeState.state.current_background
    const config = { classes: [], style }
    const objects = [richWrap(DATA_CATEGORY_CONTENT_TITLE, title, _.find(positions, (v, k) => k.includes("title")), TITLE_SIZE),
    richWrap(DATA_CATEGORY_RICH, content, _.find(positions, (v, k) => k.includes("text")), TEXT_SIZE)]
    return { config, objects }
}
export function generateMuluSlide(count, layout, context, scene, color_serial, has_icon, logo, image_use_border, bg) {
    const use_number = Math.random() < 0.5
    const number_icons = randomNumberIcons()
    const positions = getPositions(generateMuLuHtml(count, layout), context)
    const content_style = _.find(positions, (v, k) => k.includes("content"))
    const content_titles = _.filter(positions, (v, k) => k.includes("content_title"))
    const content_texts = _.filter(positions, (v, k) => k.includes("content_text"))
    const images = _.sortBy(_.filter(positions, (v, k) => k.includes("image")), (v, k) => k)
    const random_content = {
        html: randomOne(["CONTENT", "目录", "CATALOG", "CATALOGUE"]),
        config: {
            classes: _.concat(['position-absolute'], randomOne(['text-primary', 'text-success', 'text-danger', 'text-info'])), style: _.merge({}, content_style, { 'font-size': `${content_size}px` })
        }
    }
    const { size, pos } = has_icon ? randomSizePos(positions) : {}
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, pos, "xMidYMid", logo)
    const background_style = bg || { [SVG_IMAGE_ID]: generated_bg_id }
    return {
        config: {
            classes: [], style: { ...background_style, ...officeState.getters.current_slide_size_style }
        },
        objects: [_.merge({}, {
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_CONSTANT,
            html: "CONTENT",
            config: {
                classes: ['position-absolute'], style: _.merge({}, content_style, { 'font-size': `${content_size}px` })
            }
        }, random_content),
        ..._.map(content_titles, t => ({
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_CONTENT_TITLE,
            html: "输入标题内容",
            config: {
                classes: ['position-absolute'], style: _.merge({}, t, { 'font-size': `${content_title_size}px` })
            }
        })),
        ..._.map(content_texts, t => ({
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_CONTENT_SUB_TITLE,
            html: "输入具体内容",
            config: {
                classes: ['position-absolute'], style: _.merge({}, t, { 'font-size': `${content_text_size}px` })
            }
        })),
        ..._.map(images, (t, index) => ({
            type: ITEM_TYPE_IMAGE,
            base64: use_number ? number_icons[index + 1] : randomNoNumberIcon(),
            config: {
                classes: _.concat(['position-absolute'], image_use_border ? ["border-end", "border-secondary", "border-2", "pe-1"] : []), style: _.merge({}, t, { width: "32px", height: "32px" })
            }
        }))
        ]
    }
}
const filters = [
    { filter: "contrast(200%)" },
    { filter: "grayscale(80%)" },
    { filter: "drop-shadow(16px 16px 20px red) invert(75%);" },
]
const borderColors = [
    "border-primary",
    "border-secondary",
    "border-success",
    "border-danger",
    "border-warning",
    "border-info",
]
const roundeds = [
    "rounded",
    "rounded-top",
    "rounded-end",
    "rounded-bottom",
    "rounded-start",
]
function randomClassesRound() {
    return [randomOne(roundeds), randomOne(borderColors), "border", "p-3", "position-absolute", "rounded-5"]
}

export function imageWrap(style, img) {
    return {
        type: ITEM_TYPE_IMAGE,
        base64: img || IMAGE_URL_HOLDER(),
        config: {
            classes: ['position-absolute'], style,
        }
    }
}
function chartWrap(config, option) {
    return {
        type: ITEM_TYPE_CHART,
        option,
        config
    }
}

function tableWrap(config) {
    return {
        type: ITEM_TYPE_TABLE,
        data: _.cloneDeep(sample_table_data),
        config
    }
}

export function crossJoin4(arr, op) {
    const [a0, a1, a2, a3] = arr
    return _.flattenDeep(_.map(a0, p0 => _.map(a1, p1 => _.map(a2, p2 => _.map(a3, p3 => op([p0, p1, p2, p3]))))))
}
export function crossJoin3(arr, op) {
    const [a0, a1, a2] = arr
    return _.flattenDeep(_.map(a0, p0 => _.map(a1, p1 => _.map(a2, p2 => op([p0, p1, p2])))))
}
export function crossJoin2(arr, op) {
    const [a0, a1] = arr
    return _.flattenDeep(_.map(a0, p0 => _.map(a1, p1 => op([p0, p1]))))
}
export function generateZhangJieSlides(scene, color_serial, has_icon, logo, option) {
    const zhang_index = _.get(option, "zhang_index")
    const context = _.get(option, "context")
    const layouts = [
        "normal_row",
        "normal_column",
    ]
    const image_uses = [true, false]
    const image_arrows = [true, false]
    const zhang_names = [`第 0${zhang_index} 部分`, `第 0${zhang_index} 章`, `第 0${zhang_index} 讲`, `PART 0${zhang_index}`, `0${zhang_index}`]
    return crossJoin4([layouts, image_uses, image_arrows, zhang_names], ([layout, image_use, image_arrow, zhang_name]) => {
        return generateZhangjieSlide(layout, image_use, image_arrow, context, zhang_name, has_icon, scene, color_serial, logo, zhang_index)
    })
}
export function generateZhangjieSlide(layout, image_use, image_arrow, context, zhang_name, has_icon, scene, color_serial, logo, zhang_index) {
    const number_icons = randomNumberIcons()
    const positions = getPositions(generateZhangjieHtml(layout, image_use, image_arrow), context)
    const images = _.filter(positions, (v, k) => k.includes("image"))
    const arrows = _.filter(positions, (v, k) => k.includes("arrow"))
    const zhang_index_style = _.find(positions, (v, k) => k.includes("zhang_index"))
    const zhang_titles = _.filter(positions, (v, k) => k.includes("zhang_title"))
    const zhang_sub_titles = _.filter(positions, (v, k) => k.includes("zhang_sub_title"))
    const zhang_name_style = {
        html: zhang_name,
        config: {
            classes: _.concat(['position-absolute'], randomOne(['text-primary', 'text-success', 'text-danger', 'text-info'])), style: _.merge({}, zhang_index_style, { 'font-size': `${TITLE_SIZE}px` })
        }
    }
    const { size, pos } = has_icon ? randomSizePos(positions) : {}
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, pos, "xMidYMid", logo)
    const zhangs = image_use ? _.map(images, (t) => ({
        type: ITEM_TYPE_IMAGE,
        base64: number_icons[zhang_index],
        config: {
            classes: _.concat(['position-absolute'], []), style: _.merge({}, t, { width: "32px", height: "32px" })
        }
    })) : [_.merge({}, {
        type: ITEM_TYPE_RICH,
        data_category: DATA_CATEGORY_ZHANG,
        html: "章节",
        config: {
            classes: ['position-absolute']
        }
    }, zhang_name_style)]
    const arrow_objects = _.map(arrows, (t) => ({
        type: ITEM_TYPE_IMAGE,
        base64: randomOne(arrow_down_icons),
        config: {
            classes: _.concat(['position-absolute', 'm-3'], []), style: _.merge({}, t, { width: "32px", height: "32px" })
        }
    }))
    const sub_title_objects = _.map(zhang_sub_titles, t => ({
        type: ITEM_TYPE_RICH,
        data_category: DATA_CATEGORY_ZHANG_TEXT,
        html: SUB_TITLE_HOLDER,
        config: {
            classes: ['position-absolute'], style: _.merge({}, t, { 'font-size': `${TEXT_SIZE}px` })
        }
    }))
    const title_objects = _.map(zhang_titles, t => ({
        type: ITEM_TYPE_RICH,
        data_category: DATA_CATEGORY_ZHANG,
        html: TITLE_HOLDER,
        config: {
            classes: ['position-absolute'], style: _.merge({}, t, { 'font-size': `${TITLE_SIZE}px` })
        }
    }))
    return {
        config: {
            classes: [], style: { [SVG_IMAGE_ID]: generated_bg_id, ...officeState.getters.current_slide_size_style }
        },
        objects: _.concat(arrow_objects, zhangs, sub_title_objects, title_objects)
    }
}

export function generateJieShuSlides(scene, color_serial, has_icon, logo, option) {
    const context = _.get(option, "context")
    const thanks = ["感谢观看", "敬请指教", "感谢您的光临", "汇报结束"]
    const border_colors = ["border-primary", "border-success"]
    const border_widths = ["border-1", "border-3"]
    const radiuses_single = ["rounded", "rounded-pill"]
    return crossJoin4([thanks, border_colors, border_widths, radiuses_single], ([thank, border_color, border_width, radius]) => {
        return generateJieShuSlide(thank, context, has_icon, scene, color_serial, logo, border_color, border_width, radius)
    })
}
export function generateJieShuSlide(thank, context, has_icon, scene, color_serial, logo, border_color, border_width, radius) {
    const positions = getPositions(generateThankHtml(thank), context)
    const thank_style = _.find(positions, (v, k) => k.includes("thank"))
    const heart_style = _.find(positions, (v, k) => k.includes("heart"))
    const { size, pos } = has_icon ? randomSizePos(positions) : {}
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, pos, "xMidYMid", logo)
    const thank_config = {
        html: `<center>${thank}</center>`,
        config: {
            classes: _.concat(['position-absolute', 'border-bottom', 'p-3', 'shadow'], [border_color, border_width, radius]), style: _.merge({}, thank_style, { 'font-size': `${TITLE_SIZE}px`, width: "300px" })
        }
    }
    return {
        config: {
            classes: [], style: { [SVG_IMAGE_ID]: generated_bg_id, ...officeState.getters.current_slide_size_style }
        },
        objects: [_.merge({}, {
            type: ITEM_TYPE_RICH,
            data_category: DATA_CATEGORY_THANK,
        }, thank_config), {
            type: ITEM_TYPE_IMAGE,
            base64: randomOne(heart_icons),
            config: {
                classes: _.concat(['position-absolute', 'm-3'], []), style: _.merge({}, heart_style, { width: "32px", height: "32px" })
            }
        }]
    }
}

export function wrapperHtmlEditor(html, classes, position_style, font_size, data_category) {
    let config = {
        html: html,
        config: {
            classes: _.concat(['position-absolute'], classes), style: _.merge({}, position_style, { 'font-size': `${font_size}px` })
        }
    }
    return _.merge({}, config, {
        type: ITEM_TYPE_RICH,
        data_category: data_category,
    })
}
export function numberImages(count) {
    const number_icons = randomNumberIcons()
    return _.map(_.range(1, count + 1), i => number_icons[i])
}
export function wrapperImage(image_url, classes, style, color_serial) {
    const svg_image_props = color_serial ? { svg_props: { fill: randomColor(color_serial || "blue", 0.6) } } : {}
    return _.merge({
        type: ITEM_TYPE_IMAGE,
        base64: image_url,
        config: {
            classes: _.concat(['position-absolute'], classes || []), style: _.merge({}, { width: "24px", height: "24px" }, style)
        }
    }, svg_image_props)
}
export function addStyle(old, new_style) { return _.merge({}, old, new_style) }
export function wrapperSlide(scene, color_serial, objects, positions, has_icon, logo) {
    const { size, pos } = has_icon ? randomSizePos(positions) : {}
    const generated_bg_id = generateSVGBackground(scene, size, color_serial, pos, "xMidYMid", logo)
    return ({ config: { classes: [], style: { [SVG_IMAGE_ID]: generated_bg_id, ...officeState.getters.current_slide_size_style } }, objects })
}
export function generateTuWenSlides(scene, color_serial, has_icon, logo, option) {
    return _.flatMap(generateTuWenHtmls(option['shape'], option['count']), html => {
        const positions = getPositions(html, option['context'])
        const riches = richObjs(positions)
        return _.map(_.range(0, 3), () => {
            const images = imageObjs(positions)
            const objects = _.concat(riches, images)
            return wrapperSlide(scene, color_serial, objects, positions, has_icon, logo)
        })
    })
}
export function generateWenBenSlides(scene, color_serial, has_icon, logo, option) {
    return _.flatMap(generateTextHtmls(), html => {
        const positions = getPositions(html, option && option['context'])
        const riche_list = richObjs(positions)
        return _.map(_.range(0, 3), () => {
            const riches = _.cloneDeep(riche_list)
            _.forEach(riches, r => { if (r["data_category"] === DATA_CATEGORY_RICH) _.set(r, ["config", "classes"], randomClassesRound()) })
            const images = _.map(imageObjs(positions), o => _.merge(o, { base64: IMAGE_URL_QUOTE() }))
            const objects = _.concat(riches, images)
            return wrapperSlide(scene, color_serial, objects, positions, has_icon, logo)
        })
    })
}

export function generateTuBiaoSlides(scene, color_serial, has_icon, logo, option) {
    if (_.isNil(option["shape"])) return []
    const htmls = [generateTuBiaoHtml(true), generateTuBiaoHtml(false)]
    const examples = _.find(DEFAULT_CHARTS, ({ name }) => name === option["shape"])["examples"]
    return _.flatMap(htmls, html => {
        const positions = getPositions(html, option['context'])
        const riche_list = richObjs(positions)
        const image_obj = _.first(imageObjs(positions))
        return _.map(examples, ({ option }) => {
            const chart_obj = chartWrap(image_obj["config"], option)
            return wrapperSlide(scene, color_serial, _.concat(riche_list, [chart_obj]), positions, has_icon, logo)
        })
    })
}


export function generateBiaogeSlides(scene, color_serial, has_icon, logo, option) {
    const htmls = [generateTuBiaoHtml(true), generateTuBiaoHtml(false)]
    return _.flatMap(htmls, html => {
        const positions = getPositions(html, option && option['context'])
        const riche_list = richObjs(positions)
        const image_obj = _.first(imageObjs(positions))
        const table_obj = tableWrap(image_obj["config"])
        return crossJoin3([[true, false], [true, false], [true, false]], ([stripe, border, show_header]) => {
            const table_obj_format = _.merge({}, table_obj, { stripe, border, show_header })
            return wrapperSlide(scene, color_serial, _.concat(riche_list, [table_obj_format]), positions, has_icon, logo)
        })
    })
}

function imageObjs(positions) {
    const image_styles = _.filter(positions, (v, k) => k.includes("image"))
    return _.map(image_styles, style => imageWrap(style))
}
function kInclude(key, cls) {
    return _.split(key, ",").includes(cls)
}
function richObjs(positions) {
    const title_styles = _.filter(positions, (v, k) => kInclude(k, "title"))
    const sub_title_styles = _.filter(positions, (v, k) => kInclude(k, "sub_title"))
    const text_styles = _.filter(positions, (v, k) => kInclude(k, "text"))
    const title_objs = _.map(title_styles, style => richWrap(DATA_CATEGORY_TITLE, TITLE_HOLDER_HTML(), style, TITLE_SIZE, ["justify-content-center"]))
    const sub_title_objs = _.map(sub_title_styles, style => richWrap(DATA_CATEGORY_SUB_TITLE, SUB_TITLE_HOLDER, style, SUB_TITLE_SIZE))
    const text_objs = _.map(text_styles, style => richWrap(DATA_CATEGORY_RICH, TEXT_HOLDER, style, TEXT_SIZE))
    const rich_objs = _.concat(title_objs, sub_title_objs, text_objs)
    return rich_objs
}
export function generateTimelineSlides(scene, color_serial, has_icon, logo) {
    return [
        wrapperSlide(scene, color_serial, timelineWraps(), {}, has_icon, logo),
    ]
}
export function generateStepsSlides(scene, color_serial, has_icon, logo) {
    return [
        wrapperSlide(scene, color_serial, stepsWrapsSimple(), {}, has_icon, logo),
        wrapperSlide(scene, color_serial, stepsWraps(true), {}, has_icon, logo),
        wrapperSlide(scene, color_serial, stepsWraps(false), {}, has_icon, logo),
    ]
}
export function generatePopupSlides(scene, color_serial, has_icon, logo) {
    return [
        wrapperSlide(scene, color_serial, popupWraps(), {}, has_icon, logo),
    ]
}
export function generateCollapseSlides(scene, color_serial, has_icon, logo) {
    return [
        wrapperSlide(scene, color_serial, [collapseWrap()], {}, has_icon, logo),
    ]
}
export function generateTabsSlides(scene, color_serial, has_icon, logo) {
    return [
        wrapperSlide(scene, color_serial, [tabsWrap()], {}, has_icon, logo),
    ]
}
export function generateDongHuaSlides(scene, color_serial, has_icon, logo, option) {
    return []
}

