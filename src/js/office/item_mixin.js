import { formatMixin } from "./format_object_mixin";
import { dragMixin } from "./drag_mixin";
import { isNotEmpty } from '@/js/lodash_util'
export const itemMixin = {
    emits: ["annotation_added"],
    mixins: [formatMixin, dragMixin],
    data(){
        return {
            selected_tab: "html_editor",
        }
    },
    methods: {
        findParent(name) {
            let tmp = this
            while (isNotEmpty(tmp) && tmp.$options.name !== name) tmp = tmp.$parent
            return tmp
        },
        findParentSlide() { return this.findParent("Slide") },
        findParentPPTItem() { return this.findParent("PPTItem") },
        getPPTItemIndex() {
            return _.get(this.findParentPPTItem(), ["index_in_parent"])
        },
    }
}