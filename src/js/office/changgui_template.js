import { TYPE_SLD_LAYOUT_BLANK, TYPE_SLD_LAYOUT_COMPARISON, TYPE_SLD_LAYOUT_CONTENTS, TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION, TYPE_SLD_LAYOUT_TITLE, TYPE_SLD_LAYOUT_TITLE_AND_CONTENT, TYPE_SLD_LAYOUT_TITLE_ONLY, TYPE_SLD_LAYOUT_TWO_CONTENT } from '@/js/ppt';

import { containerWrap, REPORTER, REPORT_SIZE, TEXT_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE, SUB_TITLE_SIZE, SUB_TITLE_HOLDER } from './template_constants';


function divWrapText(classes) {
    return `<div class="k position_size text text-break word-break overflow-auto ${classes} " style='font-size:${TEXT_SIZE}px;white-space: break-spaces;'>${TEXT_HOLDER}</div>`
}
function divWrapTitle(classes) {
    return `<div class="k position_size title ${classes || ""}" style='font-size:${TITLE_SIZE}px;width:350px;height:60px;'>${TITLE_HOLDER}</div>`
}
function divWrapSubTitle(classes) {
    return `<div class="k position_size sub_title ${classes || ""}" style='font-size:${SUB_TITLE_SIZE}px;width:350px;height:40px;'>${SUB_TITLE_HOLDER}</div>`
}
function divWrapReporter(classes) {
    return `<div class="k position_size report ${classes || ""}" style='width:150px;height:40px;font-size:${REPORT_SIZE}px;'>${REPORTER}</div>`
}
export function generateChangguiHtml(layout) {
    switch (layout) {
        case TYPE_SLD_LAYOUT_TITLE:
            return containerWrap(true, 'justify-content-around', undefined, `
            ${divWrapTitle()}
            ${divWrapSubTitle()}
            ${divWrapReporter("align-self-end")}
            `)
        case TYPE_SLD_LAYOUT_CONTENTS:
            return ""
        case TYPE_SLD_LAYOUT_TITLE_AND_CONTENT:
            return containerWrap(true, 'justify-content-start', 'align-items-stretch', `
        ${divWrapTitle()}
        ${divWrapText("flex-grow-1")}
        `)
        case TYPE_SLD_LAYOUT_TWO_CONTENT:
            return containerWrap(true, 'justify-content-start', 'align-items-stretch', `
        ${divWrapTitle()}
        <div class="d-flex flex-grow-1 align-items-stretch" style='font-size:${TEXT_SIZE}px'>
            ${divWrapText("text1 flex-grow-1")}
            ${divWrapText("text2 flex-grow-1")}
        </div>
        `)
        case TYPE_SLD_LAYOUT_COMPARISON:
            return containerWrap(true, 'justify-content-start', 'align-items-stretch', `
            <div class="d-flex align-items-stretch">
                ${divWrapTitle("title1 flex-grow-1")}
                ${divWrapTitle("title2 flex-grow-1")}
            </div>
            <div class="d-flex flex-grow-1 align-items-stretch">
                ${divWrapText("text1 flex-grow-1")}
                ${divWrapText("text2 flex-grow-1")}
            </div>
        `)
        case TYPE_SLD_LAYOUT_TITLE_ONLY:
            return containerWrap(true, 'justify-content-center', 'align-items-center', `
                ${divWrapTitle()}
            `)
        case TYPE_SLD_LAYOUT_BLANK: break
        case TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION:
            return containerWrap(true, 'justify-content-between', 'align-items-center', `
        <div class="k position_size image  flex-grow-1" style="min-width:300px;">
            <img class="h-100" style="object-fit: contain" src="/img/office/picture.svg"/>
        </div>
         ${divWrapTitle()}
        `)
        default:
            return "<div></div>"
    }
}