import _ from "lodash";
import { MARGIN, SUB_TITLE_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE, SUB_TITLE_SIZE, TEXT_HOLDER } from "./template_constants";
import { officeState } from '@/js/office/office_state';
export const GUANXI_TEXT_WIDTH = 180
export const GUANXI_TEXT_HEIGHT = 60
export const GUANXI_SHAPDES = ["并列", "流程", "循环", "金字塔", "时间轴", "组织", "对比", "象限",]
export function generateGuanXiSlidesBinglieHtml(shape, count) {
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column align-items-center"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                <div class="k position_size guanxi_title d-flex justify-content-center" style="font-size:${TITLE_SIZE}px;width:170px;height:45px;">${TITLE_HOLDER}</div>
                <div class="d-flex flex-column align-items-center flex-grow-1  flex-wrap w-100" style="min-height:100px;">
                    ${_.join(_.map(_.range(0, count), idx => {
        return `<div class="d-flex justify-content-start align-items-center mb-3" style="width:fit-content">
                            <div class="d-flex flex-column ">
                                <div class="d-flex align-items-center">
                                    <div class="k position_size k_${idx} guanxi_sub_title" style='font-size:${SUB_TITLE_SIZE}px;height:48px;width:${GUANXI_TEXT_WIDTH}px;'>${SUB_TITLE_HOLDER}</div>
                                    <img class="k position_size k_${idx} image me-3" src='/img/office/content.svg' style="width:24px;height:24px;"/>
                                </div>
                                <div class="k position_size k_${idx} guanxi_text" style='font-size:${TEXT_SIZE}px;width:${GUANXI_TEXT_WIDTH}px;height:${GUANXI_TEXT_HEIGHT}px;'>${TEXT_HOLDER}</div>
                            </div>
                        </div>`;
    }), "")}
                </div>
            </div>
        </div>`;
}
export function generateGuanXiSlidesTimeAxisHtml(count, is_vertical) {
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    const time_image = is_vertical ? '/img/office/shapes/time_axis/vertical.svg':'/img/office/shapes/time_axis/horizontal.svg'
    return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex flex-column align-items-center"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                <div class="k position_size guanxi_title d-flex justify-content-center mb-3" style="font-size:${TITLE_SIZE}px;width:170px;height:45px;">${TITLE_HOLDER}</div>
                <div class="d-flex ${is_vertical ? 'flex-row' : 'flex-column '} justify-content-center flex-grow-1  flex-wrap w-100" style="min-height:100px;">
                    <img class="k position_size image_axis ${is_vertical ? 'h-100' : 'w-100'} position_size m-3" src='${time_image}' style="${is_vertical ? 'width:40px;' : 'height:10px;'}"/>
                    <div class="d-flex ${is_vertical ? 'flex-column':'flex-row'} align-items-center justify-content-center ">
                    ${_.join(_.map(_.range(0, count), idx => {
        return `<div class="d-flex justify-content-start align-items-center mb-3" style="width:fit-content">
                        <img class="k position_size image_tag k_image_${idx} m-2" src='/img/office/shapes/time_axis/tag1.svg' style="width:24px;width:24px;"/>
                        <div class="k position_size k_${idx} guanxi_sub_title d-flex align-items-center" style="height:48px;width:${is_vertical ? GUANXI_TEXT_WIDTH : 100}px;">里程碑</div>
                </div>`;
    }), "")}
                    </div>
                </div>
            </div>
        </div>`;
}
function separotor2numbers(separator) {
    return _.map(_.split(separator, "/"), _.toInteger)
}
function direction2image(image_sep, item_sep) {
    const [g_row_start, g_col_start, g_row_end, g_col_end] = separotor2numbers(image_sep)
    const [t_row_start, t_col_start, t_row_end, t_col_end] = separotor2numbers(item_sep)
    const h = t_col_end <= g_col_start ? 'left' : (t_col_start >= g_col_end ? 'right' : 'none')
    const v = t_row_end <= g_row_start ? 'top' : (t_row_start >= g_row_end ? 'bottom' : 'none')
    return [h, v]
}
function direction2image_text(image_sep, item_sep) {
    return _.join(direction2image(image_sep, item_sep), "_")
}
function direction2classes(direction) {
    switch (direction) {
        case "left_top": return 'justify-content-end align-items-end'
        case "none_top": return 'justify-content-center align-items-end'
        case "right_top": return 'justify-content-start align-items-end'

        case "left_bottom": return 'justify-content-end align-items-start'
        case "none_bottom": return 'justify-content-center align-items-start'
        case "right_bottom": return 'justify-content-start align-items-start'

        case "left_none": return 'justify-content-end align-items-center'
        case "right_none": return 'justify-content-start align-items-center'

    }
}
export function getGridItemsHtml(option) {
    const { image_area, item_areas } = option
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    const layout_width = slide_width - MARGIN
    const layout_height = slide_height - MARGIN
    const CELL_COUNT = 7
    const cell_width = layout_width / CELL_COUNT
    const cell_height = layout_height / CELL_COUNT
    return `<div class="border border-primary d-flex flex-column align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="k position_size guanxi_title my-3" style="font-size:${TITLE_SIZE}px">${TITLE_HOLDER}</div>
            <div class="guanxi_layout" style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;grid-template-columns:repeat(${CELL_COUNT},${cell_width}px); grid-template-rows: repeat(${CELL_COUNT},${cell_height}px);">
                <img class="k position_size image_area w-100 h-100 position_size p-3" style="grid-area:${image_area}" src='/img/office/shapes/side_by_side/item6/1.svg'/>
                ${_.join(_.map(item_areas, (item_area, index) => {
        const direction = direction2image_text(image_area, item_area)
        const classes = direction2classes(direction)
        return `<div class="d-flex ${classes} overflow-auto " style="grid-area:${item_area};">
                        <div class="k position_size item_area item_area_${index}" style="background-color:red;height:42px;width:136px">${SUB_TITLE_HOLDER}</div>
                    </div>`
    }), "")}
            </div>
        </div>`;
}