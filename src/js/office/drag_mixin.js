import { officeState } from '@/js/office/office_state'
import { MODE_DESIGN } from '@/js/ppt'
import '@/js/jquery-ui/jquery-ui.js'
export const dragMixin = {
    props: {
        mode: { type: Number, default: MODE_DESIGN, required: false },
    },
    computed: {
        is_design() { return this.mode === MODE_DESIGN },
        slide_width: () => officeState.getters.slide_width,
        slide_height: () => officeState.getters.slide_height,
    },
    methods: {
        getUIJquery() {
            return $(this.format_container())
        },
        uiChanged(ui) {
            this.style = _.merge({}, this.style, ui)
        },
        dragStart() {
            officeState.state.dragging_info = {
                drag_index: this.getPPTItemIndex(),
                dragging: true
            }
        },
        dragStop() {
            _.set(officeState.state.dragging_info, ["dragging"], false)
        },
        enableUIOperation() {
            const outer_this = this
            this.getUIJquery().draggable({
                distance: 10,
                handle: ".move_handle",
                cursor: "crosshair",
                containment: "parent",
                drag: function (event, ui) {
                    const { helper, position } = ui
                    const size = { width: `${helper.width()}px`, height: `${helper.height()}px` }
                    const style = _.merge({}, size, position)
                    outer_this.xywhChanged(style)
                },
                start: this.dragStart,
                stop: this.dragStop
            }).resizable({
                delay: 250,
                autoHide: true,
                containment: "parent",
                grid: [8, 6],
                handles: "all",
                maxHeight: this.slide_height,
                maxWidth: this.slide_width,
                stop: function (event, { position, size }) {
                    outer_this.xywhChanged({ ...position, ...size })
                }
            })
        },
        xywhChanged({ left, top, width, height }) {
            this.uiChanged({ left: `${left}px`, top: `${top}px`, width: `${width}px`, height: `${height}px` })
        },
    },
    mounted() {
        if (this.is_design)
            this.enableUIOperation()
    },
}