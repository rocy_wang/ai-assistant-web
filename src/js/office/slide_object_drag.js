export const slideObjectDragMixin = {
    props: {
        id: {
            type: String,
            required: false
        },
    },
    data() {
        return {
            in_drag_range_state: false,
            drag_range_callback: undefined,
            drag_track_callback: undefined,
            drag_range_state_xywh: undefined,
            drag_range_state_down_point: undefined,
            drag_range_state_track_points: [],
            in_drag_range_state_mousedown: false,
        }
    },
    watch: {
        in_drag_range_state(val) {
            const container = $(this.$refs.format_container)
            if (val === true) {
                container.on("mousedown", this.mouseDragDown)
                container.on("mousemove", this.mouseDragMove)
                container.on("mouseup", this.mouseDragUp)
            } else {
                container.off("mousedown", this.mouseDragDown)
                container.off("mousemove", this.mouseDragMove)
                container.off("mouseup", this.mouseDragUp)
            }
        }
    },
    methods: {
        slide_obj() {
            return this.$refs.format_container
        },
        mouseDragUp(event) {
            if (this.in_drag_range_state !== true) return
            if (this.in_drag_range_state_mousedown !== true) return
            const c = _.cloneDeep(this.drag_range_state_xywh)
            this.drag_range_state_xywh = undefined
            this.in_drag_range_state_mousedown = false
            this.drag_range_callback && this.drag_range_callback(c)
        },
        mouseDragMove(event) {
            if (this.in_drag_range_state !== true) return
            if (this.in_drag_range_state_mousedown !== true) return
            const { pageX: nowPageX, pageY: nowPageY } = event
            this.drag_range_state_track_points.push(_.pick(event, ["pageX", "pageY"]))
            const { pageX, pageY } = this.drag_range_state_down_point
            const change = { old: this.drag_range_state_down_point, new: event, offset: { left: nowPageX - pageX, top: nowPageY - pageY } }
            if (this.drag_range_callback !== undefined) {
                const { left, top } = $(this.$refs["format_container"]).offset()
                const new_x = Math.min(pageX, nowPageX) - left
                const new_y = Math.min(pageY, nowPageY) - top
                const width = Math.abs(pageX - nowPageX)
                const height = Math.abs(pageY - nowPageY)
                this.drag_range_state_xywh = { left: `${new_x}px`, top: `${new_y}px`, width: `${width}px`, height: `${height}px` }
            }
            if (this.drag_track_callback !== undefined) this.drag_track_callback(this.drag_range_state_track_points)
        },
        mouseDragDown(event) {
            if (this.in_drag_range_state !== true) return
            this.in_drag_range_state_mousedown = true
            const { pageX, pageY } = event
            this.drag_range_state_down_point = { pageX, pageY }
            this.drag_range_state_track_points = [{ pageX, pageY }]
        },
    }
}