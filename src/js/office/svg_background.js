import { getRandomImage } from '@/js/office/illustration.js'
import { get_default } from '../lodash_util';
export const background_type_pure = "pure"
export const background_type_radial = "radial"
export const background_type_linear2 = "linear2"
export const background_type_linear3 = "linear3"
export const positions = { "left-top": "左上", "right-top": "右上", "left-bottom": "左下", "right-bottom": "右下", "center": "居中", "left": "左侧", "top": "上侧", "right": "右侧", "bottom": "下侧" }
export const position_values = Object.keys(positions)
export const position_values_sides = ["top", "bottom", "left", "right"]
export const position_corners = ["left-top", "right-top", "left-bottom", "right-bottom"]
export const LOGO_POS = "right-top"
export const LOGO_SIZE = 1 / 10
const opposites = { "top": "bottom", "bottom": "top", "left": "right", "right": "left" }
export function opposite(position) {
    return opposites[position]
}
export function iconRangesForSize(width, height, icon_size) {
    const ranges = _.mapValues(positions, (name, pos) => get_position(pos, icon_size, width, height))
    return _.mapValues(ranges, ({ selected_icon_x, selected_icon_y, selected_icon_width, selected_icon_height }) => ({
        x: selected_icon_x, y: selected_icon_y, width: selected_icon_width, height: selected_icon_height
    }))
}
export function iconRanges(width, height, percent) {
    const wh = Math.min(width, height)
    const icon_size = _.toInteger(wh * percent)
    return iconRangesForSize(width, height, icon_size)
}
export function pointInRange({ x, y }, { x: rx, y: ry, width, height }) {
    return x >= rx && x <= rx + width && y >= ry && y <= ry + height
}
export function steps(start, length) {
    const count = 5
    const step = length / count
    return _.map(_.range(0, count + 1), idx => _.toInteger(start + step * idx))
}
export function pointsForRange({ x, y, width, height }) {
    return _.flatMap(steps(x, width), xx => _.map(steps(y, height), yy => ({ x: xx, y: yy })))
}
export function is_rect_intersect([x01, x02, y01, y02], [x11, x12, y11, y12]) {
    const zx = Math.abs(x01 + x02 - x11 - x12);
    const x = Math.abs(x01 - x02) + Math.abs(x11 - x12);
    const zy = Math.abs(y01 + y02 - y11 - y12);
    const y = Math.abs(y01 - y02) + Math.abs(y11 - y12);
    return (zx <= x && zy <= y)
}
function range2arr(range) {
    return [range.x, range.x + range.width, range.y, range.y + range.height]
}
export function inter(range1, range2) {
    range1 = object_range_convert(range1)
    range2 = object_range_convert(range2)
    return is_rect_intersect(range2arr(range1), range2arr(range2))
}
export function object_range_convert({ x, y, left, top, width, height }) {
    function c(v) { return v === undefined ? 0 : parseInt(v) }
    return { x: c(x || left), y: c(y || top), width: c(width), height: c(height) }
}
export function iconRangesNoObject(width, height, object_ranges) {
    object_ranges = _.map(object_ranges, object_range_convert)
    const icon_sizes = [1 / 4, 1 / 2]
    const result = _.map(icon_sizes, s => {
        let icon_ranges = iconRanges(width, height, s)
        icon_ranges = _.map(icon_ranges, (value, pos) => ({ pos, ...value }))
        let rs = _.differenceWith(icon_ranges, object_ranges, (i, o) => inter(i, o))
        return [s, _.map(rs, ({ pos }) => pos)]
    })
    return _.reject(result, ([size, positions]) => positions.length === 0)
}

export const position_values_no_sides = _.difference(position_values, position_values_sides)
export const background_types = [background_type_pure, background_type_radial, background_type_linear2, background_type_linear3]

export const SIZE_NAMES = {
    "很小": 1 / 16,
    "稍小": 1 / 8,
    "正常": 1 / 4,
    "稍大": 1 / 2,
    "最大": 1,
}

export const DEFAULT_ICON_SIZE = 1 / 4

export function randomNumberExclude(max, min) {
    const _min = min || 0
    const v = _min + Math.random() * (max - _min)
    return _.toInteger(v)
}
export function randomFloat(min, max) {
    const v = Math.random() * (max - min)
    return v + min
}
export function randomColor(color_type, min_opaticy) {
    const red = color_type === COLOR_TYPE_RED ? 255 : randomNumberExclude(color_type === undefined ? 256 : 150)
    const green = color_type === COLOR_TYPE_GREEN ? 255 : randomNumberExclude(color_type === undefined ? 256 : 150)
    const blue = color_type === COLOR_TYPE_BLUE ? 255 : randomNumberExclude(color_type === undefined ? 256 : 150)
    return `rgba(${red},${green},${blue},${randomFloat(min_opaticy || 0.1, 0.8)})`
}
function randomBackground(color_type) {
    const _type = background_types[randomNumberExclude(background_types.length)]
    switch (_type) {
        case background_type_pure:
            return randomColor(color_type)
        case background_type_radial:
            return {
                c: {
                    c_x: `${randomNumberExclude(30, 80)}%`,
                    c_y: `${randomNumberExclude(30, 80)}%`,
                    c_r: `${randomNumberExclude(30, 50)}%`,
                    c_color: randomColor(color_type),
                    c_opacity: randomFloat(0.1, 0.8)
                }, f: {
                    f_x: `${randomNumberExclude(30, 80)}%`,
                    f_y: `${randomNumberExclude(30, 80)}%`,
                    f_r: `${randomNumberExclude(30, 80)}%`,
                    f_color: randomColor(color_type),
                    f_opacity: randomFloat(0.1, 0.8)
                }
            }
        case background_type_linear2:
            return _.map(_.range(0, 2), idx => ({
                x: `${randomNumberExclude(0, 100)}%`,
                y: `${randomNumberExclude(0, 100)}%`,
                color: randomColor(color_type),
                opacity: randomFloat(0.1, 0.8)
            }))
        case background_type_linear3:
            return _.map(_.range(0, 3), idx => ({
                x: `${randomNumberExclude(0, 100)}%`,
                y: `${randomNumberExclude(0, 100)}%`,
                color: randomColor(color_type),
                opacity: randomFloat(0.1, 0.8)
            }))
    }
}
export function randomInt(max) {
    return _.toInteger(Math.random() * max)
}
export const COLOR_TYPE_RED = "red"
export const COLOR_TYPE_GREEN = "green"
export const COLOR_TYPE_BLUE = "blue"
export const COLOR_TYPES = {
    [COLOR_TYPE_RED]: "红",
    [COLOR_TYPE_GREEN]: "绿",
    [COLOR_TYPE_BLUE]: "蓝",
}
export const COLOR_TYPES_REVERT = _.invert(COLOR_TYPES)
export const SVG_IMAGE_ID = "svg_image_id"
const clip_pathes = [
    "clip-path: circle(50%);",
    "clip-path: ellipse(40% 50%);",
    `clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);`
]
export function get_position(pos, selected_icon_size, width, height) {
    let selected_icon_x = 0;
    let selected_icon_y = 0;
    let selected_icon_width = selected_icon_size
    let selected_icon_height = selected_icon_size
    switch (pos) {
        case "left":
            selected_icon_height = height
            break;
        case "top":
            selected_icon_width = width
            break;
        case "right":
            selected_icon_x = width - selected_icon_width
            selected_icon_height = height
            break;
        case "bottom":
            selected_icon_y = height - selected_icon_height
            selected_icon_width = width
            break;
        case "left-top":
            break;
        case "right-top":
            selected_icon_x = width - selected_icon_width
            break;
        case "left-bottom":
            selected_icon_y = height - selected_icon_height
            break;
        case "right-bottom":
            selected_icon_x = width - selected_icon_width
            selected_icon_y = height - selected_icon_height
            break;
        case "center":
            selected_icon_x = width / 2 - selected_icon_size / 2
            selected_icon_y = height / 2 - selected_icon_size / 2
            break;
    }
    return ({
        selected_icon_x, selected_icon_y, selected_icon_width, selected_icon_height
    })
}
export function appendLogo(setting, image) {
    get_default(setting, ["icons"], []).push({
        image,
        position: LOGO_POS,
        size: LOGO_SIZE,
        props: { opacity: 1, margin: 3 }
    })
}
export function autoSVGBackgroundSetting(width, height, category, size, color_type, icon_position, preserve_ratio) {
    const no_icon = size === undefined && icon_position === undefined
    const icons = no_icon ? [] : [{
        image: getRandomImage(category),
        position: icon_position || position_values_no_sides[randomInt(position_values_no_sides.length)],
        size: size || DEFAULT_ICON_SIZE,
        preserve_ratio: preserve_ratio || "none",
        props: {
            opacity: 1,//randomFloat(0.8, 1),
            style: clip_pathes[randomInt(clip_pathes.length)],
        }
    }]
    return {
        is_share: false,
        canvas: {
            width: width || 800,
            height: height || 600,
            background: randomBackground(color_type),
        },
        icons
    }
}