export const categories = ["标准", "封面", "目录", "章节", "结束", "关系", "图文", "文本", "图表", "步骤", "弹出", "折叠", "切换", "时间", "动画"]
export const scenes = ["总结", "企业", "教育", "民生", "庆典", "职场"]
export const relation_scenes = ["并列", "总分", "流程", "循环", "金字塔", "时间轴"]
export const image_text_scenes = ["横向图区", "竖向图区", "大图片", "小图留白", "多图拼图", "图文均分"]

export const GENERATE_SCENE = "企业"
