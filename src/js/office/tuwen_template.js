import { containerSize, containerWrap, imageDiv, subTitleDiv, textDiv, titleDiv, wrapDiv } from "./template_constants"

export const TUWEN_CATEGORIES = ["横向", "纵向", "大图"]
export const TUWEN_COUNT_MAX = 4
export function generateTuWenBigHtml(count, is_horizontal) {
    is_horizontal = is_horizontal === true
    return containerWrap(true, undefined, "align-items-stretch", `
        ${titleDiv("w-100")}
        ${wrapDiv(`${is_horizontal ? 'flex-row' : 'flex-column'} flex-grow-1 flex-wrap justify-content-center`, ..._.map(_.range(0, count), idx => {
            return imageDiv(`${count===1 ? 'w-100 h-100' : 'w-50 h-50'} image_${idx}`)
    }
    ))}
    `)
}
export function generateTuWenHtmlSwitch(count, _switch, is_horizontal) {
    is_horizontal = is_horizontal === true
    return containerWrap(true, undefined, "align-items-stretch", `
        ${titleDiv("w-100")}
        ${wrapDiv(`${is_horizontal ? 'flex-row' : 'flex-column'} flex-grow-1`, ..._.map(_.range(0, count), idx => {
        const direction = is_horizontal ? "flex-column" : "flex-row"
        const tmp = (_.toInteger(idx % 2) === 0 ? direction : `${direction}-reverse`)
        const s = (_switch === true ? tmp : direction)
        const title = wrapDiv(`flex-column ${is_horizontal ? "w-100" : "w-50"} align-items-stretch`, subTitleDiv(`sub_title_${idx}`), textDiv(`text_${idx} flex-grow-1`))
        const image = imageDiv(`${is_horizontal ? "w-100" : "w-50"} flex-grow-1 image_${idx}`)
        return wrapDiv(`${s} flex-grow-1`, title, image)
    }
    ))}
    `)
}

export function generateTuWenHtmls(shape, count) {
    switch (shape) {
        case "横向": return [
            generateTuWenHtmlSwitch(count, true, true),
            generateTuWenHtmlSwitch(count, false, true),
        ]
        case "纵向": return [
            generateTuWenBigHtml(count, true, false),
            generateTuWenBigHtml(count, false, false),
        ]
        case "大图":return [
            generateTuWenBigHtml(count,true),
            generateTuWenBigHtml(count,false),
        ]
    }
}