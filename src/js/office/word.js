

import { v4 as uuid4 } from 'uuid';
import { sample_base64, sample_table_data } from "@/js/data.js";
import _, { head } from 'lodash';
export const ITEM_TYPE_HEADER = "header"
export const ITEM_TYPE_PARAGRAPH = "paragraph"
export const ITEM_TYPE_IMAGE = "image"
export const ITEM_TYPE_CHART = "chart"
export const ITEM_TYPE_TABLE = "table"
export const ITEM_TYPE_VIDEO = "video"
export const ITEM_TYPE_RICH = "rich"
export const ITEM_TYPE_COLLAPSE = "carousel"
export const ITEM_TYPE_TABS = "tabs"
export const ITEM_TYPE_POPUP = "popup"
export const ITEM_TYPE_STEPS = "steps"
export const ITEM_TYPE_TIMELINE = "timeline"
export const TYPE_SLD_LAYOUT_TITLE_WORD = 101
export const TYPE_SLD_LAYOUT_CONTENT_WORD = 102
export const sample_word_data_empty = []



const INCH_PIXES = 96
const INCH_MM = 25.4
const MM_PIXES = INCH_PIXES / INCH_MM
export const PAGE_A4 = "A4"
export const PAGE_B5 = "B5"
export const PAGES = [PAGE_A4, PAGE_B5]
export const A4_SIZE = {
    height: 297 * MM_PIXES,
    width: 210 * MM_PIXES
}

export const B5_SIZE = {
    height: 257 * MM_PIXES,
    width: 182 * MM_PIXES
}

export function getPageSize(size) {
    switch (size) {
        case PAGE_A4: return A4_SIZE
        case PAGE_B5: return B5_SIZE
    }
}

export function sample_word() {
    return {
        filename: "AI Word示例演示",
        slides: JSON.stringify(sample_slides_word())
    }
}


function getContentIndex(word_slide_data, content) {
    return content ? _.findIndex(word_slide_data, item => item["id"] === content["id"]) : -1
}

export function addSibling(word_slide_data, content, new_content, is_before) {
    const content_index = getContentIndex(word_slide_data, content)
    word_slide_data.splice(is_before ? content_index : content_index + 1, 0, {
        type: ITEM_TYPE_HEADER,
        id: get_item_id(),
        parent_id: content["parent_id"],
        text: new_content
    })
    return sort_items(word_slide_data)
}

export function addChildren(word_slide_data, content, children) {
    children = _.reject(children, _.isEmpty)
    const old_children = _.filter(word_slide_data, item => item["parent_id"] === content["id"])
    const old_children_texts = _.map(old_children, child => child["text"])
    const new_children = _.reject(children, child => old_children_texts.includes(child))
    const last_child_index = _.findLastIndex(word_slide_data, child => child["parent_id"] === content["id"])
    const content_index = getContentIndex(word_slide_data, content)
    const insert_index = (last_child_index === -1 ? content_index : last_child_index) + 1
    word_slide_data.splice(insert_index, 0, ..._.map(new_children, child => {
        return ({
            type: ITEM_TYPE_HEADER,
            id: get_item_id(),
            parent_id: content && content["id"],
            text: child
        })
    }))
    return sort_items(word_slide_data)
}

export function levelContent(word_slide_data, current, is_up, is_before) {
    const current_obj_index = getContentIndex(word_slide_data, current)
    if (is_up === true) {
        const deleted_items = word_slide_data.splice(current_obj_index, 1)
        const parent_obj = _.find(word_slide_data, item => item["id"] === current["parent_id"])
        const parent_obj_index = getContentIndex(word_slide_data, parent_obj)
        const new_index = parent_obj ? (is_before ? parent_obj_index : parent_obj_index + 1) : 0
        word_slide_data.splice(new_index, 0, ..._.map(deleted_items, deleted => _.assign({}, deleted, { parent_id: parent_obj && parent_obj["parent_id"] })))
    } else {
        const sibling_obj = _.find(word_slide_data, (item, index) => item["parent_id"] === current["parent_id"] && item["type"] === ITEM_TYPE_HEADER && index >= current_obj_index - 1 && index !== current_obj_index)
        const delete_items = word_slide_data.splice(current_obj_index, 1)
        const sibling_obj_index = getContentIndex(word_slide_data, sibling_obj)
        word_slide_data.splice(sibling_obj_index + 1, 0, ..._.map(delete_items, deleted => _.assign({}, deleted, { parent_id: _.get(sibling_obj, "id") })))
    }
    return sort_items(word_slide_data)
}
export function moveContent(event) {
    let { word_slide_data, from_content, to_content, is_before } = event
    const delete_items = word_slide_data.splice(getContentIndex(word_slide_data, from_content), 1)
    const to_content_index = getContentIndex(word_slide_data, to_content)
    word_slide_data.splice(is_before ? to_content_index : to_content_index + 1, 0, ..._.map(delete_items, item => _.assign({}, item, {
        parent_id: to_content["parent_id"]
    })))
    return sort_items(word_slide_data)
}

export const empty_paragraph = {
    type: ITEM_TYPE_PARAGRAPH,
    text: undefined
}
export function get_item_id() {
    return `word_${uuid4()}`
}
export const sample_word_data = [{
    type: ITEM_TYPE_HEADER,
    id: "0",
    text: "项目概述"
}, {
    type: ITEM_TYPE_PARAGRAPH,
    id: "0.1",
    parent_id: "0",
    text: "主要目标是为东风汽车集团股份有限公司技术中心（以下简称技术中心）新能源汽车的三电部件数据开展分析，实现针对新能源车辆的行驶及动力系统工作工况分析、动力电池分析、电机工作性能分析等，解决研发部门在进行三电部件性能及系统匹配分析时缺少真实车主用车的数据支撑和因为过设计或欠设计导致的研发成本高或车辆质量问题等难题。基于车联网大数据实现上市车辆三电核心零部件及相关信号的数据实时监控管理、数据统计分析以及数据可视化展示等功能，用于指导新能源车型的设计和研发，为故障排查提供数据支持。"
}, {
    type: ITEM_TYPE_HEADER,
    id: "1",
    text: "项目需求"
}, {
    type: ITEM_TYPE_HEADER,
    id: "2",
    parent_id: "1",
    text: "总体需求概述"
}, {
    type: ITEM_TYPE_PARAGRAPH,
    id: "3",
    parent_id: "2",
    text: `动力蓄电池、电机、电控等作为新能源汽车的核心部件，对新能源汽车的安全使用、价值评估方面有重大的影响。针对新能源车辆的实际行驶及动力系统工作工况分析、动力电池分析、电机工作特性分析等，构建以用户为中心的新能源汽车用车工况及核心零部件性能评估体系，研发部门可精准识别性能需求，优化定义下一代车型性能指标。
    项目主要建设目标：
    1）通过车辆行驶及动力系统工况分析，指导研发部门优化部件性能目标和系统效率、部件效率设计，建立系统、各部件实际使用工况模型并建立对应的验证工况，合理优化部件及系统的设计；
    2）动力电池分析提取电池工况特征及对应的工作特性，评估电池状态，电池潜在故障隐患、提前预警维修，提炼电池使用状态，对电池一致性、寿命等性能状态进行预估；
    3）电机工作特性分析，指导研发进行电机最优动力区间调校，为部件选型和设计提供参考依据；
    4）基本异常报警分析，提取真实用车数据用于开发阶段的设计、试验和仿真输入，快速识别异常爆发原因指导解决方案；
    数据分析挖掘功能均需提供二次开发功能，可对算子或参数进行动态调整，对外提供大数据服务接口。`
}, {
    type: ITEM_TYPE_IMAGE,
    id: "4.1",
    parent_id: "1",
    base64: [sample_base64]
},
{
    type: ITEM_TYPE_TABLE,
    id: "4.2",
    parent_id: "1",
    data: sample_table_data
},
{
    type: ITEM_TYPE_HEADER,
    id: "4",
    parent_id: "1",
    text: "需求总体分类"
}, {
    type: ITEM_TYPE_HEADER,
    id: "5.1",
    parent_id: "1",
    text: "业务需求详细描述"
}, {
    type: ITEM_TYPE_HEADER,
    id: "6",
    parent_id: "4",
    text: "基本数据分析"
}, {
    type: ITEM_TYPE_HEADER,
    id: "7",
    parent_id: "6",
    text: "需求目标"
}, {
    id: "8",
    parent_id: "7",
    type: ITEM_TYPE_PARAGRAPH,
    text: `（1）实时监控车辆运行状态，及时处理故障问题
    （2）掌握故障发生的总体情况，优化部件设计`
}, {
    type: ITEM_TYPE_HEADER,
    id: "8.1",
    parent_id: "7",
    text: "需求功能内容描述"
}, {
    id: "8.2",
    parent_id: "8",
    type: ITEM_TYPE_PARAGRAPH,
    text: `（1）不同地区、季节电机、电池历史故障频次及占比情况
    （2）电机、电池实时故障显示，并统计不同地区、季节故障数占比情况
    （3）统计不同地区、季节车辆总数、故障总数以及电机、电池故障占比情况`
}, {
    id: "9",
    parent_id: "7",
    type: ITEM_TYPE_HEADER,
    text: "基本业务实现逻辑"
}, {
    id: "10",
    parent_id: "9",
    type: ITEM_TYPE_PARAGRAPH,
    text: `（1）可以选择地区、季节、时段，统计电机、电池发生故障的频次，以及占总故障数占比
    （2）实时显示当前时刻电机、电池、发生故障的情况，可以选择地区、季节，分别统计电机、电池的总数，以及发生故障电机、电池数量与部件总数的比值
    （3）可以选择地区、季节，统计车辆的总数、故障发生的总次数，统计电机、电池故障总数的比值`
}, {
    type: ITEM_TYPE_HEADER,
    id: "11",
    parent_id: "6",
    text: "数据输入"
}, {
    id: "12",
    parent_id: "11",
    type: ITEM_TYPE_PARAGRAPH,
    text: `（1）当前时间
    （2）电机故障
    （3）电机故障码
    （4）电池故障
    （5）电池故障码
    （6）OBC/DCDC故障
    （7）车型
    （8）当前时间
    （9）系统号
    （10）经度
    （11）维度
    （12）车辆运行状态
    （13）ONC故障
    （14）VCU故障
    （15）TCU故障
    （16）空压机故障
    （17）绝缘故障
    （18）助力转向故障
    （19）ABS故障
    （20）冷却系统故障
    （21）总里程`
}
]
export const slide_title_sample_word = {
    layout: TYPE_SLD_LAYOUT_TITLE_WORD,
    data: {
        title: "AI PPT Example",
        author: "若馨科技",
        date: new Date().toLocaleDateString()
    }
}
export const slide_content_sample_word = {
    layout: TYPE_SLD_LAYOUT_CONTENT_WORD,
    data: sample_slides_word_split()
}
export const slide_content_sample_word_empty = {
    layout: TYPE_SLD_LAYOUT_CONTENT_WORD,
    data: sample_word_data_empty
}
export const only_title_slides_word = [slide_title_sample_word, slide_content_sample_word_empty]
export function sample_slides_word() {
    return [slide_title_sample_word, slide_content_sample_word]
}
export function sample_slides_word_split() {
    return sample_word_data
}
export function item2header(item) {
    return (item.type === ITEM_TYPE_HEADER) ? `${_.join(item.numbers, ".")} ` : " "
}
export function data2anchor(item) {
    return _.join(_.get(item, 'numbers'), ".")
}
export function sort_items(content_items) {
    if (_.isNil(content_items)) return []
    const rs = []
    const nodes = _.filter(content_items, item => _.isNil(item["parent_id"]))
    function loop(nodes) {
        _.forEach(nodes, node => {
            rs.push(node)
            const children = _.filter(content_items, item => item["parent_id"] === node["id"])
            loop(children)
        })
    }
    loop(nodes)
    return rs
}
export function data2contents_tree(data) {
    const headers = _.filter(data, item => _.get(item, "type") === ITEM_TYPE_HEADER)
    const level_one = _.filter(headers, node => _.isNil(node["parent_id"]))
    function loop(parent, items) {
        _.forEach(items, (item, index) => {
            item["numbers"] = _.concat(parent ? parent['numbers'] : [], [index + 1])
            item["label"] = `${_.join(item["numbers"], ".")} ${item["text"]}`
            item["title"] = item["text"]
            item.children = _.filter(headers, header => header["parent_id"] === item["id"])
            loop(item, item.children)
        })
    }
    loop(undefined, level_one)
    return level_one
}
function addNumber(root) {
    const { parent, children } = root
    const index_in_parent = _.findIndex(_.get(parent, ["children"]), child => _.isEqual(child, root))
    _.set(root, ["index_in_parent"], index_in_parent)
    _.forEach(children, addNumber)
}
function addNumbers(root) {
    function getNumbers(r) {
        const { parent, numbers, index_in_parent } = r
        if (numbers !== undefined) return numbers
        if (parent === undefined) return [index_in_parent]
        return _.concat(getNumbers(parent), index_in_parent)
    }
    if (root['numbers'] === undefined) {
        root['numbers'] = getNumbers(root)
        root['numbers_text'] = _.join(_.map(_.reject(root['numbers'], n => n === -1), n => n + 1), ".")
    }
    _.forEach(root["children"], addNumbers)
}
export const test_data = [
    { level: 1, header: "t1" },
    { level: 2, header: "t2" },
    { level: 3, header: "t3" },
    { level: 6, header: "t6" },
    { level: 2, header: "t22" },
    { level: 1, header: "t11" },
]
export function data2contents_tree_ai(data) {
    const root = { level: 0, header: "目录", children: [] }
    let prev_added = root
    function findRecentLevel(prev, level) {
        if (prev === undefined) return undefined
        if (prev.level <= level) return prev
        else return findRecentLevel(prev.parent, level)
    }
    _.forEach(data, (header) => {
        const { level } = header
        const parent = findRecentLevel(prev_added, level - 1)
        const new_add = { ...header, children: [], parent }
        if (parent === undefined) root.children.push(new_add)
        else parent.children.push(new_add)
        prev_added = new_add
    })
    addNumber(root)
    addNumbers(root)
    return root.children
}
