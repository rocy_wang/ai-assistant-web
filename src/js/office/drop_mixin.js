import { officeState } from '@/js/office/office_state'
import { MODE_DESIGN } from '@/js/ppt'
import _ from 'lodash'
import { isAnyEmpty } from '../lodash_util'
const drop_classes = ["grand", "father", "child"]
export const dropMixin = {
    props: {
        mode: { type: Number, default: MODE_DESIGN, required: false },
    },
    computed: {
        is_design() { return this.mode === MODE_DESIGN },
        slide_width: () => officeState.getters.slide_width,
        slide_height: () => officeState.getters.slide_height,
    },
    methods: {
        getUIJquery() {
            if (isAnyEmpty(this.$refs.grand)) return undefined
            const parent = $(this.$refs.grand.$el)
            const sub = $(".father", parent)
            const children = $(".child", sub)
            return parent.add(sub).add(children)
        },
        uiChanged(ui) {
            this.style = _.merge({}, this.style, ui)
        },
        findChild(classes) {
            const child = _.find(classes, c => _.startsWith(c, "child_"))
            if (_.isNil(child)) return undefined
            return _.map(_.split(child.substring("child_".length), "_"), idx => _.isInteger(idx) ? _.toInteger(idx) : idx)
        },
        findFather(classes) {
            const father = _.find(classes, c => _.startsWith(c, "father_"))
            if (_.isNil(father)) return undefined
            return _.toInteger(father.substring("father_".length))
        },
        isInContainer(event) {
            const classes = getTargetClasses(event)
            return _.intersection(classes, drop_classes).length > 0
        },
        addDroppedObject() {
            throw new Error("addDropObject not implemented")
        },
        drop(event) {
            this.getUIJquery().removeClass("my_drop_over")
            if (_.isNil(officeState.state.dragging_info)) return
            const classes = getTargetClasses(event)
            const child_location = this.findChild(classes)
            const slide = this.findParentSlide()
            const drop_index = this.getPPTItemIndex()
            const drag_index = _.get(officeState.state.dragging_info, ["drag_index"])
            _.invoke(slide, ["addDroppedObject"], { drag_index, drop_index, child_location })
        },
        enableUIOperation() {
            if (isAnyEmpty(this.getUIJquery())) return
            this.getUIJquery().droppable({
                greedy: true,
                over: (event, ui) => this.isInContainer(event) && $(event.target).addClass("my_drop_over"),
                out: (event, ui) => this.isInContainer(event) && $(event.target).removeClass("my_drop_over"),
                drop: (event, ui) => this.isInContainer(event) && this.drop(event),
            })
        }
    },
    mounted() {
        if (this.is_design) {
            this.enableUIOperation()
        }
    },
}

function getTargetClasses(event) {
    return $(event.target).attr("class").split(" ")
}
