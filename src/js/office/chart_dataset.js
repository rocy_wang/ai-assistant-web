import _, { result } from "lodash"
import { renameKey } from '@/js/lodash_util.js'

export const SOURCE_TYPE_2DIM = "二维数组"
export const SOURCE_TYPE_1DIM = "一维数组"
export const SOURCE_TYPE_OBJECT_ARR = "对象数组"
export const SOURCE_TYPE_COLUMN = "列key-value "

const common = {
    legend: { textStyle: { fontSize: 6 } },
    title: { textStyle: { fontSize: 12 } },
    toolbox: {
        show: true, feature: {
            saveAsImage: {},
            dataView: {},
            dataZoom: {},
            magicType: {
                show: true,
                type:['line','bar','stack']
            },
        }
    },
    tooltip: { trigger: 'item' },
}
const common_series = {
    label: {
        show: true
    }
}
const COMMON_MAP = {
    "grid": { xAxis: { axisLabel: { fontSize: 6 } } },
}
const SERIES_COMMON_MAP = {
    "pie": { itemStyle: { borderRadius: 10, borderColor: '#fff', borderWidth: 2 }, label: { position: 'center' }, emphasis: { label: { show: true, fontSize: 20, fontWeight: 'bold' } }, labelLine: { show: false }, },
}
function merge_common_series(option) {
    const series = _.get(option, "series")
    function m(type) {
        return _.merge({}, common_series, _.get(SERIES_COMMON_MAP, type))
    }
    const to_merge = _.isArray(series) ? _.map(series, ({ type }) => m(type)) : common_series
    return _.merge({}, option, { series: to_merge })
}
export function merge_common(option) {
    const cordinate = get_cordinate(option)
    option = _.merge({}, option, common, _.get(COMMON_MAP, cordinate))
    return merge_common_series(option)
}


export function updateData(_chart_option, { column_data, column_added, column_deleted }) {
    let series = _.get(_chart_option, ["series"])
    if (series_is_type(_chart_option, "bar3D")) return source2bar3d(column_data, _chart_option)
    if (series_is_type(_chart_option, "themeRiver")) return source2river(column_data, _chart_option)
    if (series_is_type(_chart_option, "gauge")) return source2gauge(column_data, _chart_option)
    if (series_is_type(_chart_option, "funnel")) return source2funnel(column_data, _chart_option)
    const is_sankey = _.find(series, s => s["type"] === "sankey") !== undefined
    if (is_sankey) return source2sankey(column_data, _chart_option)
    const is_tree = _.find(series, s => s["type"] === "tree" || s["type"] === "treemap" || s["type"] === "sunburst") !== undefined
    if (is_tree) return column2tree(column_data, _chart_option)
    const is_heatmap = _.find(series, s => s["type"] === "heatmap") !== undefined
    if (is_heatmap) return source2heat(column_data, _chart_option)
    const is_boxplot = _.find(series, ({ type }) => type === "boxplot") !== undefined
    if (is_boxplot) {
        const arr2dim = _.map(column_data["value"], (join) => _.split(join, ","))
        _.set(_chart_option, ["dataset", 0, "source"], arr2dim)
        return _chart_option
    } else {
        const cordinate = get_cordinate(_chart_option)
        switch (cordinate) {
            case "radar":
                return column2radar(column_data, _chart_option)
            default:
                const source = { dataset: { source: column_data } }
                _chart_option = _.merge({}, _chart_option, source)
                deleteColumn(_chart_option, column_deleted)
                addColumn(_chart_option, column_added)
                return _chart_option
        }
    }
}

export function addColumn(_chart_option, add_column) {
    if (add_column === undefined) return
    const { old_name, new_name } = add_column
    let series = _.get(_chart_option, ["series"])
    if (_.isArray(series) === false) _.set(_chart_option, ["series"], [series])
    series = _.get(_chart_option, ["series"])
    let clone = _.cloneDeep(_.find(series, s => s["name"] === old_name))
    _.set(clone, ["name"], new_name)
    _.forEach(clone["encode"], (value, key) => {
        if (value === old_name) _.set(clone, ["encode", key], new_name)
    })
    series.push(clone)
}
export function deleteColumn(_chart_option, old_name) {
    if (old_name === undefined) return
    let series = _.get(_chart_option, ["series"])
    if (_.isArray(series) === false) {
        if (series["name"] === old_name) delete _chart_option["series"]
    } else if (_.isArray(series) === true) {
        const idx = _.findIndex(series, (s, index) => s["name"] === old_name)
        delete _chart_option["series"][idx]
    }
}
export function updateColumn(_chart_option, { old_name, new_name }) {
    renameKey(_chart_option, ["dataset", "source", old_name], ["dataset", "source", new_name])
    const dimension = _.get(_chart_option, ["visualMap", "dimension"])
    if (dimension === old_name) _.set(_chart_option, ["visualMap", "dimension"], new_name)
    else if (dimension.includes(old_name)) _.set(_chart_option, ["visualMap", "dimension"], _.map(dimension, d => d === old_name ? new_name : d))
    let series = _.get(_chart_option, ["series"])
    if (_.isArray(series) === false) series = [series]
    _.forEach(series, s => {
        if (s["name"] === old_name) _.set(s, ["name"], new_name)
        _.forEach(s["encode"], (value, key) => {
            if (value === old_name) _.set(s, ["encode", key], new_name)
        })
    })
}
export function judge_source_type(source) {
    if (_.isArray(source) && _.every(source, item => _.isArray(item))) return SOURCE_TYPE_2DIM
    else if (_.isArray(source)) return SOURCE_TYPE_OBJECT_ARR
    else if (_.isObject(source) && _.every(source, item => _.isArray(item))) return SOURCE_TYPE_COLUMN
    throw new Error(`judge_source_type ${source}`)
}
export function dim2column(source, has_header) {
    let header = _.head(source)
    if (has_header === false) header = _.map(header, (h, index) => `col${index}`)
    const result = {}
    _.forEach(_.tail(source), (row, row_index) => _.forEach(row, (item, col_index) => _.set(result, [header[col_index], row_index], item)))
    return { header, result }
}
export function column2dim(source) {
    return _.zip(..._.values(source))
}
export function object_arr2column(source, names_include) {
    if (judge_source_type(source) === SOURCE_TYPE_OBJECT_ARR) {
        const rs = {}
        _.forEach(source, (item, index) => _.forEach(item, (value, key) => _.set(rs, [key, index], value)))
        return names_include === undefined ? rs : _.pick(rs, names_include)
    } else throw new Error(`object_arr2column ${source}`)
}
export function column2object_arr(source) {
    if (judge_source_type(source) === SOURCE_TYPE_COLUMN) {
        const rs = []
        _.forEach(source, (arr, key) => _.forEach(arr, (item, index) => _.set(rs, [index, key], item)))
        return rs
    } else throw new Error(`column2object_arr ${source}`)
}
function row2columns(dim2arr) {
    const rs = []
    _.forEach(dim2arr, (arr, row_index) => {
        _.forEach(arr, (item, col_index) => {
            _.set(rs, [col_index, row_index], item)
        })
    })
    return rs
}
function extract_data(data) {
    if (data === undefined) return data
    switch (judge_source_type(data)) {
        case SOURCE_TYPE_2DIM: return row2columns(data)
        case SOURCE_TYPE_OBJECT_ARR: return [_.map(data, item => _.isObject(item) ? item["value"] : item)]
        default: throw new Error(`extract_data:${data}`)
    }
}
function extract_names(data) {
    if (data === undefined) return undefined
    if (_.isArray(data) && _.every(data, item => _.isObject(item) && _.has(item, "name"))) return _.map(data, item => _.get(item, "name"))
    return undefined
}
function findHasSource(option, names) {
    return _.find(names, name => _.get(option, ["dataset", "source", name]) !== undefined)
}
function setIfNotEmpty(obj, path, value) {
    if (_.isNil(value)) return
    _.set(obj, path, value)
}
export function processDataset(option) {
    let series = _.get(option, ["series"])
    option = _.cloneDeep(option)
    const is_boxplot = _.find(series, ({ type }) => type === "boxplot") !== undefined
    if (is_boxplot) {
        let source = _.first(_.get(option, ["dataset"]))["source"]
        const column_obj = {
            "name": _.map(_.range(0, source.length), i => `数据${i}`),
            "value": _.map(source, (s) => _.join(s, ","))
        }
        _.set(option, ["dataset", "source"], column_obj)
        return option
    } else {
        let source = _.get(option, ["dataset", "source"])
        const { header, result } = dim2column(source)
        _.set(option, ["dataset", "source"], result)
        const dimension = _.get(option, ["visualMap", "dimension"])
        if (_.isNumber(dimension)) _.set(option, ["visualMap", "dimension"], header[dimension])
        const x = _.head(header)
        const ys = _.tail(header)
        _.forEach(series, (s, index) => {
            _.set(s, ["name"], ys[index])
            _.set(s, ["encode"], { x, y: ys[index] })
        })
        return option
    }
}
function move(obj, from, to) {
    const v = _.get(obj, from)
    if (v === undefined) return
    _.set(obj, to, v)
    _.unset(obj, from)
}
export function get_cordinate(option) {
    return _.findKey({ grid: ["grid", "xAxis", "yAxis"], singleAxis: "singleAxis", polar: "polar", geo: "geo", radar: "radar" }, (value, key) => _.find(_.concat(value), item => _.has(option, item)) !== undefined)
}
function tree2source(option) {
    const rs = { name: [], parent: [], value: [] }
    function loop(arr, parent) {
        _.forEach(arr, ({ name, children, value }) => {
            rs["name"].push(name)
            rs["parent"].push(parent)
            rs["value"].push(value)
            loop(children || [], name)
        })
    }
    loop(_.get(option, ["series", 0, "data"]), undefined)
    _.set(option, ["dataset", "source"], rs)
    return option
}
function column2tree(column_obj, option) {
    const { name, value, parent } = column_obj
    const array = _.zip(name, value, parent)
    function loop(parent) {
        const filtered = _.filter(array, ([n, v, p]) => (parent === undefined) ? (p === undefined || _.isEmpty(_.trim(p))) : p === parent)
        return _.map(filtered, ([name, value]) => ({ name, value, children: loop(name) }))
    }
    _.set(option, ["series", 0, "data"], loop(undefined))
    return option
}
function series_is_type(option, type) {
    let series = _.get(option, "series")
    return _.find(series, s => s["type"] === type) !== undefined
}
export function convert2dataset(option) {
    option = _.cloneDeep(option)
    let series = _.get(option, "series")
    if (series_is_type(option, "bar3D")) return bar3d2source(option)
    if (series_is_type(option, "themeRiver")) return river2source(option)
    if (series_is_type(option, "gauge")) return gauge2source(option)
    if (series_is_type(option, "funnel")) return funnel2source(option)
    const is_sankey = _.find(series, s => s["type"] === "sankey") !== undefined
    if (is_sankey) return sankey2source(option)
    const is_tree = _.find(series, s => s["type"] === "tree" || s["type"] === "treemap" || s["type"] === "sunburst") !== undefined
    if (is_tree) return tree2source(option)
    const is_heatmap = _.find(series, s => s["type"] === "heatmap") !== undefined
    if (is_heatmap) return heat2source(option)
    if (_.get(option, ["dataset"]) !== undefined) return processDataset(option)
    const cordinate = get_cordinate(option)
    switch (cordinate) {
        case "grid":
            _.forEach(["xAxis", "yAxis"], axis => {
                let axis_list = _.get(option, [axis])
                if (axis_list !== undefined) {
                    if (_.isArray(axis_list) === false) move(option, [axis, "data"], ["dataset", "source", axis])
                    else _.forEach(axis_list, ({ type }, index) => move(option, [axis, index, "data"], ["dataset", "source", `${axis}${index === 0 ? "" : `_${index}`}`]))
                }
            })
            break
        case "polar":
            move(option, ["angleAxis", "data"], ["dataset", "source", "angleAxis"])
            move(option, ["radiusAxis", "data"], ["dataset", "source", "radiusAxis"])
            break
    }
    if (_.isObject(series) && _.isArray(series) === false) series = [series]
    _.forEach(series, (s, index) => {
        let { name, data, type } = s
        let serial_name = name || `series_${index + 1}`
        function setItemName() {
            let names = extract_names(data)
            if (names !== undefined) {
                _.set(option, ["dataset", "source", `${serial_name}_name`], names)
                _.set(s, ["encode", "itemName"], `${serial_name}_name`)
            }
        }
        if (cordinate !== "radar") setItemName()
        const columns = extract_data(data)
        switch (cordinate) {
            case "radar":
                _.set(option, ["dataset", "source"], radar2column(option))
                break
            case "grid":
                switch (type) {
                    case "":
                        break
                    case "candlestick":
                        const [p1, p3, min, max] = row2columns(data)
                        setIfNotEmpty(option, ["dataset", "source", `${serial_name}_p1`], p1)
                        setIfNotEmpty(option, ["dataset", "source", `${serial_name}_p3`], p3)
                        setIfNotEmpty(option, ["dataset", "source", `${serial_name}_min`], min)
                        setIfNotEmpty(option, ["dataset", "source", `${serial_name}_max`], max)
                        _.set(s, ["encode"], { x: "xAxis", y: [`${serial_name}_p1`, `${serial_name}_p3`, `${serial_name}_min`, `${serial_name}_max`] })
                        const type_set = _.get(option, ["xAxis", "type"]) || _.get(option, ["yAxis", "type"])
                        if (type_set === undefined) _.set(option, ["xAxis", "type"], "category")
                        break
                    case "line":
                    case "bar":
                        setIfNotEmpty(option, ["dataset", "source", serial_name], _.head(columns))
                        _.set(s, ["encode"], { x: findHasSource(option, ["xAxis", serial_name]), y: findHasSource(option, ["yAxis", serial_name]) })
                        break
                    case "scatter":
                        if (columns.length === 1) {
                            setIfNotEmpty(option, ["dataset", "source", serial_name], _.head(columns))
                            _.set(s, ["encode"], { x: findHasSource(option, ["xAxis", serial_name]), y: findHasSource(option, ["yAxis", serial_name]) })
                        } else {
                            const [x, y] = columns
                            setIfNotEmpty(option, ["dataset", "source", `${serial_name}_x`], x)
                            setIfNotEmpty(option, ["dataset", "source", `${serial_name}_y`], y)
                            _.set(s, ["encode"], { x: `${serial_name}_x`, y: `${serial_name}_y` })
                        }
                        break
                    default:
                        const e = extract_data(data) || {}
                        values = e["values"]
                        x_values = e["x_values"]
                        y_values = e["y_values"]
                        setIfNotEmpty(source, [serial_name], values)
                        setIfNotEmpty(source, `${serial_name}_xAxis`, x_values)
                        setIfNotEmpty(source, `${serial_name}_yAxis`, y_values)
                        if (category_is_x === true) _.set(s, ["encode"], { x: findHasSource(source, [`${serial_name}_xAxis`, serial_name]), y: findHasSource(source, [`${serial_name}_yAxis`, serial_name]) })
                        else _.set(s, ["encode"], { y: findHasSource("yAxis", serial_name), x: serial_name })
                        break
                }
                break
            case "polar":
                setIfNotEmpty(option, ["dataset", "source", serial_name], _.head(columns))
                _.set(s, ["encode"], { radius: findHasSource(option, ["radiusAxis", serial_name]), angle: findHasSource(option, ["angleAxis", serial_name]) })
                break
            default:
                switch (type) {
                    case "pie":
                        setIfNotEmpty(option, ["dataset", "source", serial_name], _.head(columns))
                        _.set(s, ["encode", "value"], serial_name)
                        break
                }
        }
        switch (cordinate) {
            case "radar":
                break
            default:
                _.unset(s, ["data"])
        }

    })
    return merge_common(option)
}
function column2radar(column_obj, option) {
    const indicators = _.map(_.get(column_obj, ["indicators"]), i => ({ name: i }))
    _.set(option, ["radar", "indicator"], indicators)
    const data = _.map(_.omit(column_obj, ["indicators"]), (value, key) => ({ name: key, value }))
    _.set(option, ["series"], { type: 'radar', data })
    _.set(option, ["dataset", "source"], column_obj)
    return option
}
function radar2column(option) {
    const indicators = _.map(_.get(option, ["radar", "indicator"]), i => i["name"])
    const data_obj = _.fromPairs(_.map(_.flatMap(_.get(option, ["series"]), ({ name, type, data }, index) => data), ({ name, value }) => [name, value]))
    return _.merge({}, data_obj, { indicators })
}
function heat2source(option) {
    let series = _.get(option, "series")
    const [x, y, heat] = row2columns(_.head(series)["data"])
    _.set(option, ["dataset", "source"], {
        "X轴": x,
        "Y轴": y,
        "value": heat,
    })
    return option
}
function source2heat(column_obj, option) {
    const x = column_obj["X轴"]
    const y = column_obj["Y轴"]
    const heat = column_obj["value"]
    const rs = []
    _.forEach(_.range(0, x.length), i => {
        _.set(rs, [i, 0], x[i])
        _.set(rs, [i, 1], y[i])
        _.set(rs, [i, 2], heat[i])
    })
    _.set(option, ["series", 0, "data"], rs)
    _.set(option, ["xAxis", "data"], _.sortBy(_.uniq(x)))
    _.set(option, ["yAxis", "data"], _.sortBy(_.uniq(y)))
    return option
}
function sankey2source(option) {
    _.set(option, ["dataset", "source"], object_arr2column(_.get(option, ["series", 0, "links"])))
    return option
}
function source2sankey(column_obj, option) {
    const { source, target } = column_obj
    const names = _.map(_.uniq(_.concat(source, target)), name => ({ name }))
    _.set(option, ["series", 0, "data"], names)
    _.set(option, ["series", 0, "links"], column2object_arr(column_obj))
    return option
}
function funnel2source(option) {
    _.set(option, ["dataset", "source"], object_arr2column(_.get(option, ["series", 0, "data"])))
    return option
}
function source2funnel(column_obj, option) {
    _.set(option, ["series", 0, "data"], column2object_arr(column_obj))
    return option
}
function gauge2source(option) {
    _.set(option, ["dataset", "source"], object_arr2column(_.get(option, ["series", 0, "data"]), ["name", "value"]))
    return option
}
function source2gauge(column_obj, option) {
    const { value } = column_obj
    const count = value.length
    const title = _.map(_.range(0, count), idx => ({
        offsetCenter: [`${_.toInteger((idx + 1) / 2) * 80 * (idx % 2 === 0 ? (-1) : 1)}px`, '80%']
    }))
    const detail = _.map(_.range(0, count), idx => ({
        offsetCenter: [`${_.toInteger((idx + 1) / 2) * 80 * (idx % 2 === 0 ? (-1) : 1)}px`, '95%']
    }))
    _.set(option, ["series", 0, "data"], column2object_arr(_.merge({}, column_obj, { title, detail })))
    return option
}

function river2source(option) {
    const { header, result } = dim2column(_.get(option, ["series", 0, "data"]), false)
    renameKey(result, "col0", "date")
    renameKey(result, "col1", "value")
    renameKey(result, "col2", "name")
    _.set(option, ["dataset", "source"], result)
    return option
}
function source2river(column_obj, option) {
    _.set(option, ["series", 0, "data"], column2dim(column_obj))
    return option
}

function bar3d2source(option) {
    const { header, result } = dim2column(_.get(option, ["series", 0, "data"]), false)
    renameKey(result, "col0", "X轴")
    renameKey(result, "col1", "Y轴")
    renameKey(result, "col2", "value")
    _.set(option, ["dataset", "source"], result)
    return option
}
function source2bar3d(column_obj, option) {
    _.set(option, ["series", 0, "data"], column2dim(column_obj))
    return option
}

