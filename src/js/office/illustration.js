import { randomOne } from "./auto_ppt_layout"

export const BASE_URL = "/img/office/backgrounds/templates"
export const CATEGORIES = {
    "民生": 15,
    "教育": 18,
    "庆典": 25,
    "企业": 19,
    "职场": 25,
    "总结": 14,
}
export function getRandomImage(category) {
    const category_names = Object.keys(CATEGORIES)
    category = category || randomOne(category_names)
    return getCategoryIndexImage(category)
}
export function getCategoryIndexImage(category, index) {
    const count = CATEGORIES[category]
    const rdm = index === undefined ? _.toInteger(Math.random() * count) + 1 : index
    return `${BASE_URL}/${category}/${rdm}.svg`
}
export function getImages(category) {
    if (category !== undefined) return _.map(_.range(1, CATEGORIES[category] + 1), index => `${BASE_URL}/${category}/${index}.svg`)
    else return _.mapValues(CATEGORIES, (count, category) => _.map(_.range(1, count + 1), index => getCategoryIndexImage(category, index)))
}