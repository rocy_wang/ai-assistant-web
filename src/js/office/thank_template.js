import { MARGIN, SUB_TITLE_HOLDER, TEXT_SIZE, TITLE_HOLDER, TITLE_SIZE } from "./template_constants";
import { officeState } from '@/js/office/office_state';

export function generateThankHtml(thank) {
    const slide_width = officeState.getters.slide_width;
    const slide_height = officeState.getters.slide_height;
    return `<div class="border border-primary d-flex justify-content-center align-items-center"
            style='position:relative;width:${slide_width}px;height:${slide_height}px;'>
            <div class="border border-primary d-flex justify-content-center align-items-center"
                style="width:${slide_width - MARGIN}px;height: ${slide_height - MARGIN}px;">
                <div class="k position_size thank" style="font-size:${TITLE_SIZE};width:300px;height:80px;">${thank}</div>
                <img class="k position_size heart m-3" src='/img/office/content.svg' style="width:32px;height:32px;"/>
            </div>
        </div>`;
}