import { officeState } from '@/js/office/office_state'
import { POPOVER_STYLE } from '@/js/ppt'
import { float2percent } from '@/js/lodash_util.js'
export const menuBarMixin = {
    props: {
        flex_wrap: { type: Boolean, required: false, default: true },
        is_editor_theme: { type: Boolean, default: false }
    },
    data() {
        return {
            POPOVER_STYLE,
        }
    },
    computed: {
        buttons_class() {
            const rs = []
            if (this.is_editor_theme === true) return []
            rs.push(...(this.is_column ? ['d-flex', 'flex-column'] : ['d-flex', 'flex-row']))
            rs.push(this.flex_wrap ? 'flex-wrap' : 'flex-nowrap')
            return rs
        },
        space_direction() { return this.is_editor_theme === true ? "horizontal" : (this.is_column ? 'vertical' : 'horizontal') },
        placement() { return this.is_editor_theme === true ? 'bottom' : officeState.getters.placement },
        is_column() { return officeState.getters.is_column },
        current_format_obj() { return officeState.getters.current_format_obj },
        slide_width: () => officeState.getters.slide_width,
        slide_height: () => officeState.getters.slide_height,
    },
    methods: {
        float2percent,
        getDemo() { return $(this.$refs.demo) },
        getContainer() { return _.invoke(this.current_format_obj, "getContainer") },
        styleChanged(style) {
            return _.invoke(this.current_format_obj, "styleChanged", style)
        }
    }
}