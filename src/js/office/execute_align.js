import _, { round } from "lodash"

export function align_objects(align, to, locations, slide_width, slide_height, last_select_key) {
    const location_keys = Object.keys(locations)
    last_select_key = last_select_key || _.toString(_.max(_.map(location_keys, _.toInteger)))
    const location_pairs = _.toPairs(locations)
    const location_item_last = _.get(locations, last_select_key)
    const count = location_keys.length
    const lefts = _.map(locations, item => item["left"])
    const rights = _.map(locations, item => item["left"] + item["width"])

    const min_left = to === "group" ? _.min(lefts) : (to === "slide" ? 0 : location_item_last["left"])
    const max_right = to === "group" ? _.max(rights) : (to === "slide" ? slide_width : location_item_last["right"])

    const sum_width = _.sum(_.map(locations, item => item["width"]))
    const middle_h = (min_left + max_right) / 2
    const space_h = (max_right - min_left - sum_width) / (count - 1)
    const space_h_round = (max_right - min_left - sum_width) / (count + 1)
    const sorted_pairs_h = _.sortBy(location_pairs, ([key, item]) => item["left"])
    function distribute_h_left(index) { return _.sum(_.map(_.take(sorted_pairs_h, index), ([key, item]) => item["width"] + space_h)) + min_left }
    function distribute_h_left_round(index) { return _.sum(_.map(_.take(sorted_pairs_h, index), ([key, item]) => item["width"] + space_h_round)) + min_left + space_h_round }

    const tops = _.map(locations, item => item["top"])
    const bottoms = _.map(locations, item => item["top"] + item["height"])
    const min_top = to === "group" ? _.min(tops) : (to === "slide" ? 0 : location_item_last["top"])
    const max_bottom = to === "group" ? _.max(bottoms) : (to === "slide" ? slide_height : location_item_last["bottom"])
    const sum_height = _.sum(_.map(locations, item => item["height"]))
    const middle_v = (min_top + max_bottom) / 2
    const space_v = (max_bottom - min_top - sum_height) / (count - 1)
    const space_v_round = (max_bottom - min_top - sum_height) / (count + 1)
    const sorted_pairs_v = _.sortBy(location_pairs, ([key, item]) => item["top"])
    function distribute_v_top(index) {
        return _.sum(_.map(_.take(sorted_pairs_v, index), ([key, item]) => item["height"] + space_v)) + min_top
    }
    function distribute_v_top_round(index) {
        return _.sum(_.map(_.take(sorted_pairs_v, index), ([key, item]) => item["height"] + space_v_round)) + min_top + space_v_round
    }
    function set(op) {
        return _.mapValues(locations, (item, key) => opObject(key, op))
    }
    function opObject(key, op) {
        let clone_object = _.get(locations, key)
        op(clone_object)
        return clone_object
    }
    function setLeftTop(clone_object, path, value) {
        _.set(clone_object, path, value)
        if (path.includes("left")) _.set(clone_object, ["right"], "auto")
        if (path.includes("top")) _.set(clone_object, ["bottom"], "auto")
        return clone_object
    }
    function width(index) { return locations[_.toString(index)]["width"] }
    function height(index) { return locations[_.toString(index)]["height"] }
    switch (align) {
        case "align-left": return set(clone_object => setLeftTop(clone_object, ["left"], `${min_left}px`))
        case "align-center-h": return set((clone_object, index) => setLeftTop(clone_object, ["left"], `${middle_h - width(index) / 2}px`))
        case "align-right": return set((clone_object, index) => setLeftTop(clone_object, ["left"], `${max_right - width(index)}px`))
        case "align-top": return set(clone_object => setLeftTop(clone_object, ["top"], `${min_top}px`))
        case "align-center-v": return set((clone_object, index) => setLeftTop(clone_object, ["top"], `${middle_v - height(index) / 2}px`))
        case "align-bottom": return set((clone_object, index) => setLeftTop(clone_object, ["top"], `${max_bottom - height(index)}px`))
        case "align-center":
            const center_xs = _.map(locations, l => _.get(l, ["center", "x"]))
            const center_ys = _.map(locations, l => _.get(l, ["center", "y"]))
            const widths = _.map(locations, l => _.get(l, ["width"]))
            const heights = _.map(locations, l => _.get(l, ["height"]))
            const max_width = _.max(widths)
            const max_height = _.max(heights)
            const center_x = to === group ? _.mean([_.min(center_xs), _.max(center_xs)]) : (slide_width / 2)
            const center_y = to === group ? _.mean([_.min(center_ys), _.max(center_ys)]) : (slide_height / 2)
            const r = to === group ? Math.min(max_right - min_left, max_bottom - min_top) / 2 : Math.max((Math.min(slide_width, slide_height) / 2 - Math.max(max_width, max_height) / 2), 20)
            const step = (2 * Math.PI) / count
            const sorted_location_radians = _.sortBy(location_pairs, ([key, item]) => radian(item["center"], { center_x, center_y }))
            return _.map(sorted_location_radians, ([key, item], index) => opObject(key, clone_object => {
                const { x, y } = alignCenter({ center_x, center_y }, index, step, r)
                const { width, height } = item
                setLeftTop(clone_object, ["left"], `${x - width / 2}px`)
                setLeftTop(clone_object, ["top"], `${y - height / 2}px`)
            }))
        case "distribute-h":
            return _.map(sorted_pairs_h, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["left"], `${distribute_h_left(index)}px`)))
        case "distribute-h-round":
            return _.map(sorted_pairs_h, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["left"], `${distribute_h_left_round(index)}px`)))
        case "distribute-v":
            return _.map(sorted_pairs_v, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["top"], `${distribute_v_top(index)}px`)))
        case "distribute-v-round":
            return _.map(sorted_pairs_v, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["top"], `${distribute_v_top_round(index)}px`)))

    }
}