
let performanceTime = {
    "ai-office": { "total": 0, "begin": undefined, "end": undefined, "total2": undefined }
}
export const performanceMixin = {
    data() {
        return {
            beforeCreateTime: undefined,
            mountedTime: undefined,
            createdTime: undefined
        }
    },
    created() {
        this.createdTime = new Date().valueOf()
        if (performanceTime["ai-office"]["begin"] === undefined)
            performanceTime["ai-office"]["begin"] = this.createdTime
    },
    mounted() {
        this.mountedTime = new Date().valueOf()
        performanceTime["ai-office"]["end"] = this.mountedTime
        performanceTime["ai-office"]["total"] += this.mountedTime - this.createdTime
        performanceTime["ai-office"]["total2"] = performanceTime["ai-office"]["end"] - performanceTime["ai-office"]["begin"]
    },
}