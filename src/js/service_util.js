

export function service_is_readonly(service) {
    if (_.get(service, ['active']) === true && _.get(service, ['allow_update_after_active']) === false) return true
    if (_.isNil(_.get(service, ['id'])) === false && _.isNil(_.get(service, ['api_key']))) return true
    return false
}