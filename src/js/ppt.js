import { v4 as uuid4 } from 'uuid';
import { sample_base64 } from "@/js/data.js";
import { ITEM_TYPE_PARAGRAPH, ITEM_TYPE_HEADER } from '@/js/office/word'
import { default_background } from '@/js/office/office'
import { officeState } from '@/js/office/office_state'
import { ITEM_TYPE_RICH } from '@/js/office/word.js'
import { aiMixin } from '@/components/office/js/ai_mixin';
import { generateBiaogeSlides, generateChangguiSlide, generateCollapseSlides, generateFengMianSlidePos, generateJieShuSlide, generateMuluSlide, generatePopupSlides, generateStepsSlides, generateTabsSlides, generateTimelineSlides, generateTitleContentSlide, generateTuBiaoSlides, generateTuWenSlides, generateWenBenSlides, generateZhangjieSlide, randomOne } from './office/auto_ppt_layout';
import { CONTENT_LEFT, CONTENT_TOP } from './office/mulu_template';
import { COLOR_TYPE_BLUE } from './office/svg_background';
import { GENERATE_SCENE } from './office/ppt_layout';
import { ZHANGJIE_LAYOUT_NORMAL_ROW } from './office/zhangjie_template';
import { generateGuanXiSlides } from './office/generate_guanxi_slides';

export const categories = ["编辑", "复制", "播放"]
export const scenes = ["工作", "生活", "其他"]
export function canEdit(category) { return category === "编辑" }
export function canCopy(category) { return category === "复制" || category === "编辑" }
export function canPlay(category) { return category !== undefined }

let running_slides_index = 0
export const DEFAULT_VIDEO_URL = "https://rocy-data.wang/videos/rocy_data_saas.mp4"

export const DATA_CATEGORY_THANK = "THANK"
export const DATA_CATEGORY_ZHANG = "ZHANG"
export const DATA_CATEGORY_ZHANG_TEXT = "ZHANG_TEXT"
export const DATA_CATEGORY_TITLE = "TITLE"
export const DATA_CATEGORY_CONSTANT = "CONSTANT"
export const DATA_CATEGORY_CONTENT_TITLE = "CONTENT_TITLE"
export const DATA_CATEGORY_CONTENT_SUB_TITLE = "CONTENT_SUB_TITLE"
export const DATA_CATEGORY_CONTENT_TEXT = "CONTENT_TEXT"
export const DATA_CATEGORY_SUB_TITLE = "SUB_TITLE"
export const DATA_CATEGORY_REPORTER = "REPORTER"
export const DATA_CATEGORY_REPORT_DATE = "REPORT_DATE"
export const DATA_CATEGORY_RICH = "RICH"

export const object_mode_resize = "resize"
export const object_mode_location = "location"
export const object_mode_rotation = "rotation"
export const object_mode_scale = "scale"
export const object_modes = {
    [object_mode_location]: "位置",
    [object_mode_scale]: "缩放",
    [object_mode_resize]: "大小",
    [object_mode_rotation]: "旋转"
}

export const mode_border = "border"
export const mode_margin = "margin"
export const mode_padding = "padding"
export const mode_shadow = "shadow"
export const border_modes = {
    [mode_border]: "边框",
    [mode_margin]: "外边距",
    [mode_padding]: "内边距",
    [mode_shadow]: "阴影"
}

export const TYPE_SLD_LAYOUT_TITLE = 0
export const TYPE_SLD_LAYOUT_TITLE_ONLY = 5

export const TYPE_SLD_LAYOUT_CONTENTS = 9

export const TYPE_SLD_LAYOUT_TITLE_AND_CONTENT = 1

export const TYPE_SLD_LAYOUT_TWO_CONTENT = 3
export const TYPE_SLD_LAYOUT_COMPARISON = 4
export const TYPE_SLD_LAYOUT_CONTENT_WITH_CAPTION = 7
export const TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION = 8

export const LAYOUTS_PPT = [
    { name: "封面", layouts: [TYPE_SLD_LAYOUT_TITLE, TYPE_SLD_LAYOUT_TITLE_ONLY] },
    { name: "目录", layouts: [TYPE_SLD_LAYOUT_CONTENTS] },
    { name: "章节", layouts: [TYPE_SLD_LAYOUT_TITLE_AND_CONTENT] },
    { name: "结束", layouts: 1 }, { name: "关系", layouts: 1 }, { name: "图文", layouts: 1 }, { name: "场景", layouts: 1 }, { name: "正文", layouts: 1 }, { name: "文本", layouts: 1 }, { name: "图表", layouts: 1 }, { name: "特效", layouts: 1 }, { name: "动画", layouts: 1 }
]

export const TYPE_SLD_LAYOUT_BLANK = 6
export const POPOVER_STYLE = { 'min-width': 'auto!important', 'width': 'auto!important', 'padding': '3px!important' }
export const POPOVER_STYLE_FULL = { 'min-width': 'auto!important', 'width': '90%!important', 'padding': '3px!important' }
export function sample_ppt() {
    return ({
        filename: "AI PPT示例演示",
        slides: sample_slides()  //sample_slides()
    })
}

const sample_contents = [
    { content: "content1" },
    { content: "content2" },
    { content: "content3" }
]
export function getSlideObjectId(index) {
    return `slide_object_${index}`
}
export function getIndexFromSlideObjectId(id) {
    return parseInt(_.last(_.split(id, "_")))
}
function sample_text() {
    return _.join(_.shuffle(_.reject(_.split(
        `
Things to know when using the tooltip plugin:

Tooltips rely on the third party library Popper for positioning. You must include popper.min.js before bootstrap.js, or use one bootstrap.bundle.min.js which contains Popper.
Tooltips are opt-in for performance reasons, so you must initialize them yourself.
Tooltips with zero-length titles are never displayed.
Specify container: 'body' to avoid rendering problems in more complex components (like our input groups, button groups, etc).
Triggering tooltips on hidden elements will not work.
Tooltips for .disabled or disabled elements must be triggered on a wrapper element.
When triggered from hyperlinks that span multiple lines, tooltips will be centered. Use white-space: nowrap; on your <a>s to avoid this behavior.
Tooltips must be hidden before their corresponding elements have been removed from the DOM.
Tooltips can be triggered thanks to an element inside a shadow DOM.
Got all that? Great, let’s see how they work with some examples.

`, "\n"), _.isEmpty)), "\n")
}
function title2html(text) {
    return $("<div style='text-align: center;'></div>").wrapInner(`<span>${text}</span>`).prop("outerHTML")
}
export const sample_slide_title = [{
    type: ITEM_TYPE_RICH,
    html: title2html("单击此处添加标题"),
    data_category: DATA_CATEGORY_TITLE,
    config: {
        classes: ["position-absolute", "fs-1"],
        style: {
            "top": "30%",
            "left": "20%",
            "width": "60%",
        }
    }
}, {
    type: ITEM_TYPE_RICH,
    html: title2html("单击此处添加副标题"),
    data_category: DATA_CATEGORY_SUB_TITLE,
    config: {
        classes: ["position-absolute", "fs-2"],
        style: {
            "bottom": "30%",
            "left": "30%",
            "width": "60%",
        }
    }
}]

export const sample_slide_title2 = [{
    type: ITEM_TYPE_RICH,
    html: title2html("单击此处添加标题2"),
    data_category: DATA_CATEGORY_TITLE,
    config: {
        classes: ["position-absolute", "fs-1"],
        style: {
            "top": "30%",
            "left": "20%",
            "width": "60%",
        }
    }
}, {
    type: ITEM_TYPE_RICH,
    html: title2html("单击此处添加副标题2"),
    data_category: DATA_CATEGORY_SUB_TITLE,
    config: {
        classes: ["position-absolute", "fs-2"],
        style: {
            "bottom": "30%",
            "left": "30%",
            "width": "60%",
        }
    }
}]
export const slide_contents_sample = {
    layout: TYPE_SLD_LAYOUT_CONTENTS,
    data: {
        contents: sample_contents,
        title: "汇报内容"
    }
}
export const slide_title_content_sample = {
    layout: TYPE_SLD_LAYOUT_TITLE_AND_CONTENT,
    data: {
        title: "这里是标题",
        items: text2items("这里是内容", ITEM_TYPE_PARAGRAPH)
    }
}
export function get_ppt_item_id() {
    return `ppt_${uuid4()}`
}
export function text2items(text, type) {
    return [{ type, id: get_ppt_item_id(), text }]
}

export const slide_picture_caption_sample = {
    layout: TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION,
    data: {
        title: "这里显示标题",
        items: text2items("在这里输入内容", ITEM_TYPE_PARAGRAPH),
        picture: sample_base64
    }
}
export const only_title_slides_ppt = [sample_slide_title]
export function get_sample_slide_by_layout(layout) {
    return _.find(switch_slides, slide => slide.layout === layout)
}
export const switch_slides = [sample_slide_title, slide_contents_sample, slide_title_content_sample, {
    layout: TYPE_SLD_LAYOUT_TWO_CONTENT,
    data: {
        title: "这里显示标题",
        items: text2items("在这里输入内容", ITEM_TYPE_PARAGRAPH),
        items2: text2items("在这里输入内容2", ITEM_TYPE_PARAGRAPH),
    }
}, {
        layout: TYPE_SLD_LAYOUT_COMPARISON,
        data: {
            title: "这里显示标题",
            items: text2items("在这里输入内容", ITEM_TYPE_PARAGRAPH),
            items2: text2items("在这里输入内容2", ITEM_TYPE_PARAGRAPH),
            header: "在这里输入内容",
            header2: "在这里输入内容2",
        }
    }, {
        layout: TYPE_SLD_LAYOUT_TITLE_ONLY,
        data: {
            title: "这里显示标题"
        }
    }, {
        layout: TYPE_SLD_LAYOUT_CONTENT_WITH_CAPTION,
        data: {
            title: "这里显示标题",
            items: text2items("在这里输入内容", ITEM_TYPE_PARAGRAPH),
            items2: text2items("在这里输入内容2", ITEM_TYPE_PARAGRAPH),
        }
    }, slide_picture_caption_sample,
]

export function simple_slides_ppt() {
    return sample_slides2()
}

function to_slide(objects, config) {
    return { objects, config: config || {} }
}

export function sample_slides() {
    const context = window.document
    return _.concat(
        generateFengMianSlidePos("top", "部门工作汇报", GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined),

        generateChangguiSlide(TYPE_SLD_LAYOUT_TITLE, context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined),
        generateChangguiSlide(TYPE_SLD_LAYOUT_CONTENTS, context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined),
        generateChangguiSlide(TYPE_SLD_LAYOUT_TWO_CONTENT, context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined),
        generateChangguiSlide(TYPE_SLD_LAYOUT_COMPARISON, context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined),
        generateChangguiSlide(TYPE_SLD_LAYOUT_CONTENT_WITH_CAPTION, context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined),

        generateMuluSlide(5, CONTENT_LEFT, context, GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, false),
        generateZhangjieSlide(ZHANGJIE_LAYOUT_NORMAL_ROW, true, true, context, "第 01 部分", true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined, 1),

        sample_slides_other(),

        generateJieShuSlide("感谢观看", context, true, GENERATE_SCENE, COLOR_TYPE_BLUE, undefined, "border-success", "border-1", "rounded")
    )
}

export function sample_slides_other() {
    const option = { "context": window.document }
    return _.concat(
        randomOne(generateGuanXiSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, { ...option, "shape": "金字塔", "count": 3 })),
        randomOne(generateTuWenSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, { ...option, "shape": "横向", "count": 3 })),
        randomOne(generateWenBenSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generateTuBiaoSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, { ...option, "shape": "仪表盘" })),
        randomOne(generateBiaogeSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generateTabsSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generateCollapseSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generatePopupSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generateStepsSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
        randomOne(generateTimelineSlides(GENERATE_SCENE, COLOR_TYPE_BLUE, true, undefined, option)),
    )
}


$(initialize)
export const keyHandler = {}
export const KEYCODE_ESCAPE = "Escape"
export const KEYCODE_BACKSPACE = "Backspace"
export const HANDLER_KEY_ADDOBJECT = "addObject"
export const META_KEY_CTRL = "CTRL"
export const META_KEY_SHIFT = "SHIFT"
export const META_KEY_ALT = "ALT"
export function registerKeyHandler(key, handler_key, handler, meta_key) {
    meta_key = meta_key || ""
    key = `${meta_key}${key}`
    _.set(keyHandler, [key, handler_key], handler)
}
export function unregisterKeyHandler(key, handler_key, meta_key) {
    meta_key = meta_key || ""
    key = `${meta_key}${key}`
    _.unset(keyHandler, [key, handler_key])
}
function initialize() {
    keyUpMonitor()
    document.addEventListener("selectionchange", function () {
        const selection = window.getSelection()
        officeState.state.current_range = (selection.rangeCount === 0) ? undefined : selection.getRangeAt(0)
    }, { passive: true })
}
function keyUpMonitor() {
    $(document.body).on("keyup keydown", function (event) {
        let key = _.get(event, ["originalEvent", "code"])
        const { ctrlKey, shiftKey, altKey } = event
        let meta = ""
        if (ctrlKey === true) meta = META_KEY_CTRL
        else if (shiftKey === true) meta = META_KEY_SHIFT
        else if (altKey === true) meta = META_KEY_ALT
        key = `${meta}${key}`
        _.forEach(keyHandler[key] || {}, (handler) => {
            handler()
        })
    })
}
function keyMonitor() {
    $(document.body).on("keyup", function (event) {
        const key = _.get(event, ["originalEvent", "code"])
        _.forEach(keyHandler[key] || [], handler => handler())
        if (officeState.state.is_run_slide === true) {
            const slides = $(".running_slides div.section")
            switch (key) {
                case "ArrowUp":
                    running_slides_index = (running_slides_index - 1 + slides.length) % slides.length
                    runSlideIndex()
                    break;
                case "ArrowDown":
                case "Space":
                case "Enter":
                    running_slides_index = (running_slides_index + 1 + slides.length) % slides.length
                    runSlideIndex()
                    break;
            }
        } else if (key === "F5") startSlides()
    })
}
export function startSlides(run_index) {
    officeState.state.is_run_slide = true
    running_slides_index = run_index || 0
    runSlides()
}

export function runSlides() {
    $("#ID_RUN_SLIDES").trigger("blur")
    $("div.tooltip.show").removeClass("show").addClass("hide")
    handleFullScreen()
    setTimeout(() => {
        prepare()
        const slides = $(".running_slides  div.section")
        if (slides.length > 0) runSlideIndex()
    }, 100);
}
export function handleFullScreen() {
    var de = document.documentElement;
    if (de.requestFullscreen) {
        de.requestFullscreen();
    } else if (de.msRequestFullscreen) {
        de.msRequestFullscreen();
    } else if (de.webkitRequestFullScreen) {
        de.webkitRequestFullScreen();
    } else if (de.mozRequestFullScreen) {
        de.mozRequestFullScreen();
    } else return false
    return true
}

export function exitFullscreen() {
    var de = document;
    if (de.exitFullscreen) {
        de.exitFullscreen();
    } else if (de.msExitFullscreen) {
        de.msExitFullscreen();
    } else if (de.mozCancelFullScreen) {
        de.mozCancelFullScreen();
    } else if (de.webkitExitFullscreen) {
        de.webkitExitFullscreen();
    }
}

export function isFullScreen() {
    return document.fullscreenElement !== null
}


function fullFill(div, z_index) {
    z_index = z_index || "z-1"
    div.addClass("position-absolute")
    div.addClass("z-1")
    div.addClass("h-100")
    div.addClass("w-100")
    div.addClass("d-block")
    div.removeClass("my-3")
    div.css("top", "0")
    div.css("left", "0")
    div.css("width", "100%")
    div.css("height", "100%")
}


function prepare() {
    $(document.body).addClass("position-relative")
    $(".running_slides").remove()
    const running_slides = $(".run_slides").clone(true, true).removeClass("run_slides").addClass("running_slides").css("background-color", "white")
    running_slides.appendTo($(document.body))
    $(".running_slides textarea").css("resize", "none")
    fullFill(running_slides)
    $(window).on("fullscreenchange webkitfullscreenchange mozfullscreenchange", function (event) {
        if (isFullScreen() === false) {
            officeState.state.is_run_slide = false
            $(".running_slides").remove()
        }
    });
}


function block(element) {
    element.addClass("d-block");
    element.removeClass("d-none")
}
function hide(element) {
    element.addClass("d-none");
    element.removeClass("d-block")
}
function runSlideIndex() {
    const slides = $(".running_slides  div.section")
    _.forEach(slides, (slide, index) => {
        slide = $(slide)
        if (index === running_slides_index) {
            fullFill(slide, "z-1")
            block(slide)
        }
        else {
            hide(slide)
        }
    })
}

export const default_order_layouts = [
    TYPE_SLD_LAYOUT_TITLE,
    TYPE_SLD_LAYOUT_CONTENTS,
    TYPE_SLD_LAYOUT_TITLE_AND_CONTENT,
    TYPE_SLD_LAYOUT_TWO_CONTENT,
    TYPE_SLD_LAYOUT_COMPARISON,
    TYPE_SLD_LAYOUT_TITLE_ONLY,
    TYPE_SLD_LAYOUT_BLANK,
    TYPE_SLD_LAYOUT_CONTENT_WITH_CAPTION,
    TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION
]

function getOrder(layout) {
    return default_order_layouts.indexOf[layout]
}

export const CONTENTS_MAX_COUNT = 10
export const MODE_DESIGN = 0
export const MODE_EYE = 1
export const MODE_RUN = 2
export const MODE_LAYOUT = 3

export function focusShowControl(parent_class, to_show_class) {
    $(`${parent_class}`).on("focusin mouseenter", function () {
        $(`${to_show_class}`, $(this)).addClass("visible")
        $(`${to_show_class}`, $(this)).removeClass("invisible")
    }).on("focusout mouseleave", function () {
        $(`${to_show_class}`, $(this)).addClass("invisible")
        $(`${to_show_class}`, $(this)).removeClass("visible")
    })
}

function getNotNull(obj, properties) {
    return _.find(obj, (value, key) => properties.includes(key) && value !== undefined && _.isEmpty(value) === false)
}

export const pptMixin = {
    emits: ["update:objects"],
    mixins: [aiMixin],
    props: {
        mode: {
            type: Number,
            default: MODE_DESIGN,
            required: false
        },
        id: {
            type: String,
            required: false
        },
        objects: {
            type: Array,
            required: false
        }
    },
    data() {
        return {
            _objects: undefined,
        }
    },

    watch: {
        background: {
            handler(val) {
                if (val === undefined) return
                if (this.isLayout === true) return
                const apply_obj = $(this.$el)
                if (apply_obj.hasClass("layout_container") === false) return
                this.$nextTick(() => {
                    _.forEach(val, (value, key) => apply_obj.css(key, value))
                })
            }, immediate: true, deep: true
        },
    },
    computed: {
        my_title: {
            get() { return _.get(this.model_config, "my_title") },
            set(value) { _.set(this.model_config, "my_title", value) },
        },
        background() {
            return _.get(this, ["data", "background"]) || default_background
        },
        isDesign() {
            return this.mode === MODE_DESIGN
        },
        isEye() {
            return this.mode === MODE_EYE || this.mode === MODE_LAYOUT
        },
        isRun() {
            return this.mode === MODE_RUN
        },
        isLayout() {
            return this.mode === MODE_LAYOUT
        }
    },
    methods: {
        generateContentTextSlides(how) {
            const title = _.get(how, ["title"])
            this.generateContentTextAI(how)
                .then(contents => {
                    const slides = _.map(contents, content => generateTitleContentSlide(title, content, true, { context: window.document }))
                    this.addNewSlide(slides)
                })
        },
        generateContentSlide(how) {
            this.generateContentAI(how)
                .then(contents => {
                    contents = _.take(contents, CONTENTS_MAX_COUNT)
                    const slide = generateMuluSlide(contents, CONTENT_TOP, { body: document.body }, COLOR_TYPE_BLUE, true, undefined, undefined)
                    const content_titles = _.filter(_.get(slide, ["objects"]), object => object["data_category"] === DATA_CATEGORY_CONTENT_TITLE)
                    _.forEach(content_titles, (ct, idx) => _.set(ct, ["html"], contents[idx]))
                    this.addSlideAfter(slide)
                })
        },

        oneKeyGenerateSlide(how) {
            this.oneKeyGenerateAI(how)
                .then(title_contents => {
                    const slides = _.map(title_contents, ({ title, content }) => generateTitleContentSlide(title, content, true, { context: window.document }))
                    this.addNewSlide(slides)
                })
        },
        updateoOjects() {
            this.$emit("update:objects", this._objects)
        },
        focusShowControl,
        sortSlides(slides) {
            return _.sortBy(slides, slide => getOrder(slide.layout))
        },
        wrapLayout(to_layout, data) {
            return { layout: to_layout, data }
        },
        title2layout(current_slide_data, to_layout) {
            function f(props) {
                return getNotNull(current_slide_data, props)
            }

            let new_items = f(["items", "items2"])
            if (new_items === undefined) new_items = _.map(_.filter(current_slide_data, (value, key) => ["sub_title", "header2"].includes(key)), text2items)
            switch (to_layout) {
                case TYPE_SLD_LAYOUT_TITLE:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", sub_title: f(["sub_title", "header2"]) || "此处是副标题" })
                case TYPE_SLD_LAYOUT_CONTENTS:
                    let new_contents = f(["items", "items2"])
                    const contents1 = _.filter(current_slide_data, (value, key) => ["sub_title", "header2"].includes(key))
                    const contents2 = _.map(_.filter(new_contents, item => item.type === ITEM_TYPE_HEADER), item => item.text)
                    new_contents = (new_contents === undefined) ? contents1 : contents2
                    new_contents = _.map(new_contents, content => ({ content }))
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", contents: new_contents || sample_contents })
                case TYPE_SLD_LAYOUT_TITLE_AND_CONTENT:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", items: new_items })
                case TYPE_SLD_LAYOUT_TWO_CONTENT:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", items: new_items, items2: new_items })
                case TYPE_SLD_LAYOUT_COMPARISON:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", header: f(["header", "title", "header2", "sub_title"]), header2: f(["header2", "sub_title", "header", "title"]), items: new_items, items2: new_items })
                case TYPE_SLD_LAYOUT_TITLE_ONLY:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题" })
                case TYPE_SLD_LAYOUT_CONTENT_WITH_CAPTION:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", items: new_items, items2: new_items })
                case TYPE_SLD_LAYOUT_PICTURE_WITH_CAPTION:
                    return this.wrapLayout(to_layout, { title: f(["title", "header"]) || "此处是标题", items: new_items })
            }
        },
        toLayout(currend_slide, to_layout) {
            return this.title2layout(currend_slide.data, to_layout)
        },
    }
}

