
import { CloseBold, ArrowDown, ArrowUp, Avatar, DocumentAdd, Money, Position, Promotion, SwitchButton, User } from "@element-plus/icons";
import MoneyIn from "@/components/MoneyIn.vue";
import AiPager from "@/components/Pager.vue";
import { CHAT, insureLogged, mixinCommon } from "@/js/document";
import { service_post } from "@/js/request/service";
import { recommend, showPhone, userStore } from "@/js/user";


import { ElMessage, ElMessageBox } from "element-plus";
import { defineComponent } from 'vue';
import { startCountDown } from "@/js/timer";
import { aa_examples } from "@/js/examples";
import { v4 as uuid4 } from 'uuid';
import AiRecord from "@/components/Record.vue";
import { timestampToText } from "@/js/time_util";
import { IMAGE_SIZE_VALUES, IMAGE_SIZES, AI_MODEL2TOKENS, AI_IMAGE_MODELS, IMAGE_SD_XL, OPEN_AI_DEFAULT_IMAGE_SIZE, SD_DEFAULT_STYLE, SD_DEFAULT_IMAGE_SIZE, COUNTING_SECONDS, DIALOGUE_RETURN_COUNT, GPT_3DOT5_TURBO_16K, IMAGE_DALL_E3, GPT_4, MAX_TOKENS_TIMES, TEMPERATURE_DEFAULT } from "@/js/constants";
import AiDialogue from "@/components/Dialogue.vue";
import { loading_state } from "@/js/request/global_state";
import AiFloatNavigation from "@/components/FloatNavigation.vue";
import { ChatRound } from "@element-plus/icons-vue";
import { print_text } from "@/js/chat/printer";
import { ElLoading } from 'element-plus'

export default defineComponent({
    name: "ai-chat",
    mixins: [mixinCommon],
    components: { ChatRound, CloseBold, AiFloatNavigation, AiDialogue, AiRecord, AiPager, Promotion, Money, User, SwitchButton, Avatar, MoneyIn, Position, ArrowUp, ArrowDown, DocumentAdd },
    data() {
        return {
            sd_steps: 20,
            IMAGE_SIZES,
            AI_IMAGE_MODELS,
            selectedImageModelName: IMAGE_DALL_E3,
            selectedImageModelSize: OPEN_AI_DEFAULT_IMAGE_SIZE,
            selectedImageModelStyle: undefined,
            IMAGE_SD_XL,
            selected_image_text: "会话模型",
            counting: undefined,
            examples: aa_examples,
            fullscreen: true,
            dialogues: [],
            SEND_LOADING_KEY: "SEND",
            LOAD_DIALOGUE_KEY: "DIALOGUE",
            DIALOGUE_RETURN_COUNT,
            COUNTING_SECONDS,
            question: undefined,
            dialogue_id: undefined,
            temperature: TEMPERATURE_DEFAULT,
            max_tokens: MAX_TOKENS_TIMES,
            models: {},
            public_models: {},
            public_models_top: undefined,
            public_models_loading: false,
            ai_models: AI_MODEL2TOKENS,
            use_public_model: false,
            selectedModelName: GPT_4,
            selectedModelId: undefined,
            showConfigModel: false,
            DELETE_DIALOGUE_KEY: "DELETE_DIALOGUE"
        }
    },
    watch: {
        selectedImageModelName: {
            handler(val) {
                if (val === IMAGE_DALL_E3) {
                    this.selectedImageModelSize = OPEN_AI_DEFAULT_IMAGE_SIZE
                    this.selectedImageModelStyle = undefined
                } else if (val === IMAGE_SD_XL) {
                    this.selectedImageModelSize = SD_DEFAULT_IMAGE_SIZE
                    this.selectedImageModelStyle = SD_DEFAULT_STYLE
                }
            }, immediate: true, deep: true
        }
    },
    mounted: function () {
        if (insureLogged(this, true)) {
            service_post("/model/get_files", null, (data) => this.models = data, undefined, this)
            this.$refs.dialogue_pager.pageFirst((data) => this.fullscreen = (data.length === 0))
        }
    },
    methods: {
        getPublicModelFilesByClick() {
            if (this.use_public_model === false) return
            this.getPublicModelFiles("")
        },
        getPublicModelFiles(keyword) {
            let is_top = _.isEmpty(keyword)
            if (is_top === true && this.public_models_top !== undefined) this.public_models = this.public_models_top
            else {
                this.public_models_loading = true
                service_post("/model/get_public_model_files", { is_top, keyword }, (data) => {
                    if (is_top === true)
                        this.public_models_top = data
                    this.public_models = data
                    this.public_models_loading = false
                }, this.public_models_loading = false, this)
            }
        },
        gotDialogs(data) {
            this.dialogues = _.map(data, (item) => _.assign(item, { question_time: item.question_time }))
            this.dialogues = _.reverse(_.sortBy(_.map(_.groupBy(this.dialogues, dialogue => dialogue.dialogue_id), (list) => _.minBy(list, item => item.question_time)), "question_time"))
        },
        handleSelect(dialogue_id) {
            this.dialogue_id = dialogue_id
            this.$nextTick(() => this.$refs.qa_dialogue.pageFirst())
        },
        activateDialogue(dialogue) {
            this.dialogues.forEach(dialog => dialog.active = (dialog.id === dialogue.id));
        },
        timestampToText: timestampToText,
        deleteDialogById: function (dialogue_id_delete) {
            ElMessageBox.confirm('确定要删除这个会话吗?', '请您确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => (value !== "confirm") ? _.noop() : service_post("/chat/delete_dialogue", { dialogue_id: dialogue_id_delete }, () => {
                    this.dialogues = _.reject(this.dialogues, d => _.get(d, "dialogue_id") === dialogue_id_delete)
                    if (this.dialogue_id === dialogue_id_delete) {
                        this.dialogue_id = undefined
                        this.$refs.qa_dialogue.clear_qa_items()
                    }
                    userStore.commit("dialogueCountDecrease")
                }, undefined, this, this.DELETE_DIALOGUE_KEY))
                .catch(console.log)
        },
        deleteDialogue(dialogue) {
            const dialogue_id_delete = dialogue["dialogue_id"];
            this.deleteDialogById(dialogue_id_delete);
        },
        recommendIt: () => recommend(document),
        logout() {
            userStore.commit("updateToken", undefined)
            this.toPage()
        },
        deleteMe() {
            ElMessageBox.confirm('确定要注销账号吗?所有相关信息都将会被删除。', '请您确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => (value !== "confirm") ? _.noop() : service_post("/user/delete_me", undefined, () => {
                    ElMessage({ message: "注销成功了", type: "success", showClose: true })
                    userStore.commit("unsetUserInfo")
                    this.toPage()
                }, undefined, this))
                .catch(console.log)
        },
        sendDrawing() {
            const post_data = {
                "model_name": this.selectedImageModelName,
                "size": IMAGE_SIZE_VALUES[this.selectedImageModelSize],
                "style": this.selectedImageModelStyle,
            }
            const fun = (data) => this.sendRobot(data, "/image/generate")
            if (this.selectedImageModelName === IMAGE_DALL_E3) fun(post_data)
            else if (this.selectedImageModelName === IMAGE_SD_XL)
                ElMessageBox.prompt('如果需要，请给AI输入负向提示。<br/>例如输入"白色"，则表示输出图片不是白色', '负向提示', { confirmButtonText: '确认', cancelButtonText: '取消', dangerouslyUseHTMLString: true })
                    .then(({ value }) => {
                        fun(_.isEmpty(_.trim(value)) ? post_data : _.merge({}, post_data, {
                            "negative_prompt": _.trim(value),
                            "steps": this.sd_steps
                        }))
                    }).catch(_.noop)
        },
        add(answer) {
            this.$refs.qa_dialogue.unshift_answer(answer)
            this.question = undefined
        },
        sendRobot(post_data, api) {
            if (this.canSend === false) return
            if (_.isNil(this.dialogue_id)) this.dialogue_id = uuid4()
            this.$nextTick(() => {
                const data_to_post = _.merge({}, post_data || {}, {
                    id: this.noAnswers ? this.dialogue_id : uuid4(),
                    question: this.question,
                    dialogue_id: this.dialogue_id,
                });
                const loading = ElLoading.service({ lock: true, text: 'AI思考中...', background: 'rgba(0, 0, 0, 0.7)' })
                service_post(api, data_to_post, (answer) => {
                    startCountDown(CHAT, this.COUNTING_SECONDS, () => this.counting = undefined, (cnt) => this.counting = cnt)
                    if (this.noAnswers) {
                        this.dialogues.unshift(answer)
                        this.activateDialogue(answer)
                        userStore.commit("dialogueCountIncrease")
                    }
                    userStore.commit("getUserInfo")
                    let answer_text = _.get(answer, "answer")
                    if (_.isNil(answer_text)) { this.add(answer) } else {
                        $(".printing_answer").show()
                        print_text(".printing_answer", answer_text, () => {
                            $(".printing_answer").hide()
                            this.add(answer)
                        })
                    }
                    loading.close()
                }, (e) => {
                    loading.close()
                    if (e.response.status === 402) ElMessage({ type: 'warning', message: "您的余额不足了", showClose: true })
                    else startCountDown(CHAT, this.COUNTING_SECONDS, () => this.counting = undefined, (cnt) => this.counting = cnt)
                }, this, this.SEND_LOADING_KEY)
            })
        },
        sendMessage() {
            this.sendRobot({
                model_name: this.selectedModelName,
                model_id: this.selectedModelId,
                temperature: this.temperature,
                max_tokens: this.maxTokenNumber
            }, "/chat/get_answer")
        },
        createDialogue() {
            this.dialogue_id = uuid4()
            this.$nextTick(() => this.$refs.qa_dialogue.reset())
        },
        modelDict2Arr(models) {
            return _.sortBy(_.map(models, ({ files, meta }, model_id) => ({ model_id, file_size: Object.keys(files).length, meta })), item => -item.file_size)
        }
    },
    computed: {
        noAnswers() {
            return this.dialogue_id !== undefined && _.get(this.$refs.qa_dialogue, "noAnswers");
        },
        isLoadingDialogue() {
            return _.get(loading_state.state.isLoading, this.LOAD_DIALOGUE_KEY) === true
        },
        maxTokenNumber() {
            return Math.round(this.max_tokens * this.ai_models[this.selectedModelName])
        },
        modelId2fileSizesPublic() {
            return this.modelDict2Arr(this.public_models)
        },
        modelId2fileSizes() {
            return this.modelDict2Arr(this.models)
        },
        headImgUrl: () => _.get(userStore.state.info, "headimgurl"),
        mobile: () => showPhone(_.get(userStore.state.info, "phone")),
        canSend() {
            return this.question !== undefined && this.question.length > 0 && !_.isNil(this.selectedModelName)
        },
        modelInfo() {
            return _.omitBy({
                "大模型": this.selectedModelName,
                "私有模型": this.selectedModelId,
                "回复长度": this.maxTokenNumber,
                "模型调优": this.temperature < 0.5 ? "更准确" : "更发散"
            }, (v) => _.isNil(v))
        }
    }
})