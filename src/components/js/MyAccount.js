
import { defineComponent } from "vue";
import { recommend, showDate, showPhone, userStore } from "@/js/user";
import { insureLogged, mixinCommon } from "@/js/document";
import AiPager from "@/components/Pager.vue";

import { bind } from 'lodash'
import { CHARGE_REASONS } from "@/js/constants";
import { get_or_else } from "@/js/lodash_util";
import { service_post } from "@/js/request/service";
import { ElMessage } from "element-plus";
import { date2LocalText, days_diff } from "@/js/time_util";
import AiFloatNavigation from "@/components/FloatNavigation.vue";
import AiMoneyShow from "@/components/MoneyShow.vue";
import AiLayout from "@/components/AILayout.vue";

export default defineComponent({
    name: "ai-my-account",
    mixins: [mixinCommon],
    components: { AiLayout, AiMoneyShow, AiFloatNavigation, AiPager },
    data() {
        return {
            REASONS: CHARGE_REASONS,
            KEY_LOADING_ACCOUNT: "ACCOUNT",
            records: undefined,
            PAGE_SIZE: 20,
            service_ids: {},
            binding: { can_change_im_name: false, wework_company: undefined, wework_name: undefined, mm_name: undefined, mobile_qq_name: undefined, im_name_update_time: undefined },
            binding_original: undefined
        }
    },
    mounted() {
        if (insureLogged(this, true)) {
            service_post("/provider/list_services", undefined, (data) => this.service_ids = _.fromPairs(_.map(data, item => [item["id"], item["description"]])), undefined, this)
            this.$refs.pager.pageFirst()
            this.binding = {
                can_change_im_name: _.get(userStore.state.info, "can_change_im_name") !== false,
                wework_company: _.get(userStore.state.info, "wework_company"),
                wework_name: _.get(userStore.state.info, "wework_name"),
                mm_name: _.get(userStore.state.info, "mm_name"),
                mobile_qq_name: _.get(userStore.state.info, "mobile_qq_name"),
                im_name_update_time: _.get(userStore.state.info, "im_name_update_time")
            }
            this.binding_original = _.cloneDeep(this.binding)
        }
    },
    computed: {
        bindingChanged() {
            return _.isEqual(this.binding, this.binding_original) === false
        },
        isEmptyApiKey() {
            return _.every(this.records, r => _.isNil(r["api_key"]))
        },
        isEmptyExtra() {
            return _.every(this.records, r => _.isNil(r["extra_json_str"]))
        },
        account() {
            return userStore.state.info
        },
        is_month() {
            return userStore.getters.is_month
        },
        month_balances() {
            return userStore.getters.month_balances
        },
        phone() {
            return showPhone(this.account.phone)
        },
        recommenderPhone() {
            return showDate(this.account["by_share_phone"])
        },
        createDateTime() {
            return showDate(this.account["create_time"])
        },
        pagerFilter() {
            return { reason: { name: '项目', enums: this.REASONS }, change: { name: '金额', number: true }, change_time: { name: '时间', date: true }, service_id: { name: "服务", enums: this.service_ids } }
        }
    },
    methods: {
        date2LocalText,
        days_diff,
        bind,
        readableReason(reason) {
            return get_or_else(this.REASONS, reason, reason)
        },
        jsonReadable(json) {
            return _.map(json || {}, (v, k) => `${k}=${v}`).join(" ")
        },
        recommendIt: () => recommend(document),
        gotRecords(records) {
            this.records = records
        },
        bindingIt() {
            service_post("/user/binding", {
                "wework_company": this.binding.wework_company,
                "wework_name": this.binding.wework_name,
                "mm_name": this.binding.mm_name,
                "mobile_qq_name": this.binding.mobile_qq_name,
            },
                () => ElMessage({ message: "绑定成功", type: "success", duration: 3000, showClose: false }),
                () => ElMessage({ message: "绑定成功", type: "success", duration: 3000, showClose: false }),
                this)
        }
    }
})