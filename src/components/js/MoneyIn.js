
import { defineComponent } from 'vue';
;
import { showPhone, userStore } from "@/js/user";
import { service_post } from "@/js/request/service";
import { insureLogged, mixinCommon } from "@/js/document";
import AiFloatNavigation from "@/components/FloatNavigation.vue";
import AiMoneyShow from "@/components/MoneyShow.vue";
import AiLayout from "@/components/AILayout.vue";
import Order from "@/components/Order.vue";


export default defineComponent({
    components: { AiLayout, AiMoneyShow, AiFloatNavigation, Order },
    mixins: [mixinCommon],
    name: "ai-money-in",
    methods: {
        titleMonth(month_item) {
            const { category, level } = month_item
            return `${category}${level}`
        },
        descriptionMonth(month_item) {
            const { text_count, image_count, ocr_count } = month_item
            return `提问=${text_count}次,绘画=${image_count}次,OCR=${ocr_count}次,其他免费`
        },
        description(charge, give_percent) {
            const percent = give_percent * this.GIVE_PERCENT_STEP;
            return `赠送${(percent * 100).toFixed(0)}%,得${(charge * (1 + percent)).toFixed(2)}元`
        },
        pay() {
            this.showOrder = true;
            const monthIndex = this.charge_category == "常规充值" ? undefined : _.findIndex(this.MONTH_ITEMS, item => item.money == this.charge_value)
            this.order = { charge_value: this.charge_value, pay_type: this.pay_type, month_index: monthIndex }
        },
    },
    mounted() {
        if (insureLogged(this, true)) {
            service_post("/admin/get_setting", undefined, (data) => {
                this.MONTH_ITEMS = _.get(data, ["MONTH_ITEMS"])
                this.GIVE_PERCENT_STEP = _.get(data, ["GIVE_PERCENT_STEP"])
                this.types = _.map(_.get(data, ["GIVER_POLICY"]), (value, key) => ({ charge: parseInt(key), give_percent: value }))
            }, undefined, this)
        }
    },
    computed: {
        chargeTitle() {
            return `正在为账号[${this.mobile}]充值[${this.charge_value}]元`
        },
        canPay() {
            return this.pay_type !== undefined && this.charge_value !== undefined
        },
        mobile: () => showPhone(_.get(userStore.state.info, "phone"))
    },
    data() {
        return {
            monthIndex: undefined,
            charge_category: "常规充值",
            showOrder: false,
            order: undefined,
            GIVE_PERCENT_STEP: 0.05,
            types: [],
            MONTH_ITEMS: [],
            pay_type: undefined,
            charge_value: undefined
        }
    },
})