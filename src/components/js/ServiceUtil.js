import _ from "lodash"

export function filterOutSystem(list) {
    const props = ["is_system_mm", "is_system_wework", "is_system_mobileqq", "is_system_boss", "is_system_liepin", "is_system_toutiao"]
    function check(item){
        return _.find(props,prop=>_.get(item,[prop])===true)!==undefined
    }
    return _.reject(list, check)
}