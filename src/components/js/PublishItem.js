
import { defineComponent } from "vue";
import AiServiceInvoke from "@/components/ServiceInvoke.vue";

import { MP_MAPPINGS } from "@/js/constants_mp";
import { service_get, service_post } from "@/js/request/service";
import { ElMessage } from "element-plus";
import { userStore } from "@/js/user";
import { mixinCommon } from "@/js/document";
import { date2LocalText } from "@/js/time_util";
import { mpStore } from "@/js/mp";
import AiActiveService from "@/components/ActiveService.vue";
import { MPRobotLoopNewMessageOnlineConfig } from "@/js/constants";


export default defineComponent({
  name: "ai-publish-item",
  mixins: [mixinCommon],
  emits: ["deleted", "updated"],
  components: { AiActiveService, AiServiceInvoke },
  props: {
    service: {
      type: Object,
      required: true,
    },
    readonly: {
      type: Boolean,
      required: true,
    },
    index_in_parent: {
      type: Number,
      required: false
    }
  },
  watch: {
    service: {
      handler(val) {
        if (this.isNil(val)) return
        if (this.isNil(this.original)) this.original = _.cloneDeep(val)
        else if (val["id"] !== this.original["id"]) this.original = _.cloneDeep(val)
      }, immediate: true
    }
  },
  mounted() {
    mpStore.commit("getSysMps")
    this.original = _.cloneDeep(this.service)
  },
  data() {
    return {
      MP_MAPPINGS,
      COUNT_INVOKE_KEY: "PUBLISH",
      COUNT_INVOKE_TEXT: "刷新",
      original: undefined,
      sys_mps: undefined,
      showInvoke: false,
      show_active: false,
      password: undefined,
      again: undefined,
      selected_sys_mp: undefined
    }
  },
  methods: {
    date2LocalText,
    publish() {
      service_post("/provider/generate_service", this.service, () => {
        ElMessage({ showClose: true, duration: 6000, message: `${this.action}成功`, type: "success" })
        userStore.commit("getUserInfo")
        this.original = _.cloneDeep(this.service)
        if (this.index_in_parent !== undefined)
          this.$emit("updated")
      }, (e) => ElMessage({ showClose: true, duration: 6000, message: `${this.action}失败了`, type: "warning" }), this)
    },
    addFeature() {
      const clone = _.cloneDeep(this.selected_sys_mp);
      this.service["mps"].push(clone)
      this.selected_sys_mp = undefined
    },
    is_loop(mp) {
      return _.get(mp, "category") === MPRobotLoopNewMessageOnlineConfig
    },
    addAllFeature() {
      const no_added = _.reject(this.sys_mp_list, mp => this.mp_names.includes(mp['name']) || (this.exist_loop && this.is_loop(mp)));
      this.service["mps"].push(..._.map(no_added, mp => _.cloneDeep(mp)))
      this.selected_sys_mp = undefined
    },
    activeIt() {
      service_get("/provider/active_service", {
        api_key: this.apiKey,
        password: this.password
      },
        () => {
          ElMessage({ message: "激活成功", type: "success", duration: 3000 })
          this.show_active = false
          this.service["active"] = true
        },
        () => ElMessage({ message: "激活失败", type: "warning", duration: 3000 }),
        this
      )
    },
    deleteIt() {
      if (this.apiKey !== undefined) {
        service_post("/provider/delete_service", { api_key: this.apiKey }, () => {
          ElMessage({ message: "删除成功", type: "success", duration: 3000 })
          if (this.index_in_parent === undefined) this.$router.push("/publish_list")
          else this.$emit("deleted", this.index_in_parent)
        }, () => ElMessage({ message: "删除失败", type: "warning", duration: 3000 }), this)
      } else if (this.index_in_parent !== undefined) this.$emit("deleted", this.index_in_parent)
    },
    refreshOnline() {
      this.countUp()
      service_post("/provider/service_is_online", { api_key: this.apiKey }, (data) => this.service['online'] = data, this.stopCountDown, this)
    },
    get_image(mp) {
      return `/img/mp/${(_.get(this.MP_MAPPINGS, [_.get(mp, "category"), "img"]))}.svg`
    },
    add_loop() {
      const loop_mp = _.find(this.sys_mp_list, s => _.get(s, "category") === MPRobotLoopNewMessageOnlineConfig);
      const clone = _.cloneDeep(loop_mp);
      this.service["mps"].push(clone)
      this.selected_sys_mp = undefined
    },
  },


  computed: {
    is_active() {
      return this.service['active'] === true
    },
    sys_mp_list: () => mpStore.getters.sys_mp_list,
    action() {
      return this.is_published ? '保存' : '发布'
    },
    changed() {
      return _.isEqual(this.service, this.original) === false
    },
    mps() {
      return this.service["mps"]
    },
    mp_names() {
      return _.map(this.service["mps"], mp => mp['name'])
    },
    user_price_max() {
      if (this.is_published === false) return (this.account - this.lock_money)
      else return (this.account - this.lock_money + this.original['user_price'])
    },
    apiKey() {
      return _.get(this.service, ["api_key"])
    },
    exist_loop() {
      return this.exists(this.mps, s => _.get(s, "category") === MPRobotLoopNewMessageOnlineConfig);
    },
    add_prompt() {
      if (this.exist_loop === false) return "您需要配置一个'上线同步消息'能力"
      return undefined
    },
    before_add_prompt() {
      if (this.isNil(this.selected_sys_mp)) return undefined
      if (this.exist_loop === true && _.get(this.selected_sys_mp, "is_loop") === true)
        return "一个机器人只能有一个'上线同步消息'能力"
      if (this.exists(this.mps, mp => _.isEqual(mp, this.selected_sys_mp)))
        return "已经存在了"
      return undefined
    },
    service_error() {
      if (this.isAnyEmpty(this.service['description'])) return "请指定机器人描述"
      if (this.isNil(this.service['user_price'])) return "请指定机器人价格"
      if (this.service['user_price'] > this.user_price_max) return "机器人价格超过可用余额了"
      if (this.isNil(this.service['days'])) return "请指定机器人有效期"
      if (this.mps.length === 0) return "至少需要配置一种能力"
      return undefined
    },
    all_errors() {
      const service_error = this.isNil(this.service_error) ? ({}) : { [this.service['description']]: this.service_error };
      const add_error = this.isNil(this.add_prompt) ? ({}) : { "机器人能力配置": this.add_prompt };
      return _.merge({}, this.mp_errors, service_error, add_error)
    },
    first_error() {
      if (this.isAnyEmpty(this.all_errors)) return undefined
      const [key, error] = _.first(_.toPairs(this.all_errors))
      return `${key}:${error}`
    },
    mp_errors() {
      return _.reject(_.map(this.mps, mp => [mp['name'], this.MP_MAPPINGS[mp['category']].check && this.MP_MAPPINGS[mp['category']].check(mp)]), ([name, error]) => _.isNil(error))
    },
    service_id() {
      return _.get(this.service, "id")
    },
    is_new_service() {
      return this.isNil(this.service_id)
    },
    is_published() {
      return this.isNotNil(this.service["id"]) && this.isNotNil(this.service["api_key"])
    },
    cant_action() {
      return this.is_published ? this.changed === false : this.isNotEmpty(this.all_errors)
    },
    added_all() {
      return _.every(this.sys_mp_list, mp => this.mp_names.includes(mp['name']))
    },
    can_delete() {
      if (this.is_published === false) {
        if (this.index_in_parent !== undefined) return true
      } else if (this.is_active === false) return true
      return false
    }
  }
})
