import '@/js/jquery-ui/jquery-ui.js'
import { defineComponent } from "vue";
import {
    Download,
    Aim,
    Bottom,
    CaretBottom,
    CaretRight,
    CaretTop,
    ChatRound,
    DCaret,
    Files,
    Finished,
    Flag,
    Grid,
    Menu,
    MessageBox,
    Money,
    MoreFilled,
    Operation,
    PriceTag,
    Search,
    Service,
    Top,
    UploadFilled,
    User,
    UserFilled
} from "@element-plus/icons-vue";
import { mixinCommon } from "@/js/document";
import { recommend } from "@/js/user";
import { dragMixin } from '@/js/office/drag_mixin.js'
import { officeState } from '@/js/office/office_state';

export default defineComponent({
    methods: {
        recommend,
        drag(event, ui) {
            const { position } = ui
            this.xywhChanged(position)
        },
        enableUIOperation() {
            $("div.float_menu").draggable({
                handle: ".drag_handle"
            })
        },
        xywhChanged({ top, left }) {
            this.float_menu_style = _.merge({}, this.float_menu_style, { top, left })
        }
    },
    mixins: [mixinCommon, dragMixin],
    props: {
        position_input: {
            type: String,
            required: false
        }
    },
    watch: {
        is_dark: {
            handler(val) {
                if (val === true) ($("html").addClass("dark"))
                else ($("html").removeClass("dark"))
            }, immediate: true, deep: true
        },
        position_input: {
            handler(val) {
                this.position = val
            },
            immediate: true
        }
    },
    name: "ai-float-navigation",
    computed:{
        is_dark: { get() { return officeState.state.is_dark }, set(val) { officeState.commit('setTheme', val) } },
    },
    components: { Download, CaretRight, Menu, PriceTag, Flag, Search, CaretTop, CaretBottom, MoreFilled, Bottom, Top, DCaret, Files, ChatRound, Operation, MessageBox, Service, UploadFilled, Finished, Aim, User, UserFilled, Grid, Money },
    data() {
        return {
            float_menu_style: {
                right: "16px",
                top: "16px",
                "z-index": 65535
            },
            document: document,
            isCollapse: true,
            position: "bottom",
            menu_visible: false
        }
    },
    mounted() {
        this.enableUIOperation()
        this.is_dark = false
    }
})