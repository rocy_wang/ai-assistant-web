
import {defineComponent} from "vue";
import {insureLogged, mixinCommon} from "@/js/document";
import {service_post} from "@/js/request/service";

import AiServiceInvoke from "@/components/ServiceInvoke.vue";
import AiPublishItem from "@/components/PublishItem.vue";
import AiMoneyShow from "@/components/MoneyShow.vue";
import AiFloatNavigation from "@/components/FloatNavigation.vue";
import {service_is_readonly} from "@/js/service_util";
import AiLayout from "@/components/AILayout.vue";
import {CREATE_SERVICE} from "@/js/publish_util";
import { filterOutSystem } from "./ServiceUtil";

export default defineComponent({
  name: "ai-publish-list",
  components: {AiLayout, AiFloatNavigation, AiMoneyShow, AiPublishItem, AiServiceInvoke},
  mixins: [mixinCommon],
  data() {
    return {
      records: [],
      record_index: undefined
    }
  },
  computed: {
    service_id() {
      const value = _.get(this.$route, ["params", "service_id"]);
      return this.isAnyEmpty(value) ? undefined : Number(value)
    },
    service() {
      return _.get(this.records, this.record_index)
    }
  },
  mounted: function () {
    this.load_service_list()
  },
  methods: {
    service_is_readonly,
    load_service_list() {
      if (insureLogged(this, true)) service_post("/provider/list_services", undefined, (data) => {
        this.records = filterOutSystem(data)
        if (this.records.length > 0)
          if (this.isNotNil(this.service_id)) this.record_index = _.findIndex(this.records, r => _.get(r, "id") === this.service_id)
          else this.record_index = 0
      }, undefined, this)
    },
    copyItem() {
      let copy = {}
      if (this.service === undefined) copy = _.cloneDeep(CREATE_SERVICE)
      else {
        copy = _.cloneDeep(this.service);
        copy["description"] = `${copy["description"]}的副本`
      }
      delete copy["id"]
      delete copy["active"]
      delete copy["online"]
      delete copy["api_key"]
      delete copy["consumed"]
      this.records.unshift(copy)
      this.record_index = 0
    }
  }
})