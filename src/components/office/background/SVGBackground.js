

import { base64image, randomColor } from '@/js/office/office.js'
import { webServerUrl, web_service } from '@/js/request/service'
import { positions, DEFAULT_ICON_SIZE, get_position } from '@/js/office/svg_background.js'
import { nextIncreasedIndex } from '@/js/office/office_state'
import _ from 'lodash'

export default {
    props: {
        setting: {
            type: Object, required: false
        },
    },
    computed: {
        viewBox() {
            return this.rect(this.width, this.height)
        },
    },
    methods: {
        get_position,
        head: _.head,
        getBase64() {
            const svg_text = new XMLSerializer().serializeToString($(this.canvas()).get(0))
            return `data:image/svg+xml;base64,${btoa(unescape(encodeURIComponent(svg_text)))}`
        },
        clean() {
            $(this.canvas()).children("*:not(.initial)").remove()
        },
        update() {
            if (_.isNil(this.$refs.canvas)) return
            if (_.isNil(this.setting)) return
            const { canvas, icons } = this.setting
            const { width, height, background } = canvas
            const background_size = Math.min(width, height)
            this.clean()
            this.set(this.canvas(), { width, height, viewBox: this.rect(width, height) })
            this.set(this.r(), { width, height })
            if (_.isString(background)) this.setPureBackground(background)
            else if (_.isArray(background)) this.setLinearGradient(background)
            else if (_.isObject(background)) {
                const { c, f } = background
                this.setRadialGradient(c || {}, f || {})
            }
            _.forEach(icons, ({ image, position, size, props, path_props, preserve_ratio }) => {
                image = base64image(image)
                const setWebImage = (icon) => {
                    const added = icon.addClass("i").appendTo($(this.canvas())).get(0)
                    const icon_size = (size || DEFAULT_ICON_SIZE) * background_size
                    let icon_width = icon_size
                    let icon_height = icon_size
                    this.set(added, {
                        "preserveAspectRatio": preserve_ratio || "none",
                        width: icon_width,
                        height: icon_height,
                        ...props,
                    })
                    if (position !== undefined) {
                        const location_size = this.get_position(position, icon_size, width, height)
                        this.set(added, {
                            x: location_size["selected_icon_x"],
                            y: location_size["selected_icon_y"],
                            width: location_size["selected_icon_width"],
                            height: location_size["selected_icon_height"],
                        })
                    }
                    _.forEach($("path", $(added)).get(), path => this.set(path, path_props))
                }
                if (_.startsWith(image, "http"))
                    web_service.get(image).then(rs => setWebImage($(_.find($(rs.data), item => item.nodeName === "svg"))))
                else setWebImage($(`<svg><image style="width:100%;height:100%" href="${image}"/></svg>`))
            })
        },
        setObjProp(obj, prop, value) {
            obj && obj.setAttribute(prop, value)
        },
        setRadialGradient({ c_x, c_y, c_r, c_color, c_opacity }, { f_x, f_y, f_r, f_color, f_opacity }) {
            const stop_id = `grad2_r_${this.increased_index}`
            const g = this.getGrad(stop_id)
            this.setObjProp(g, "cx", c_x || "50%")
            this.setObjProp(g, "cy", c_y || "50%")
            this.setObjProp(g, "cr", c_r || "50%")
            this.setObjProp(g, "fx", f_x || "50%")
            this.setObjProp(g, "fy", f_y || "50%")
            this.setObjProp(g, "fr", f_r || "50%")
            const c_stop = this.gradStops(stop_id)[0]
            const f_stop = this.gradStops(stop_id)[1]
            $(c_stop).css("stop-color", c_color || randomColor()).css("stop-opacity", c_opacity === undefined ? 0.5 : c_opacity)
            $(f_stop).css("stop-color", f_color || randomColor()).css("stop-opacity", f_opacity === undefined ? 0.5 : f_opacity)
            this.setObjProp(this.r(), "fill", `url(#${stop_id})`)
        },
        setPureBackground(color) {
            this.setObjProp(this.r(), "fill", color)
        },
        setLinearGradient(settings) {
            const len = settings.length
            const stop_id = `${len === 2 ? "grad2" : "grad3"}_${this.increased_index}`
            const g = this.getGrad(stop_id)
            _.forEach(settings, ({ x, y, color, opacity }, idx) => {
                const default_value = idx === 0 ? 0 : (idx === 1 ? (len === 2 ? 1 : 0.5) : 1)
                this.setObjProp(g, `x${idx + 1}`, x === undefined ? default_value : x)
                this.setObjProp(g, `y${idx + 1}`, y === undefined ? default_value : y)
                $(this.gradStops(stop_id)[idx]).css("stop-color", color || randomColor()).css("stop-opacity", opacity === undefined ? 0.5 : opacity)
            })
            this.setObjProp(this.r(), "fill", `url(#${stop_id})`)
        },
        ok() {
            this.selected_image = base64image(this.head(this.selected_images_tmp))
            this.icon_select_visible = false
        },
        rect(w, h) {
            return _.join([0, 0, w, h || w], " ")
        },
        getGrad(id) { return $(`#${id}.initial`, $(this.$refs.canvas)).get(0) },
        gradStops(id) { return $(`#${id} stop`, $(this.$refs.canvas)).get() },
        canvas() { return $(this.$refs.canvas).get(0) },
        i() { return $(".i", $(this.$refs.canvas)).get(0) },
        p() { return $(".i path", $(this.$refs.canvas)).get() },
        r() { return $(".r", $(this.$refs.canvas)).get(0) },
        removeIcon() {
            $(".i", $(this.$refs.canvas)).remove()
        },
        set(obj, props) {
            const arr = _.isArray(obj) ? obj : [obj]
            _.forEach(arr, item => {
                _.forEach(props, (value, key) => {
                    if (value === undefined) item.removeAttribute(key)
                    else this.setObjProp(item, key, value)
                })
            })
        },
        as(key, value) {
            if (value === undefined) this.canvas().removeAttribute(key)
            else this.setObjProp(this.canvas(), key, value)
        },
        ai(key, value) {
            if (value === undefined) this.i().removeAttribute(key)
            else this.setObjProp(this.i(), key, value)
        },
        ap(key, value) {
            _.each(this.p(), pp => {
                if (value === undefined) pp.removeAttribute(key)
                else this.setObjProp(pp, key, value)
            })
        },

        set_position(pos, icon_size, width, height) {
            const { selected_icon_x, selected_icon_y, selected_icon_width, selected_icon_height } = this.get_position(pos, icon_size, width, height)
            this.selected_icon_x = selected_icon_x;
            this.selected_icon_y = selected_icon_y;
            this.selected_icon_width = selected_icon_width
            this.selected_icon_height = selected_icon_height
        }
    },
    watch: {
        setting: {
            handler(val) {
                if (_.isNil(val)) return
                this.update()
            }, immediate: true, deep: true
        },
        selected_position: {
            handler(pos) {
                this.set_position(pos, this.selected_icon_size, this.width, this.height)
            }, immediate: true

        },
        preserveAspectRatio(val) {
            if (val === undefined) return
            this.ai("preserveAspectRatio", val)
        },
        selected_icon_size(val) {
            if (val === undefined) return
            this.selected_icon_width = val
            this.selected_icon_height = val
        },
        selected_icon_x(val) {
            if (val === undefined) return
            this.ai("x", val)
        },
        selected_icon_y(val) {
            if (val === undefined) return
            this.ai("y", val)
        },
        selected_icon_width(val) {
            if (val === undefined) return
            this.ai("width", val)
        },
        selected_icon_height(val) {
            if (val === undefined) return
            this.ai("height", val)
        },
        is_solid(val) {
            this.ap("fill", val === true ? this.selected_fill_color : undefined)
            this.ap("stroke", val === true ? this.selected_stroke_color : undefined)
            this.ap("stroke-width", val === true ? this.selected_stroke_width : undefined)
        },
        selected_image(image) {
            image = base64image(image)
            web_service.get(image).then(rs => {
                this.removeIcon()
                $(this.$refs.canvas).append($(rs.data).addClass("i"))
                this.$nextTick(() => {
                    this.ai("preserveAspectRatio", "none")
                    this.ai("x", this.selected_icon_x)
                    this.ai("y", this.selected_icon_y)
                    this.ai("width", this.selected_icon_width)
                    this.ai("height", this.selected_icon_width)
                })
            })
        },
        fill_rull_is_nonzero(val) {
            this.ap("fill-rule", val === true ? "nonzero" : "evenodd")
        },
        selected_fill_opacity(val) {
            this.ap("fill-opacity", val)
        },
    },
    mounted() {
        this.update()
    },
    data() {
        return {
            increased_index: nextIncreasedIndex(),
            selected_fill_opacity: 0,
            fill_rull_is_nonzero: true,
            preserveAspectRatio: "none",
            positions,
            is_solid: true,
            icon_select_visible: false,
            selected_images_tmp: undefined,
            selected_image: undefined,
            transform: undefined,
            selected_icon_size: 200,
            selected_icon_width: 200,
            selected_icon_height: 200,
            selected_icon_x: 0,
            selected_icon_y: 0,
            selected_fill_color: "yellow",
            selected_stroke_color: "blue",
            selected_stroke_width: 1,
            selected_position: "center"
        }
    },
}