
import SVGBackground from '@/components/office/background/SVGBackground.vue'
import IllustrationSelector from '@/components/office/background/IllustrationSelector.vue'
import FilterEffect from '@/components/office/background/FilterEffect.vue'
import { officeState } from '@/js/office/office_state';

import { randomColor } from '@/js/office/office.js'
import { get_default, noNumber2value, float2percent } from '@/js/lodash_util'
import { autoSVGBackgroundSetting, DEFAULT_ICON_SIZE } from '@/js/office/svg_background.js'
import { service_post } from '@/js/request/service.js'
import { ElMessage } from "element-plus";
import { userStore } from '@/js/user.js'
import { insureLogged } from "@/js/document";

const default_image = "/img/office/backgrounds/templates/企业/1.svg"
const default_size = 200
const gradients = [{
    type: "pure",
    name: "背景纯色",
    default_bg: randomColor()
}, {
    type: "radial",
    name: "背景径向",
    default_bg: {
        c: {
            c_x: "50%",
            c_y: "50%",
            c_r: "50%",
            c_color: "red",
            c_opacity: 1
        }, f: {
            f_x: "50%",
            f_y: "50%",
            f_r: "50%",
            f_color: "green",
            f_opacity: 1
        }
    }
}, {
    type: "linear2",
    name: "两阶线性",
    default_bg: [{
        x: "0%",
        y: "0%",
        color: "red",
        opacity: 0
    }, {
        x: "100%",
        y: "100%",
        color: "green",
        opacity: 1
    }]
}, {
    type: "linear3",
    name: "三阶线性",
    default_bg: [{
        x: "0%",
        y: "0%",
        color: "red",
        opacity: 0
    }, {
        x: "50%",
        y: "50%",
        color: "green",
        opacity: 1
    }, {
        x: "100%",
        y: "100%",
        color: "blue",
        opacity: 0
    },]
},]

const all_background_settings = [{
    is_share: true,
    canvas: {
        width: 800,
        height: 600,
        background: {
            c: {
                c_x: "50%",
                c_y: "50%",
                c_r: "50%",
                c_color: "red",
                c_opacity: 1
            }, f: {
                f_x: "50%",
                f_y: "50%",
                f_r: "50%",
                f_color: "green",
                f_opacity: 1
            }
        },
    },
    icons: [{
        image: "/img/office/backgrounds/templates/企业/1.svg",
        position: "left-bottom",
        size: 250,
        // props: {},
        // path_props: {
        //     fill: "#123456",
        //     stroke: "#234567",
        //     "stroke-width": 2,
        // }
    }]
}, {
    is_share: false,
    canvas: {
        width: 800,
        height: 600,
        background: {
            c: {
                c_x: "50%",
                c_y: "50%",
                c_r: "50%",
                c_color: "red",
                c_opacity: 1
            }, f: {
                f_x: "50%",
                f_y: "50%",
                f_r: "50%",
                f_color: "blue",
                f_opacity: 1
            }
        },
    },
    icons: [{
        image: "/img/office/backgrounds/templates/企业/1.svg",
        position: "left-bottom",
        size: 250,
        // props: {},
        // path_props: {
        //     fill: "#123456",
        //     stroke: "#234567",
        //     "stroke-width": 2,
        // }
    }]
},]
export default {
    mounted() {
        if (insureLogged(this, true)) {
            this.randomBackground()
            this.loadSettings()
        }
    },
    components: { FilterEffect, SVGBackground, IllustrationSelector },
    methods: {
        set: _.set,
        get: _.get,
        filter: _.filter,
        cloneDeep: _.cloneDeep,
        randomBackground() {
            const { width, height } = this.current_slide_size
            let setting = autoSVGBackgroundSetting(width, height, "企业", DEFAULT_ICON_SIZE, "blue")
            _.set(setting, "user_id", this.userId)
            _.set(setting, "is_new", true)

            this.all_background_settings = _.reject(this.all_background_settings, s => s["is_new"] === true)
            this.all_background_settings.push(setting)
            this.editing_background_setting = setting
            this.selected_background_type = undefined
            this.selected_background_setting_id = undefined
        },
        newBackground() {
            let setting = { canvas: this.current_slide_size, user_id: this.userId, "is_new": true }

            this.all_background_settings = _.reject(this.all_background_settings, s => s["is_new"] === true)
            this.all_background_settings.push(setting)
            this.editing_background_setting = setting
            this.selected_background_type = undefined
            this.selected_background_setting_id = undefined
        },
        loadSettings() {
            service_post("/office/load_background_settings").then(data => this.all_background_settings = _.unionBy(data, item => _.pick(item, 'id'))).catch(_.noop)
        },
        deleteBackground() {
            const background_id = _.get(this.editing_background_setting, "id")
            service_post("/office/delete_background_setting", { background_id }).then(() => {
                this.all_background_settings = _.reject(this.all_background_settings, b => _.get(b, "id") === background_id)
                if (this.selected_background_owner === "我的" && this.background_settings_my.length > 0) this.editing_background_setting = _.head(this.background_settings_my)
                else this.editing_background_setting = _.head(this.background_settings_share)
                ElMessage({ message: "删除成功", type: "success" })
            }).catch(_.noop)
        },
        save() {
            service_post("/office/save_background_setting", { background_setting: this.editing_background_setting }).then((id) => {
                _.set(this.editing_background_setting, "id", id)
                this.loadSettings()
                ElMessage({ message: "保存成功", type: "success" })
            }).catch(_.noop)
        },
        backgroundChanged(background_setting) {
            this.selected_background_setting_id = background_setting['id']
            this.editing_background_setting = _.merge({}, background_setting, { canvas: this.current_slide_size })
            if (this.editing_background_setting['user_id'] !== this.userId) {
                this.editing_background_setting['user_id'] = this.userId
                this.editing_background_setting['id'] = undefined
            }
            this.selected_background_type = this.editing_background_type
        },
        getBackgroundType(background) {
            if (_.isNil(background)) return undefined
            if (_.isString(background)) return "pure"
            if (this.is_radial(background)) return "radial"
            if (this.is_linear2_background(background)) return "linear2"
            if (this.is_linear3_background(background)) return "linear3"
            return undefined
        },
        is_radial(background) { return _.isObject(background) && _.isArray(background) === false },
        is_linear2_background(background) { return _.isArray(background) && background.length === 2 },
        is_linear3_background(background) { return _.isArray(background) && background.length === 3 },
        setBackgroundType(to_type) {
            if (to_type === "radial") {
                if (this.editing_background_type === "radial") return
                else _.set(this.editing_background_setting, ["canvas", "background"], {})
            } else {
                if (["linear2", "linear3"].includes(this.editing_background_type)) return
                else _.set(this.editing_background_setting, ["canvas", "background"], to_type === "linear2" ? [{}, {}] : [{}, {}, {}])
            }
        },
        getIconPercentProp(index, prop, other) {
            const v = _.get(this.icons, [index, "props", prop])
            const p = parseInt(v) / 100
            return noNumber2value(p, other)
        },
        setIconPercentProp(index, prop, v) {
            if (["x", "y", "width", "height"].includes(prop))
                this.unsetIconPosition(index)
            _.set(this.icons, [index, "props", prop], float2percent(v))
        },
        unsetIconPosition(index) {
            _.unset(this.icons, [index, "position"])
        },
        iconAdd() {
            this.icons.push({
                image: default_image,
                position: "left-top",
                x: 0,
                y: 0,
                width: default_size,
                height: default_size,
                fill: randomColor(),
                stroke: randomColor(),
                "stroke-width": 1
            })
        },
        iconRemove(index) {
            this.icons.splice(index, 1)
        },
        setPosition(index, position) {
            _.set(this.icons, [index, "position"], position)
        }
    },
    data() {
        return {
            selected_background_owner: '我的',
            illustration_selector_visible: false,
            filter_effect_visible: false,
            selected_background_setting_id: undefined,
            editing_background_setting: undefined,
            all_background_settings: _.cloneDeep(all_background_settings),
            left_side_hide: false,
            right_side_hide: false,
            gradients: _.cloneDeep(gradients),
            selected_background_type: undefined
        }
    },
    computed: {
        userId: () => userStore.getters.userId,
        can_delete() {
            return _.get(this.editing_background_setting, "user_id") === this.userId && _.get(this.editing_background_setting, "id") !== undefined
        },
        cant_delete() {
            return this.can_delete === false
        },
        icons: {
            get() { return get_default(this.editing_background_setting, ["icons"], []) },
            set(val) { _.set(this.editing_background_setting, ["icons"], val) }
        },
        editing_background_linear_x0: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 0, "x"])) / 100, 0) },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 0, "x"], `${val * 100}%`)
            }
        },
        editing_background_linear_y0: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 0, "y"])) / 100, 0) },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 0, "y"], `${val * 100}%`)
            }
        },
        editing_background_linear_o0: {
            get() {
                const v = _.get(this.editing_background_setting, ["canvas", "background", 0, "opacity"])
                return v === undefined ? 0.5 : v
            },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 0, "opacity"], val)
            }
        },
        editing_background_linear_c0: {
            get() {
                return _.get(this.editing_background_setting, ["canvas", "background", 0, "color"]) || randomColor()
            },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 0, "color"], val)
            }
        },

        editing_background_linear_x1: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 1, "x"])) / 100, this.is_linear2 ? 1 : 0.5) },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 1, "x"], `${val * 100}%`)
            }
        },
        editing_background_linear_y1: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 1, "y"])) / 100, this.is_linear2 ? 1 : 0.5) },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 1, "y"], `${val * 100}%`)
            }
        },
        editing_background_linear_o1: {
            get() {
                const v = _.get(this.editing_background_setting, ["canvas", "background", 1, "opacity"])
                return v === undefined ? 0.5 : v
            },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 1, "opacity"], val)
            }
        },
        editing_background_linear_c1: {
            get() {
                return _.get(this.editing_background_setting, ["canvas", "background", 1, "color"]) || randomColor()
            },
            set(val) {
                this.setBackgroundType("linear2")
                _.set(this.editing_background_setting, ["canvas", "background", 1, "color"], val)
            }
        },

        editing_background_linear_x2: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 2, "x"])) / 100, 1) },
            set(val) {
                this.setBackgroundType("linear3")
                _.set(this.editing_background_setting, ["canvas", "background", 2, "x"], `${val * 100}%`)
            }
        },
        editing_background_linear_y2: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", 2, "y"])) / 100, 1) },
            set(val) {
                this.setBackgroundType("linear3")
                _.set(this.editing_background_setting, ["canvas", "background", 2, "y"], `${val * 100}%`)
            }
        },
        editing_background_linear_o2: {
            get() {
                const v = _.get(this.editing_background_setting, ["canvas", "background", 2, "opacity"])
                return v === undefined ? 0.5 : v
            },
            set(val) {
                this.setBackgroundType("linear3")
                _.set(this.editing_background_setting, ["canvas", "background", 2, "opacity"], val)
            }
        },
        editing_background_linear_c2: {
            get() {
                return _.get(this.editing_background_setting, ["canvas", "background", 2, "color"]) || randomColor()
            },
            set(val) {
                this.setBackgroundType("linear3")
                _.set(this.editing_background_setting, ["canvas", "background", 2, "color"], val)
            }
        },



        editing_background_cx: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "c", "c_x"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "c", "c_x"], `${val * 100}%`)
            }
        },
        editing_background_cy: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "c", "c_y"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "c", "c_y"], `${val * 100}%`)
            }
        },
        editing_background_cr: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "c", "c_r"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "c", "c_r"], `${val * 100}%`)
            }
        },
        editing_background_co: {
            get() {
                const v = _.get(this.editing_background_setting, ["canvas", "background", "c", "c_opacity"])
                return v === undefined ? 0.5 : v
            },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "c", "c_opacity"], val)
            }
        },
        editing_background_cc: {
            get() {
                return _.get(this.editing_background_setting, ["canvas", "background", "c", "c_color"]) || randomColor()
            },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "c", "c_color"], val)
            }
        },
        editing_background_fx: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "f", "f_x"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "f", "f_x"], `${val * 100}%`)
            }
        },
        editing_background_fy: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "f", "f_y"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "f", "f_y"], `${val * 100}%`)
            }
        },
        editing_background_fr: {
            get() { return noNumber2value(parseInt(_.get(this.editing_background_setting, ["canvas", "background", "f", "f_r"])) / 100, 0.5) },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "f", "f_r"], `${val * 100}%`)
            }
        },
        editing_background_fo: {
            get() {
                const v = _.get(this.editing_background_setting, ["canvas", "background", "f", "f_opacity"])
                return v === undefined ? 0.5 : v
            },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "f", "f_opacity"], val)
            }
        },
        editing_background_fc: {
            get() {
                return _.get(this.editing_background_setting, ["canvas", "background", "f", "f_color"]) || randomColor()
            },
            set(val) {
                this.setBackgroundType("radial")
                _.set(this.editing_background_setting, ["canvas", "background", "f", "f_color"], val)
            }
        },
        editing_background_pure: {
            get() {
                const old = _.get(this.editing_background_setting, ["canvas", "background"])
                return _.isString(old) ? old : randomColor()
            },
            set(val) {
                _.set(this.editing_background_setting, ["canvas", "background"], val)
            }
        },
        editing_background() { return _.get(this.editing_background_setting, ["canvas", "background"]) },
        editing_background_type() { return this.getBackgroundType(this.editing_background) },
        is_linear2() { return this.editing_background_type === "linear2" },
        current_slide_size: () => officeState.getters.current_slide_size,
        background_settings_share() { return _.filter(this.all_background_settings, s => s["user_id"] !== this.userId) },
        background_settings_my() { return _.filter(this.all_background_settings, s => s["user_id"] === this.userId) },
    },
}