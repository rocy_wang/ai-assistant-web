import { aiMixin } from './ai_mixin'
import { addSibling, levelContent, moveContent } from '@/js/office/word'
import { addChildren } from '@/js/office/word'
import { officeCostantMixin } from './office_constant_mixin'
export const wordAIMixin = {
    mixins: [aiMixin, officeCostantMixin],
    methods: {
        addSiblingContent(event) {
            const { content, new_content, is_before } = event
            this.word_slide_data = addSibling(this.word_slide_data, content, new_content, is_before)
        },
        deleteContent(content) {
            const index = _.findIndex(this.word_slide_data, item => item["id"] === content["id"])
            this.word_slide_data.splice(index, 1)
        },
        contentMove(event) {
            this.word_slide_data = moveContent({ ...event, word_slide_data: this.word_slide_data })
        },
        upContent(event) {
            const { current, is_before } = event
            this.word_slide_data = levelContent(this.word_slide_data, current, true, is_before)
        },
        downContent(event) {
            const { current, is_before } = event
            this.word_slide_data = levelContent(this.word_slide_data, current, false, is_before)
        },
        childrenAdded(event) {
            const { content, children } = event
            this.word_slide_data = addChildren(this.word_slide_data, content, children)
        },
        childrenRequired(event) {
            const { content, how } = event
            this.generateContentAI({
                ...this.model_config,
                ...how,
                title: content["title"]
            }).then((children) => this.childrenAdded({
                content, children: _.map(children, child => child["content"])
            }))
        },
    }
}