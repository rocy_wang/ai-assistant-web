import { DATA_CATEGORY_CONTENT_TITLE, DATA_CATEGORY_RICH, DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_TITLE } from '@/js/ppt.js'
import { officeState } from '@/js/office/office_state'
import { str_splice } from '@/js/str_util'
import { is_ancestor } from '@/js/dom_util'
import { itemMixin } from '@/js/office/item_mixin'

const MIN_GENERATE_LENGTH = 3
export default {
    name: "HtmlEditor",
    emits: ["update:html", "maxized", "generate_content", "generate_content_text", "one_key_generate", "deleted"],
    mixins: [itemMixin],
    props: {
        html: { type: String, required: true },
        data_category: { type: String, required: true },
    },
    data() {
        return {
            current_range: undefined,
            prev_html: undefined,
            generate_content_text_visible: false
        }
    },
    watch: {
        html(val) {
            this.editor_div().innerHTML = val
        },
        current_reference_text(val) {
            if (this.rangeInEditor === true && this.is_rich)
                this.current_ai_generation = {
                    reference: val,
                    generate_continue_paragraph: this.insertAfter,
                    generate_keywords_paragraph: this.show("关键字"),
                    generate_subject_paragraph: this.show("内容摘要"),
                    generate_bigger_paragraph: this.replace,
                    generate_replace_paragraph: this.replace,
                    generate_smaller_paragraph: this.replace,
                }
        },
        current_range_global: {
            handler(val) {
                if (_.isNil(val)) return
                if (this.checkrangeInEditor(val)) {
                    this.current_range = val
                    if (this.rangeInEditor === true) {
                        const val = this.current_editor_data()
                        if (_.isEqual(this.current_editor, val)) return
                        this.current_editor = val
                    }
                }
            }, immediate: true, deep: true
        }
    },
    computed: {
        placement() { return officeState.getters.placement },
        is_title() { return [DATA_CATEGORY_TITLE, DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_CONTENT_TITLE].includes(this.data_category) },
        is_rich() { return this.data_category === DATA_CATEGORY_RICH },
        current_ai_generation: { get: () => officeState.state.current_ai_generation, set: (val) => officeState.state.current_ai_generation = val },
        current_editor: { get: () => officeState.state.current_editor, set: (val) => officeState.state.current_editor = val },
        is_run_slide() { return officeState.state.is_run_slide },
        current_range_global: () => officeState.state.current_range,
        current_range_text() { return this.current_range && this.current_range.toString() },
        current_range_collapsed() { return this.current_range && this.current_range.collapsed },
        current_reference_text() {
            if (this.current_range === undefined || this.current_range_text === undefined) return undefined
            if (this.current_range_text.length >= MIN_GENERATE_LENGTH) return this.current_range_text
            else if (this.current_range_collapsed === true) {
                if (_.isNil(this.editor_div())) return
                if (this.editor_div().childNodes.length === 0) return
                let range = document.createRange()
                range.setStart(_.first(this.editor_div().childNodes), 0)
                range.setEnd(this.current_range.endContainer, this.current_range.endOffset)
                if (range.toString().length >= MIN_GENERATE_LENGTH) return range.toString()
            }
            return undefined
        },
        current_range_end_offset() {
            return this.current_range.endOffset
        },
        current_range_end_container() {
            return this.current_range.endContainer
        },
        current_range_end_is_text() {
            return this.current_range_end_container && this.current_range_end_container.nodeType === Node.TEXT_NODE
        },
        rangeInEditor() {
            return this.checkrangeInEditor(this.current_range_global)
        },
        current_editor_div() {
            return this.editor_div()
        }
    },
    methods: {
        checkrangeInEditor(range) {
            return range && this.editor_div() && is_ancestor(range.commonAncestorContainer, this.editor_div())
        },
        generateText(how) {
            this.generate_content_text_visible = false
            this.$nextTick(() => this.$emit('generate_content_text', how))
        },
        current_editor_data() {
            return this.current_range && {
                editor_div: this.editor_div(),
                is_rich: this.is_rich,
                html: this.editor_html(),
                htmlChanged: this.notifyHtmlChanged,
                current_range: this.current_range
            }
        },
        notifyHtmlChanged() {
            this.$emit('update:html', this.editor_html())
        },
        focusOut() {
            const current = this.editor_html()
            if (_.isEqual(this.prev_html, current) === false) {
                this.updateTitleObject()
                this.notifyHtmlChanged()
            }
            else this.prev_html = current
        },
        updateTitleObject() {
            if (this.is_title) officeState.state.current_title_object = ({ getTitle: this.editor_text, text: this.editor_text() })
        },
        titleFocusin() {
            this.updateTitleObject()
        },
        getFirstChild() {
            if (_.isNil(this.current_editor_div)) return undefined
            if (this.current_editor_div.childNodes.length !== 1) return undefined
            const first = this.current_editor_div.childNodes[0]
            if (first.nodeType !== Node.ELEMENT_NODE) return undefined
            return first
        },
        addFirstChild() {
            if (_.isNil(this.getFirstChild())) {
                let range = document.createRange()
                range.selectNodeContents(this.current_editor_div)
                range.surroundContents(document.createElement(`div`))
            }
        },
        focusIn() {

            this.prev_html = this.editor_html()
            if (this.is_title === true) this.titleFocusin()


        },
        show(title) {
            function showGenerated(generated) {
                ElMessageBox.alert(generated, title, { type: "success", center: true, draggable: true }).catch(_.noop)
            }
            return showGenerated
        },
        insertAfter(generated) {
            this.insertTextAfterRange(this.format_paragraph(generated))
            this.notifyHtmlChanged()
        },
        replace(generated) {
            this.replaceRange(this.format_paragraph(generated))
            this.notifyHtmlChanged()
        },
        format_paragraph(generated) {
            return _.join(_.map(_.split(generated, "\n"), line => `\t${_.trim(line)}`), "</br>")
        },
        editor_div() { return this.$refs.RTC },
        replaceRange(text) {
            this.current_range.deleteContents();
            this.current_range.insertNode(document.createTextNode(text));
        },
        insertTextAfterRange(text) {
            if (this.current_range_end_is_text === true) {
                const range_end_text = this.current_range_end_container.textContent
                const new_text = str_splice(range_end_text, this.current_range_end_offset, 0, text)
                this.current_range_end_container.textContent = new_text
            } else {
                const div = document.createElement("div")
                div.innerHTML = text
                $(this.current_range_end_container).after(div)
            }
        },
        editor_html() {
            return this.editor_div() && $(this.editor_div()).prop("innerHTML")
        },
        editor_text() {
            return this.editor_div() && $(this.editor_div()).text()
        },
    },
    mounted() {
        this.editor_div().innerHTML = this.html
    }
}