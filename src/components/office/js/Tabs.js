import { containerMixin } from '@/components/office/js/container_mixin';
import { tabsSampleItem } from '@/js/office/auto_ppt_layout';
export default {
    mixins: [containerMixin],
    methods: {
        getItemHolder(){
            return tabsSampleItem()
        }
    },
}