import ModelConfig from '@/components/ModelConfig.vue'
import AIGenerate from '@/components/office/AIGenerate.vue'
import AddObject from '@/components/office/AddObject.vue'
import Align from '@/components/office/Align.vue'
import FormatObject from '@/components/office/FormatObject.vue'
import GenerateByTitle from '@/components/office/GenerateByTitle.vue'
import ImageSelector from '@/components/office/ImageSelector.vue'
import MASK from '@/components/office/MASK.vue'
import Slide from '@/components/office/Slide.vue'
import SlideSize from '@/components/office/SlideSize.vue'
import WordContents from '@/components/office/WordContents.vue'
import EditorBar from '@/components/office/editor/EditorBar.vue'
import SLD_WORD_CONTENT from '@/components/office/layout_templates/word/SLD_WORD_CONTENT.vue'
import SLD_WORD_TITLE from '@/components/office/layout_templates/word/SLD_WORD_TITLE.vue'
import Buttons from '@/components/office/menu/Buttons.vue'
import FileButtons from '@/components/office/menu/FileButtons.vue'
import ClipEffect from '@/components/office/ppt/ClipEffect.vue'
import ShowHideEffectBar from '@/components/office/ppt/ShowHideEffectBar.vue'
import UseLayout from '@/components/office/ppt/UseLayout.vue'
import { generateStructureSlides, imageWrap, richWrap } from '@/js/office/auto_ppt_layout'
import { GENERATE_SCENE, scenes } from '@/js/office/ppt_layout.js'
import { COLOR_TYPE_BLUE, SVG_IMAGE_ID } from '@/js/office/svg_background.js'
import { DATA_CATEGORY_RICH, KEYCODE_BACKSPACE, KEYCODE_ESCAPE, META_KEY_CTRL, canCopy, canEdit, getSlideObjectId, registerKeyHandler, sample_ppt } from '@/js/ppt'
import { POPOVER_STYLE, pptMixin } from '@/js/ppt.js'
import { defineComponent } from 'vue'

import html2pdf from 'html2pdf.js/dist/html2pdf.bundle.js'

import { PPT_IMPORT_EXTENSIONS } from '@/js/constants'
import { copyText } from '@/js/copy'
import { insureLogged } from "@/js/document"
import { isAnyEmpty, isNotEmpty, randomShareCode } from '@/js/lodash_util'
import { getHeaders } from '@/js/office/auto_ppt_layout'
import { OFFICE_TYPE_PPT, OFFICE_TYPE_WORD, default_background } from '@/js/office/office.js'
import { SHOW_RATIO_MAX, SHOW_RATIO_MIN, SLIDE_RATIO_HW_34, SLIDE_RATIO_HW_916, menu_position_names, menu_positions, officeState, officeStateCache, updateStyle } from '@/js/office/office_state'
import { COLOR_TYPES, COLOR_TYPES_REVERT } from '@/js/office/svg_background.js'
import { ITEM_TYPE_RICH, PAGE_A4, data2contents_tree_ai, getPageSize } from '@/js/office/word'
import { pptUrl, service_post, service_post_config, sharePPTUrl } from "@/js/request/service"
import { SHARE_TOKEN_KEY } from '@/js/user'
import { ElLoading, ElMessage, ElMessageBox } from "element-plus"
import { postAIImage, postAIWithModel } from './ai_mixin'
import { wordAIMixin } from './ai_word'
import { officeCostantMixin } from './office_constant_mixin'
import { slideDragMixin } from '@/components/office/js/slide_drag'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import { downloadBlob } from '@/js/download_util'

const DEFAULT_PPT_NAME = "未命名文件"
const INITIAL_OFFICE = {
    filename: DEFAULT_PPT_NAME,
    slides: []
}

const PAGE_SIZE = 5

export default defineComponent({
    name: "ai-office",
    mixins: [pptMixin, wordAIMixin, officeCostantMixin, slideDragMixin],
    components: { UseLayout, FileButtons, Buttons, ClipEffect, ShowHideEffectBar, Slide, Align, GenerateByTitle, SlideSize, FormatObject, AddObject, AIGenerate, EditorBar, WordContents, SLD_WORD_TITLE, SLD_WORD_CONTENT, ModelConfig, ImageSelector, MASK, },
    data() {
        return {
            office_json_content_show: false,
            office_json_content: undefined,
            office_json_content_before: undefined,
            page_size: PAGE_SIZE,
            current_page: 1,
            zhCn,
            show_status_bar: true,
            SHOW_RATIO_MIN,
            SHOW_RATIO_MAX,
            selected_view_mode: undefined,
            selected_background: undefined,
            POPOVER_STYLE,
            background_visible: false,
            mask_context_visible: false,
            current_mask_ref: undefined,
            current_slide_index_selection: undefined,
            copy_slides: undefined,
            percentage_progress: undefined,
            selected_scene: undefined,
            selected_color_serial_name: undefined,
            COLOR_TYPES,
            scenes,
            current_office_initial: undefined,
            show_use_layout: false,
            save_dialog_visible: false,
            save_dialog_model: undefined,
            loading: false,
            selected_tab: null,
            menu_is_expand: true,
            layout_controler: {},
            my_background: default_background,
            office_selector_visible: false,
            left_side_hide: false,
            current_slide_index: undefined,
            current_office_index: undefined,
            history: [],
            structures: [],
            show_structures: false,
            _sample: undefined,
            extensions: PPT_IMPORT_EXTENSIONS,
            show_test: false,
        }
    },
    methods: {
        isNil: _.isNil,
        bytesSaveFile(base64_arr) {
            service_post_config("/ppt/generate_pdf_from_images", { images: base64_arr }, { responseType: 'blob' }).then(data => {
                downloadBlob(new Blob([data], { type: "application/octet-stream" }), `${this.current_office_filename}.pdf`)
            })
        },
        downloadPPT(format) {
            const loading = ElLoading.service({ lock: true, text: '生成中...', background: 'rgba(0, 0, 0, 0.85)' })
            new Promise((resolve) => {
                switch (format) {
                    case "pdf":
                        const page_size_before = this.page_size
                        this.page_size = 1000
                        html2pdf().set({
                            pagebreak: { mode: 'avoid-all', before: '.section' },
                            margin: 0,
                            filename: `${this.current_office_filename}.pdf`,
                            image: { type: 'jpeg', quality: 0.9 },
                            html2canvas: { logging: false, backgroundColor: "transparent" },
                            jsPDF: { unit: 'in', format: 'letter', orientation: "landscape" },
                        }).from($(`.run_slides`).get(0)).save()
                        this.page_size = page_size_before
                        break
                    case "ai":
                        this.downloadPptToRocy()
                        break
                }
                resolve()
            }).then(() => loading.close())
        },
        saveToJson() {
            try {
                this.current_office = JSON.parse(this.office_json_content)
                this.office_json_content_show = false
            } catch (error) {
                ElMessage({ message: `输入有误，请纠正`, type: "warning", duration: 3000 })
            }
        },
        downloadPptToRocy() {
            const text = JSON.stringify(this.current_office)
            const blob = new Blob([text], { type: 'text/plain' })
            downloadBlob(blob, `${this.current_office_filename}.ai`)
        },
        showJSON() {
            this.office_json_content_show = !this.office_json_content_show
        },
        indexNoPage(index) {
            return (this.current_page - 1) * PAGE_SIZE + index
        },
        moveElementInArray(array, fromIndex, toIndex) {
            const element = array.splice(fromIndex, 1)[0]
            array.splice(toIndex, 0, element)
        },
        slideDragEnd(from_index, to_index) {
            if (from_index === to_index) return
            this.moveElementInArray(this.current_slides, from_index, to_index)
        },
        aiWrite() {
            const QUESTION = "请写一首歌颂秋天的五言绝句,要求对仗工整"
            const x = 100 + Math.round(Math.random() * 80) * (Math.random() < 0.5 ? 1 : -1)
            const y = 100 + Math.round(Math.random() * 80) * (Math.random() < 0.5 ? 1 : -1)
            const addText = (data) => {
                const objects = _.get(this.current_slide, ["objects"]) || []
                const new_obj = richWrap(DATA_CATEGORY_RICH, data.answer, { left: `${x}px`, top: `${y}px` }, 12)
                objects.push(new_obj)
                _.set(this.current_slide, ["objects"], objects)
            }
            const model_id = _.get(officeState.state.model_config, ["model_id"])
            ElMessageBox.prompt(`请给AI下达指令，详细说明您的要求.\n当前私有模型设置: ${model_id || '未设置'}`, 'AI写作', { roundButton: true, inputValue: QUESTION, confirmButtonText: '确认', cancelButtonText: '取消' })
                .then(({ value }) => postAIWithModel(value, addText)).catch(ex => console.log("aiWrite", ex))
        },
        aiDraw() {
            const QUESTION = "请根据如下要求绘图:风格为粉笔画，颜色为黑白色，内容为可爱的小男孩在操场嬉戏"
            const x = 100 + Math.round(Math.random() * 80) * (Math.random() < 0.5 ? 1 : -1)
            const y = 100 + Math.round(Math.random() * 80) * (Math.random() < 0.5 ? 1 : -1)
            const addImage = (base64) => {
                const objects = _.get(this.current_slide, ["objects"]) || []
                const new_obj = imageWrap({ right: `${x}px`, bottom: `${y}px` }, base64)
                objects.push(new_obj)
                _.set(this.current_slide, ["objects"], objects)
            }
            ElMessageBox.prompt(`请给AI下达指令，详细说明您的要求.`, 'AI绘画', { roundButton: true, inputValue: QUESTION, confirmButtonText: '确认', cancelButtonText: '取消' })
                .then(({ value }) => postAIImage(value, addImage)).catch(ex => console.log("aiDraw", ex))
        },
        deleteAllNote() {
            const outer_this = this
            ElMessageBox.confirm(`您确定要删除所有备注吗 ? `, '删除所有备注', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => {
                    if (value === "confirm") _.forEach(outer_this.current_slides, slide => slide["note"] = undefined)
                })
                .catch(_.noop)
        },
        deleteCurrentNote() {
            const outer_this = this
            ElMessageBox.confirm(`您确定要删除备注吗 ? `, '删除备注', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => {
                    if (value === "confirm") outer_this.current_slides[outer_this.current_slide_index]["note"] = undefined
                })
                .catch(_.noop)
        },
        deleteAllAnnotation() {
            const outer_this = this
            ElMessageBox.confirm(`您确定要删除所有幻灯片批注吗 ? `, '删除所有批注', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => {
                    if (value === "confirm") _.forEach(outer_this.current_slides, this.deleteSlideAnnotations)
                })
                .catch(_.noop)
        },
        deleteCurrentAnnotation() {
            const outer_this = this
            ElMessageBox.confirm(`您确定要删除当前幻灯片批注吗 ? `, '删除批注', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => {
                    if (value === "confirm") this.deleteSlideAnnotations(outer_this.current_slides[outer_this.current_slide_index])
                })
                .catch(ex => console.log("deleteCurrentAnnotation", ex))
        },
        deleteSlideAnnotations(slide) {
            _.forEach(slide["objects"], obj => _.unset(obj, ["annotations"]))
        },
        toggleShowNote() {
            officeState.commit('setShowNote', !officeState.state.show_note)
        },
        toggleAnnotation() {
            officeState.commit('setShowAnnotation', !officeState.state.show_annotation)
        },
        slideMaskContextClicked(index) {
            const m = (mask_index) => {
                this.current_mask_ref = _.first(this.$refs[`mask_${mask_index}`])
                this.mask_context_visible = !this.mask_context_visible
            }
            if (this.current_slide_index_selection !== undefined) {
                const in_range = this.current_slide_index_selection.includes(index)
                const [min, max] = [_.min(this.current_slide_index_selection), _.max(this.current_slide_index_selection)]
                if (in_range) m(Math.round((min + max) / 2))
                else {
                    this.current_slide_index_selection = undefined
                    this.current_slide_index = index
                    m(index)
                }
            } else {
                this.current_slide_index = index
                m(index)
            }
        },
        slideMaskClicked(event, index) {
            this.mask_context_visible = false
            const { shiftKey, ctrlKey, altKey } = event
            if (shiftKey === false && ctrlKey === false && altKey === false) {
                this.current_slide_index = index
                this.current_slide_index_selection = undefined
            }
            else {
                if (this.current_slide_index_selection === undefined) {
                    const min = Math.min(index, this.current_slide_index)
                    const max = Math.max(index, this.current_slide_index)
                    this.current_slide_index_selection = _.range(min, max + 1)
                } else {
                    if (this.current_slide_index_selection.includes(index)) this.current_slide_index_selection = _.reject(this.current_slide_index_selection, idx => idx === index)
                    else this.current_slide_index_selection.push(index)
                }
            }
        },
        processName(name) {
            return _.replace(name, /\\|\//g, "_")
        },
        importSlidesAsync(slides) {
            const outer_this = this
            if (_.isEmpty(slides)) return
            let index = 0
            function add() {
                outer_this.addNewSlide(slides[index], false)
                index += 1
                outer_this.percentage_progress = index === slides.length ? undefined : Math.ceil(index * 100 / slides.length)
                if (index !== slides.length) setTimeout(add, 10)
            }
            setTimeout(add, 10)
        },
        importOK() {
            const outer_this = this
            this.show_structures = false
            const scene = this.selected_scene || GENERATE_SCENE
            const color = this.selected_color_serial_name ? COLOR_TYPES_REVERT[this.selected_color_serial_name] : COLOR_TYPE_BLUE
            const bg = { scene, color, bg: this.selected_background }
            function f() {
                const loading = ElLoading.service({ lock: true, text: '生成中...', background: 'rgba(0, 0, 0, 0.85)' })
                setTimeout(() => {
                    const slides = generateStructureSlides(outer_this.structures, bg, false, undefined, { context: window.document })
                    loading.close()
                    importSlidesAsync(slides)
                }, 10);
            }
            setTimeout(f, 10);
        },
        handleImportFile(event) {
            const outer_this = this
            const files = event.target.files
            if (files.length !== 1) return
            const file = files[0]
            const filename = file["name"]
            const filename_no_ext = _.join(_.initial(_.split(filename, ".")), ".")
            let can_add = _.find(this.extensions, e => _.endsWith(filename, e)) != undefined
            function readOfficeFile(file, success, error) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    const content = e.target.result;
                    try {
                        const office = JSON.parse(content)
                        const keys = Object.keys(office)
                        const valid = _.every(["filename", "id", "slides"], key => keys.includes(key))
                        if (valid === true) success(_.omit(office, ["id"]))
                        else error()
                    } catch (error) {
                        error()
                    }
                };
                reader.onerror = function (e) {
                    error()
                };
                reader.readAsText(file);
            }
            if (can_add === true) {
                if (_.endsWith(filename, ".ai")) {
                    readOfficeFile(file, (office_no_id) => {
                        if (this.all_office_names.includes(filename_no_ext)) {
                            ElMessageBox.confirm(`同名文件已经存在, 是否覆盖 ? `, '请您确认', { distinguishCancelAndClose: true, confirmButtonText: '覆盖', cancelButtonText: '均保留', type: 'warning' })
                                .then(() => {
                                    const index = _.findIndex(this.history, office => _.get(office, ["filename"]) === filename_no_ext)
                                    const office_id = _.get(this.history[index], "id")
                                    service_post("/office/delete", { office_id }).then(() => {
                                        this.history.splice(index, 1)
                                        this.saveOfficePost(office_no_id)
                                    })
                                })
                                .catch((value) => {
                                    switch (value) {
                                        case "cancel":
                                            this.saveOfficePost(office_no_id)
                                            break;
                                        case "close":
                                            ElMessage({ message: `导入取消了 ${filename}`, type: "info" })
                                            break;
                                    }
                                })
                        }
                    }, () => ElMessage({ message: `这个是无效文件`, type: "warning" }))
                } else
                    service_post("/ppt/parse_file_as_ppt", { file }).then(structures => {
                        $("#import_file_structure").val(undefined)
                        outer_this.structures = structures
                        const is_objects = _.every(structures, structure => Object.keys(structure).includes("objects"))
                        if (is_objects === true) outer_this.importOK()
                        else outer_this.show_structures = true
                    })
            }
        },
        importFileStructure() {
            $("#import_file_structure").trigger("click")
        },
        resized({ width, height }) {
            officeState.state.page_width = width
            officeState.state.page_height = height
        },
        slideSizeChanged({ size, ratio, range }) {
            console.log("slideSizeChanged before", JSON.stringify(officeState.getters.current_slide_size))
            let slides_change_range = []
            let new_size = undefined
            if (["当前及以后", "所有页面"].includes(range)) {
                if (this.is_ppt === true) officeState.commit("updateSlideSizeRatio", ratio)
                else officeState.commit("updateSlideSize", size)
                new_size = officeState.getters.current_slide_size
            } else {
                if (this.is_ppt === true) new_size = { width: officeState.getters.slide_width_ratio(ratio), height: officeState.getters.slide_height }
                else if (_.isBoolean(size)) {
                    const r = size === true ? SLIDE_RATIO_HW_34 : SLIDE_RATIO_HW_916
                    new_size = { width: officeState.getters.slide_width_ratio(r), height: officeState.getters.slide_height }
                } else new_size = size
            }
            switch (range) {
                case "当前页面":
                    slides_change_range = [this.current_slide_index, this.current_slide_index + 1]
                    break
                case "当前及以前":
                    slides_change_range = [0, this.current_slide_index + 1]
                    break
                case "当前及以后":
                    slides_change_range = [this.current_slide_index, this.current_slides.length]
                    break
                case "所有页面":
                    slides_change_range = [0, this.current_slides.length]
                    break
            }
            console.log("slideSizeChanged after", JSON.stringify(officeState.getters.current_slide_size))
            const [start, end] = slides_change_range
            let slides_change = _.cloneDeep(_.slice(this.current_slides, start, end))
            slides_change = this.resizeSlides(slides_change, new_size)
            console.log("slideSizeChanged check", slides_change)
            this.current_slides.splice(start, (end - start), ...slides_change)
        },

        resizeSlides(slides_change, new_size) {
            const { width, height } = new_size
            return _.map(slides_change, slide => _.assign({}, slide, { "config": { "style": { "height": `${height}px`, width: `${width}px` } } }))
        },
        addSlideAfter(slide) {
            this.addNewSlide(slide, true, this.current_slide_index + 1)
        },

        addNewSlide(slide, to_last, add_to_index) {
            to_last = to_last !== false
            if (add_to_index === undefined) add_to_index = this.current_slides.length
            this.multi_select_object_keys = []
            this.$nextTick(() => {
                const adds = _.isArray(slide) ? slide : [slide]
                if (_.isNil(this.current_slide_index)) this.current_slide_index = this.slides.length - 1
                this.current_slides.splice(add_to_index, 0, ...adds)
                if (to_last) {
                    this.current_slide_index = this.current_slide_index + adds.length
                    this.scrollToSlide()
                }
            })
        },
        office_file_image(office) {
            const map = { "工作": "/img/office/office.svg", "生活": "/img/office/home.svg", "其他": "/img/office/ppt.svg" }
            return _.get(map, office['scene'], map['其他'])
        },
        dontShare() {
            _.unset(this.current_office, ["category"])
            _.set(this.current_office, ["is_share"], false)
            this.current_office_share_code = undefined
        },
        shareCodeAction() {
            if (isAnyEmpty(this.current_office_share_code)) {
                _.set(this.current_office, ["category"], "播放")
                _.set(this.current_office, ["is_share"], true)
                this.current_office_share_code = randomShareCode()
            }
            this.play_ppt = { slides: this.current_slides, auto_play: false, auto_seconds: 3, perserve_ratio: false }
            copyText(document, sharePPTUrl(this.current_office_share_code))
            ElMessage({ message: `共享链接已经拷贝至剪切板`, type: "success", duration: 3000 })
        },
        slideTemplateSelected(templateSlide) {
            this.show_use_layout = false
            this.addNewSlide(templateSlide)
        },
        getSlideId(index) { return 'slide_' + index },
        getMaskId(index) { return 'mask_' + index },
        startSlides(run_index) {
            const play_option = { auto_play: false, auto_seconds: 3, perserve_ratio: false }
            if (this.current_slide_index_selection === undefined) this.play_ppt = { index: run_index, slides: this.current_slides, ...play_option }
            else {
                const slides = _.map(this.current_slide_index_selection, idx => this.current_slides[idx])
                this.play_ppt = { index: 0, slides, ...play_option }
            }
            this.current_slide_index_selection = undefined
            this.mask_context_visible = false
            this.$router.push({ path: '/ppt/play' })
        },
        switchLayoutHandler(to_switch_layout) {
            const new_slide = this.toLayout(this.current_slide, to_switch_layout)
            this.current_slides[this.current_slide_index] = new_slide
        },
        switchLayout() {
            this.layout_controler = {
                visible: true,
                handler: this.switchLayoutHandler,
                current_slide_layout: this.current_slide_layout
            }
        },


        deleteOffice(index) {
            let office_id = this.getOfficeId(index)
            const office_name = this.getOfficeName(index)
            ElMessageBox.confirm(`您确定要删除如下文件吗 ?\n${office_name}`, '请您确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => {
                    if (value === "confirm") {
                        service_post("/office/delete", { office_id }).then(() => {
                            ElMessage({ message: "删除成功", type: "success", duration: 3000 })
                            this.history.splice(index, 1)
                            if (this.current_office_index > index) this.current_office_index = this.current_office_index - 1
                        })
                    }
                })
                .catch(_.noop)

        },
        openOffice(index) {
            this.current_office_index = index
            this.office_selector_visible = false
        },
        saveBackgrounds() {
            const all_saves = _.map(this.current_slides, (slide, index) => {
                let slide_obj = this.$refs[this.getSlideId(index)]
                if (_.isArray(slide_obj)) slide_obj = _.head(slide_obj)
                return slide_obj.saveAutoSVGBackground().then(id => ({ id, index }))
            })
            return Promise.all(all_saves)
        },
        clipboardShareLink(office) {
            const share_code = office["share_code"]
            copyText(document, canCopy(office['category']) ? pptUrl(share_code) : sharePPTUrl(share_code))
            ElMessage({ message: `共享链接已经拷贝至剪切板`, type: "success", duration: 3000 })
        },
        save(save_info) {
            this.save_dialog_visible = false
            this.saveBackgrounds().then((id_index_list) => {
                const slide_ids = []
                _.forEach(id_index_list, ({ id, index }) => _.set(slide_ids, [index, "config", "style", SVG_IMAGE_ID], id))
                const { filename, category, scene, is_share, is_save_as } = save_info || {}
                let new_filename = filename || this.current_office_filename
                if (is_share === true && isAnyEmpty(this.current_office_share_code)) this.current_office_share_code = randomShareCode()
                let merged = _.merge({}, this.current_office, { "slides": slide_ids })
                merged = _.merge({}, merged, { filename: new_filename, office_type: this.office_type, category, scene, is_share, })
                if (is_share === true) this.clipboardShareLink(merged)
                if (is_save_as === true) delete merged["id"]
                this.saveOfficePost(merged)
            })
        },
        saveOfficePost(merged) {
            const new_filename = merged["filename"]
            const is_save_as = _.get(merged, ["id"]) === undefined
            service_post("/office/save", { office: merged }).then((office_id) => {
                if (_.isNil(office_id)) {
                    ElMessage({ message: `保存失败`, type: "error", duration: 3000 })
                    return
                }
                ElMessage({ message: `保存成功: ${new_filename}`, type: "success", duration: 3000 })
                const office_with_id = _.assign({}, merged, { id: office_id })
                if (is_save_as === true) {
                    this.history.unshift(office_with_id)
                    this.current_office_index = 0
                } else {
                    const index = _.findIndex(this.history, office => _.get(office, ["id"]) === office_id)
                    _.set(this.history, [index], merged)
                }
            })
        },
        saveOffice() {
            if (_.isNil(this.current_office_id)) this.saveAsShareOffice(undefined, undefined)
            else this.save()
        },
        shareOffice() {
            this.saveAsShareOffice(undefined, true)
        },
        saveAsOffice() {
            this.saveAsShareOffice(true, undefined)
        },
        saveAsShareOffice(is_save_as, is_share) {
            const model = _.pick(this.current_office, ["filename", "category", "scene", "is_share"])
            this.save_dialog_model = _.merge(model, { is_save_as, is_share })
            this.save_dialog_visible = true
        },
        downloadOffice() {
            this.saveOffice()
            service_post("/office/download", { office_id: this.current_office_id, download: "true" }, (data) => {
                const blob = new Blob([data], { type: "application/octet-stream" })
                downloadBlob(blob, `${this.current_office_filename}${this.extension}`)
            })
        },
        isCurrent(index) {
            if (this.current_slide_index_selection !== undefined)
                return this.current_slide_index_selection.includes(index)
            return index === this.current_slide_index
        },
        initializeOffice() {
            const new_office = { office_type: this.office_type, ...INITIAL_OFFICE }
            this.history.unshift(new_office)
            this.current_office_index = 0
            this.current_slide_index = undefined
            this.addSlide()
        },

        newSlides() {
            if (this.changed === true)
                ElMessageBox.confirm('您要保存更改吗?', '请您确认', { confirmButtonText: '保存', cancelButtonText: '不保存', type: 'warning' })
                    .then(() => {
                        this.saveOffice()
                        this.initializeOffice()
                    })
                    .catch(() => this.initializeOffice())
            else this.initializeOffice()
        },

        scrollToSlide(index) {
            const to_index = index || this.current_slide_index
            this.$nextTick(() => {
                $(`#${this.getSlideId(to_index)}`).get(0).scrollIntoView({ behavior: 'smooth' })
            })
        },
        getSlideId(index) {
            return `slide_${index}`
        },
        addLayout() {
            this.layout_controler = {
                visible: true,
                handler: this.switchLayoutHandler,
                current_slide_layout: this.current_slide_layout
            }
        },
        addSlide() {
            this.show_use_layout = true
        },
        copySlide() {
            const slides = (this.current_slide_index_selection === undefined) ? [this.current_slides[this.current_slide_index]] : _.map(this.current_slide_index_selection, idx => this.current_slides[idx])
            this.copy_slides = _.cloneDeep(slides)
            this.current_slide_index_selection = undefined
            this.mask_context_visible = false
            ElMessage({ message: `${slides.length}项已经拷贝`, type: "success" })
        },
        pasteSlide() {
            this.addNewSlide(this.copy_slides, true, this.current_slide_index + this.copy_slides.length)
            this.copy_slides = undefined
            this.mask_context_visible = false
        },
        deleteSlide() {
            const section = this.current_slide_index_selection || [this.current_slide_index]
            const d = () => {
                this.current_slides = _.reject(this.current_slides, (_, idx) => section.includes(idx))
                if (this.current_slide_index > this.current_slides.length - 1) this.current_slide_index = this.current_slides.length - 1
                this.current_slide_index_selection = undefined
                this.mask_context_visible = false
            }
            ElMessageBox.confirm(`确定要删除这${section.length === 1 ? "" : section.length}张幻灯片吗 ? `, '请您确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => (value !== "confirm") ? _.noop() : d())
                .catch(_.noop)

        },
        showSample() {
            this.history.unshift(this.sample)
            this.current_office_index = 0
        },
        getHistoryByIndex(index) { return _.get(this.history, [index]) },
        getOfficeName(index) { return _.get(this.getHistoryByIndex(index), "filename") },
        getOfficeId(index) { return _.get(this.getHistoryByIndex(index), "id") },
        gotOffice() {
            if (this.history.length === 0) this.history.push(this.sample)
            this.current_office_index = 0
            if (isNotEmpty(this.current_slides)) {
                this.current_slide_index = 0
                this.$nextTick(() => {
                    let slide = this.$refs[this.getSlideId(this.current_slide_index)]
                    if (_.isArray(slide)) slide = _.head(slide)
                    slide && slide.slideFocusin()
                })
            }
            this.loading = false
        }
    },
    computed: {
        current_slide_objects() {
            return _.get(this.current_slide, ["objects"]) || []
        },
        all_office_names() {
            return _.map(this.history, office => _.get(office, "filename"))
        },
        json_can_save() {
            try {
                const j = JSON.parse(this.office_json_content)
                return _.isEqual(j, this.current_office) === false
            } catch (error) {
                return false
            }
        },
        state_size() { return officeState.getters.size },
        current_model_id() { return officeState.getters.current_model_id },
        current_slide_zoom_percent() { return this.current_slide_zoom / 100 },
        dynamic_name: { get() { return officeStateCache.state.dynamic_name }, set(val) { officeStateCache.commit('setDynamicName', val) } },
        is_slide_view: { get() { return officeStateCache.state.is_slide_view }, set(val) { officeStateCache.commit('setSlideView', val) } },
        current_slide_zoom: { get() { return officeState.state.current_slide_zoom }, set(val) { officeState.commit('setCurrentSlideZoom', val) } },
        is_travel: { get() { return officeState.state.is_slide_view_travel }, set(val) { officeState.commit('setTravel', val) } },
        is_dark: { get() { return officeState.state.is_dark }, set(val) { officeState.commit('setTheme', val) } },
        left_side_can_show() { return this.is_ppt ? isNotEmpty(this.current_slides) : isNotEmpty(this.word_slide_data) },
        word_slide_data() {
            if (isAnyEmpty(this.current_slides)) return []
            if (this.is_ppt) return []
            const all_objects = _.flatMap(this.current_slides, (slide, slide_index) => _.map(slide["objects"] || [], (object, object_index) => ({ object, slide_index, object_index })))
            const all_editors = _.filter(all_objects, ({ object }) => object["type"] === ITEM_TYPE_RICH)
            const all_htmls = _.map(all_editors, ({ object, slide_index, object_index }) => ({ html: object["html"], slide_index, object_index }))
            const all_headers = _.flatMap(all_htmls, ({ html, slide_index, object_index }) => _.map(getHeaders(html, document.body), (header, header_index) => ({ ...header, slide_index, object_index, header_index })))
            return data2contents_tree_ai(all_headers)
        },
        multi_select_object_keys: { get() { return officeState.state.multi_select_object_keys }, set(val) { officeState.state.multi_select_object_keys = val } },
        maxized_item: { get() { return officeState.state.maxized_item }, set(val) { officeState.state.maxized_item = val } },
        show_maxized_editor: { get() { return this.maxized_item !== undefined }, set(val) { if (val === false) this.maxized_item = undefined } },
        share_token() {
            return _.get(this.$route, ["query", SHARE_TOKEN_KEY]);
        },
        shared() {
            return _.get(this.current_office, ["is_share"]) === true
        },
        share_image() {
            return this.shared ? '/img/office/share-fill.svg' : '/img/office/share.svg'
        },
        play_buttons() {
            return [{
                content: "放映",
                click: () => this.startSlides(0),
                src: "/img/office/play.svg"
            }, {
                content: this.current_office_share_code_prompt,
                click: this.shareCodeAction,
                src: this.share_image
            }, {
                content: "取消共享",
                vif: this.shared,
                click: this.dontShare,
                src: '/img/cancel_black.svg'
            }, {
                content: "切换至PPT",
                vif: this.is_word,
                click: () => this.$router.push('/ppt'),
                src: "/img/office/ppt.svg"
            }, {
                content: "切换至Word",
                vif: this.is_ppt,
                click: () => this.$router.push('/word'),
                src: "/img/office/word.svg"
            }, {
                content: "下载",
                vif: this.is_word,
                click: this.downloadOffice,
                src: "/img/office/download.svg"
            },]
        },
        design_buttons() {
            return [{
                content: this.is_ppt ? "新建幻灯片" : "添加页面",
                click: this.addSlide,
                src: "/img/office/new.svg"
            }, {
                content: this.is_ppt ? "删除幻灯片" : "删除页面",
                disabled: this.current_slide_index === undefined,
                click: this.deleteSlide,
                src: "/img/office/delete.svg"
            }, {
                content: "模型",
                click: () => this.model_config_visible = !this.model_config_visible,
                src: "/img/office/ai.svg"
            },]
        },
        file_buttons() {
            return [{
                content: "导入",
                click: this.importFileStructure,
                src: "/img/office/import.svg"
            }, {
                content: "新建",
                click: this.newSlides,
                src: "/img/office/new.svg"
            }, {
                content: "保存",
                disabled: this.cant_save === true,
                click: this.saveOffice,
                src: "/img/office/save.svg"
            }, {
                content: "另存",
                disabled: this.cant_save_as === true,
                click: this.saveAsOffice,
                src: "/img/office/save_as.svg"
            }, {
                content: "协作",
                click: this.shareOffice,
                src: this.share_image
            }, {
                content: "取消共享",
                vif: this.shared,
                click: this.dontShare,
                src: '/img/cancel_black.svg'
            }, {
                content: "打开",
                vif: this.history_has_id.length !== 0,
                click: () => this.office_selector_visible = true,
                src: "/img/office/open.svg"
            }, {
                content: "示例",
                click: this.showSample,
                vif: this.is_example === false,
                src: "/img/office/example.svg"
            }, {
                content: "JSON",
                click: this.showJSON,
                vif: this.current_office_id !== undefined,
                src: "/img/filetype-json.svg"
            }, {
                content: "删除",
                vif: this.current_office_id !== undefined,
                click: () => this.deleteOffice(this.current_office_index),
                src: "/img/office/delete.svg"
            }, {
                content: this.is_dark ? "高亮" : "暗黑",
                click: () => this.is_dark = !this.is_dark,
                src: this.is_dark ? "/img/sun.svg" : "/img/moon.svg"
            }, {
                content: "AI Chat",
                click: () => this.$router.push("/chat"),
                src: "/img/chat_primary.svg"
            }]
        },
        play_ppt: {
            get() { return officeState.state.play_ppt },
            set(val) { officeState.state.play_ppt = val }
        },
        state: { get: () => officeState.state },
        image_selection_visible: { get: () => officeState.state.image_selection_visible, set: (val) => officeState.commit('updateImageVisible', val) },
        is_column() { return ['left', 'right'].includes(this.menu_position) },
        is_column_class() { return { 'd-flex flex-column': this.is_column, 'd-flex flex-row': !this.is_column } },
        menu_position_class() {
            return { 'bg-transparent': !this.menu_is_expand, 'bg-light': this.menu_is_expand, 'flex-column h-100': this.is_column, 'w-100': !this.is_column }
        },
        office_layout() {
            switch (this.menu_position) {
                case "top": return ["flex-column"]
                case "bottom": return ["flex-column-reverse"]
                case "left": return ["flex-row"]
                case "right": return ["flex-row-reverse"]
            }
        },
        menu_position_style() {
            return _.fromPairs(_.map(["top", "left", "right", "bottom"], pos => [pos, this.menu_position === pos ? 0 : "unset"]))
        },
        placement() { return officeState.getters.placement },
        menu_position_name: {
            get() { return menu_positions[this.menu_position] },
            set(val) { this.menu_position = menu_position_names[val] }
        },
        menu_position: { get: () => officeState.state.menu_position, set: (val) => officeState.state.menu_position = val },
        current_title_object: () => officeState.state.current_title_object,
        current_slide_selcted_object_count() { return officeState.getters.current_slide_selcted_object_count },
        current_slide_object() { return officeState.state.current_slide_object },
        current_format_slide_object() { return officeState.state.current_format_slide_object },
        current_format_object() { return officeState.state.current_format_object },
        current_format_obj() { return officeState.getters.current_format_obj },
        current_ai_generation() { return officeState.state.current_ai_generation },
        current_editor() { return officeState.state.current_editor },
        is_run_slide: { get: () => officeState.state.is_run_slide, set: (val) => officeState.state.is_run_slide = val },
        route_path() {
            return _.get(this.$route, "path")
        },
        is_ppt() { return this.route_path === "/ppt" || this.route_path === "/ppt/" },
        is_word() {
            return this.is_ppt === false
        },
        office_type() {
            return this.is_ppt ? OFFICE_TYPE_PPT : OFFICE_TYPE_WORD
        },
        extension() {
            return this.is_ppt ? ".pptx" : ".docx"
        },
        icon() {
            return this.is_ppt ? "ppt.svg" : "word.svg"
        },
        sample() {
            if (this._sample === undefined) this._sample = _.cloneDeep(sample_ppt())
            return this._sample
        },
        history_has_id() {
            return _.reject(this.history, h => _.isNil(h["id"]))
        },
        current_slides: {
            get() { return _.get(this.current_office, "slides") },
            set(val) { _.set(this.current_office, "slides", val) }
        },
        current_slides_page() {
            return _.slice(this.current_slides, (this.current_page - 1) * this.page_size, this.current_page * this.page_size)
        },
        current_slides_count() { return (this.current_slides || []).length },
        current_office_id: {
            get() { return _.get(this.history, [this.current_office_index, "id"]) },
            set(val) { _.set(this.history, [this.current_office_index, "id"], val) }
        },
        current_office_share_code_prompt() {
            return this.shared ? "共享文档" : "拷贝链接"
        },
        current_office_share_code: {
            get() { return _.get(this.current_office, "share_code") },
            set(val) {
                if (_.isNil(val)) _.unset(this.current_office, "share_code")
                else _.set(this.current_office, "share_code", val)
            }
        },
        current_office_filename: {
            get() { return _.get(this.current_office, "filename") },
            set(val) { _.set(this.current_office, "filename", val) }
        },
        current_office: {
            get() { return _.get(this.history, [this.current_office_index]) },
            set(val) { _.set(this.history, [this.current_office_index], val) }
        },
        pageShow() {
            if (_.isEmpty(this.current_slides)) return undefined
            return _.isNil(this.current_slide_index) ? `共${this.current_slides.length}页` : `${this.current_slide_index + 1} / ${this.current_slides.length}`
        },
        changed() {
            return _.isEqual(this.current_office_initial, this.current_office) === false
        },
        can_save() {
            return (this.changed === true || _.isNil(this.current_office_id)) && isNotEmpty(this.current_slides)
        },
        is_example() { return _.isNil(this.current_office_id) },
        can_save_as() { return _.isNil(this.current_office_id) === false },
        cant_save() { return this.can_save === false },
        cant_save_as() { return this.can_save_as === false },
        current_slide() {
            return _.get(this.current_slides, [this.current_slide_index])
        },
        current_slide_layout() {
            return _.get(this.current_slide, "layout")
        }
    },
    mounted() {
        registerKeyHandler("KeyA", "object_selection", () => {
            const keys = _.map(this.current_slide_objects, (o, index) => getSlideObjectId(index))
            officeState.state.multi_select_object_keys = keys
        }, META_KEY_CTRL)
        registerKeyHandler(KEYCODE_ESCAPE, "object_selection", () => {
            officeState.state.multi_select_object_keys = []
        })
        registerKeyHandler(KEYCODE_BACKSPACE, "object_selection", () => {
            const cnt = officeState.getters.current_slide_selcted_object_count
            if (cnt < 2) return
            ElMessageBox.confirm(`确定删除这${cnt}项吗 ?`, '请您确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then(() => {
                    let objects = _.get(this.current_slides, [this.current_slide_index, "objects"]) || []
                    objects = _.reject(objects, (obj, index) => officeState.getters.current_slide_selcted_object_index_list.includes(index))
                    _.set(this.current_slides, [this.current_slide_index, "objects"], objects)
                })
                .catch(_.noop)
        })
        updateStyle('透明')
        $("link[rel='icon']").remove()
        $("head").append(`<link rel = "icon" href = "/img/${this.icon}"/>`)
        $("head title").text(this.is_word ? "AI Word" : "AI PPT")
        officeState.commit("unsetSize")
        if (this.is_ppt) officeState.commit("updateSlideSize", true)
        else officeState.commit("updateSlideSize", PAGE_A4)
        if (insureLogged(this, true)) {
            const loading = ElLoading.service({ lock: true, text: '加载中...', background: 'rgba(0, 0, 0, 0.85)' })
            this.loading = true
            service_post("/office/get_offices", { "office_type": this.office_type }).then((offices) => {
                this.history = _.reverse(_.sortBy(offices, "create_time"))
                if (isNotEmpty(this.share_token)) service_post("/office/get_shared", { [SHARE_TOKEN_KEY]: this.share_token }).then(office => {
                    const category = _.get(office, ["category"])
                    if (canCopy(category)) {
                        if (canEdit(category) === false) {
                            _.unset(office, ["share_code"])
                            _.unset(office, ["is_share"])
                            _.unset(office, ["category"])
                            _.unset(office, ["last_user_id"])
                            _.unset(office, ["user_id"])
                            _.unset(office, ["id"])
                        }
                        this.history.unshift(office)
                        this.gotOffice()
                    } else ElMessage({ message: "该文件已经失效", type: "warning" })
                })
                else this.gotOffice()
                loading.close()

            }).catch((ex) => {
                console.error("ex", ex)
                loading.close()
                this.loading = false
            })
        }
    },
    watch: {
        page_size: {
            handler(val) {
                this.current_page = 1
            }, immediate: true, deep: true
        },
        is_ppt: {
            handler(val) {
                officeState.state.is_ppt = val
            }, immediate: true, deep: true
        },
        office_json_content_show: {
            handler(val) {
                if (val === true) {
                    this.office_json_content = JSON.stringify(this.current_office, undefined, 4)
                    this.office_json_content_before = _.cloneDeep(this.office_json_content)
                }
            }, immediate: true, deep: true
        },
        dynamic_name: {
            handler(val) {
                if (val !== undefined) this.selected_tab = val
            }, immediate: true, deep: true
        },
        is_dark: {
            handler(val) {
                if (val === true) ($("html").addClass("dark"))
                else ($("html").removeClass("dark"))
            }, immediate: true, deep: true
        },
        current_office() {
            this.current_page = 1
            this.current_office_initial = _.cloneDeep(this.current_office)
        },
        current_office_filename(val) {
            const category = this.is_word ? "AI Word" : "AI PPT"
            $("head title").text(`${category} - ${val}`)
        }
    }
})