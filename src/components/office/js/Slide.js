import { deleteItem, get_default, setClone, spliceClone } from '@/js/lodash_util'
import { MODE_EYE, MODE_RUN, MODE_LAYOUT, MODE_DESIGN, DATA_CATEGORY_CONTENT_TITLE, DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_TITLE, DATA_CATEGORY_RICH, DEFAULT_VIDEO_URL, registerKeyHandler, KEYCODE_ESCAPE, HANDLER_KEY_ADDOBJECT, unregisterKeyHandler, getSlideObjectId } from '@/js/ppt.js'
import { base64image } from '@/js/office/office.js'
import { formatMixin } from '@/js/office/format_object_mixin.js'
import { get_item_id, ITEM_TYPE_HEADER, ITEM_TYPE_PARAGRAPH, ITEM_TYPE_IMAGE, ITEM_TYPE_TABLE, ITEM_TYPE_VIDEO, ITEM_TYPE_RICH, item2header, data2anchor, ITEM_TYPE_CHART, ITEM_TYPE_TABS, ITEM_TYPE_COLLAPSE, ITEM_TYPE_POPUP, ITEM_TYPE_STEPS, ITEM_TYPE_TIMELINE } from '@/js/office/word.js'
import { ElMessageBox } from 'element-plus'
import { sample_table_data } from "@/js/data.js";
import { officeState, officeStateCache } from '@/js/office/office_state'
import { extractNumber, far_point } from '@/js/office/style'
import { slideObjectDragMixin } from '@/js/office/slide_object_drag.js'
import { absolute2relative, collapseWrap, setExampleHeight, tabsWrap, popupWrap, timelineWrap, stepsWrap } from '@/js/office/auto_ppt_layout'
import { POPOVER_STYLE } from '@/js/ppt'
import { userStore } from '@/js/user'

const default_reference_length = 128
const names = {
    [ITEM_TYPE_RICH]: "文本工具",
    [ITEM_TYPE_IMAGE]: "图片工具",
    [ITEM_TYPE_CHART]: "图表工具",
    [ITEM_TYPE_VIDEO]: "视频工具",
    [ITEM_TYPE_TABLE]: "表格工具",
    [ITEM_TYPE_COLLAPSE]: "折叠工具",
    [ITEM_TYPE_TABS]: "标签页工具",
    [ITEM_TYPE_POPUP]: "弹出工具",
    [ITEM_TYPE_STEPS]: "步骤工具",
    [ITEM_TYPE_TIMELINE]: "时间线工具",
}

export default {
    name: "Slide",
    emits: ["update:objects", "update:note", "generate_content", "generate_content_text", "one_key_generate"],
    mixins: [formatMixin, slideObjectDragMixin],
    props: {
        zoom: {
            type: Number,
            required: false
        },
        mode: {
            type: Number,
            default: MODE_DESIGN,
            required: false
        },
        objects: {
            type: Array,
            required: false
        },
        note: {
            type: String,
            required: false
        },
    },
    methods: {
        set: _.set,
        far_point,
        extractNumber,
        get_default,
        item2header,
        data2anchor,
        base64image,
        getSlideObjectId,
        itemFocused(index) {
            this.current_item_index = index
        },
        generateAnnotationLines(point_pair_list) {
            return _.map(point_pair_list, ([point1, point2]) => generateAnnotationLine(point1, point2))
        },
        generateAnnotationLine(point1, point2) {
            const [x1, y1] = point1
            const [x2, y2] = point2
            const width = Math.max(x1, x2) - Math.min(x1, x2)
            const height = Math.max(y1, y2) - Math.min(y1, y2)
            return `<svg width="${width}" height="${height}" xmlns="http://www.w3.org/2000/svg">
                <line x1="${x1}" y1="${y1}" x2="${x2}" y2="${y1}" stroke="red" stroke-width="2" />
                <line x1="${x2}" y1="${y2}" x2="${x2}" y2="${y1}" stroke="red" stroke-width="2" />
            </svg>`
        },
        noteChanged() {
            this.$emit("update:note", this._note)
        },
        addDroppedObject(event) {
            this.$nextTick(() => {
                const { drag_index, drop_index, child_location } = event
                const drag = _.cloneDeep(this._objects[drag_index])
                const tmp_objects = spliceClone(this._objects, drag_index, 1)
                const drop = tmp_objects[drop_index]
                const drop_type = drop['type']
                const drop_object = drop['object']
                const converted_drop = setExampleHeight(absolute2relative(drag), 100)
                switch (drop_type) {
                    case ITEM_TYPE_TABS:
                    case ITEM_TYPE_COLLAPSE:
                    case ITEM_TYPE_POPUP:
                        const [name, item_index] = child_location
                        drop_object[name].splice(item_index, 0, converted_drop)
                        break
                    case ITEM_TYPE_TIMELINE:
                        const [father_index, object_index] = child_location
                        _.get(drop_object, [father_index, ["content_object"]]).splice(object_index, 0, converted_drop)
                        break
                    default:
                        break
                }
                this.notifyObjectsChanged(tmp_objects)
            })
        },
        annotationAdd(index) {
            ElMessageBox.prompt('请输入批注', '添加', { confirmButtonText: '确认', cancelButtonText: '取消' })
                .then(({ value }) => this.addAnnotation(index, value)).catch(_.noop)
        },
        annotationUpdate(event, index, aindex) {
            ElMessageBox.prompt('请输入批注', '编辑', { roundButton: true, inputValue: event, inputType: "textArea", confirmButtonText: '确认', cancelButtonText: '取消' })
                .then(({ value }) => this.updateAnnotation(value, index, aindex)).catch(_.noop)
        },
        annotationDelete(index, aindex) {
            ElMessageBox.confirm('确定要删除这个批注吗?', '确认', { confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning' })
                .then((value) => (value !== "confirm") ? _.noop() : this.deleteAnnotation(index, aindex))
                .catch(console.log)
        },
        deleteSlideObject(index) {
            this.multi_select_object_keys = deleteItem(this.multi_select_object_keys, getSlideObjectId(index))
            this.$nextTick(() => this.notifyObjectsChanged(spliceClone(this._objects, index, 1)))
        },
        deleteAnnotation(index, aindex) {
            const annotations = _.get(this._objects, [index, "annotations"]) || []
            annotations.splice(aindex, 1)
            _.set(this._objects, [index, "annotations"], annotations)
            this.notifyObjectsChanged(this._objects)
        },
        updateAnnotation(annotation, index, aindex) {
            _.set(this._objects, [index, "annotations", aindex], { annotation, userId: userStore.getters.userEmailTail, ts: Date().valueOf() })
            this.notifyObjectsChanged(this._objects)
        },
        addAnnotation(index, annotation) {
            const annotations = _.get(this._objects, [index, "annotations"]) || []
            annotations.push({ annotation, userId: userStore.getters.userEmailTail, ts: Date().valueOf() })
            _.set(this._objects, [index, "annotations"], annotations)
            this.notifyObjectsChanged(this._objects)
        },
        updateSlideObject(index, o) {
            this.notifyObjectsChanged(setClone(this._objects, [index], o))
        },
        notifyObjectsChanged(new_objects) {
            this.$emit('update:objects', new_objects)
        },
        
        mouseUp(event) {
            this.multi_select_object_keys = []
            this.focused_object_key = undefined
            this.slideFocusin()
        },
        executeAlign(align, to) {
            const outer_this = this
            const locations = this.selected_item_locations
            const location_pairs = _.toPairs(locations)
            const location_item_last = _.get(locations, this.last_select_key)
            const _objects = this._objects
            const count = Object.keys(locations).length
            const lefts = _.map(locations, item => item["left"])
            const rights = _.map(locations, item => item["left"] + item["width"])

            const min_left = to === "group" ? _.min(lefts) : (to === "slide" ? 0 : location_item_last["left"])
            const max_right = to === "group" ? _.max(rights) : (to === "slide" ? this.slide_width : location_item_last["right"])
            const group_width = max_right - min_left

            const sum_width = _.sum(_.map(locations, item => item["width"]))
            const middle_h = (min_left + max_right) / 2
            const space_h = (max_right - min_left - sum_width) / (count - 1)
            const sorted_pairs_h = _.sortBy(location_pairs, ([key, item]) => item["left"])
            function distribute_h_left(index) { return _.sum(_.map(_.take(sorted_pairs_h, index), ([key, item]) => item["width"] + space_h)) + min_left }

            const tops = _.map(locations, item => item["top"])
            const bottoms = _.map(locations, item => item["top"] + item["height"])
            const min_top = to === "group" ? _.min(tops) : (to === "slide" ? 0 : location_item_last["top"])
            const max_bottom = to === "group" ? _.max(bottoms) : (to === "slide" ? this.slide_height : location_item_last["bottom"])
            const group_height = max_bottom - min_top


            const sum_height = _.sum(_.map(locations, item => item["height"]))
            const middle_v = (min_top + max_bottom) / 2
            const space_v = (max_bottom - min_top - sum_height) / (count - 1)
            const sorted_pairs_v = _.sortBy(location_pairs, ([key, item]) => item["top"])
            function distribute_v_top(index) {
                return _.sum(_.map(_.take(sorted_pairs_v, index), ([key, item]) => item["height"] + space_v)) + min_top
            }

            function getIndexByObject(key) { return outer_this.extractNumber(key) }
            function getKey(index) { return getSlideObjectId(index) }
            function width(index) { return locations[getKey(index)]["width"] }
            function height(index) { return locations[getKey(index)]["height"] }
            function opObject(key, op) {
                const object_index = getIndexByObject(key)
                let clone_object = _.cloneDeep(_objects[object_index])
                op(clone_object, object_index)
                _objects.splice(object_index, 1, clone_object)
            }
            function set(op) {
                _.forEach(locations, (item, key) => opObject(key, op))
            }
            function setLeftTop(clone_object, path, value) {
                _.set(clone_object, _.concat(["config", "style"], path), value)
                if (path.includes("left")) _.set(clone_object, ["config", "style", "right"], "auto")
                if (path.includes("top")) _.set(clone_object, ["config", "style", "bottom"], "auto")
            }
            function setWidthHeight(clone_object, path, value) {
                _.set(clone_object, _.concat(["config", "style"], path), value)
            }
            function radian(point, center) {
                const x = point["x"] - center["center_x"]
                const y = point["y"] - center["center_y"]
                return Math.atan2(y, x)
            }
            function alignCenter({ center_x, center_y }, index, step, r) {
                const x = center_x + Math.cos(index * step) * r
                const y = center_y + Math.sin(index * step) * r
                return { x, y }
            }
            switch (align) {
                case "horizontal_fill": return set(clone_object => {
                    setLeftTop(clone_object, ["left"], `${min_left}px`)
                    setWidthHeight(clone_object, ["width"], `${group_width}px`)
                })
                case "vertical_fill": return set(clone_object => {
                    setLeftTop(clone_object, ["top"], `${min_top}px`)
                    setWidthHeight(clone_object, ["height"], `${group_height}px`)
                })
                case "align-left": return set(clone_object => setLeftTop(clone_object, ["left"], `${min_left}px`))
                case "align-center-h": return set((clone_object, index) => setLeftTop(clone_object, ["left"], `${middle_h - width(index) / 2}px`))
                case "align-right": return set((clone_object, index) => setLeftTop(clone_object, ["left"], `${max_right - width(index)}px`))
                case "align-top": return set(clone_object => setLeftTop(clone_object, ["top"], `${min_top}px`))
                case "align-center-v": return set((clone_object, index) => setLeftTop(clone_object, ["top"], `${middle_v - height(index) / 2}px`))
                case "align-bottom": return set((clone_object, index) => setLeftTop(clone_object, ["top"], `${max_bottom - height(index)}px`))
                case "align-center":
                    const center_xs = _.map(locations, l => _.get(l, ["center", "x"]))
                    const center_ys = _.map(locations, l => _.get(l, ["center", "y"]))
                    const widths = _.map(locations, l => _.get(l, ["width"]))
                    const heights = _.map(locations, l => _.get(l, ["height"]))
                    const max_width = _.max(widths)
                    const max_height = _.max(heights)
                    const center_x = to === "group" ? _.mean([_.min(center_xs), _.max(center_xs)]) : (this.slide_width / 2)
                    const center_y = to === "group" ? _.mean([_.min(center_ys), _.max(center_ys)]) : (this.slide_height / 2)
                    const r = to === "group" ? Math.min(max_right - min_left, max_bottom - min_top) / 2 : Math.max((Math.min(this.slide_width, this.slide_height) / 2 - Math.max(max_width, max_height) / 2), 20)
                    const step = (2 * Math.PI) / count
                    const sorted_location_radians = _.sortBy(location_pairs, ([key, item]) => radian(item["center"], { center_x, center_y }))
                    _.forEach(sorted_location_radians, ([key, item], index) => opObject(key, clone_object => {
                        const { x, y } = alignCenter({ center_x, center_y }, index, step, r)
                        const { width, height } = item
                        setLeftTop(clone_object, ["left"], `${x - width / 2}px`)
                        setLeftTop(clone_object, ["top"], `${y - height / 2}px`)
                    }))
                    break
                case "distribute-h":
                    _.forEach(sorted_pairs_h, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["left"], `${distribute_h_left(index)}px`)))
                    break
                case "distribute-v":
                    _.forEach(sorted_pairs_v, ([key, item], index) => opObject(key, clone_object => setLeftTop(clone_object, ["top"], `${distribute_v_top(index)}px`)))
                    break
            }
        },
        slideFocusin() {
            officeState.state.current_slide_object = {
                getContainer: this.format_container,
                execute_align: this.executeAlign,
                sizeChanged: this.sizeChanged,
                switchChanged: this.switchChanged,
                image_selection: this.image_selection(false),
                add_table: this.addTable,
                add_video: this.addVideo,
                add_rich: this.addRich,
                add_shape: this.addShape,
                add_chart: this.addChart,
                add_tabs: this.addTabs,
                add_collapse: this.addCollapse,
                add_popup: this.addPopup,
                add_timeline: this.addTimeline,
                add_steps: this.addSteps,
            }
            this.formatObjectFocusin(true)
        },
        showImageSelection: (image_selection) => officeState.commit("showImageSelection", image_selection),
        generateTableParagraph(generated) {
            if (_.isNil(generated)) ElMessageBox.alert("没有发现表格数据", '提示', { type: "info", center: true, draggable: true }).catch(_.noop)
            else {
                this._objects.splice(this.current_item_index + 1, 0, {
                    type: ITEM_TYPE_TABLE,
                    id: get_item_id(),
                    parent_id: this.current_parent_id,
                    data: JSON.parse(generated)
                })
            }
        },
        generateContinueParagraph(generated) {
            this.current_item_text = [this.current_item_text, this.format_paragraph(generated)].join(this.line_wrap)
        },
        generateKeywordsParagraph(generated) {
            ElMessageBox.alert(generated, '关键字', { type: "info", center: true, draggable: true }).catch(_.noop)
        },
        generateBiggerParagraph(generated) {
            this.current_item_text = this.format_paragraph(generated)
        },
        generateSubjectParagraph(generated) {
            ElMessageBox.alert(generated, '内容摘要', { type: "info", center: true, draggable: true }).catch(_.noop)
        },
        generateReplaceParagraph(generated) {
            this.current_item_text = this.format_paragraph(generated)
        },
        generateSmallerParagraph(generated) {
            this.current_item_text = this.format_paragraph(generated)
        },
        moreReference(lines) {
            if (lines < 3) lines = 3
            this.continue_reference = _.join(_.reverse(_.take(this.before_lines, lines)), "\n")
        },
        deleteDataItem(index) {
            this._objects.splice(index || this.current_item_index, 1)
        },
        removeParagraph(item, index) {
            if (_.isEmpty(item["text"])) this._objects.splice(index, 1)
        },
        addParagraph() {
            if (undefined === _.find(this._objects, value => value["parent_id"] === this.current_item["id"] && value["type"] === ITEM_TYPE_PARAGRAPH)) {
                this._objects.splice(this.current_item_index + 1, 0, {
                    id: get_item_id(),
                    parent_id: this.current_item["id"],
                    type: ITEM_TYPE_PARAGRAPH,
                    text: undefined
                })
                this.current_item_index = this.current_item_index + 1
            }
        },
        style2ImageObjects(images, style) {
            let { left, top, width, height } = style
            left = this.extractNumber(left)
            top = this.extractNumber(top)
            width = this.extractNumber(width)
            height = this.extractNumber(height)
            const margin = 8
            return _.map(images, (image, index) => {
                const row = _.toInteger((left + index * (width || 100 + margin)) / this.slide_width)
                const image_left = _.toInteger((left + index * (width || 100 + margin))) % this.slide_width
                const image_top = top + row * (height || 100 + margin)
                const new_config = {
                    style: {
                        left: `${image_left}px`,
                        top: `${image_top}px`,
                        width: width ? `${width}px` : "auto",
                        height: height ? `${height}px` : "auto",
                    }
                }
                return ({
                    id: get_item_id(),
                    parent_id: this.current_parent_id,
                    config: _.merge({}, this.new_config(), new_config),
                    type: ITEM_TYPE_IMAGE,
                    base64: image,
                })
            })
        },
        addObject(option) {
            this.in_drag_range_state = true
            registerKeyHandler(KEYCODE_ESCAPE, HANDLER_KEY_ADDOBJECT, () => {
                this.in_drag_range_state = false
                unregisterKeyHandler(KEYCODE_ESCAPE, HANDLER_KEY_ADDOBJECT)
            })
            this.drag_range_callback = style => {
                this.current_item_index = this.current_item_index + 1
                this.notifyObjectsChanged(spliceClone(this._objects, this.current_item_index + 1, 0, _.merge({}, option, {
                    id: get_item_id(),
                    parent_id: this.current_parent_id,
                    config: { ...this.new_config(), style },
                })))
                this.in_drag_range_state = false
            }
        },

        addImages(images, replace) {
            const r = (style) => {
                this.current_item_index = this.current_item_index + images.length
                this.notifyObjectsChanged(spliceClone(this._objects, this.current_item_index + (replace ? 0 : 1), (replace ? 1 : 0), ...this.style2ImageObjects(images, style)))
            }
            if (replace === true) r(_.get(this.current_item, ["config", "style"]))
            else {
                this.in_drag_range_state = true
                this.drag_range_callback = r
            }
        },
        receiveImage(selected_images) {
            if (_.isNil(this.current_item_index)) return
            this.addImages(selected_images, this.is_image ? this.image_replace : false)
        },
        uploadImage() {
            this.showImageSelection(this.image_selection(false))
        },

        headerClass(item) {
            return ({
                [`fs-${item["numbers"].length}`]: true
            })
        },
        format_paragraph(generated) {
            return _.join(_.map(_.split(generated, "\n"), line => `\t${_.trim(line)}`), this.line_wrap)
        },
        deleteImage() {
            if (this.is_image) _.get(this.current_item, "base64").splice(this.current_image_index, 1)
        },
        deleteDataItem() {
            if (_.isNil(this.current_item_index) === false) this._objects.splice(this.current_item_index, 1)
        },
        replaceImage() {
            this.showImageSelection(this.image_selection(true))
        },
        addTable() {
            this.addObject({ type: ITEM_TYPE_TABLE, data: sample_table_data, })
        },
        removeConfig(o) { _.unset(o, ["config"]); return o },
        addTabs() {
            this.addObject(this.removeConfig(tabsWrap()))
        },
        addCollapse() {
            this.addObject(this.removeConfig(collapseWrap()))
        },
        addPopup() {
            this.addObject(this.removeConfig(popupWrap()))
        },
        addTimeline() {
            this.addObject(this.removeConfig(timelineWrap()))
        },
        addSteps() {
            this.addObject(this.removeConfig(stepsWrap()))
        },
        addVideo(url) {
            ElMessageBox.prompt('视频地址', '视频', { confirmButtonText: '确认', cancelButtonText: '取消', inputValue: url || DEFAULT_VIDEO_URL })
                .then(({ value }) => {
                    if (url === undefined) this.addObject({ type: ITEM_TYPE_VIDEO, url: value || DEFAULT_VIDEO_URL })
                    else this.current_item['url'] = value
                }).catch(_.noop)
        },

        addRich() {
            this.addObject({ type: ITEM_TYPE_RICH, html: "在这里输入文本", data_category: DATA_CATEGORY_RICH, })
        },
        addShape(src) {
            this.addObject({ type: ITEM_TYPE_IMAGE, base64: src, })
        },
        addChart(option) {
            this.addObject({ type: ITEM_TYPE_CHART, option })
        },
        image_selection(replace) {
            const outer_this = this
            return {
                change_image_callback(images) {
                    outer_this.image_replace = replace
                    outer_this.receiveImage(images)
                },
                selected_images_multiple: true,
                selected_images: []
            }
        },
        html2text(html) {
            return $(`<div>${html}</div>`).text()
        },
        new_style() {
            const x_points = _.concat(_.map(this.item_locations, item => _.get(item, "left")), [0, this.slide_width])
            const new_left = this.far_point(x_points)
            const y_points = _.concat(_.map(this.item_locations, item => _.get(item, "top")), [0, this.slide_height])
            const new_top = this.far_point(y_points)
            return { left: `${new_left}px`, top: `${new_top}px` }
        },
        new_config() {
            return {
                classes: ["position-absolute"],
                style: this.new_style()
            }
        },
        updateMaxizedItem(val) {
            this.maxized_item = _.cloneDeep(val)
            const new_classes = _.reject(_.concat(['w-100', 'h-100'], _.get(this.maxized_item, ["config", "classes"]) || []), i => i === "position-absolute")
            _.set(this.maxized_item, ["config", "classes"], new_classes)
            const new_style = _.merge({}, _.get(this.maxized_item, ["config", "style"]) || {}, { top: 'unset!important', left: 'unset!important', right: 'unset!important', bottom: 'unset!important', })
            _.set(this.maxized_item, ["config", "style"], new_style)
            this.show_maxized_editor = true
        }
    },
    computed: {
        current_object_type: { get() { return officeStateCache.state.current_object_type }, set(val) { officeStateCache.commit('setCurrentObjectType', val) } },
        dynamic_name: { get() { return officeStateCache.state.dynamic_name }, set(val) { officeStateCache.commit('setDynamicName', val) } },
        is_slide_view: { get() { return officeStateCache.state.is_slide_view }, set(val) { officeStateCache.commit('setSlideView', val) } },
        tableEvents() {
            return function (index) {
                return _.merge({}, this.itemEvents(index), {
                    "update:data": d => this.notifyObjectsChanged(setClone(this._objects, [index, 'data'], d))
                })
            }
        },
        tableProps() {
            return function (index) {
                return _.merge({}, this.itemProps(index), {
                    data: this._objects[index]['data'],
                    stripe: this._objects[index]['stripe'] === true,
                    border: this._objects[index]['border'] === true,
                    show_header: this._objects[index]['show_header'] === true,
                })
            }
        },
        videoEvents() {
            return function (index) {
                return _.merge({}, this.itemEvents(index), {
                    change: url => { this.addVideo(url) }
                })
            }
        },
        videoProps() {
            return function (index) {
                return _.merge({}, this.itemProps(index), {
                    url: this._objects[index]['url']
                })
            }
        },
        chartEvents() {
            return function (index) {
                return _.merge({}, this.itemEvents(index), {
                    "update:option": o => this.notifyObjectsChanged(setClone(this._objects, [index, 'option'], o)),
                    "update:data": data => this.notifyObjectsChanged(setClone(this._objects, [index, 'data'], data))
                })
            }
        },
        chartProps() {
            return function (index) {
                return _.merge({}, this.itemProps(index), {
                    chart_option: this._objects[index]['option'],
                    chart_data: this._objects[index]['data']
                })
            }
        },
        imageEvents() {
            return function (index) {
                return _.merge({}, this.itemEvents(index), {
                    replace: () => this.replaceImage(_objects[index])
                })
            }
        },
        imageProps() {
            return function (index) {
                return _.merge({}, this.itemProps(index), {
                    image: this._objects[index]['base64'],
                    svg_props: this._objects[index]['svg_props']
                })
            }
        },
        htmlProps() {
            return function (index) {
                return _.merge({}, this.itemProps(index), {
                    html: this._objects[index]['html'],
                    data_category: this._objects[index]['data_category'] || DATA_CATEGORY_RICH
                })
            }
        },
        htmlEvents() {
            return function (index) {
                return _.merge({}, this.itemEvents(index), {
                    maxized: () => this.updateMaxizedItem(this._objects[index]),
                    "update:html": html => this.notifyObjectsChanged(setClone(this._objects, [index, 'html'], html)),
                    generate_content: how => this.$emit('generate_content', how),
                    one_key_generate: how => this.$emit('one_key_generate', how)
                })
            }
        },
        itemProps() {
            return function (index) {
                return {
                    tabindex: index,
                    ref: getSlideObjectId(index),
                    id: getSlideObjectId(index),
                    mode: this.mode,
                    config: this.get_default(this._objects[index], ['config'], {}),
                    is_selected: this.multi_select_object_keys.includes(getSlideObjectId(index))
                }
            }
        },
        itemEvents() {
            return function (index) {
                return {
                    deleted: () => this.deleteDataItem(index),
                    mouseup: event => this.objectClicked(event, getSlideObjectId(index)),
                    "update:config": config => this.notifyObjectsChanged(setClone(this._objects, [index, 'config'], config)),
                    "focusin:object": () => this.current_item_index = index
                }
            }
        },
        slide_classes() {
            return _.concat(this.classes, this.isRun ? [] : ["rounded-4", "shadow-lg", "border", "border-1"])
        },
        isEye() {
            return this.mode === MODE_EYE || this.mode === MODE_LAYOUT
        },
        isRun() {
            return this.mode === MODE_RUN
        },
        last_select_key() { return _.last(this.multi_select_object_keys) },
        current_parent_id() { return this.is_header ? _.get(this.current_item, ["id"]) : _.get(this.current_item, ["parent_id"]) },
        is_ppt: () => officeState.state.is_ppt,
        line_wrap() { return this.is_rich ? "</br>" : "\n" },
        current_slide_size() { return officeState.getters.current_slide_size },
        is_paragraph() {
            return _.get(this.current_item, "type") === ITEM_TYPE_PARAGRAPH
        },
        is_header() {
            return _.get(this.current_item, "type") === ITEM_TYPE_HEADER
        },
        is_image() {
            return _.get(this.current_item, "type") === ITEM_TYPE_IMAGE
        },
        is_rich() {
            return _.get(this.current_item, "type") === ITEM_TYPE_RICH
        },
        current_item() {
            return _.get(this._objects, [this.current_item_index])
        },
        current_item_type() {
            return _.get(this.current_item, ["type"])
        },
        current_item_is_title() {
            return [DATA_CATEGORY_TITLE, DATA_CATEGORY_SUB_TITLE, DATA_CATEGORY_CONTENT_TITLE].includes(this.current_item_data_category)
        },
        current_item_data_category() {
            return _.get(this.current_item, ["data_category"])
        },
        current_item_text: {
            get() {
                return this.is_rich ? _.get(this.current_item, "html") : _.get(this.current_item, "text")
            },
            set(val) {
                if (this.is_rich)
                    _.set(this.current_item, "html", val)
                else if (this.is_paragraph)
                    _.set(this.current_item, "text", val)
            }
        },
        before_lines() {
            const lines = _.reverse(_.map(_.flatMap(_.filter(this._objects, (item, index) => index <= this.current_item_index), item => _.split(`${item2header(item)}${item['text'] || this.html2text(item['html']) || ""}`, "\n")), item => `\t${_.trim(item)}`))
            return _.reject(lines, line => _.isEmpty(_.trim(line)))
        },
        slide_width: () => officeState.getters.slide_width,
        show_note: () => officeState.state.show_note,
        show_annotation: () => officeState.state.show_annotation,
        slide_height: () => officeState.getters.slide_height,
        slide_item_refs() { return this.refs_updated !== undefined && this.$refs },
        slide_items() {
            return _.mapValues(_.pickBy(this.slide_item_refs, (value, key) => _.startsWith(key, "slide_object_") && value.length === 1), (value, key) => _.head(value))
        },
        item_locations() {
            return _.mapValues(this.slide_items, (item, key) => {
                let el = $(item.$el)
                let rect = _.assign(_.pick(el.position(), ["left", "top"]), { width: el.width(), height: el.height() })
                _.set(rect, ["center"], { x: rect["left"] + rect["width"] / 2, y: rect["top"] + rect["height"] / 2 })
                return rect
            })
        },
        selected_item_locations() {
            return _.pickBy(this.item_locations, (value, key) => this.multi_select_object_keys.includes(key))
        },
        current_item_dynamic_name() {
            return _.get(names, this.current_item_type)
        }
    },
    watch: {
        current_item_dynamic_name: {
            handler(val, old) {
                if (val === undefined) return
                this.dynamic_name = val
            }, immediate: true, deep: true
        },
        current_item_is_title: {
            handler(val, old) {
                officeStateCache.state.current_object_is_title = val
            }, immediate: true, deep: true
        },
        current_item_type: {
            handler(val, old) {
                this.current_object_type = val

            }, immediate: true, deep: true
        },
        note: {
            handler(val, old) {
                this._note = val
            }, immediate: true, deep: true
        },
        objects: {
            handler(val) {
                this._objects = _.cloneDeep(_.map(val || [], (item, index) => {
                    _.set(item, ["object_index"], index)
                    return item
                }))
            }, immediate: true, deep: true
        },
        _objects: {
            handler(val) {
                this.refs_updated = !this.refs_updated
            }, immediate: true, deep: true
        },
        current_slide_size: {
            handler(val, old) {
                this.sizeChanged(val)
            }, immediate: true
        },
        before_lines: {
            handler(val, old) {
                if (_.isEqual(val, old)) return
                const rs = []
                _.forEach(val, line => {
                    if (_.sum(_.map(rs, item => item.length)) < default_reference_length) rs.push(line)
                })
                this.continue_reference = _.join(_.reverse(rs), "\n")
            }, immediate: true
        }
    },
    data() {
        return {
            POPOVER_STYLE,
            _note: undefined,
            max_note: false,
            ITEM_TYPE_CHART,
            show_maxized_editor: false,
            maxized_item: undefined,
            _objects: [],
            refs_updated: false,
            continue_reference: undefined,
            image_replace: false,
            current_item_index: undefined,
            current_image_index: undefined,
            ITEM_TYPE_HEADER,
            ITEM_TYPE_PARAGRAPH,
            ITEM_TYPE_IMAGE,
            ITEM_TYPE_TABLE,
            ITEM_TYPE_VIDEO,
            ITEM_TYPE_RICH
        }
    }
}