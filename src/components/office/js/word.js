import { aiMixin } from './ai_mixin'
import { addSibling, levelContent, moveContent } from '@/js/office/word'
import { addChildren } from '@/js/office/word'
import { officeCostantMixin } from './office_constant_mixin'

export const wordMixin = {
    mixins: [aiMixin, officeCostantMixin],
    data() {
        return {
            slides: []
        }
    },
    computed: {
        word_slide_title_data() {
            return _.get(_.find(this.slides, slide => slide["layout"] === TYPE_SLD_LAYOUT_TITLE_WORD), "data")
        },
        word_slide() {
            return _.find(this.slides, slide => slide["layout"] === TYPE_SLD_LAYOUT_CONTENT_WORD)
        },
        word_slide_data: {
            get() { return _.get(this.word_slide, "data") },
            set(val) { _.set(this.word_slide, "data", val) }
        },
    },
    methods: {
        addSiblingContent(event) {
            const { content, new_content, is_before } = event
            this.word_slide_data = addSibling(this.word_slide_data, content, new_content, is_before)
        },
        deleteContent(content) {
            const index = _.findIndex(this.word_slide_data, item => item["id"] === content["id"])
            this.word_slide_data.splice(index, 1)
        },
        contentMove(event) {
            this.word_slide_data = moveContent({ ...event, word_slide_data: this.word_slide_data })
        },
        upContent(event) {
            const { current, is_before } = event
            this.word_slide_data = levelContent(this.word_slide_data, current, true, is_before)
        },
        downContent(event) {
            const { current, is_before } = event
            this.word_slide_data = levelContent(this.word_slide_data, current, false, is_before)
        },
        childrenAdded(event) {
            const { content, children } = event
            this.word_slide_data = addChildren(this.word_slide_data, content, children)
        },
        childrenRequired(event) {
            const { content, how } = event
            this.generateContentAI({
                ...this.model_config,
                ...how,
                title: content["title"]
            }).then((children) => this.childrenAdded({
                content, children: _.map(children, child => child["content"])
            }))
        },
    }
}