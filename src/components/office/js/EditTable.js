import { POPOVER_STYLE } from '@/js/ppt';
import { ElMessageBox } from 'element-plus'
import { MODE_DESIGN } from '@/js/ppt'
import _ from 'lodash';
const READONLY_NAMES = ["indicators", "X轴", "Y轴", "name", "value", "parent", "source", "target", "date"]
export default {
    emits: ["update:data", "update:column"],
    props: {
        mode: { type: Number, default: MODE_DESIGN, required: false },
        data: { type: Object },
        stripe: { type: Boolean, default: true },
        border: { type: Boolean, default: true },
        show_header: { type: Boolean, default: true },
        can_update: { type: Boolean, default: false },
    },
    computed: {
        is_design() { return this.mode === MODE_DESIGN },
        max_id() {
            return (this._data.length === 0) ? 0 : _.max(_.map(this._data, item => item["id"]))
        },
    },
    methods: {
        emitData(column_added, column_deleted) {
            const data = _.map(this._data, item => _.omit(item, ["id"]))
            this.$emit("update:data", { data, column_added, column_deleted })
        },
        deleteRow(row) {
            this._data = _.reject(this._data, item => item['id'] === row["id"])
            this.emitData()
        },
        addRow() {
            if (_.isEmpty(this.new_row)) this.new_row = _.last(this._data)
            this._data.push(_.merge({}, this.new_row, { id: this.max_id + 1 }))
            this.new_row = {}
            this.emitData()
        },
        updateRow() {
            this._data = _.map(this._data, row => row["id"] === this.new_row["id"] ? _.merge({}, row, this.new_row) : row)
            this.emitData()
        },
        addHeader() {
            ElMessageBox.prompt('请输入表格表头,表头用逗号分隔', '创建表格', { confirmButtonText: '确认', cancelButtonText: '取消' })
                .then(({ value }) => {
                    const names = _.reject(_.map(_.split(value, ","), _.trim), _.isEmpty)
                    if (_.isEmpty(names)) return
                    const pairs = _.map(names, name => [name, 'No'])
                    pairs.push(["id", 0])
                    this._data.push(_.fromPairs(pairs))
                })
        },
        deleteColumn(col) {
            _.forEach(this._data, row => delete row[col])
            this.emitData(undefined, col)
        },
        addColumn(col) {
            const new_name = `${col}_new`
            _.forEach(this._data, (row) => {
                let value = _.get(row, [col])
                if (_.isNumber(value)) value = 1.1 * value
                _.set(row, [new_name], value)
            })
            this.emitData({ old_name: col, new_name })
        },
        columnChange(old_name, event) {
            const new_name = $(event.target).val()
            let table_data = this._data
            _.forEach(table_data, (row, index) => table_data[index] = _.mapKeys(row, (value, key) => key === old_name ? new_name : key))
            this.$emit("update:column", { old_name, new_name })
        },
        getColumns(arr) {
            const rs = _.unionBy(_.flatMap(arr, item => Object.keys(item)))
            return _.reject(rs, item => item === "id")
        },
    },
    data() {
        return {
            READONLY_NAMES,
            new_row: {},
            POPOVER_STYLE,
            _data: [],
            current_row: undefined,

        }
    },
    watch: {
        current_row(val, old) {
            if (_.get(val, ["id"]) === _.get(old, ["id"])) return
            this.new_row = _.cloneDeep(val) || {}
        },
        data: {
            handler(val) {
                this._data = _.map(_.cloneDeep(val) || [], (row, index) => _.merge(row, { id: index }))
            }, immediate: true, deep: true
        }
    }
}