import { officeState } from '@/js/office/office_state'

export const generateByTitleMixin = {
    computed: {
        current_title() { return _.get(officeState.state.current_title_object, ["text"]) },
        my_title: {
            get() {
                return _.get(officeState.state.model_config, ["my_title"])
            },
            set(val) {
                _.set(officeState.state.model_config, ["my_title"], val)
            }
        },
    },
    watch: {
        current_title: {
            handler(val) {
                this.title = val
            }, immediate: true
        },
    },
    data() {
        return {
            title: undefined,
            max_tokens: 4096,
        }
    }
}
