
import { base64image } from '@/js/office/office.js'
import { web_service } from '@/js/request/service'
import { itemMixin } from '@/js/office/item_mixin'

const TO_SVG_PATTERNS = [
    "img",
]
export default {
    mixins: [itemMixin],
    emits: ["replace"],
    props: {
        image: { type: String },
        svg_props: { type: Object, required: false }
    },
    data() {
        return {
            selected_tab: "设计",
            _image: undefined
        }
    },
    computed: {
        compose_prop() { return { image: this.image, svg_props: this.svg_props } },
        svg_html() {
            if (_.isNil(this.image)) return false
            if (_.startsWith(this.image,"data:image")) return false
            return _.find(TO_SVG_PATTERNS, t => this.image.includes(t)) !== undefined
        },
        style_width() {
            return parseInt(_.get(this.style, ["width"]))
        },
        style_height() {
            return parseInt(_.get(this.style, ["height"]))
        },
    },
    methods: {
        base64image,
        imageContainer() {
            return this.$refs.image_container
        },
        imageChanged(compose_prop) {
            if (compose_prop === undefined) return
            const { image, svg_props } = compose_prop
            if (image === undefined) return
            if (_.startsWith(this.image,"data:image")) return
            if (_.find(TO_SVG_PATTERNS, t => image.includes(t)) !== undefined) {
                if (this.svg_html !== true) return
                if (_.isNil(this.imageContainer())) return
                web_service.get(image).then(rs => {
                    const svg = _.find($(rs.data), item => item.nodeName === "svg")
                    svg.setAttribute("preserveAspectRatio", _.get(svg_props, ["preserveAspectRatio"], "none"))
                    svg.setAttribute("class", "w-100 h-100")
                    svg.removeAttribute("width")
                    svg.removeAttribute("height")
                    if (_.isNil(svg_props) === false) $("*", $(svg)).each((idx, item) => _.forEach(svg_props, (value, key) => item.setAttribute(key, value)))
                    $(this.imageContainer()).empty().append($(svg))
                })
            }
        }
    },
    mounted() {
        this.imageChanged(this.compose_prop)
    },
    watch: {
        compose_prop: {
            handler(val) {
                if (_.isNil(val)) return
                let { image } = val
                if (_.isNil(image)) return
                this._image = image
                this.imageChanged(val)
            }, immediate: true
        }
    }
}