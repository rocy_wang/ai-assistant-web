export const slideDragMixin = {
    data() {
        return {
            slides: []
        }
    },
    methods: {
        dragStart(event, index) {
            event.dataTransfer.dropEffect = 'move'
            event.dataTransfer.setData("index", index);
        },
        dragOver(event, _index) {
            event.preventDefault();
        },
        dragEnter(event) {
            event.preventDefault();
        },
        dragLeave(event) {
            event.preventDefault();
        },
        dropExecute(event, to_index,op) {
            event.preventDefault();
            if (_.isNil(event.dataTransfer)) return
            const from_index = event.dataTransfer.getData("index")
            op(from_index,to_index)
        },
    }
}