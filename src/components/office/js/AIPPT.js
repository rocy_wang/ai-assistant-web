
import AIOffice from '@/components/office/AIOffice.vue'
import { defineComponent } from 'vue'
export default defineComponent({
    components: { AIOffice },
    data() {
        return {
            office: undefined
        }
    }
})