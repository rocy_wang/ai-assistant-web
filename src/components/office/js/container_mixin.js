import { isAnyEmpty, renameKey, setClone, spliceClone, splicePathClone, swapClone, unsetClone } from '@/js/lodash_util';
import { itemMixin } from '@/js/office/item_mixin';
import { ElMessageBox } from 'element-plus';
import { getIndexOfKey, isNotEmpty } from '@/js/lodash_util';
import { dropMixin } from '@/js/office/drop_mixin';
import _ from 'lodash';
import { MODE_DESIGN } from '@/js/ppt';

export const containerMixin = {
    mixins: [itemMixin, dropMixin],
    emits: ["update:object", "deleted", "annotation_added", "update:option"],
    props: {
        mode: {
            type: Number,
            required: true
        },
        object: {
            type: Object
        },
    },
    methods: {
        set: _.set,
        unset: _.unset,
        first: _.first,
        getItemHolder() {
            throw new Error("getItemHolder not implemented ")
        },
        nameProcess(name) {
            _.forEach(["_", " "], c => name = _.replace(name, c, "-"))
            return name.substring(0, 20)
        },
        add() {
            ElMessageBox.prompt("请输入名称", { type: "success" }).then(({ value }) => {
                if (isAnyEmpty(value)) return
                this.notifyObjectChanged(setClone(this._object, [this.nameProcess(value)], [this.getItemHolder()]))
            })
        },
        moveFatherUp() {
            this._selected_father = this._selected_father - 1
            this.notifyObjectChanged(swapClone(this._object, this._selected_father, this._selected_father - 1))
        },
        moveFatherDown() {
            this._selected_father = this._selected_father + 1
            this.notifyObjectChanged(swapClone(this._object, this._selected_father, this._selected_father + 1))
        },
        editFatherName(name) {
            ElMessageBox.prompt("请输入名称", { type: "success", inputValue: name }).then(({ value }) => {
                if (isAnyEmpty(value)) return
                this.notifyObjectChanged(renameKey(this._object, name, [this.nameProcess(value)]))
            })
        },
        deleteFather() {
            let o = undefined
            if (_.isArray(this._object)) o = spliceClone(this._object, this._selected_father, 1)
            else if (_.isObject(this._object)) o = unsetClone(this._object, [this._selected_father])
            if (isAnyEmpty(o)) this._selected_father = undefined
            this.notifyObjectChanged(o)
        },

        updateChild(index, new_obj, children) {
            if (_.isNil(new_obj)) return
            children.splice(index, 1, new_obj)
            this.notifyObjectChanged()
        },
        moveUp(path, index) {
            path = path === undefined ? [this._selected_father] : path
            index = index === undefined ? this.selected_child_index : index
            const arr = _.get(this._object, path)
            this.selected_child_index = index - 1
            this.notifyObjectChanged(setClone(this._object, path, swapClone(arr, index, index - 1)))
        },
        moveDown(path, index) {
            path = path === undefined ? [this._selected_father] : path
            index = index === undefined ? this.selected_child_index : index
            const arr = _.get(this._object, path)
            this.selected_child_index = index + 1
            this.notifyObjectChanged(setClone(this._object, path, swapClone(arr, index, index + 1)))
        },

        deleteChild(index, path) {
            const o = splicePathClone(this._object, path || [this._selected_father], index, 1)
            if (isAnyEmpty(_.get(o, path || [this._selected_father]))) this.selected_child_index = undefined
            this.$nextTick(() => this.notifyObjectChanged(o))
        },
        notifyObjectChanged(new_object) {
            this.$emit('update:object', new_object || this._object)
        },
        selectFirstToggle() {
            this._selected_father = _.isNil(this._selected_father) ? _.first(Object.keys(this._object)) : null
        },
        getFatherNameIndex(name) {
            return getIndexOfKey(this._object, name)
        },
    },
    computed: {
        is_design() { return this.mode === MODE_DESIGN },
        selected_father() { return _.get(this._object, [this._selected_father]) },
        isExpaned() { return isNotEmpty(this._selected_father) }
    },
    data() {
        return {
            config_visible: false,
            add_father_visible: false,
            selected_child_index: undefined,
            _object: undefined,
            item_hoder: undefined,
            _selected_father: null

        }
    },
    watch: {
        object: {
            handler(val) {
                this._object = _.cloneDeep(val)
            }, immediate: true, deep: true
        }
    },
    mounted() {
        this.selectFirstToggle()
    }
}