import { officeState } from '@/js/office/office_state'
import { ElLoading } from "element-plus"
import { TYPE_SLD_LAYOUT_CONTENTS } from '@/js/ppt'
import _ from 'lodash'
import { service_post } from '@/js/request/service'
import { v4 as uuid4 } from 'uuid';
import { base64image } from '@/js/office/office.js'

export function postWrap(how, service, op) {
    const loading = ElLoading.service({ lock: true, text: 'AI思考中...', background: 'rgba(0, 0, 0, 0.7)' })
    return service_post(service, { how })
        .then(data => {
            const rs = op(data)
            loading.close()
            return rs
        })
        .catch(ex => {
            loading.close()
        })
}
export const TEXT_API = "/chat/get_answer"
export const IMAGE_API = "/office/generate_image"
export function postAIWithModel(question, op) {
    const loading = ElLoading.service({ lock: true, text: 'AI思考中...', background: 'rgba(0, 0, 0, 0.7)' })
    const data = {
        ...officeState.state.model_config,
        id: uuid4(),
        question,
        dialogue_id: "ai_ppt_generation",
    }
    service_post(TEXT_API, data, (data) => {
        const rs = op(data)
        loading.close()
        return rs
    })
        .catch(() => loading.close())
}

export function postAIImage(question, op) {
    const loading = ElLoading.service({ lock: true, text: 'AI绘画中...', background: 'rgba(0, 0, 0, 0.7)' })
    const data = { prompt: question }
    service_post(IMAGE_API, data, (rs) => {
        const base64 = base64image(rs)
        const result = op(base64)
        loading.close()
        return result
    })
        .catch(() => loading.close())
}


const MAX_TEXT_LEN = 512

function trip_prefix_number(text) {
    return text.replace(/^[0-9.\s]+/g, "")
}

export function textSplits(text) {
    const splits = _.reject(_.map(_.split(text, "\n"), _.trim), _.isEmpty)
    let texts = []
    let buffer = []
    _.forEach(splits, (split) => {
        buffer.push(split)
        if (_.sum(_.map(buffer, paragraph => paragraph.length)) > MAX_TEXT_LEN) {
            texts.push(_.join(buffer, "\n"))
            buffer = []
        }
    })
    if (buffer.length !== 0) texts.push(_.join(buffer, "\n"))
    return texts
}
export const aiMixin = {
    data() {
        return {
            slides: [],
            TYPE_SLD_LAYOUT_CONTENTS,
            current_slide_index: undefined,
        }
    },
    methods: {
        mergeModel(how) {
            return { ...this.model_config, ...how }
        },
        generateParagraphAI(how, service_name, callback) {
            return postWrap(this.mergeModel(how), `/word/${service_name}`, callback)
        },
        generateContentTextAI(how) {
            return postWrap(this.mergeModel(how), "/ppt/generate_text_for_content", textSplits)
        },
        generateContentAI(how) {
            const op = data => _.map(_.map(_.reject(data, _.isEmpty), trip_prefix_number))
            return postWrap(this.mergeModel(how), "/ppt/generate_contents", op)
        },
        oneKeyGenerateAI(how) {
            return postWrap(this.mergeModel(how), "/ppt/one_key_generate", JSON.parse)
        },
    },
    computed: {
        model_config: () => officeState.state.model_config,
        model_config_visible: { get: () => officeState.state.model_config_visible, set: (val) => officeState.state.model_config_visible = val },
    }
}