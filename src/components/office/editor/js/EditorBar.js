

import { officeState, officeStateCache } from '@/js/office/office_state'
import { ElMessageBox } from "element-plus";
import { rgb2hext } from '@/js/color_util'
import { menuBarMixin } from '@/js/office/menu-bar-mixin';
import _ from 'lodash';

const icon_map = {
    "InsertUnorderedList": "unordered_list",
    "InsertOrderedList": "ordered_list",
    "InsertImage": "insert_image",
    "createLink": "create_link",
    "insertHorizontalRule": "horizontal_rule",
}
const aligns = [{
    name: "文字居中",
    command: "justifyCenter",
    img: "justify_center"
}, {
    name: "文本对齐",
    command: "justifyFull",
    img: "justify_full"
}, {
    name: "左对齐",
    command: "justifyLeft",
    img: "justify_left"
}, {
    name: "右对齐",
    command: "justifyRight",
    img: "justify_right"
},]
const commands = {
    "Bold": "加粗", "Italic": "斜体", "Underline": "下划线", "InsertUnorderedList": "无序列表", "InsertOrderedList": "有序列表",
    "Indent": "增加缩进", "Outdent": "减少缩进", "InsertImage": "插入图片", "createLink": "创建链接",
    "insertHorizontalRule": "水平线", "subscript": "下标", "superscript": "上标"
}
const rich_commands = ["InsertUnorderedList", "InsertOrderedList", "InsertImage", "insertHorizontalRule"]
export default {
    mixins: [menuBarMixin],
    data() {
        return {
            commands,
            is_more_buttons: false,
            selected_fore_color: "#20a0ff",
            selected_hilite_color: "#eeeeee",
            aligns
        }
    },
    computed: {
        current_object_is_rich() { return officeStateCache.getters.isRich },
        current_editor() { return officeState.state.current_editor },
        htmlChanged() { return _.get(this.current_editor, "htmlChanged") },
        current_range() { return _.get(this.current_editor, "current_range") },
        editor_div() { return _.get(this.current_editor, "editor_div") },
        editor_html() { return _.get(this.current_editor, "html") },
        is_rich() { return _.get(this.current_editor, "is_rich") },
        is_line() { return !this.is_rich },
        is_run_slide() { return officeState.state.is_run_slide }
    },
    methods: {
        notifyChanged() {
            if (_.isFunction(this.htmlChanged)) this.htmlChanged()
        },
        heading(size) {
            if (this.current_range) {
                this.current_range.surroundContents(document.createElement(`h${size}`))
                this.notifyChanged()
            }
        },
        getFontSize: (size) => 8 + size * 4,
        fontSize(size) {
            if (this.current_range) {
                this.current_range.surroundContents($("<div></div>").css("display", "inline-block").css("font-size", `${this.getFontSize(size)}px`).get(0))
                this.notifyChanged()
            }
        },
        foreColor(value) { this.execute('foreColor', rgb2hext(value)) },
        hiliteColor(value) { this.execute('hiliteColor', rgb2hext(value)) },
        focusDiv() {
            if (this.current_range) {
                const range = document.createRange()
                range.setStart(this.current_range.startContainer, this.current_range.startOffset)
                range.setEnd(this.current_range.endContainer, this.current_range.endOffset)
            }
        },
        hideCommand(command) { return this.is_line && rich_commands.includes(command) },
        showCommand(command) { return !this.hideCommand(command) },
        editor_document() { return document },
        commandIcon(command) { return `/img/editor/${icon_map[command] || _.lowerCase(command)}.svg` },
        exeCommand(command, value) {
            if (command === "InsertImage" || command === "createLink")
                ElMessageBox.prompt('请输入路径', '插入图片', { confirmButtonText: '确认', cancelButtonText: '取消' })
                    .then(({ value }) => {
                        this.editor_document().execCommand(command, false, value);
                        this.notifyChanged()
                    }).catch(_.noop)
            else {
                this.editor_document().execCommand(command, false, String(value))
                this.notifyChanged()
            }
        },
        execute(command, value) {
            this.editor_div.focus()
            this.exeCommand(command, value);
        }
    }
}