
import { POPOVER_STYLE, border_modes } from '@/js/ppt.js'

import { officeState } from "@/js/office/office_state"

import BorderEffect from '@/components/office/BorderEffect.vue'
import Background from '@/components/office/Background.vue'
import ObjectSizeEffect from '@/components/office/ObjectSizeEffect.vue'
import ShadowEffect from '@/components/office/ShadowEffect.vue'
import { object_modes } from '@/js/ppt';
import { menuBarMixin } from '@/js/office/menu-bar-mixin'

const STYLES = [
    "left", "top", "right", "bottom",
    "width", "height",
    "transform-origin",
    "transform",
    "box-shadow"
]
export default {
    mixins:[menuBarMixin],
    components: { ShadowEffect, ObjectSizeEffect, BorderEffect, Background },
    props: {
        is_slide: { type: Boolean, required: true },
    },
    data() {
        return {
            object_modes,
            border_modes,
            POPOVER_STYLE,
            background_visible: false,
            object_size_visible: false,
            shadow_visible: false,
            border_visible: _.fromPairs(_.map(border_modes, mode => [mode, false])),
        }
    },
    computed: {
        current_format_object() { return this.is_slide === true ? officeState.state.current_format_slide_object : officeState.state.current_format_object },
        current_format_element() {
            return this.invoke(this.current_format_object,"getContainer")
        },
        current_format_object_style() {
            this.invoke(this.current_format_object, "getStyleValues",STYLES)
        },
        current_format_object_bg() {
            this.invoke(this.current_format_object, "getBackground")
        }
    },
    methods: {
        invoke(obj, func, arg) {
            const get_func = _.get(obj, func)
            return _.isFunction(get_func) ? get_func(arg) : undefined
        },
        styleChanged(style) {
            this.current_format_object && this.current_format_object.styleChanged(style)
            this.object_size_visible = false
        },
        borderChanged(borders, mode) {
            this.current_format_object && this.current_format_object.borderChanged(borders)
            this.border_visible[mode] = false
        },
        backgroundChanged({ background, full_apply }) {
            this.current_format_object && this.current_format_object.backgroundChanged({ background, full_apply })
            this.background_visible = false
        }
    }
}