
import { scenes, categories } from '@/js/office/ppt_layout.js'
import { generateSlides } from '@/js/office/auto_ppt_layout.js'
import { MODE_EYE } from '@/js/ppt'
import { COLOR_TYPES, COLOR_TYPES_REVERT } from '@/js/office/svg_background.js'
import { extractNumber } from '@/js/office/style'
import { GUANXI_SHAPDES } from '@/js/office/guanxi_template'
import { officeState } from '@/js/office/office_state'
import { base64image } from '@/js/office/office'
import { TUWEN_CATEGORIES, TUWEN_COUNT_MAX } from '@/js/office/tuwen_template'
import { DEFAULT_CHARTS } from '@/js/office/my_charts.js'

export default {
    emits: ["selected", "cancelled"],
    props: { is_show: { type: Boolean, default: false } },
    data() {
        return {
            show_dialog_aaaa: true,
            scenes,
            selected_chart_shape: "折线图",
            DEFAULT_CHART_NAMES: _.map(DEFAULT_CHARTS, ({ name }) => name),
            selected_tuwen_count: "3图",
            TUWEN_COUNT_MAX,
            selected_tuwen_shape: "横向",
            TUWEN_CATEGORIES,
            has_icon: true,
            has_logo: false,
            logo: undefined,
            selected_guanxi_count: "2项",
            selected_guanxi_shape: "时间轴",
            GUANXI_SHAPDES: GUANXI_SHAPDES,
            selected_zhang_index: "第1章",
            selected_content_count: "2级目录",
            selected_color_serial_name: undefined,
            COLOR_TYPES,
            MODE_EYE,
            categories,
            selected_category: "标准",
            selected_scene: undefined,
            slides: [],
            current_slide_index: undefined,
            key_increased: 0
        }
    },
    methods: {
        range: _.range,
        change_image_callback(images) {
            this.logo = base64image(_.head(images))
        },
        isCurrent(index) { return this.current_slide_index === index },
    },
    watch: {
        has_logo: {
            handler(val) {
                if (val === true) officeState.commit("showImageSelection", { change_image_callback: this.change_image_callback })
                else this.logo = undefined
            }, immediate: true, deep: true
        },
        generation_option: {
            handler({ selected_category, selected_scene, selected_color_serial, has_icon, logo,
                selected_content_count, selected_zhang_index, selected_guanxi_shape, selected_guanxi_count,
                selected_tuwen_shape, selected_tuwen_count, selected_chart_shape }) {
                if (this.show_dialog === false) return
                let option = undefined
                switch (this.selected_category) {
                    case "目录":
                        option = { count: parseInt(selected_content_count), context: window.document }
                        break;
                    case "章节":
                        option = { zhang_index: parseInt(selected_zhang_index), context: window.document }
                        break;
                    case "关系":
                        option = { count: parseInt(selected_guanxi_count), shape: (selected_guanxi_shape), context: window.document }
                        break;
                    case "图文":
                        option = { count: parseInt(selected_tuwen_count), shape: (selected_tuwen_shape), context: window.document }
                        break;
                    case "图表":
                        option = { shape: (selected_chart_shape), context: window.document }
                        break;
                    default:
                        option = { context: window.document }
                }
                this.slides = Object.freeze(generateSlides(selected_category, selected_scene, selected_color_serial, has_icon, logo, option))
                this.key_increased += this.slides.length
            }, immediate: true, deep: true
        }
    },
    computed: {
        current_slide_size: () => officeState.getters.current_slide_size,
        show_dialog: { get() { return this.is_show }, set(val) { if (val === false) this.$emit("cancelled") } },
        generation_option() {
            return ({
                show_dialog: this.show_dialog,
                current_slide_size: this.current_slide_size,
                selected_category: this.selected_category,
                selected_scene: this.selected_scene,
                selected_color_serial: this.selected_color_serial,
                has_icon: this.has_icon,
                logo: this.logo,
                selected_content_count: extractNumber(this.selected_content_count),
                selected_zhang_index: extractNumber(this.selected_zhang_index),
                selected_guanxi_shape: this.selected_guanxi_shape,
                selected_guanxi_count: this.selected_guanxi_count,
                selected_tuwen_shape: this.selected_tuwen_shape,
                selected_tuwen_count: this.selected_tuwen_count,
                selected_chart_shape: this.selected_chart_shape
            })
        },
        selected_color_serial() { return _.get(COLOR_TYPES_REVERT, this.selected_color_serial_name) },
    }
}