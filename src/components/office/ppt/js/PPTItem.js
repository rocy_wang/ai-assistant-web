import { officeState } from '@/js/office/office_state'
import {
    ITEM_TYPE_HEADER, ITEM_TYPE_PARAGRAPH, ITEM_TYPE_IMAGE, ITEM_TYPE_TABLE, ITEM_TYPE_VIDEO, ITEM_TYPE_RICH,
    ITEM_TYPE_CHART,
    ITEM_TYPE_COLLAPSE,
    ITEM_TYPE_TABS,
    ITEM_TYPE_POPUP,
    ITEM_TYPE_STEPS,
    ITEM_TYPE_TIMELINE
} from '@/js/office/word.js'
import { deleteItem, get_default, setClone } from '@/js/lodash_util'
import { MODE_DESIGN } from '@/js/ppt'
export default {
    name: "PPTItem",
    emits: ["update:object", "generate_content", "generate_content_text", "one_key_generate", "deleted", "annotation_added", "deleted_and_add", "focused"],
    props: {
        object: {
            type: Object,
            required: true
        },
        object_id: {
            type: String,
            required: false
        },
        mode: {
            type: Number,
            required: true
        },
        index_in_parent: {
            type: Number,
            required: false
        }
    },
    watch: {
        object: {
            handler(val, old) {
                if (_.isEqual(val, old)) return
                this._object = _.cloneDeep(val)
            }, immediate: true, deep: true
        },
    },
    data() {
        return {
            ITEM_TYPE_HEADER,
            ITEM_TYPE_PARAGRAPH,
            ITEM_TYPE_IMAGE,
            ITEM_TYPE_TABLE,
            ITEM_TYPE_VIDEO,
            ITEM_TYPE_RICH,
            ITEM_TYPE_CHART,
            ITEM_TYPE_COLLAPSE,
            ITEM_TYPE_TABS,
            ITEM_TYPE_POPUP,
            ITEM_TYPE_STEPS,
            ITEM_TYPE_TIMELINE,
            _object: undefined,
        }
    },
    methods: {
        objectClicked(event) {
            if (_.isNil(this.object_id)) return
            if (this.mode !== MODE_DESIGN) return
            this.focused_object_key = this.object_id
            event.stopPropagation()
            const { ctrlKey, shiftKey } = event
            if (ctrlKey === false && shiftKey === false) this.multi_select_object_keys = [this.object_id]
            else {
                if (ctrlKey === true)
                    if (this.multi_select_object_keys.includes(this.object_id) === false) this.multi_select_object_keys.push(this.object_id)
                    else this.multi_select_object_keys = deleteItem(this.multi_select_object_keys, this.object_id)
            }
            if (this.multi_select_object_keys.length > 1) this.focused_object_key = undefined
        },
        replaceImage() {
            officeState.commit("showImageSelection", {
                change_image_callback(images) {
                    this.notifyObjectChanged(setClone(this._object, ["base64"], _.head(images)))
                },
                selected_images_multiple: false,
            })
        },
        notifyObjectChanged(new_object) {
            this.$emit('update:object', new_object)
        },
        notifyObjectDeleted() {
            this.$emit('deleted')
        },
        updateMaxizedItem() {
            const maxized_item = _.cloneDeep(this._object)
            const new_classes = _.reject(_.concat(['w-100', 'h-100'], _.get(maxized_item, ["config", "classes"]) || []), i => i === "position-absolute")
            _.set(maxized_item, ["config", "classes"], new_classes)
            const new_style = _.merge({}, _.get(maxized_item, ["config", "style"]) || {}, { top: 'unset!important', left: 'unset!important', right: 'unset!important', bottom: 'unset!important', })
            _.set(maxized_item, ["config", "style"], new_style)
            this.maxized_item = maxized_item
        }
    },
    computed: {
        multi_select_object_keys: { get() { return officeState.state.multi_select_object_keys }, set(val) { officeState.state.multi_select_object_keys = val } },
        focused_object_key: { get() { return officeState.state.focused_object_key }, set(val) { officeState.state.focused_object_key = val } },
        maxized_item: { get() { return officeState.state.maxized_item }, set(val) { officeState.state.maxized_item = val } },
        timelineEvents() {
            return _.merge({}, this.itemEvents, {
                "update:object": d => this.notifyObjectChanged(setClone(this._object, ['object'], d)),
            })
        },
        timelineProps() {
            return _.merge({}, this.itemProps, {
                object: this._object['object'],
            })
        },
        stepsEvents() {
            return _.merge({}, this.itemEvents, {
                "update:object": d => this.notifyObjectChanged(setClone(this._object, ['object'], d)),
                "update:option": o => this.notifyObjectChanged(setClone(this._object, ['option'], o)),
            })
        },
        stepsProps() {
            return _.merge({}, this.itemProps, {
                object: this._object['object'],
                option: this._object['option'],
            })
        },
        popupEvents() {
            return _.merge({}, this.itemEvents, {
                "update:option": o => this.notifyObjectChanged(setClone(this._object, ['option'], o)),
            })
        },
        popupProps() {
            return _.merge({}, this.itemProps, {
                object: this._object['object'],
                option: this._object['option'],
            })
        },
        containerEvents() {
            return _.merge({}, this.itemEvents, {
                "update:object": d => this.notifyObjectChanged(setClone(this._object, ['object'], d)),
            })
        },
        containerProps() {
            return _.merge({}, this.itemProps, {
                object: this._object['object'],
            })
        },
        tableEvents() {
            return _.merge({}, this.itemEvents, {
                "update:data": d => this.notifyObjectChanged(setClone(this._object, ['data'], d)),
                "update:option": o => this.notifyObjectChanged(setClone(this._object, ['option'], o)),
            })
        },
        tableProps() {
            return _.merge({}, this.itemProps, {
                data: this._object['data'],
                option: this._object['option'],
            })
        },
        videoEvents() {
            return _.merge({}, this.itemEvents, {
                change: url => this.notifyObjectChanged(setClone(this._object, ['url'], url)),
            })
        },
        videoProps() {
            return _.merge({}, this.itemProps, {
                url: this._object['url']
            })
        },
        chartEvents() {
            return _.merge({}, this.itemEvents, {
                "update:option": o => this.notifyObjectChanged(setClone(this._object, ['option'], o)),
                "update:data": data => this.notifyObjectChanged(setClone(this._object, ['data'], data))
            })
        },
        chartProps() {
            return _.merge({}, this.itemProps, {
                chart_option: this._object['option'],
                chart_data: this._object['data']
            })
        },
        imageEvents() {
            return _.merge({}, this.itemEvents, {
                replace: () => this.replaceImage()
            })
        },
        imageProps() {
            return _.merge({}, this.itemProps, {
                image: this._object['base64'],
                svg_props: this._object['svg_props']
            })
        },
        htmlProps() {
            return _.merge({}, this.itemProps, {
                html: this._object['html'],
                data_category: this._object['data_category'] || DATA_CATEGORY_RICH
            })
        },
        htmlEvents() {
            return _.merge({}, this.itemEvents, {
                maxized: () => this.updateMaxizedItem(),
                "update:html": html => this.notifyObjectChanged(setClone(this._object, ['html'], html)),
                generate_content: how => this.$emit('generate_content', how),
                generate_content_text: how => this.$emit('generate_content_text', how),
                one_key_generate: how => this.$emit('one_key_generate', how)
            })
        },
        itemProps() {
            return {
                ref: this.object_id,
                id: this.object_id,
                mode: this.mode,
                config: get_default(this._object, ['config'], {}),
                is_selected: this.multi_select_object_keys.includes(this.object_id),
                is_focused: this.focused_object_key === this.object_id
            }
        },
        itemEvents() {
            return {
                deleted: () => this.notifyObjectDeleted(),
                "mouseup": event => {
                    this.$emit("focused");
                    this.objectClicked(event)
                },
                "update:config": config => this.notifyObjectChanged(setClone(this._object, ['config'], config)),
                "focusin:object": () => this.$emit("focused"),
                "annotation_added": (event) => this.$emit("annotation_added", event)
            }
        },
    }
}