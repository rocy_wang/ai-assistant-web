import { handleFullScreen, isFullScreen, MODE_RUN } from '@/js/ppt.js'
import { officeState } from '@/js/office/office_state'
import { ElMessageBox } from 'element-plus'
import { SHARE_TOKEN_KEY } from '@/js/user'
import { service_post } from '@/js/request/service'
import _ from 'lodash'
import { POPOVER_STYLE } from '@/js/ppt.js'
import { slideObjectDragMixin } from '@/js/office/slide_object_drag'
import { COLORS_MAP } from '@/js/office/office'
let handler = undefined
const pen_mapping = {
    '箭头': "jian_tou", '圆珠笔': "yuan_zhu_bi", '水彩笔': "shui_cai_bi", '荧光笔': "ying_guang_bi"
}
const WAVE_VALUE = 10
export default {
    mixins: [slideObjectDragMixin],
    data() {
        return {
            track_points: undefined,
            pen_mapping,
            POPOVER_STYLE,
            is_erase: false,
            selected_color: undefined,
            selected_shape: '曲线',
            selected_pen: undefined,
            expand: true,
            perserve_ratio: false,
            MODE_RUN,
            slide_index: 0,
        }
    },
    unmounted() {
        $(document.body).off("keyup", $(this.$refs.format_container), this.keyMonitor)
    },
    async mounted() {
        officeState.commit("setShowNote", false)
        officeState.commit("setShowAnnotation", false)
        if (_.isNil(this.play_ppt)) {
            if (_.isNil(this.share_token) === false) {
                const office = await service_post("/office/get_shared", { [SHARE_TOKEN_KEY]: this.share_token })
                this.play_ppt = { slides: office['slides'] }
                this.show()
            }
        }
        else this.show()
    },
    methods: {
        eraseAll() {

        },
        stopAuto() {
            if (handler !== undefined) {
                clearInterval(handler)
                handler = undefined
            }
        },
        autoPlay() {
            handler = setInterval(() => this.nextPage(), this.auto_seconds * 1000)
        },
        show() {
            if (isFullScreen() == false) ElMessageBox.confirm("全屏显示效果更好", "请求全屏显示", { type: "success" }).then(handleFullScreen).catch(_.noop)
            const { index, auto_play, perserve_ratio } = this.play_ppt || {}
            this.slide_index = index || 0
            this.perserve_ratio = perserve_ratio || false
            $(document.body).on("keyup", $(this.$refs.format_container), this.keyMonitor)
            if (auto_play === true) setInterval(() => this.nextPage(), this.auto_seconds * 1000)
        },
        prevPage() {
            this.slide_index = (this.slide_index - 1 + this.slides.length) % this.slides.length
        },
        nextPage() {
            this.slide_index = (this.slide_index + 1 + this.slides.length) % this.slides.length
        },
        keyMonitor(event) {
            const key = _.get(event, ["originalEvent", "code"])
            switch (key) {
                case "ArrowUp":
                    this.prevPage()
                    break;
                case "ArrowDown":
                case "Space":
                case "Enter":
                    this.nextPage()
                    break;
                case "F5":
                    if (handleFullScreen() === false) ElMessageBox.alert("当前浏览器不支持全屏", { type: "warning" })
                    break
            }
        },
        resized({ width, height }) {
            officeState.state.page_width = width
            officeState.state.page_height = height
        },
        applyShowHide(switch_config) {
            const { name, options } = switch_config
            const apply_obj = $(this.$refs.slide_obj.$el)
            apply_obj.removeClass("d-flex")
            apply_obj.addClass("d-none")
            setTimeout(() => {
                apply_obj.removeClass("d-none")
                apply_obj.effect(name, options, 1000, () => {
                    apply_obj.show()
                    apply_obj.addClass("d-flex")
                })
            }, 300)
        },
        wrapSVG(width, height, inner) {
            return `<svg width="${width}" height="300" xmlns="http://www.w3.org/2000/svg">${inner}</svg>`
        },
        pushTrack() {
            if (this.track_points === undefined) return
            if (this.is_track_pen === false) return
            if (_.get(this.play_ppt, ["slides", this.slide_index, "tracks"]) === undefined)
                _.set(this.play_ppt, ["slides", this.slide_index, "tracks"], [])
            _.get(this.play_ppt, ["slides", this.slide_index, "tracks"]).push({
                selected_shape: this.selected_shape,
                track_points: this.track_points,
                selected_color_hex: this.selected_color_hex
            })
            this.track_points = undefined
        },

        get_track_points_left(track_points) { return Math.min(this.get_track_points_x_first(track_points), this.get_track_points_x_last(track_points)) },
        get_track_points_top(track_points) { return Math.min(this.get_track_points_y_first(track_points), this.get_track_points_y_last(track_points)) },
        get_track_points_right(track_points) { return Math.max(this.get_track_points_x_first(track_points), this.get_track_points_x_last(track_points)) },
        get_track_points_bottom(track_points) { return Math.max(this.get_track_points_y_first(track_points), this.get_track_points_y_last(track_points)) },

        get_track_points_x_first(track_points) { return track_points && _.get(_.first(track_points), ["pageX"]) - this.get_track_points_x_min(track_points) },
        get_track_points_y_first(track_points) { return track_points && _.get(_.first(track_points), ["pageY"]) - this.get_track_points_y_min(track_points) },
        get_track_points_x_last(track_points) { return track_points && _.get(_.last(track_points), ["pageX"]) - this.get_track_points_x_min(track_points) },
        get_track_points_y_last(track_points) { return track_points && _.get(_.last(track_points), ["pageY"]) - this.get_track_points_y_min(track_points) },

        get_track_points_x_min(track_points) { return track_points && _.min(_.map(track_points, p => p["pageX"])) },
        get_track_points_y_min(track_points) { return track_points && _.min(_.map(track_points, p => p["pageY"])) },
        get_track_points_x_max(track_points) { return track_points && _.max(_.map(track_points, p => p["pageX"])) },
        get_track_points_y_max(track_points) { return track_points && _.max(_.map(track_points, p => p["pageY"])) },

        get_track_points_width(track_points) {
            if (track_points === undefined) return undefined
            let v = (this.get_track_points_x_max(track_points) - this.get_track_points_x_min(track_points))
            if (v < 2 * WAVE_VALUE) v = 2 * WAVE_VALUE
            return v
        },
        get_track_points_height(track_points) {
            if (track_points === undefined) return undefined
            let v = (this.get_track_points_y_max(track_points) - this.get_track_points_y_min(track_points))
            if (v < 2 * WAVE_VALUE) v = 2 * WAVE_VALUE
            return v
        },

        get_polyline_points(track_points) {
            if (track_points === undefined) return undefined
            const ymin = this.get_track_points_y_min(track_points)
            const xmin = this.get_track_points_x_min(track_points)
            return _.map(track_points, ({ pageX, pageY }) => `${pageX - xmin},${pageY - ymin}`).join(' ')
        },

        get_wave_data(track_points) {
            if (track_points === undefined) return undefined
            const x1 = this.get_track_points_x_first(track_points)
            const y1 = this.get_track_points_y_first(track_points)
            const x2 = this.get_track_points_x_last(track_points)
            const y2 = this.get_track_points_y_last(track_points)
            const is_horizontal = (Math.abs(x2 - x1) > Math.abs(y2 - y1))
            const wave_step_x = 30
            const wave_count = Math.round((x2 - x1) / wave_step_x)
            const wave_step_y = Math.round((y2 - y1) / wave_count)
            const points = _.join(_.map(_.range(1, wave_count + 1), (idx) => {
                const times = idx % 2 === 0 ? 1 : -1
                const qx = Math.round(wave_step_x / 2) + (is_horizontal ? 0 : times * WAVE_VALUE)
                const qy = Math.round(wave_step_y / 2) + (is_horizontal ? times * WAVE_VALUE : 0)
                return `q ${qx} ${qy} ${wave_step_x} ${wave_step_y}`
            }), " ")
            return `M ${x1} ${y1} ${points}`
        },
    },
    watch: {
        selected_pen: {
            handler(val) {
                this.in_drag_range_state = val !== "箭头" && val !== undefined
                if (this.in_drag_range_state) {
                    this.drag_track_callback = (points) => this.track_points = points
                    this.drag_range_callback = this.pushTrack
                } else {
                    this.drag_track_callback = undefined
                    this.drag_range_callback = undefined
                }
            }, immediate: true
        },
        current_slide_index(val) { this.slide_index = val },
        slide: {
            handler(val) {
                if (val === undefined) return
                const switch_config = _.get(val, ["config", "switch"])
                if (_.isEmpty(switch_config)) return
                this.$nextTick(() => this.applyShowHide(switch_config))
            }, immediate: true
        }
    },
    computed: {

        slide_track_points() { return _.get(this.play_ppt, ["slides", this.slide_index, "tracks"]) || [] },
        is_track_pen() { return this.selected_pen !== undefined && this.selected_pen !== "箭头" },
        auto_seconds() { return _.get(this.play_ppt, ["auto_seconds"]) || 3 },
        slides() { return _.get(this.play_ppt, ["slides"]) },

        wave_data() { return this.get_wave_data(this.track_points) },
        polyline_points() { return get_polyline_points(this.track_points) },
        selected_color_hex() { return COLORS_MAP[this.selected_color || "primary"] },

        track_points_left() { return get_track_points_left(this.track_points) },
        track_points_top() { return get_track_points_top(this.track_points) },
        track_points_right() { return get_track_points_right(this.track_points) },
        track_points_bottom() { return get_track_points_bottom(this.track_points) },

        track_points_x_first() { return this.get_track_points_x_first(this.track_points) },
        track_points_y_first() { return this.get_track_points_y_first(this.track_points) },
        track_points_x_last() { return this.get_track_points_x_last(this.track_points) },
        track_points_y_last() { return this.get_track_points_y_last(this.track_points) },

        track_points_x_min() { return this.get_track_points_x_min(this.track_points) },
        track_points_y_min() { return this.get_track_points_y_min(this.track_points) },
        track_points_x_max() { return this.get_track_points_x_max(this.track_points) },
        track_points_y_max() { return this.get_track_points_y_max(this.track_points) },
        track_points_width() { return this.get_track_points_width(this.track_points) },
        track_points_height() { return this.get_track_points_height(this.track_points) },

        pen() { return this.selected_pen ? [this.pen_mapping[this.selected_pen]] : [] },
        show_maxized_editor: { get() { return this.maxized_item !== undefined }, set(val) { if (val === false) this.maxized_item = undefined } },
        maxized_item: { get() { return officeState.state.maxized_item }, set(val) { officeState.state.maxized_item = val } },
        play_ppt: {
            get() { return officeState.state.play_ppt },
            set(val) { officeState.state.play_ppt = val }
        },
        share_token() {
            return _.get(this.$route, ["query", SHARE_TOKEN_KEY]);
        },
        page_width() { return officeState.state.page_width },
        page_height() { return officeState.state.page_height },
        slide_width_specified() { return officeState.state.slide_width_specified },
        slide_height_specified() { return officeState.state.slide_height_specified },
        slide_width_global() { return officeState.getters.slide_width },
        slide_height_global() { return officeState.getters.slide_height },
        slide_width() { return parseFloat(_.get(this.slide, ["config", "style", "width"]) || this.slide_width_global) + 1 },
        slide_height() { return parseFloat(_.get(this.slide, ["config", "style", "height"]) || this.slide_height_global) + 1 },
        width_ratio() { return this.page_width / this.slide_width },
        height_ratio() { return this.page_height / this.slide_height },
        slide() { return _.get(this.slides, [this.slide_index]) },
        slide_config() {
            const config = _.cloneDeep(_.get(this.slide, ["config"]))
            if (_.get(config, ["classes"]) === undefined) _.set(config, ["classes"], [])
            if (_.get(config, ["classes"]).includes("position-relative") === false) _.get(config, ["classes"]).push("position-relative")
            _.unset(config, ["style", "left"])
            _.unset(config, ["style", "top"])
            _.unset(config, ["style", "right"])
            _.unset(config, ["style", "bottom"])
            return _.merge({}, config, { style: this.max_style })
        },
        max_ratio() { return officeState.getters.max_ratio },
        max_style() {
            const min = Math.min(this.width_ratio, this.height_ratio)
            return ({
                "transform-origin": "50% 50%",
                transform: this.perserve_ratio === true ? `scale(${min}, ${min})` : `scale(${this.width_ratio}, ${this.height_ratio})`,
            })
        }
    }
}