import { defineComponent } from "vue"
import { base64image } from '@/js/office/office.js'
import { service_post } from "@/js/request/service";
import { ElLoading } from 'element-plus'
import { ElMessage } from "element-plus";
import { saveImage } from '@/js/download_util.js'
import { insureLogged } from "@/js/document";

export default defineComponent({
    data() {
        return {
            to_change_source: true,
            source_base64: undefined,
            target_base64: undefined,
            result_base64: undefined,
        }
    },
    mounted(){
        insureLogged(this, true)
    },
    computed: {
        canSwap() { return this.source_base64 && this.target_base64 },
        sourceUrl() { return base64image(this.source_base64) || "/img/face_swap/source.png" },
        targetUrl() { return base64image(this.target_base64) || "/img/face_swap/target.png" },
        resultUrl() { return base64image(this.result_base64) || "/img/face_swap/result.png" },
        images() { return [this.sourceUrl, this.targetUrl, this.resultUrl] }
    },
    methods: {
        saveImage() {
            saveImage($("#result_image").get(0), "换脸结果.png")
        },
        toChangeImage(is_source) {
            this.to_change_source = is_source
            $("#face_swap_file").trigger("click")
        },
        changeImage() {
            let file = _.get($("#face_swap_file").get(0), ["files", 0])
            if (file) {
                var reader = new FileReader()
                reader.readAsDataURL(file)
                reader.onload = () => {
                    $("#face_swap_file").val(undefined)
                    if (this.to_change_source === true) this.source_base64 = reader.result
                    else this.target_base64 = reader.result
                }
            }
        },
        swapFace() {
            const loading = ElLoading.service({ lock: true, text: 'AI换脸中...', background: 'rgba(0, 0, 0, 0.7)' })
            service_post("/face_swap/swap", { source: this.source_base64, target: this.target_base64 })
                .then(data => {
                    loading.close()
                    if (data) {
                        this.result_base64 = data
                        ElMessage({ message: "恭喜您，换脸成功了", type: "success", showClose: true, duration: 6000 })
                    }
                    else ElMessage({ message: "很抱歉,换脸失败了", type: "warning", showClose: true, duration: 6000 })
                })
                .catch(() => ElMessage({ message: "很抱歉,换脸失败了", type: "warning", showClose: true, duration: 6000 }))
        }
    },
})