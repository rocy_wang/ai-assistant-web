import { createRouter, createWebHistory } from 'vue-router';
import Login from '@/components/Login.vue';
import Chat from '@/components/Chat.vue';
import MoneyIn from '@/components/MoneyIn.vue';
import MyAccount from "@/components/MyAccount.vue";
import Resource from "@/components/Resource.vue";
import Publish from "@/components/Publish.vue";
import PublishList from "@/components/PublishList.vue";
import ActiveService from "@/components/ActiveService.vue";
import ShowService from "@/components/ShowService.vue";
import InvokeService from "@/components/ServiceInvoke.vue";
import Questioner from "@/components/mps/Questioner.vue";
import Responder from "@/components/Responder.vue";
import Search from "@/components/SearchService.vue";
import Admin from "@/components/Admin.vue";
import Prices from "@/components/Prices.vue";
import PPT from "@/components/office/AIPPT.vue";
import Word from "@/components/office/AIWord.vue";
import Navigator from "@/components/Navigator.vue";
import Background from "@/components/office/Background.vue";
import AutoSVGBackground from "@/components/office/background/AutoSVGBackground.vue";
import SVGBackgroundSelector from "@/components/office/background/SVGBackgroundSelector.vue";
import UseLayout from '@/components/office/ppt/UseLayout.vue';
import PlaySlides from "@/components/office/ppt/PlaySlides.vue";
import Test from '@/components/office/Test.vue';
import Point from '@/components/tools/Point.vue';
import CanvasSave from '@/components/tools/CanvasSave.vue';
import NavigatorItem from '@/components/NavigatorItem.vue';
import Swap from './components/face_swap/Swap.vue'
const routes = [
    {
        path: '/swap',
        name: 'swap',
        component: Swap
    },
    {
        path: '/',
        name: 'index',
        component: Chat
    },
    {
        path: '/Point',
        name: 'Point',
        component: Point
    },
    {
        path: '/NavigatorItem',
        name: 'NavigatorItem',
        component: NavigatorItem
    },
    {
        path: '/Test',
        name: 'Test',
        component: Test
    },{
        path: '/canvas',
        name: 'canvas',
        component: CanvasSave
    },
    {
        path: '/ppt/play',
        name: 'play',
        component: PlaySlides
    },
    {
        path: '/UseLayout',
        name: 'UseLayout',
        component: UseLayout
    },
    {
        path: '/background_selector',
        name: 'background_selector',
        component: SVGBackgroundSelector
    },
    {
        path: '/auto_background',
        name: 'auto_background',
        component: AutoSVGBackground
    },
    {
        path: '/background',
        name: 'background',
        component: Background
    },
    {
        path: '/navigator',
        name: 'navigator',
        component: Navigator
    },
    {
        path: '/ppt',
        name: 'ppt',
        component: PPT
    },
    {
        path: '/word',
        name: 'word',
        component: Word
    },
    {
        path: '/chat',
        name: 'chat',
        component: Chat
    },
    {
        path: '/prices',
        name: 'prices',
        component: Prices
    },
    {
        path: '/admin',
        name: 'admin',
        component: Admin
    },
    {
        path: '/search',
        name: 'search',
        component: Search
    },
    {
        path: '/responder',
        name: 'responder',
        component: Responder
    },
    {
        path: '/questioner',
        name: 'questioner',
        component: Questioner
    },
    {
        path: '/invoke_service',
        name: 'invoke_service',
        component: InvokeService
    },
    {
        path: '/show_service',
        name: 'show_service',
        component: ShowService
    },

    {
        path: '/active_service',
        name: 'active_service',
        component: ActiveService
    },
    {
        path: '/publish_list',
        name: 'publish_list',
        component: PublishList
    },
    {
        path: '/publish',
        name: 'publish',
        component: Publish
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/account',
        name: 'account',
        component: MyAccount
    },
    {
        path: '/model/:id?',
        name: 'model',
        component: Resource
    },
    {
        path: '/money',
        name: 'money',
        component: MoneyIn
    }
]

export const router = createRouter({
    history: createWebHistory(),
    routes: routes
})
