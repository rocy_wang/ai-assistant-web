import './polyfill'
import './assets/main.css'
import '@/js/global.js'
import { createApp } from 'vue'
import App from './App.vue'
import { router } from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'animate.css';

import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import Prism from 'prismjs';
import 'prismjs/components/prism-json';
import AiMp from "@/components/mps/MP.vue";
import AiIM from "@/components/mps/IM.vue";
import AiMpText from "@/components/mps/Text.vue";
import AiMpImage from "@/components/mps/Image.vue";
import AiProxy from "@/components/mps/Proxy.vue";
import AiSend from "@/components/mps/SendMessageOnTime.vue";
import AiSendCommand from "@/components/commands/SendMessageCommand.vue";
import AiFloatNavigation from "@/components/FloatNavigation.vue";
import DownloadRobot from "@/components/DownloadRobot.vue";
import AIOffice from "@/components/office/AIOffice.vue";
import SVGBackground from '@/components/office/background/SVGBackground.vue';
import Vue3ResizeSensor from 'vue3-resize-sensor'
import 'element-plus/theme-chalk/dark/css-vars.css'
VMdEditor.use(vuepressTheme, {
    Prism,
});

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.component("ai-mp", AiMp)
    .component("ai-im", AiIM)
    .component("ai-text-model", AiMpText)
    .component("ai-image-model", AiMpImage)
    .component("ai-proxy", AiProxy)
    .component("ai-send-message-on-time", AiSend)
    .component("ai-send-message-command", AiSendCommand)
    .component("ai-float-navigation", AiFloatNavigation)
    .component("ai-download-robot", DownloadRobot)
    .component("ai-office", AIOffice)
    .component("resize-sensor", Vue3ResizeSensor)
    .component("SVGBackground", SVGBackground)

app.directive('focus', {
    mounted: (el) => {
        el.focus()
    }
})
app.use(VMdEditor)
app.use(router)
app.use(ElementPlus)
app.config.performance = true
app.mount('#app')
$.ajaxSetup({
    xhrFields: {
        withCredentials: true,
    },
    crossDomain: true,
    headers:{
        "Access-Control-Allow-Origin": "*"
    }
});
$(document).on("contextmenu", () => false)
