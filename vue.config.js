const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath:"/nervous/",
  transpileDependencies: true,
  devServer:{
    host: 'localhost',
    port:9998,
    client: {
      webSocketURL: 'ws://localhost:9998/ws',
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  }
})
